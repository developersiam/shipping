﻿Public Class FrmShowPackedGrade
    Inherits MetroFramework.Forms.MetroForm
    Dim XGrader As String
    Dim XCustomer2 As String
    Private Sub FrmShowPackedGrade_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.security' table. You can move, or remove it, as needed.
        Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername

            Me.TypeTableAdapter.Fill(Me.ShippingDataSet.type)
            Me.CropFromPackedGradeTableAdapter.Fill(Me.ShippingDataSet.CropFromPackedGrade)
            Dim Dt As New DataTable

            If CropComboBox.Text = "All" Then
                Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
            Else
                Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub CropComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CropComboBox.SelectedIndexChanged
        Try
            If CropComboBox.Text <> "" Then
                If CropComboBox.Text = "All" Then
                    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
                Else
                    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TypeComboBox.SelectedIndexChanged
        Try
            If CropComboBox.Text <> "" Then
                If CropComboBox.Text = "All" Then
                    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
                Else
                    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            If CropComboBox.Text = "All" Then
                Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
            Else
                Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MetroGrid_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles MetroGrid.CellDoubleClick
        Try
            XGrader = MetroGrid.Item(0, MetroGrid.CurrentCell.RowIndex).Value
            XCustomer2 = MetroGrid.Item(3, MetroGrid.CurrentCell.RowIndex).Value
            Dim xfrm As New FrmShowPackedGradeDetail
            xfrm.XGraders = XGrader
            xfrm.XCustomer = XCustomer2
            xfrm.ShowDialog()
            xfrm.Dispose()
            If CropComboBox.Text = "All" Then
                Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
            Else
                Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub TransferGradeTile_Click(sender As Object, e As EventArgs) Handles TransferGradeTile.Click
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim baleBarcodeRow As ShippingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
            baleBarcodeRow = Me.ShippingDataSet.security.FindByuname(XUsernameApprove)
            If baleBarcodeRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not baleBarcodeRow Is Nothing Then
                If baleBarcodeRow.uname = XUsernameApprove And (baleBarcodeRow.pdtransfer = True Or baleBarcodeRow.level = "Admin") Then
                    If XGrader = "" Then
                        MessageBox.Show("กรุณาคีย์เลือก Gradeที่ต้องการแสดงข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If

                    If CropComboBox.Text = "All" Then
                        MessageBox.Show("กรุณาเลือกCropที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If

                    Dim xfrm As New FrmTransferPackedGrade
                    xfrm.XGraders = XGrader
                    xfrm.XCustomer = XCustomer2
                    xfrm.XCropFromShowPackedGrade = CropComboBox.Text
                    xfrm.XTypeFromShowPackedGrade = TypeComboBox.Text
                    xfrm.ShowDialog()
                    xfrm.Dispose()
                    If CropComboBox.Text = "All" Then
                        Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
                    Else
                        Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
                    End If
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้หากต้องการเพิ่มสิทธิ์นี้กรุณาติดต่อ Admin(แผนก IT)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid.CellMouseClick
        Try
            XGrader = MetroGrid.Item(0, MetroGrid.CurrentCell.RowIndex).Value
            XCustomer2 = MetroGrid.Item(3, MetroGrid.CurrentCell.RowIndex).Value
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class
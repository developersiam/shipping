﻿Imports FactoryBL
Imports FactoryEntities
Imports ShippingSystem.ShippingDataSet

Public Class FrmOrder
    Inherits MetroFramework.Forms.MetroForm
    Dim XOrderNo1 As String
    Dim db As New ShippingDataClassesDataContext
    Dim Xstatus As Boolean = False
    Dim XShipped As Boolean = False
    Dim XInvNo As String
    Dim XPackedGrade As String

    Private Function Authorization() As Boolean
        If _security.shipping = True And _security.level = "Supervisor" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub FrmOrder_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            UsernameMetroLabel.Text = XUsername

            'Sp_Shipping_SEL_ShippingOrderByCropTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrderByCrop)

            StatusCombobox.Items.Clear()
            StatusCombobox.Items.Add("Pending")
            StatusCombobox.Items.Add("Complete")
            StatusCombobox.Items.Add("All")
            StatusCombobox.SelectedIndex = 2

            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 1, XOrderDetailNoLabel.Text)
            OrderNoCustomerLabel.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).orderno
            CustomerLabel.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).customer
            GradeLabel.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).grade
            CaseTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_Order.Item(0).cases)
            WeightTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_Order.Item(0).nett)
            NetTextbox.Text = String.Format("{0:0.0}", ShippingDataSet.sp_Shipping_SEL_Order.Item(0).net)

            'หาผลรวมที่ทำ Shipping orderไปแล้ว
            Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, XOrderDetailNoLabel.Text, 1)
            CaseTotalTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalcaserequest)", String.Empty))
            WeightTotalTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalweightrequest)", String.Empty))
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub StatusCombobox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles StatusCombobox.SelectedIndexChanged
        Try
            'If YearComboBox.Text <> "" Then
            Call ShowData()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            'If YearComboBox.Text <> "" AndAlso StatusCombobox.Text <> "" Then
            '    Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, YearComboBox.Text, StatusCombobox.SelectedIndex)
            'End If

            Call ShowData()
            FinishedTile.Enabled = True
            FinishedTile.Visible = True
            finishLable.Visible = True
            UnfinishedTile.Enabled = True
            UnfinishedTile.Visible = True
            UnfinishedLabel.Visible = True
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub AddTile_Click(sender As Object, e As EventArgs) Handles AddTile.Click
        Try
            If Authorization() = False Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            Dim frm As New FrmAddOrder
            frm.XCropOrder = Year(Now)
            frm.XOrderDetailNo = XOrderDetailNoLabel.Text
            frm.ShowDialog()
            frm.Dispose()
            Call ShowData()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MetroGrid_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles MetroGrid.CellDoubleClick
        Try
            XOrderNo1 = MetroGrid.Item(0, MetroGrid.CurrentCell.RowIndex).Value
            XInvNo = MetroGrid.Item(2, MetroGrid.CurrentCell.RowIndex).Value
            Dim xfrm As New FrmEditOrder2
            xfrm.XOrderNo = XOrderNo1
            xfrm.ShowDialog()
            xfrm.Dispose()
            Call ShowData()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid.CellMouseClick
        Try
            XOrderNo1 = MetroGrid.Item(0, MetroGrid.CurrentCell.RowIndex).Value
            XInvNo = MetroGrid.Item(2, MetroGrid.CurrentCell.RowIndex).Value
            Xstatus = MetroGrid.Item(8, MetroGrid.CurrentCell.RowIndex).Value
            XShipped = False
            If Not IsDBNull(MetroGrid.Item(9, MetroGrid.CurrentCell.RowIndex).Value) Then
                XShipped = True
            End If
            XPackedGrade = MetroGrid.Item(10, MetroGrid.CurrentCell.RowIndex).Value
            'ถ้าเป้น Pending ให้ปุ่ม Finished click ได้   , ปุ่ม Unfinihed click ไม่ได้
            If Xstatus = False Then
                FinishedTile.Enabled = True
                FinishedTile.Visible = True
                finishLable.Visible = True
                UnfinishedTile.Enabled = False
                UnfinishedTile.Visible = False
                UnfinishedLabel.Visible = False
            Else
                FinishedTile.Enabled = False
                FinishedTile.Visible = False
                finishLable.Visible = False
                UnfinishedTile.Enabled = True
                UnfinishedTile.Visible = True
                UnfinishedLabel.Visible = True
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FinishedTile_Click(sender As Object, e As EventArgs) Handles FinishedTile.Click
        Try
            If Authorization() = False Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            'เช็คถ้ายังไม่มีการ Shipped ไม่อนุญาตให้Finished 
            If XShipped = False Then
                Throw New Exception("Order นี้ยังไม่ทำการ Shipped ห่อยาขึ้นตู้ จึงไม่สามารถทำการ Finished ได้")
            End If

            'เช็คถ้ายังไม่มี Inv No. ไม่อนุญาตให้Finished 
            'If XInvNo = "" Then
            '    MessageBox.Show("Order นี้ยังไม่มีเลขInv No. จึงไม่สามารถทำการFinishedได้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Return
            'End If

            Dim result = MessageBoxModule.Question("ต้องการ Finished Order No. " & XOrderNo1 & " ใช่หรือไม่ ?")
            If result = DialogResult.Yes Then
                db.sp_Shipping_FinishedShippingOrder(XOrderNo1, XUsername)
                MessageBoxModule.Info("Finished เรียบร้อยแล้ว")

                Call ShowData()
                ''หาผลรวมที่ทำ Shipping orderไปแล้ว
                'Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, XOrderDetailNoLabel.Text, 1)
                'CaseTotalTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalcaserequest)", String.Empty))
                'WeightTotalTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalweightrequest)", String.Empty))

                'If StatusCombobox.Text = "All" Then
                '    Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, XOrderDetailNoLabel.Text, 1)
                'Else
                '    Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, XOrderDetailNoLabel.Text, 0)
                'End If

            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub UnfinishedTile_Click(sender As Object, e As EventArgs) Handles UnfinishedTile.Click
        Try
            If Authorization() = False Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            Dim result = MessageBoxModule.Question("ต้องการ Finished Order No. " & XOrderNo1 & " ใช่หรือไม่ ?")
            If result = DialogResult.Yes Then
                db.sp_Shipping_UnFinishedShippingOrder(XOrderNo1, XUsername)
                MessageBoxModule.Info("UnFinished เรียบร้อยแล้ว")
                Call ShowData()
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub ShowData()
        Try
            'หาผลรวมที่ทำ Shipping orderไปแล้ว
            Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, XOrderDetailNoLabel.Text, 1)
            CaseTotalTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalcaserequest)", String.Empty))
            WeightTotalTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalweightrequest)", String.Empty))

            If StatusCombobox.Text = "All" Then
                Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, XOrderDetailNoLabel.Text, 1)
            Else
                Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, XOrderDetailNoLabel.Text, 0)
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try

    End Sub

    Private Sub ShowStockTile_Click(sender As Object, e As EventArgs) Handles ShowStockTile.Click
        Try
            Dim frm As New FrmShowPackedGrade
            frm.ShowDialog()
            frm.Close()
            Call ShowData()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub DeleteTile_Click(sender As Object, e As EventArgs) Handles DeleteTile.Click
        Try
            If Authorization() = False Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            If XShipped = True Then
                Throw New Exception("Order " & XOrderNo1 & " ทำการ Shipped ห่อยาขึ้นตู้แล้ว จึงไม่สามารถลบข้อมูล")
            End If

            If Xstatus = True Then
                Throw New Exception("Order " & XOrderNo1 & " นี้ถูก Finished แล้ว จึงไม่สามารถลบข้อมูล")
            End If

            Dim result = MessageBoxModule.Question("ต้องการลบ Order No. " & XOrderNo1 & " ใช่หรือไม่ ?")
            If result = DialogResult.Yes Then
                db.sp_Shipping_DEL_ShippingOrder(XOrderNo1, XUsername)
                MessageBoxModule.Info("ลบ Order No. นี้เรียบร้อยแล้ว")
                Call ShowData()
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub ShowBarcodeListMetroTile_Click(sender As Object, e As EventArgs) Handles ShowBarcodeListMetroTile.Click
        Try
            Dim frm As New FrmPackedCases
            frm.ShowDialog()
            frm.Close()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDestination
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MatRCMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.rcfrom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.starttime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.finishtime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.truckno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.checkerlocked = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CountOfBales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfBales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfWeight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.classifier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.CreateTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        CType(Me.MatRCMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MatRCMetroGrid
        '
        Me.MatRCMetroGrid.AllowUserToAddRows = False
        Me.MatRCMetroGrid.AllowUserToDeleteRows = False
        Me.MatRCMetroGrid.AllowUserToResizeRows = False
        Me.MatRCMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MatRCMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRCMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MatRCMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MatRCMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRCMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MatRCMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MatRCMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.rcfrom, Me.starttime, Me.finishtime, Me.truckno, Me.checkerlocked, Me.CountOfBales, Me.SumOfBales, Me.SumOfWeight, Me.classifier})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MatRCMetroGrid.DefaultCellStyle = DataGridViewCellStyle7
        Me.MatRCMetroGrid.EnableHeadersVisualStyles = False
        Me.MatRCMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MatRCMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRCMetroGrid.Location = New System.Drawing.Point(11, 94)
        Me.MatRCMetroGrid.Name = "MatRCMetroGrid"
        Me.MatRCMetroGrid.ReadOnly = True
        Me.MatRCMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRCMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.MatRCMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MatRCMetroGrid.RowTemplate.Height = 24
        Me.MatRCMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MatRCMetroGrid.Size = New System.Drawing.Size(1179, 632)
        Me.MatRCMetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MatRCMetroGrid.TabIndex = 123
        '
        'rcfrom
        '
        Me.rcfrom.DataPropertyName = "rcfrom"
        Me.rcfrom.HeaderText = "From"
        Me.rcfrom.Name = "rcfrom"
        Me.rcfrom.ReadOnly = True
        Me.rcfrom.Width = 74
        '
        'starttime
        '
        Me.starttime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.starttime.DataPropertyName = "starttime"
        DataGridViewCellStyle2.Format = "T"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.starttime.DefaultCellStyle = DataGridViewCellStyle2
        Me.starttime.HeaderText = "Starttimes"
        Me.starttime.Name = "starttime"
        Me.starttime.ReadOnly = True
        Me.starttime.Width = 112
        '
        'finishtime
        '
        Me.finishtime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.finishtime.DataPropertyName = "finishtime"
        DataGridViewCellStyle3.Format = "T"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.finishtime.DefaultCellStyle = DataGridViewCellStyle3
        Me.finishtime.HeaderText = "Finishtimes"
        Me.finishtime.Name = "finishtime"
        Me.finishtime.ReadOnly = True
        Me.finishtime.Width = 121
        '
        'truckno
        '
        Me.truckno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.truckno.DataPropertyName = "truckno"
        Me.truckno.HeaderText = "Truck no."
        Me.truckno.Name = "truckno"
        Me.truckno.ReadOnly = True
        Me.truckno.Width = 104
        '
        'checkerlocked
        '
        Me.checkerlocked.DataPropertyName = "checkerlocked"
        Me.checkerlocked.HeaderText = "Checker locked"
        Me.checkerlocked.Name = "checkerlocked"
        Me.checkerlocked.ReadOnly = True
        Me.checkerlocked.Width = 128
        '
        'CountOfBales
        '
        Me.CountOfBales.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.CountOfBales.DataPropertyName = "CountOfBales"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.CountOfBales.DefaultCellStyle = DataGridViewCellStyle4
        Me.CountOfBales.HeaderText = "Bales"
        Me.CountOfBales.Name = "CountOfBales"
        Me.CountOfBales.ReadOnly = True
        Me.CountOfBales.Width = 73
        '
        'SumOfBales
        '
        Me.SumOfBales.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.SumOfBales.DataPropertyName = "SumOfWeightbuy"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.SumOfBales.DefaultCellStyle = DataGridViewCellStyle5
        Me.SumOfBales.HeaderText = "Weightbuy"
        Me.SumOfBales.Name = "SumOfBales"
        Me.SumOfBales.ReadOnly = True
        Me.SumOfBales.Width = 116
        '
        'SumOfWeight
        '
        Me.SumOfWeight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.SumOfWeight.DataPropertyName = "SumOfWeight"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.SumOfWeight.DefaultCellStyle = DataGridViewCellStyle6
        Me.SumOfWeight.HeaderText = "Weight"
        Me.SumOfWeight.Name = "SumOfWeight"
        Me.SumOfWeight.ReadOnly = True
        Me.SumOfWeight.Width = 87
        '
        'classifier
        '
        Me.classifier.DataPropertyName = "classifier"
        Me.classifier.HeaderText = "classifier"
        Me.classifier.Name = "classifier"
        Me.classifier.ReadOnly = True
        Me.classifier.Visible = False
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.CreateTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(8, 34)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1179, 54)
        Me.MetroPanel1.TabIndex = 122
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(699, 18)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'CreateTile
        '
        Me.CreateTile.ActiveControl = Nothing
        Me.CreateTile.AutoSize = True
        Me.CreateTile.BackColor = System.Drawing.Color.White
        Me.CreateTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CreateTile.Location = New System.Drawing.Point(589, 18)
        Me.CreateTile.Name = "CreateTile"
        Me.CreateTile.Size = New System.Drawing.Size(38, 35)
        Me.CreateTile.Style = MetroFramework.MetroColorStyle.White
        Me.CreateTile.TabIndex = 120
        Me.CreateTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CreateTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Plus_32
        Me.CreateTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CreateTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.CreateTile.UseSelectable = True
        Me.CreateTile.UseTileImage = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(633, 25)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(48, 20)
        Me.MetroLabel5.TabIndex = 117
        Me.MetroLabel5.Text = "create"
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(741, 25)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1126, 5)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft502
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 3)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 22)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'FrmDestination
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1290, 815)
        Me.Controls.Add(Me.MatRCMetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmDestination"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        CType(Me.MatRCMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MatRCMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents rcfrom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents starttime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents finishtime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents truckno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents checkerlocked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CountOfBales As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SumOfBales As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SumOfWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents classifier As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents CreateTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
End Class

﻿Public Class FrmUpdateMovementTruck
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Dim XOldTruckNo As String
    Dim XOldDriver As String
    Private Sub FrmUpdateMovementTruck_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.sp_Shipping_SEL_ShippingTruck' table. You can move, or remove it, as needed.

        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername

            TruckNoTextbox.Text = ""
            DriverTextbox.Text = ""


            'Get data from matrc
            Dim XRcNoInfoRow As ShippingDataSet.sp_Shipping_SEL_ShippingTruckRow
            Me.Sp_Shipping_SEL_ShippingTruckTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingTruck)
            XRcNoInfoRow = Me.ShippingDataSet.sp_Shipping_SEL_ShippingTruck.FindByTruckID(TruckIDTextbox.Text)

            If XRcNoInfoRow Is Nothing Then
                MessageBox.Show("ไม่พบเลขนี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            TruckNoTextbox.Text = XRcNoInfoRow.TruckNo
            XOldTruckNo = XRcNoInfoRow.TruckNo
            DriverTextbox.Text = XRcNoInfoRow.Driver
            XOldDriver = XRcNoInfoRow.Driver
            If XRcNoInfoRow.Status = True Then
                Status.Text = "Active"
                Status.Checked = True
            ElseIf XRcNoInfoRow.Status = False Then
                Status.Text = "Unactive"
                Status.Checked = False
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If TruckIDTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Truck ID", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If TruckNoTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Truck No", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If DriverTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Truck Driver", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim XStatus As Boolean
            If Status.Checked = True Then
                XStatus = True
            Else
                XStatus = False
            End If

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการแก้ไข Truck ID นี้ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                db.sp_Shipping_UPD_ShippingTruck(TruckIDTextbox.Text, TruckNoTextbox.Text, DriverTextbox.Text, XStatus, XOldTruckNo, XOldDriver, XUsername)

                MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmUpdateMovementStatus
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.Status = New MetroFramework.Controls.MetroCheckBox()
        Me.StatusNameTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.StatusIDTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_SEL_ShippingMovementStatusBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingMovementStatusTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementStatusTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingMovementStatusBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(784, 76)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 116
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(709, 76)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 115
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save50
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'Status
        '
        Me.Status.AutoSize = True
        Me.Status.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.Status.Location = New System.Drawing.Point(305, 311)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(137, 20)
        Me.Status.Style = MetroFramework.MetroColorStyle.Orange
        Me.Status.TabIndex = 118
        Me.Status.Text = "MetroCheckBox1"
        Me.Status.UseSelectable = True
        '
        'StatusNameTextbox
        '
        '
        '
        '
        Me.StatusNameTextbox.CustomButton.Image = Nothing
        Me.StatusNameTextbox.CustomButton.Location = New System.Drawing.Point(383, 2)
        Me.StatusNameTextbox.CustomButton.Name = ""
        Me.StatusNameTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.StatusNameTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.StatusNameTextbox.CustomButton.TabIndex = 1
        Me.StatusNameTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.StatusNameTextbox.CustomButton.UseSelectable = True
        Me.StatusNameTextbox.CustomButton.Visible = False
        Me.StatusNameTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.StatusNameTextbox.Lines = New String(-1) {}
        Me.StatusNameTextbox.Location = New System.Drawing.Point(305, 270)
        Me.StatusNameTextbox.MaxLength = 32767
        Me.StatusNameTextbox.Name = "StatusNameTextbox"
        Me.StatusNameTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.StatusNameTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.StatusNameTextbox.SelectedText = ""
        Me.StatusNameTextbox.SelectionLength = 0
        Me.StatusNameTextbox.SelectionStart = 0
        Me.StatusNameTextbox.Size = New System.Drawing.Size(411, 30)
        Me.StatusNameTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.StatusNameTextbox.TabIndex = 120
        Me.StatusNameTextbox.UseSelectable = True
        Me.StatusNameTextbox.UseStyleColors = True
        Me.StatusNameTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.StatusNameTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(206, 270)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(84, 20)
        Me.MetroLabel3.TabIndex = 122
        Me.MetroLabel3.Text = "Status name"
        '
        'StatusIDTextbox
        '
        Me.StatusIDTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.StatusIDTextbox.CustomButton.Image = Nothing
        Me.StatusIDTextbox.CustomButton.Location = New System.Drawing.Point(76, 2)
        Me.StatusIDTextbox.CustomButton.Name = ""
        Me.StatusIDTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.StatusIDTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.StatusIDTextbox.CustomButton.TabIndex = 1
        Me.StatusIDTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.StatusIDTextbox.CustomButton.UseSelectable = True
        Me.StatusIDTextbox.CustomButton.Visible = False
        Me.StatusIDTextbox.Enabled = False
        Me.StatusIDTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.StatusIDTextbox.Lines = New String(-1) {}
        Me.StatusIDTextbox.Location = New System.Drawing.Point(305, 234)
        Me.StatusIDTextbox.MaxLength = 2
        Me.StatusIDTextbox.Name = "StatusIDTextbox"
        Me.StatusIDTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.StatusIDTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.StatusIDTextbox.SelectedText = ""
        Me.StatusIDTextbox.SelectionLength = 0
        Me.StatusIDTextbox.SelectionStart = 0
        Me.StatusIDTextbox.Size = New System.Drawing.Size(104, 30)
        Me.StatusIDTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.StatusIDTextbox.TabIndex = 119
        Me.StatusIDTextbox.UseSelectable = True
        Me.StatusIDTextbox.UseStyleColors = True
        Me.StatusIDTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.StatusIDTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(206, 234)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(62, 20)
        Me.MetroLabel5.TabIndex = 121
        Me.MetroLabel5.Text = "Stauts ID"
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_SEL_ShippingMovementStatusBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingMovementStatusBindingSource.DataMember = "sp_Shipping_SEL_ShippingMovementStatus"
        Me.Sp_Shipping_SEL_ShippingMovementStatusBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingMovementStatusTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingMovementStatusTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'FrmUpdateMovementStatus
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1011, 556)
        Me.Controls.Add(Me.StatusNameTextbox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.StatusIDTextbox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.AddMetroTile)
        Me.Controls.Add(Me.Status)
        Me.Name = "FrmUpdateMovementStatus"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Update movement status"
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingMovementStatusBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents Status As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents StatusNameTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents StatusIDTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementStatusBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementStatusTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementStatusTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
End Class

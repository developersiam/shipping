﻿Imports System.Globalization
Imports FactoryBL

Public Class FrmLogIn
    Inherits MetroFramework.Forms.MetroForm
    Dim XPassword As String
    Private Sub FrmLogIn_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            'Me.Size = New Size(1197, 734)
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
            Dim culture = CultureInfo.GetCultureInfo("en-US")
            CultureInfo.DefaultThreadCurrentCulture = culture
            CultureInfo.DefaultThreadCurrentUICulture = culture
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub UsernameTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles UsernameTextBox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                PasswordTextBox.Text = ""
                PasswordTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub PasswordTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles PasswordTextBox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                LoginButton.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles ClearButton.Click
        Try
            UsernameTextBox.Text = ""
            PasswordTextBox.Text = ""
            UsernameTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click
        Try
            XUsername = Trim(UsernameTextBox.Text)

            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameTextBox.Text)

            'Get baleBarcodeRow detail 
            Dim security As ShippingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
            security = Me.ReceivingDataSet.security.FindByuname(XUsernameApprove)
            If security Is Nothing Then
                Throw New Exception("ไม่พบ username นี้ในระบบ, กรุณาตรวจสอบข้อมูล")
            ElseIf Not security Is Nothing Then
                'เช็ค password โดยจะต้องใช้ store  decodePassword เพื่อนำ password ที่ทำการเข้ารหัสแต่ละครั้งออกมา โดยต้องเข้ารหัสจำนวน 3 ครั้งจึงจะได้ password ที่แท้จริง
                XPassword = security.pwd
                For i As Integer = 1 To 3
                    Me.Sp_Receiving_DecodePasswordTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_DecodePassword, XPassword)
                    XPassword = Me.ReceivingDataSet.sp_Receiving_DecodePassword.Rows(0).Item("PWDanswer")
                Next

                If security.uname = XUsernameApprove And XPassword = PasswordTextBox.Text And security.shipping = True Then
                    XDepartment = security.depart
                    _security = Facade.securityBL().GetSingle(UsernameTextBox.Text)
                    FrmMenu.ShowDialog()
                Else
                    MessageBox.Show("Username ไม่ถูกต้องหรือไม่มีสิทธิ์ในการใช้งานโปรแกรมนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
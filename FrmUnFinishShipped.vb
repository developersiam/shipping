﻿Public Class FrmUnFinishShipped
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private Sub FrmUnFinishShipped_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            OrderNoTextBox.Text = ""
            OrderStatusTextBox.Text = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If OrderNoTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Order No.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If OrderStatusTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Order No.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการ UnFinished การ Shipped ของ Order นี้ใช่หรือไม่?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                db.sp_Shipping_UPD_UnShipped(OrderNoTextBox.Text)
                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, OrderNoTextBox.Text)
                Me.Sp_Shipping_SEL_PdSpByOrderNoTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PdSpByOrderNo, OrderNoTextBox.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub OrderNoTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles OrderNoTextBox.KeyDown
        Try
            OrderStatusTextBox.Text = ""
            If e.KeyCode = Keys.Enter Then
                Me.Sp_Shipping_SEL_PdSpByOrderNoTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PdSpByOrderNo, OrderNoTextBox.Text)

                Dim XRow2 As ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNoRow
                Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, OrderNoTextBox.Text)
                XRow2 = Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo.FindByOrderNo(OrderNoTextBox.Text)
                If Not XRow2 Is Nothing Then
                    OrderStatusTextBox.Text = XRow2.StatusName

                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAdminSetup
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.SecurityTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter()
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.BtnUnFinishedShipped = New System.Windows.Forms.Button()
        Me.BtnShippingLocation = New System.Windows.Forms.Button()
        Me.BtnMovementTruck = New System.Windows.Forms.Button()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(76, 60)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 136
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.BtnUnFinishedShipped)
        Me.MetroPanel1.Controls.Add(Me.BtnShippingLocation)
        Me.MetroPanel1.Controls.Add(Me.BtnMovementTruck)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(28, 210)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1230, 285)
        Me.MetroPanel1.TabIndex = 135
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'BtnUnFinishedShipped
        '
        Me.BtnUnFinishedShipped.BackColor = System.Drawing.Color.YellowGreen
        Me.BtnUnFinishedShipped.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnUnFinishedShipped.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnUnFinishedShipped.FlatAppearance.BorderSize = 0
        Me.BtnUnFinishedShipped.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnUnFinishedShipped.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnUnFinishedShipped.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnUnFinishedShipped.ForeColor = System.Drawing.Color.White
        Me.BtnUnFinishedShipped.Image = Global.ShippingSystem.My.Resources.Resources.Unlock64
        Me.BtnUnFinishedShipped.Location = New System.Drawing.Point(674, 45)
        Me.BtnUnFinishedShipped.Name = "BtnUnFinishedShipped"
        Me.BtnUnFinishedShipped.Size = New System.Drawing.Size(146, 191)
        Me.BtnUnFinishedShipped.TabIndex = 11
        Me.BtnUnFinishedShipped.Text = "UnFinished shipped"
        Me.BtnUnFinishedShipped.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnUnFinishedShipped.UseVisualStyleBackColor = False
        '
        'BtnShippingLocation
        '
        Me.BtnShippingLocation.BackColor = System.Drawing.Color.DarkKhaki
        Me.BtnShippingLocation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnShippingLocation.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnShippingLocation.FlatAppearance.BorderSize = 0
        Me.BtnShippingLocation.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnShippingLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnShippingLocation.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnShippingLocation.ForeColor = System.Drawing.Color.White
        Me.BtnShippingLocation.Image = Global.ShippingSystem.My.Resources.Resources.DataConfiguration64
        Me.BtnShippingLocation.Location = New System.Drawing.Point(522, 45)
        Me.BtnShippingLocation.Name = "BtnShippingLocation"
        Me.BtnShippingLocation.Size = New System.Drawing.Size(146, 191)
        Me.BtnShippingLocation.TabIndex = 10
        Me.BtnShippingLocation.Text = "Location"
        Me.BtnShippingLocation.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnShippingLocation.UseVisualStyleBackColor = False
        '
        'BtnMovementTruck
        '
        Me.BtnMovementTruck.BackColor = System.Drawing.Color.SandyBrown
        Me.BtnMovementTruck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnMovementTruck.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMovementTruck.FlatAppearance.BorderSize = 0
        Me.BtnMovementTruck.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnMovementTruck.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnMovementTruck.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnMovementTruck.ForeColor = System.Drawing.Color.White
        Me.BtnMovementTruck.Image = Global.ShippingSystem.My.Resources.Resources.MoveStock64
        Me.BtnMovementTruck.Location = New System.Drawing.Point(370, 45)
        Me.BtnMovementTruck.Name = "BtnMovementTruck"
        Me.BtnMovementTruck.Size = New System.Drawing.Size(146, 191)
        Me.BtnMovementTruck.TabIndex = 9
        Me.BtnMovementTruck.Text = "Transport movement"
        Me.BtnMovementTruck.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnMovementTruck.UseVisualStyleBackColor = False
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ShippingDataSet
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1107, 37)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 138
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(13, 51)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 137
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'FrmAdminSetup
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1301, 648)
        Me.Controls.Add(Me.UsernameMetroLabel)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.MetroTile1)
        Me.Name = "FrmAdminSetup"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableAdapterManager As ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents SecurityTableAdapter As ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents ShippingDataSet As ShippingSystem.ShippingDataSet
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents BtnMovementTruck As System.Windows.Forms.Button
    Friend WithEvents BtnShippingLocation As Button
    Friend WithEvents BtnUnFinishedShipped As Button
End Class

﻿Public Class FrmAddMovementTruck
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            TruckIDTextbox.Text = ""
            TruckNoTextbox.Text = ""
            DriverTextbox.Text = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If TruckIDTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Truck ID", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If TruckNoTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Truck No", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If DriverTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Truck Driver", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            db.sp_Shipping_INS_ShippingTruck(TruckIDTextbox.Text, TruckNoTextbox.Text, DriverTextbox.Text)

            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
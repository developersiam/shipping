﻿Public Class FrmShowPackedGradeDetail
    Inherits MetroFramework.Forms.MetroForm
    Public XGraders As String
    Public XCustomer As String
    Private Sub FrmShowPackedGradeDetail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername

            GradersLabel.Text = XGraders
            CaseNoLabel.Text = ""
            Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, XGraders, XCustomer)
            SpShippingSELPackedgradeByGradersBindingSource.Sort = "Graders,grade,pdremark"
            CaseNoLabel.Text = ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Count & " Cases."
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            GradersLabel.Text = XGraders
            CaseNoLabel.Text = ""
            Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, XGraders, XCustomer)
            CaseNoLabel.Text = ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Count & " Cases."
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


End Class
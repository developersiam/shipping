﻿Public Class FrmEditOrder2
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Public XOrderNo As String
    Dim XSINoOld As String
    Dim XINvNoOld As String
    Dim XCustomerGradeOld As String
    Dim XTotalWeightRequestOld As Double
    Dim XTotalCaseRequestOld As Int32
    Dim XContainerOld As String
    Dim XStuffDateOld As DateTime
    Dim XBLNoOld As String
    Dim XMOPIOld As String
    Dim XETAdateOld As DateTime
    Dim XETDdateOld As DateTime
    Dim XBLdateOld As DateTime
    Dim XUserOld As String
    Dim XOrderDetailNo As String

    Private Sub FrmEditOrder2_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            UsernameMetroLabel.Text = XUsername
            Me.Sp_Shipping_SEL_CustomerTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Customer)
            Me.CropFromPackedGradeTableAdapter.Fill(Me.ShippingDataSet.CropFromPackedGrade)
            Me.TypeTableAdapter.Fill(Me.ShippingDataSet.type)


            'show ShippingOrder detail
            Dim xRow1 As ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNoRow
            Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, XOrderNo)
            xRow1 = Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo.FindByOrderNo(XOrderNo)
            If Not xRow1 Is Nothing Then
                XOrderDetailNo = xRow1.OrderDetailNo
                CropTextBox.Text = xRow1.crop
                OrderNoTextbox.Text = xRow1.OrderNo
                SINOTextbox.Text = xRow1.SINo
                XSINoOld = xRow1.SINo
                InvNoTextbox.Text = xRow1.InvNo
                If Not (xRow1.IsLTLInvoiceNoNull) Then
                    LTLInvoiceNoTextBox.Text = xRow1.LTLInvoiceNo
                End If
                ReceivedDateDatetime.Value = xRow1.ReceivedDate
                'SalesOrderTextBox.Text = xRow1.salesorder
                XINvNoOld = xRow1.InvNo
                'CustomerComboBox.Text = xRow1.Customer
                CustomerTextBox.Text = xRow1.Customer
                PackedGradeTextBox.Text = xRow1.PackedGrade
                CustomerGradeTextbox.Text = xRow1.CustomerGrade
                XCustomerGradeOld = xRow1.CustomerGrade
                NewWeightTextBox.Text = xRow1.TotalWeightRequest
                XTotalWeightRequestOld = xRow1.TotalWeightRequest
                NewCaseTextBox.Text = xRow1.TotalCaseRequest
                NetRequestTextBox.Text = String.Format("{0:0.0,#}", xRow1.NetCustomer)
                XTotalCaseRequestOld = xRow1.TotalCaseRequest
                ContainerTextbox.Text = xRow1.Container
                XContainerOld = xRow1.Container
                StuffDate.Value = xRow1.StuffDate
                XStuffDateOld = xRow1.StuffDate
                BLNoTextBox.Text = xRow1.BLNo
                XBLNoOld = xRow1.BLNo
                MOPITextBox.Text = xRow1.MOPI
                XMOPIOld = xRow1.MOPI
                ETADate.Value = xRow1.ETADate
                XETAdateOld = xRow1.ETADate
                ETDDate.Value = xRow1.ETDDate
                XETDdateOld = xRow1.ETDDate
                BLDate.Value = xRow1.BLdate
                XBLdateOld = xRow1.BLdate
                If xRow1.Status = True Then
                    AddTile.Visible = False
                    MetroLabel2.Visible = False
                    StatusTextbox.Text = "Finished"
                ElseIf xRow1.Status = False Then
                    AddTile.Visible = True
                    MetroLabel2.Visible = True
                    StatusTextbox.Text = "Pending"
                End If
                If Not xRow1.IsShippedDateNull Then
                    ShippedDateTextBox.Text = xRow1.ShippedDate
                Else
                    ShippedDateTextBox.Text = ""
                End If

                If Not xRow1.IsShippedUserNull Then
                    ShippedUserTextBox.Text = xRow1.ShippedUser
                Else
                    ShippedUserTextBox.Text = ""
                End If
                TotalWeightCustomer.Text = String.Format("{0:0.0,#}", xRow1.Weightcustomer)
                TotalCaseCustomer.Text = String.Format("{0:#,#}", xRow1.CaseCustomer)

                'ถ้า shipped ไปแล้วไม่อนุญาตให้แก้ไข case และ weight ได้
                If Not xRow1.IsShippedDateNull Then
                    ReceivedDateDatetime.Enabled = False
                    NewCaseTextBox.Enabled = False
                    NewWeightTextBox.Enabled = False
                    ReceivedDateDatetime.CalendarMonthBackground = Color.LightGray
                    NewCaseTextBox.BackColor = Color.LightGray
                    NewWeightTextBox.BackColor = Color.LightGray
                End If


            End If

            If PackedGradeTextBox.Text <> "" Then
                Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, PackedGradeTextBox.Text, CustomerTextBox.Text)
                CurrentStockTextBox.Text = Sp_Shipping_SEL_PackedgradeByGradersBindingSource.Count

                Dim XSumOfCurrentWeight As Double
                XSumOfCurrentWeight = IIf(IsDBNull(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' ")), 0, Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' "))
                CurrentWeightTextBox.Text = XSumOfCurrentWeight.ToString("N2")
            End If

            'หาผลรวมที่ทำ Shipping orderไปแล้ว
            Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, 1, XOrderDetailNo, 1)
            TotalCaseRequestTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalcaserequest)", String.Empty))
            TotalWeightRequestTextBox.Text = String.Format("{0:0.0,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalweightrequest)", String.Empty))



        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

   

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddTile_Click(sender As Object, e As EventArgs) Handles AddTile.Click
        Try
            If OrderNoTextbox.Text = "" Then
                MessageBox.Show("ไม่มี Order No. กรุณาตรวจสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            '25/06/2016 เพิ่มเช็คว่า ถ้ายังไม่ Shipping ให้เช็คพวก NetCase , NetWeight ด้วย
            If ShippedDateTextBox.Text = "" Then
                'Dim XChkCase As Boolean = True
                If NewCaseTextBox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Total case from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewCaseTextBox.Text) > Convert.ToDouble(TotalCaseCustomer.Text) Then
                    MessageBox.Show("จำนวนกล่องที่คุณระบุมากกว่าที่กำหนดให้ลูกค้ารายนี้ กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewCaseTextBox.Text) > Convert.ToDouble(CurrentStockTextBox.Text) Then
                    MessageBox.Show("จำนวนกล่องที่คุณระบุมากกว่าที่มีในสต๊อก กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return

                ElseIf Convert.ToDouble(NewCaseTextBox.Text) > (Convert.ToDouble(TotalCaseCustomer.Text) - Convert.ToDouble(TotalCaseRequestTextBox.Text)) Then
                    MessageBox.Show("จำนวนกล่องที่คุณระบุมากกว่ายอดคงค้างที่ต้องส่งให้ลูกค้า กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                'Dim XChkWeight As Boolean = True
                If NewWeightTextBox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Total weight from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewWeightTextBox.Text) > Convert.ToDouble(TotalWeightCustomer.Text) Then
                    MessageBox.Show("น้ำหนักที่คุณระบุมากกว่าที่กำหนดให้ลูกค้ารายนี้ กรุณาตรวจสอบข้อมูล!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewWeightTextBox.Text) > Convert.ToDouble(CurrentWeightTextBox.Text) Then
                    MessageBox.Show("น้ำหนักที่คุณระบุมากกว่าที่มีในสต๊อก กรุณาตรวจสอบข้อมูล!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewWeightTextBox.Text) > (Convert.ToDouble(TotalWeightCustomer.Text) - Convert.ToDouble(TotalWeightRequestTextBox.Text)) Then
                    MessageBox.Show("น้ำหนักที่คุณระบุมากกว่ายอดคงค้างที่ต้องส่งให้ลูกค้า กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

            End If



            If ContainerTextbox.Text = "" Then
                MessageBox.Show("กรุณาระบุรายละเอียด Container !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If



            db.sp_Shipping_UPD_ShippingOrder(OrderNoTextbox.Text, SINOTextbox.Text, InvNoTextbox.Text, ReceivedDateDatetime.Value _
                                             , NewWeightTextBox.Text, NewCaseTextBox.Text, NetRequestTextBox.Text _
                                             , ContainerTextbox.Text, StuffDate.Value, BLNoTextBox.Text, MOPITextBox.Text _
                                             , ETDDate.Value, ETADate.Value, BLDate.Value, XUsername, CropTextBox.Text _
                                             , XSINoOld, XINvNoOld, XTotalWeightRequestOld, XTotalCaseRequestOld, XContainerOld, XStuffDateOld _
                                             , XBLNoOld, XMOPIOld, XETDdateOld, XETAdateOld, XBLdateOld, XUserOld, LTLInvoiceNoTextBox.Text)
            MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub InfoTile_Click(sender As Object, e As EventArgs) Handles InfoTile.Click
        Try
            Dim XGrader As String
            Dim XCustomer2 As String

            XGrader = PackedGradeTextBox.Text
            XCustomer2 = CustomerTextBox.Text
            Dim xfrm As New FrmShowPackedGradeDetail
            xfrm.XGraders = XGrader
            xfrm.XCustomer = XCustomer2
            xfrm.ShowDialog()
            xfrm.Dispose()
            'If CropComboBox.Text = "All" Then
            '    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
            'Else
            '    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
            'End If

            'PackedGradeComboBox.BackColor = Color.White
            'PackedGradeComboBox.Enabled = True

            If PackedGradeTextBox.Text <> "" Then
                Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, XGrader, XCustomer2)
                CurrentStockTextBox.Text = Sp_Shipping_SEL_PackedgradeByGradersBindingSource.Count

                Dim XSumOfCurrentWeight As Double
                XSumOfCurrentWeight = IIf(IsDBNull(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & XGrader & "' ")), 0, Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & XGrader & "' "))
                CurrentWeightTextBox.Text = XSumOfCurrentWeight.ToString("N2")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

  

    Private Sub NewWeightTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NewWeightTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NewCaseTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles NewCaseTextBox.KeyUp
        Try
            If NetRequestTextBox.Text <> "" AndAlso NewCaseTextBox.Text <> "" Then
                NewWeightTextBox.Text = Convert.ToDouble(NetRequestTextBox.Text) * Convert.ToDouble(NewCaseTextBox.Text)
            Else
                NewWeightTextBox.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub NewCaseTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NewCaseTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

  
End Class
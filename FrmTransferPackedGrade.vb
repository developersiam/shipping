﻿Public Class FrmTransferPackedGrade
    Inherits MetroFramework.Forms.MetroForm
    Public XGraders As String
    Public XCustomer As String
    Public XCropFromShowPackedGrade As Int16
    Public XTypeFromShowPackedGrade As String
    Dim db As New ShippingDataClassesDataContext
    Private Sub FrmTransferPackedGrade_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.sp_Shipping_GETMAX_TransferNo' table. You can move, or remove it, as needed.
        Me.Sp_Shipping_GETMAX_TransferNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_GETMAX_TransferNo)
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername

            Me.TypeTableAdapter.Fill(Me.ShippingDataSet.type)
            Me.CropFromPackedGradeTableAdapter.Fill(Me.ShippingDataSet.CropFromPackedGrade)
            Me.Sp_Shipping_SEL_CustomerTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Customer)

            GradersLabel.Text = XGraders
            CaseNoLabel.Text = ""
            Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, XGraders, XCustomer)
            CaseNoLabel.Text = ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Count & " Cases."

            InStockRadio.Checked = True
            CropComboBox.Text = XCropFromShowPackedGrade
            TypeComboBox.Text = XTypeFromShowPackedGrade
            If CropComboBox.Text = "All" Then
                Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByInternalGrade, 0, TypeComboBox.Text, 0)
            Else
                Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByInternalGrade, CropComboBox.Text, TypeComboBox.Text, 1)
            End If
            PackedGradeComboBox.Text = XGraders
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub CropComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CropComboBox.SelectedIndexChanged
        Try
            If CropComboBox.Text <> "" And TypeComboBox.Text <> "" Then
                If CropComboBox.Text = "All" Then
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByInternalGrade, 0, TypeComboBox.Text, 0)
                Else
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByInternalGrade, CropComboBox.Text, TypeComboBox.Text, 1)
                End If

            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TypeComboBox.SelectedIndexChanged
        Try
            If CropComboBox.Text <> "" And TypeComboBox.Text <> "" Then
                If CropComboBox.Text = "All" Then
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByInternalGrade, 0, TypeComboBox.Text, 0)
                Else
                    Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByInternalGrade, CropComboBox.Text, TypeComboBox.Text, 1)
                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, XGraders, XCustomer)
            CaseNoLabel.Text = ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Count & " Cases."
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AddTile_Click(sender As Object, e As EventArgs) Handles AddTile.Click
        Try
            Dim result As DialogResult
            If InStockRadio.Checked = True Then
                If PackedGradeComboBox.Text = "" Then
                    MessageBox.Show("กรุณาเลือก PackedGrade", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If

                result = MessageBox.Show("ต้องการTransfer Packed Grade จาก  -" & XGraders & "- ไป  -" & PackedGradeComboBox.Text & "- ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            ElseIf CustomerRadio.Checked = True Then
                If CustomerPackedGradeTextbox.Text = "" Then
                    MessageBox.Show("กรุณาคีย์ Grade ของลูกค้า", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If

                If CropForCustomerTextbox.Text = "" Then
                    MessageBox.Show("กรุณาคีย์ Crop ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If

                result = MessageBox.Show("ต้องการTransfer Packed Grade จาก -" & XGraders & "-  ไป  -" & CustomerPackedGradeTextbox.Text & "-  ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            End If

            If result = DialogResult.Yes Then

                'SelectedRowCount เก็บค่าที่ทำไฮไลท์สีเขียว
                Dim SelectedRowCount As Integer
                SelectedRowCount = MetroGrid.Rows.GetRowCount(DataGridViewElementStates.Selected)

                'XBC เก็บค่า Barcodeในแถวที่ทำไฮไลท์สีเขียว
                Dim XBC As String

                'ถ้าไม่ได้เลือกแถว
                If SelectedRowCount <= 0 Then
                    MessageBox.Show("กรุณาClickเลือกแถวที่ต้องการTransferGrade", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If

                'หาค่า Transfer number
                Dim XTransferNo As Decimal
                Me.Sp_Shipping_GETMAX_TransferNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_GETMAX_TransferNo)
                XTransferNo = ShippingDataSet.sp_Shipping_GETMAX_TransferNo.Item(0).TransferNo


                'เลือกเงื่อนไขว่า ถ้าเลือกว่า Transfer ไปยัง PackedGrade In Stock (คือ Packed Grade ที่อยู่ใน  PackedGrade Table)  หรือเป็นการคีย์เกรดลูกค้าเอง
                If InStockRadio.Checked = True Then
                    If (SelectedRowCount > 0) Then
                        db.sp_Shipping_INS_pdTransfer(XUsername, XGraders, PackedGradeComboBox.Text, MetroGrid.SelectedRows(0).Cells(5).Value, CustomerComboBox.Text, XTransferNo)


                        For i As Integer = 0 To SelectedRowCount - 1
                            XBC = MetroGrid.SelectedRows(i).Cells(2).Value.ToString()
                            db.sp_Shipping_UPD_TransferGrade(CropComboBox.Text, PackedGradeComboBox.Text, CustomerComboBox.Text, XGraders, XUsername, XBC, 0, 0, Trim(PdRemarkParentTextBox.Text), XTransferNo)
                        Next
                    End If
                ElseIf CustomerRadio.Checked = True Then
                    If (SelectedRowCount > 0) Then
                        db.sp_Shipping_INS_pdTransfer(XUsername, XGraders, CustomerPackedGradeTextbox.Text, MetroGrid.SelectedRows(0).Cells(5).Value, CustomerComboBox.Text, XTransferNo)

                        For i As Integer = 0 To SelectedRowCount - 1
                            XBC = MetroGrid.SelectedRows(i).Cells(2).Value.ToString()
                            db.sp_Shipping_UPD_TransferGrade(CropForCustomerTextbox.Text, CustomerPackedGradeTextbox.Text, CustomerComboBox.Text, XGraders, XUsername, XBC, 0, 0, Trim(PdRemarkParentTextBox.Text), XTransferNo)
                        Next
                    End If
                End If

                MessageBox.Show("Transfer Grade เรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, XGraders, XCustomer)
                CaseNoLabel.Text = ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Count & " Cases."
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub InStockRadio_CheckedChanged(sender As Object, e As EventArgs) Handles InStockRadio.CheckedChanged
        Try
            If InStockRadio.Checked = True Then
                CropComboBox.Enabled = True
                TypeComboBox.Enabled = True
                PackedGradeComboBox.Enabled = True
                CustomerPackedGradeTextbox.Text = ""
                CustomerPackedGradeTextbox.Enabled = False
                CropForCustomerTextbox.Text = ""
                CropForCustomerTextbox.Enabled = False
                PackedGradeComboBox.Focus()
            Else
                CustomerPackedGradeTextbox.Text = ""
                CustomerPackedGradeTextbox.Enabled = True
                CropForCustomerTextbox.Text = ""
                CropForCustomerTextbox.Enabled = True
                CropComboBox.Enabled = False
                TypeComboBox.Enabled = False
                PackedGradeComboBox.Enabled = False
                CustomerPackedGradeTextbox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CustomerRadio_CheckedChanged(sender As Object, e As EventArgs) Handles CustomerRadio.CheckedChanged
        Try
            If InStockRadio.Checked = True Then
                CropComboBox.Enabled = True
                TypeComboBox.Enabled = True
                PackedGradeComboBox.Enabled = True
                CustomerPackedGradeTextbox.Text = ""
                CustomerPackedGradeTextbox.Enabled = False
                CropForCustomerTextbox.Text = ""
                CropForCustomerTextbox.Enabled = False
                PackedGradeComboBox.Focus()
            Else
                CustomerPackedGradeTextbox.Text = ""
                CustomerPackedGradeTextbox.Enabled = True
                CropForCustomerTextbox.Text = Year(Now)
                CropForCustomerTextbox.Enabled = True
                CropComboBox.Enabled = False
                TypeComboBox.Enabled = False
                PackedGradeComboBox.Enabled = False
                CustomerPackedGradeTextbox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CropForCustomerTextbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CropForCustomerTextbox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


End Class
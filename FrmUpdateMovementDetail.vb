﻿Public Class FrmUpdateMovementDetail
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Public fromMovementNo As String

    Private Sub FrmScanMovement_MoveDoc_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            BindTruckNo()

            Dim row As ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNoRow
            row = GetMovementInformation(fromMovementNo)
            If row Is Nothing Then
                MessageBox.Show("ไม่พบเลข Movement นี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                SaveTile.Enabled = False
                Return
            ElseIf Not row Is Nothing Then
                MovementNoTextbox.Text = row.MovementNo
                FromTextbox.Text = row.FromLocName
                'FromTruckNoTextbox.Text = row.TruckNo
                TruckNoComboBox.SelectedValue = row.TruckID
                CCaseTextbox.Text = GetCountOfCase(fromMovementNo)
                SaveTile.Enabled = Not row.MovementStatusID And Not row.MovementFromFinished

                If row.MovementStatusID Or row.MovementFromFinished Then
                    MessageBox.Show("ไม่สามารถอัพเดทข้อมูล MovementNo " + row.MovementNo + "นี้ได้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Function GetMovementInformation(MovementNo As String) As ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNoRow
        Dim dt As New ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNoDataTable()
        Dim da As New ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter()
        da.Fill(dt, MovementNo)
        Return dt.FindByMovementNo(MovementNo)
    End Function

    Function GetCountOfCase(MovementNo As String) As String
        Dim dt As New ShippingDataSet.sp_Shipping_SEL_ShippingMovementBCByMovementNoDataTable()
        Dim da As New ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter()
        da.Fill(dt, MovementNo)
        Return dt.Count.ToString()
    End Function

    Private Sub BindTruckNo()
        'Get list of MovementNo in current year
        Dim dtCombobox As New ShippingDataSet.sp_Shipping_SEL_ShippingTruckDataTable()
        Dim daCombobox As New ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingTruckTableAdapter()
        daCombobox.Fill(dtCombobox)
        'dtCombobox.DefaultView.Sort = "movementno DESC"
        TruckNoComboBox.DataSource = dtCombobox
        TruckNoComboBox.DisplayMember = "TruckNo"
        TruckNoComboBox.ValueMember = "TruckID"
    End Sub

    Private Sub SaveTile_Click(sender As Object, e As EventArgs) Handles SaveTile.Click
        Try
            'Validate & Confirmation
            If String.IsNullOrEmpty(MovementNoTextbox.Text) Then Return
            If String.IsNullOrEmpty(FromTextbox.Text) Then Return
            If String.IsNullOrEmpty(TruckNoComboBox.SelectedValue) Then Return
            If MessageBox.Show("ยืนยันการอัพเดทข้อมูลหรือไม่?", "Confirmation", MessageBoxButtons.YesNo) = DialogResult.No Then Return
            'Update Data
            db.sp_Shipping_UPD_ShippingMovement(MovementNoTextbox.Text, TruckNoComboBox.SelectedValue, XUsername)
            MessageBox.Show("อัพเดทข้อมูลสำเร็จ")
            Close()
        Catch ex As Exception
            MessageBox.Show("ไม่สามารถัพเดทข้อมูลได้ " + ex.Message)
        End Try
    End Sub

    Private Sub CancelTile_Click(sender As Object, e As EventArgs) Handles CancelTile.Click
        Close()
    End Sub
End Class
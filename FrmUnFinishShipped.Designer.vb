﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmUnFinishShipped
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.OrderNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.OrderStatusTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MetroGrid1 = New MetroFramework.Controls.MetroGrid()
        Me.OrderNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpnoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LockedDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DtrecordDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELPdSpByOrderNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_PdSpByOrderNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PdSpByOrderNoTableAdapter()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.MetroGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELPdSpByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(856, 63)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 227
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(781, 63)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 226
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save50
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MetroLabel2.Location = New System.Drawing.Point(219, 170)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(638, 20)
        Me.MetroLabel2.TabIndex = 230
        Me.MetroLabel2.Text = "ก่อนทำการ Unfinished กรุณาตรวจสอบกับ Green Leaf account ว่าให้ทำการลบข้อมูลใน Pri" &
    "cing system ก่อน"
        Me.MetroLabel2.UseCustomForeColor = True
        '
        'OrderNoTextBox
        '
        Me.OrderNoTextBox.BackColor = System.Drawing.Color.White
        Me.OrderNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.OrderNoTextBox.CustomButton.Image = Nothing
        Me.OrderNoTextBox.CustomButton.Location = New System.Drawing.Point(181, 2)
        Me.OrderNoTextBox.CustomButton.Name = ""
        Me.OrderNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.OrderNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.OrderNoTextBox.CustomButton.TabIndex = 1
        Me.OrderNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OrderNoTextBox.CustomButton.UseSelectable = True
        Me.OrderNoTextBox.CustomButton.Visible = False
        Me.OrderNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.OrderNoTextBox.Lines = New String(-1) {}
        Me.OrderNoTextBox.Location = New System.Drawing.Point(462, 210)
        Me.OrderNoTextBox.MaxLength = 0
        Me.OrderNoTextBox.Name = "OrderNoTextBox"
        Me.OrderNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.OrderNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.OrderNoTextBox.SelectedText = ""
        Me.OrderNoTextBox.SelectionLength = 0
        Me.OrderNoTextBox.SelectionStart = 0
        Me.OrderNoTextBox.Size = New System.Drawing.Size(209, 30)
        Me.OrderNoTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.OrderNoTextBox.TabIndex = 0
        Me.OrderNoTextBox.UseCustomBackColor = True
        Me.OrderNoTextBox.UseCustomForeColor = True
        Me.OrderNoTextBox.UseSelectable = True
        Me.OrderNoTextBox.UseStyleColors = True
        Me.OrderNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.OrderNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(401, 220)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(46, 20)
        Me.MetroLabel4.TabIndex = 234
        Me.MetroLabel4.Text = "Order"
        '
        'OrderStatusTextBox
        '
        Me.OrderStatusTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.OrderStatusTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.OrderStatusTextBox.CustomButton.Image = Nothing
        Me.OrderStatusTextBox.CustomButton.Location = New System.Drawing.Point(181, 2)
        Me.OrderStatusTextBox.CustomButton.Name = ""
        Me.OrderStatusTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.OrderStatusTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.OrderStatusTextBox.CustomButton.TabIndex = 1
        Me.OrderStatusTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OrderStatusTextBox.CustomButton.UseSelectable = True
        Me.OrderStatusTextBox.CustomButton.Visible = False
        Me.OrderStatusTextBox.Enabled = False
        Me.OrderStatusTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.OrderStatusTextBox.Lines = New String(-1) {}
        Me.OrderStatusTextBox.Location = New System.Drawing.Point(462, 246)
        Me.OrderStatusTextBox.MaxLength = 0
        Me.OrderStatusTextBox.Name = "OrderStatusTextBox"
        Me.OrderStatusTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.OrderStatusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.OrderStatusTextBox.SelectedText = ""
        Me.OrderStatusTextBox.SelectionLength = 0
        Me.OrderStatusTextBox.SelectionStart = 0
        Me.OrderStatusTextBox.Size = New System.Drawing.Size(209, 30)
        Me.OrderStatusTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.OrderStatusTextBox.TabIndex = 235
        Me.OrderStatusTextBox.UseCustomBackColor = True
        Me.OrderStatusTextBox.UseCustomForeColor = True
        Me.OrderStatusTextBox.UseSelectable = True
        Me.OrderStatusTextBox.UseStyleColors = True
        Me.OrderStatusTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.OrderStatusTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(362, 256)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(85, 20)
        Me.MetroLabel5.TabIndex = 236
        Me.MetroLabel5.Text = "Order status"
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrderByOrderNo"
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.MetroGrid1)
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(307, 292)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(576, 304)
        Me.GroupBox1.TabIndex = 237
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Shipped detail"
        '
        'MetroGrid1
        '
        Me.MetroGrid1.AllowUserToAddRows = False
        Me.MetroGrid1.AllowUserToDeleteRows = False
        Me.MetroGrid1.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid1.AutoGenerateColumns = False
        Me.MetroGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OrderNoDataGridViewTextBoxColumn, Me.SpnoDataGridViewTextBoxColumn, Me.DateDataGridViewTextBoxColumn, Me.LockedDataGridViewCheckBoxColumn, Me.DtrecordDataGridViewTextBoxColumn})
        Me.MetroGrid1.DataSource = Me.SpShippingSELPdSpByOrderNoBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid1.DefaultCellStyle = DataGridViewCellStyle3
        Me.MetroGrid1.EnableHeadersVisualStyles = False
        Me.MetroGrid1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid1.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid1.Location = New System.Drawing.Point(6, 22)
        Me.MetroGrid1.Name = "MetroGrid1"
        Me.MetroGrid1.ReadOnly = True
        Me.MetroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid1.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.MetroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid1.RowTemplate.Height = 28
        Me.MetroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid1.Size = New System.Drawing.Size(564, 264)
        Me.MetroGrid1.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid1.TabIndex = 205
        Me.MetroGrid1.UseCustomBackColor = True
        Me.MetroGrid1.UseCustomForeColor = True
        Me.MetroGrid1.UseStyleColors = True
        '
        'OrderNoDataGridViewTextBoxColumn
        '
        Me.OrderNoDataGridViewTextBoxColumn.DataPropertyName = "OrderNo"
        Me.OrderNoDataGridViewTextBoxColumn.HeaderText = "OrderNo"
        Me.OrderNoDataGridViewTextBoxColumn.Name = "OrderNoDataGridViewTextBoxColumn"
        Me.OrderNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.OrderNoDataGridViewTextBoxColumn.Width = 89
        '
        'SpnoDataGridViewTextBoxColumn
        '
        Me.SpnoDataGridViewTextBoxColumn.DataPropertyName = "spno"
        Me.SpnoDataGridViewTextBoxColumn.HeaderText = "spno"
        Me.SpnoDataGridViewTextBoxColumn.Name = "SpnoDataGridViewTextBoxColumn"
        Me.SpnoDataGridViewTextBoxColumn.ReadOnly = True
        Me.SpnoDataGridViewTextBoxColumn.Width = 65
        '
        'DateDataGridViewTextBoxColumn
        '
        Me.DateDataGridViewTextBoxColumn.DataPropertyName = "date"
        Me.DateDataGridViewTextBoxColumn.HeaderText = "date"
        Me.DateDataGridViewTextBoxColumn.Name = "DateDataGridViewTextBoxColumn"
        Me.DateDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateDataGridViewTextBoxColumn.Width = 63
        '
        'LockedDataGridViewCheckBoxColumn
        '
        Me.LockedDataGridViewCheckBoxColumn.DataPropertyName = "locked"
        Me.LockedDataGridViewCheckBoxColumn.HeaderText = "locked"
        Me.LockedDataGridViewCheckBoxColumn.Name = "LockedDataGridViewCheckBoxColumn"
        Me.LockedDataGridViewCheckBoxColumn.ReadOnly = True
        Me.LockedDataGridViewCheckBoxColumn.Width = 53
        '
        'DtrecordDataGridViewTextBoxColumn
        '
        Me.DtrecordDataGridViewTextBoxColumn.DataPropertyName = "dtrecord"
        Me.DtrecordDataGridViewTextBoxColumn.HeaderText = "dtrecord"
        Me.DtrecordDataGridViewTextBoxColumn.Name = "DtrecordDataGridViewTextBoxColumn"
        Me.DtrecordDataGridViewTextBoxColumn.ReadOnly = True
        Me.DtrecordDataGridViewTextBoxColumn.Width = 88
        '
        'SpShippingSELPdSpByOrderNoBindingSource
        '
        Me.SpShippingSELPdSpByOrderNoBindingSource.DataMember = "sp_Shipping_SEL_PdSpByOrderNo"
        Me.SpShippingSELPdSpByOrderNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_PdSpByOrderNoTableAdapter
        '
        Me.Sp_Shipping_SEL_PdSpByOrderNoTableAdapter.ClearBeforeFill = True
        '
        'FrmUnFinishShipped
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1182, 715)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.OrderStatusTextBox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.OrderNoTextBox)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.AddMetroTile)
        Me.Name = "FrmUnFinishShipped"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Unfinished shipped"
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.MetroGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELPdSpByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents OrderNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents OrderStatusTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents MetroGrid1 As MetroFramework.Controls.MetroGrid
    Friend WithEvents OrderNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SpnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LockedDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents DtrecordDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SpShippingSELPdSpByOrderNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PdSpByOrderNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PdSpByOrderNoTableAdapter
End Class

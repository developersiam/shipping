﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAddOrder
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.AddTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.InvNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.SINOTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.OrderNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel15 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel16 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel17 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel18 = New MetroFramework.Controls.MetroLabel()
        Me.CustomerGradeTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.NewWeightTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.NewCaseTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.ContainerTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.BLNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MOPITextBox = New MetroFramework.Controls.MetroTextBox()
        Me.StuffDate = New MetroFramework.Controls.MetroDateTime()
        Me.ETADate = New MetroFramework.Controls.MetroDateTime()
        Me.ETDDate = New MetroFramework.Controls.MetroDateTime()
        Me.BLDate = New MetroFramework.Controls.MetroDateTime()
        Me.MetroLabel19 = New MetroFramework.Controls.MetroLabel()
        Me.CropOrderTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.CurrentStockTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        Me.CurrentWeightTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.InfoTile = New MetroFramework.Controls.MetroTile()
        Me.NetRequestTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel21 = New MetroFramework.Controls.MetroLabel()
        Me.ReceivedDateDatetime = New MetroFramework.Controls.MetroDateTime()
        Me.MetroLabel22 = New MetroFramework.Controls.MetroLabel()
        Me.CustomerTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.PackedGradeTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.ShippingDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_GETMAX_ShippingOrderNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_GETMAX_ShippingOrderNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_GETMAX_ShippingOrderNoTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter()
        Me.Sp_Shipping_SEL_OrderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_OrderTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_OrderTableAdapter()
        Me.TotalWeightCustomer = New MetroFramework.Controls.MetroTextBox()
        Me.TotalCaseCustomer = New MetroFramework.Controls.MetroTextBox()
        Me.Sp_Shipping_SEL_ShippingOrderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter()
        Me.TotalCaseRequestTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.TotalWeightRequestTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel23 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel24 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_CustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_CustomerTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_CustomerTableAdapter()
        Me.LTLInvoiceNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel25 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.ShippingDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_GETMAX_ShippingOrderNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_OrderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_CustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.AddTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(1, 51)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1218, 57)
        Me.MetroPanel1.TabIndex = 187
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'AddTile
        '
        Me.AddTile.ActiveControl = Nothing
        Me.AddTile.AutoSize = True
        Me.AddTile.BackColor = System.Drawing.Color.White
        Me.AddTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddTile.Location = New System.Drawing.Point(841, 17)
        Me.AddTile.Name = "AddTile"
        Me.AddTile.Size = New System.Drawing.Size(36, 35)
        Me.AddTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddTile.TabIndex = 130
        Me.AddTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save32
        Me.AddTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddTile.UseSelectable = True
        Me.AddTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(883, 23)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(34, 19)
        Me.MetroLabel2.TabIndex = 129
        Me.MetroLabel2.Text = "save"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1166, 7)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'InvNoTextbox
        '
        '
        '
        '
        Me.InvNoTextbox.CustomButton.Image = Nothing
        Me.InvNoTextbox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.InvNoTextbox.CustomButton.Name = ""
        Me.InvNoTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.InvNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.InvNoTextbox.CustomButton.TabIndex = 1
        Me.InvNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.InvNoTextbox.CustomButton.UseSelectable = True
        Me.InvNoTextbox.CustomButton.Visible = False
        Me.InvNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.InvNoTextbox.Lines = New String(-1) {}
        Me.InvNoTextbox.Location = New System.Drawing.Point(144, 264)
        Me.InvNoTextbox.MaxLength = 32767
        Me.InvNoTextbox.Name = "InvNoTextbox"
        Me.InvNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InvNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.InvNoTextbox.SelectedText = ""
        Me.InvNoTextbox.SelectionLength = 0
        Me.InvNoTextbox.SelectionStart = 0
        Me.InvNoTextbox.ShortcutsEnabled = True
        Me.InvNoTextbox.Size = New System.Drawing.Size(272, 40)
        Me.InvNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.InvNoTextbox.TabIndex = 2
        Me.InvNoTextbox.UseSelectable = True
        Me.InvNoTextbox.UseStyleColors = True
        Me.InvNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.InvNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'SINOTextbox
        '
        '
        '
        '
        Me.SINOTextbox.CustomButton.Image = Nothing
        Me.SINOTextbox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.SINOTextbox.CustomButton.Name = ""
        Me.SINOTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.SINOTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.SINOTextbox.CustomButton.TabIndex = 1
        Me.SINOTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SINOTextbox.CustomButton.UseSelectable = True
        Me.SINOTextbox.CustomButton.Visible = False
        Me.SINOTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.SINOTextbox.Lines = New String(-1) {}
        Me.SINOTextbox.Location = New System.Drawing.Point(144, 218)
        Me.SINOTextbox.MaxLength = 0
        Me.SINOTextbox.Name = "SINOTextbox"
        Me.SINOTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.SINOTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.SINOTextbox.SelectedText = ""
        Me.SINOTextbox.SelectionLength = 0
        Me.SINOTextbox.SelectionStart = 0
        Me.SINOTextbox.ShortcutsEnabled = True
        Me.SINOTextbox.Size = New System.Drawing.Size(272, 40)
        Me.SINOTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.SINOTextbox.TabIndex = 1
        Me.SINOTextbox.UseSelectable = True
        Me.SINOTextbox.UseStyleColors = True
        Me.SINOTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.SINOTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(95, 227)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(44, 19)
        Me.MetroLabel3.TabIndex = 194
        Me.MetroLabel3.Text = "SI No."
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(73, 411)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(66, 19)
        Me.MetroLabel4.TabIndex = 192
        Me.MetroLabel4.Text = "Customer"
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(96, 284)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(43, 19)
        Me.MetroLabel6.TabIndex = 191
        Me.MetroLabel6.Text = "InvNo"
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(69, 452)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(70, 19)
        Me.MetroLabel7.TabIndex = 190
        Me.MetroLabel7.Text = "Packgrade"
        '
        'OrderNoTextbox
        '
        Me.OrderNoTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.OrderNoTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.OrderNoTextbox.CustomButton.Image = Nothing
        Me.OrderNoTextbox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.OrderNoTextbox.CustomButton.Name = ""
        Me.OrderNoTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.OrderNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.OrderNoTextbox.CustomButton.TabIndex = 1
        Me.OrderNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OrderNoTextbox.CustomButton.UseSelectable = True
        Me.OrderNoTextbox.CustomButton.Visible = False
        Me.OrderNoTextbox.Enabled = False
        Me.OrderNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.OrderNoTextbox.Lines = New String(-1) {}
        Me.OrderNoTextbox.Location = New System.Drawing.Point(144, 172)
        Me.OrderNoTextbox.MaxLength = 0
        Me.OrderNoTextbox.Name = "OrderNoTextbox"
        Me.OrderNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.OrderNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.OrderNoTextbox.SelectedText = ""
        Me.OrderNoTextbox.SelectionLength = 0
        Me.OrderNoTextbox.SelectionStart = 0
        Me.OrderNoTextbox.ShortcutsEnabled = True
        Me.OrderNoTextbox.Size = New System.Drawing.Size(272, 40)
        Me.OrderNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.OrderNoTextbox.TabIndex = 0
        Me.OrderNoTextbox.UseCustomBackColor = True
        Me.OrderNoTextbox.UseCustomForeColor = True
        Me.OrderNoTextbox.UseSelectable = True
        Me.OrderNoTextbox.UseStyleColors = True
        Me.OrderNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.OrderNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(76, 192)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(63, 19)
        Me.MetroLabel8.TabIndex = 189
        Me.MetroLabel8.Text = "OrderNo"
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(34, 554)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(105, 19)
        Me.MetroLabel9.TabIndex = 198
        Me.MetroLabel9.Text = "Customer grade"
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(475, 330)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(155, 19)
        Me.MetroLabel10.TabIndex = 199
        Me.MetroLabel10.Text = "Customer weight request"
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.Location = New System.Drawing.Point(503, 279)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(126, 19)
        Me.MetroLabel11.TabIndex = 200
        Me.MetroLabel11.Text = "current case request"
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(874, 146)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(66, 19)
        Me.MetroLabel12.TabIndex = 201
        Me.MetroLabel12.Text = "Contanier"
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.Location = New System.Drawing.Point(874, 187)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(65, 19)
        Me.MetroLabel13.TabIndex = 202
        Me.MetroLabel13.Text = "Stuff date"
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.Location = New System.Drawing.Point(891, 233)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel14.TabIndex = 203
        Me.MetroLabel14.Text = "BL No."
        '
        'MetroLabel15
        '
        Me.MetroLabel15.AutoSize = True
        Me.MetroLabel15.Location = New System.Drawing.Point(898, 279)
        Me.MetroLabel15.Name = "MetroLabel15"
        Me.MetroLabel15.Size = New System.Drawing.Size(43, 19)
        Me.MetroLabel15.TabIndex = 204
        Me.MetroLabel15.Text = "MOPI"
        '
        'MetroLabel16
        '
        Me.MetroLabel16.AutoSize = True
        Me.MetroLabel16.Location = New System.Drawing.Point(878, 320)
        Me.MetroLabel16.Name = "MetroLabel16"
        Me.MetroLabel16.Size = New System.Drawing.Size(60, 19)
        Me.MetroLabel16.TabIndex = 205
        Me.MetroLabel16.Text = "ETA date"
        '
        'MetroLabel17
        '
        Me.MetroLabel17.AutoSize = True
        Me.MetroLabel17.Location = New System.Drawing.Point(875, 361)
        Me.MetroLabel17.Name = "MetroLabel17"
        Me.MetroLabel17.Size = New System.Drawing.Size(62, 19)
        Me.MetroLabel17.TabIndex = 206
        Me.MetroLabel17.Text = "ETD date"
        '
        'MetroLabel18
        '
        Me.MetroLabel18.AutoSize = True
        Me.MetroLabel18.Location = New System.Drawing.Point(886, 402)
        Me.MetroLabel18.Name = "MetroLabel18"
        Me.MetroLabel18.Size = New System.Drawing.Size(53, 19)
        Me.MetroLabel18.TabIndex = 207
        Me.MetroLabel18.Text = "BL date"
        '
        'CustomerGradeTextbox
        '
        Me.CustomerGradeTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.CustomerGradeTextbox.CustomButton.Image = Nothing
        Me.CustomerGradeTextbox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.CustomerGradeTextbox.CustomButton.Name = ""
        Me.CustomerGradeTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CustomerGradeTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CustomerGradeTextbox.CustomButton.TabIndex = 1
        Me.CustomerGradeTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CustomerGradeTextbox.CustomButton.UseSelectable = True
        Me.CustomerGradeTextbox.CustomButton.Visible = False
        Me.CustomerGradeTextbox.Enabled = False
        Me.CustomerGradeTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CustomerGradeTextbox.Lines = New String(-1) {}
        Me.CustomerGradeTextbox.Location = New System.Drawing.Point(144, 534)
        Me.CustomerGradeTextbox.MaxLength = 32767
        Me.CustomerGradeTextbox.Name = "CustomerGradeTextbox"
        Me.CustomerGradeTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CustomerGradeTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CustomerGradeTextbox.SelectedText = ""
        Me.CustomerGradeTextbox.SelectionLength = 0
        Me.CustomerGradeTextbox.SelectionStart = 0
        Me.CustomerGradeTextbox.ShortcutsEnabled = True
        Me.CustomerGradeTextbox.Size = New System.Drawing.Size(272, 40)
        Me.CustomerGradeTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerGradeTextbox.TabIndex = 7
        Me.CustomerGradeTextbox.UseCustomBackColor = True
        Me.CustomerGradeTextbox.UseSelectable = True
        Me.CustomerGradeTextbox.UseStyleColors = True
        Me.CustomerGradeTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CustomerGradeTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'NewWeightTextBox
        '
        '
        '
        '
        Me.NewWeightTextBox.CustomButton.Image = Nothing
        Me.NewWeightTextBox.CustomButton.Location = New System.Drawing.Point(133, 2)
        Me.NewWeightTextBox.CustomButton.Name = ""
        Me.NewWeightTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.NewWeightTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NewWeightTextBox.CustomButton.TabIndex = 1
        Me.NewWeightTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NewWeightTextBox.CustomButton.UseSelectable = True
        Me.NewWeightTextBox.CustomButton.Visible = False
        Me.NewWeightTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.NewWeightTextBox.Lines = New String(-1) {}
        Me.NewWeightTextBox.Location = New System.Drawing.Point(646, 402)
        Me.NewWeightTextBox.MaxLength = 32767
        Me.NewWeightTextBox.Name = "NewWeightTextBox"
        Me.NewWeightTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NewWeightTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NewWeightTextBox.SelectedText = ""
        Me.NewWeightTextBox.SelectionLength = 0
        Me.NewWeightTextBox.SelectionStart = 0
        Me.NewWeightTextBox.ShortcutsEnabled = True
        Me.NewWeightTextBox.Size = New System.Drawing.Size(171, 40)
        Me.NewWeightTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.NewWeightTextBox.TabIndex = 5
        Me.NewWeightTextBox.UseSelectable = True
        Me.NewWeightTextBox.UseStyleColors = True
        Me.NewWeightTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NewWeightTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'NewCaseTextBox
        '
        '
        '
        '
        Me.NewCaseTextBox.CustomButton.Image = Nothing
        Me.NewCaseTextBox.CustomButton.Location = New System.Drawing.Point(133, 2)
        Me.NewCaseTextBox.CustomButton.Name = ""
        Me.NewCaseTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.NewCaseTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NewCaseTextBox.CustomButton.TabIndex = 1
        Me.NewCaseTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NewCaseTextBox.CustomButton.UseSelectable = True
        Me.NewCaseTextBox.CustomButton.Visible = False
        Me.NewCaseTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.NewCaseTextBox.Lines = New String(-1) {}
        Me.NewCaseTextBox.Location = New System.Drawing.Point(646, 264)
        Me.NewCaseTextBox.MaxLength = 32767
        Me.NewCaseTextBox.Name = "NewCaseTextBox"
        Me.NewCaseTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NewCaseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NewCaseTextBox.SelectedText = ""
        Me.NewCaseTextBox.SelectionLength = 0
        Me.NewCaseTextBox.SelectionStart = 0
        Me.NewCaseTextBox.ShortcutsEnabled = True
        Me.NewCaseTextBox.Size = New System.Drawing.Size(171, 40)
        Me.NewCaseTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.NewCaseTextBox.TabIndex = 4
        Me.NewCaseTextBox.UseSelectable = True
        Me.NewCaseTextBox.UseStyleColors = True
        Me.NewCaseTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NewCaseTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ContainerTextbox
        '
        '
        '
        '
        Me.ContainerTextbox.CustomButton.Image = Nothing
        Me.ContainerTextbox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.ContainerTextbox.CustomButton.Name = ""
        Me.ContainerTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.ContainerTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.ContainerTextbox.CustomButton.TabIndex = 1
        Me.ContainerTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.ContainerTextbox.CustomButton.UseSelectable = True
        Me.ContainerTextbox.CustomButton.Visible = False
        Me.ContainerTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.ContainerTextbox.Lines = New String(-1) {}
        Me.ContainerTextbox.Location = New System.Drawing.Point(947, 126)
        Me.ContainerTextbox.MaxLength = 32767
        Me.ContainerTextbox.Name = "ContainerTextbox"
        Me.ContainerTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.ContainerTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.ContainerTextbox.SelectedText = ""
        Me.ContainerTextbox.SelectionLength = 0
        Me.ContainerTextbox.SelectionStart = 0
        Me.ContainerTextbox.ShortcutsEnabled = True
        Me.ContainerTextbox.Size = New System.Drawing.Size(272, 40)
        Me.ContainerTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.ContainerTextbox.TabIndex = 6
        Me.ContainerTextbox.UseSelectable = True
        Me.ContainerTextbox.UseStyleColors = True
        Me.ContainerTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.ContainerTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'BLNoTextBox
        '
        '
        '
        '
        Me.BLNoTextBox.CustomButton.Image = Nothing
        Me.BLNoTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.BLNoTextBox.CustomButton.Name = ""
        Me.BLNoTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.BLNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BLNoTextBox.CustomButton.TabIndex = 1
        Me.BLNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BLNoTextBox.CustomButton.UseSelectable = True
        Me.BLNoTextBox.CustomButton.Visible = False
        Me.BLNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BLNoTextBox.Lines = New String(-1) {}
        Me.BLNoTextBox.Location = New System.Drawing.Point(947, 213)
        Me.BLNoTextBox.MaxLength = 32767
        Me.BLNoTextBox.Name = "BLNoTextBox"
        Me.BLNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BLNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BLNoTextBox.SelectedText = ""
        Me.BLNoTextBox.SelectionLength = 0
        Me.BLNoTextBox.SelectionStart = 0
        Me.BLNoTextBox.ShortcutsEnabled = True
        Me.BLNoTextBox.Size = New System.Drawing.Size(272, 40)
        Me.BLNoTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.BLNoTextBox.TabIndex = 8
        Me.BLNoTextBox.UseSelectable = True
        Me.BLNoTextBox.UseStyleColors = True
        Me.BLNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BLNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MOPITextBox
        '
        '
        '
        '
        Me.MOPITextBox.CustomButton.Image = Nothing
        Me.MOPITextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.MOPITextBox.CustomButton.Name = ""
        Me.MOPITextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.MOPITextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MOPITextBox.CustomButton.TabIndex = 1
        Me.MOPITextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MOPITextBox.CustomButton.UseSelectable = True
        Me.MOPITextBox.CustomButton.Visible = False
        Me.MOPITextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.MOPITextBox.Lines = New String(-1) {}
        Me.MOPITextBox.Location = New System.Drawing.Point(947, 259)
        Me.MOPITextBox.MaxLength = 32767
        Me.MOPITextBox.Name = "MOPITextBox"
        Me.MOPITextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MOPITextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MOPITextBox.SelectedText = ""
        Me.MOPITextBox.SelectionLength = 0
        Me.MOPITextBox.SelectionStart = 0
        Me.MOPITextBox.ShortcutsEnabled = True
        Me.MOPITextBox.Size = New System.Drawing.Size(272, 40)
        Me.MOPITextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.MOPITextBox.TabIndex = 9
        Me.MOPITextBox.UseSelectable = True
        Me.MOPITextBox.UseStyleColors = True
        Me.MOPITextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MOPITextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'StuffDate
        '
        Me.StuffDate.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.StuffDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.StuffDate.Location = New System.Drawing.Point(947, 172)
        Me.StuffDate.MinimumSize = New System.Drawing.Size(0, 35)
        Me.StuffDate.Name = "StuffDate"
        Me.StuffDate.Size = New System.Drawing.Size(272, 35)
        Me.StuffDate.Style = MetroFramework.MetroColorStyle.Lime
        Me.StuffDate.TabIndex = 7
        '
        'ETADate
        '
        Me.ETADate.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.ETADate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ETADate.Location = New System.Drawing.Point(947, 305)
        Me.ETADate.MinimumSize = New System.Drawing.Size(0, 35)
        Me.ETADate.Name = "ETADate"
        Me.ETADate.Size = New System.Drawing.Size(272, 35)
        Me.ETADate.Style = MetroFramework.MetroColorStyle.Lime
        Me.ETADate.TabIndex = 10
        '
        'ETDDate
        '
        Me.ETDDate.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.ETDDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ETDDate.Location = New System.Drawing.Point(947, 346)
        Me.ETDDate.MinimumSize = New System.Drawing.Size(0, 35)
        Me.ETDDate.Name = "ETDDate"
        Me.ETDDate.Size = New System.Drawing.Size(272, 35)
        Me.ETDDate.Style = MetroFramework.MetroColorStyle.Lime
        Me.ETDDate.TabIndex = 11
        '
        'BLDate
        '
        Me.BLDate.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.BLDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.BLDate.Location = New System.Drawing.Point(947, 387)
        Me.BLDate.MinimumSize = New System.Drawing.Size(0, 35)
        Me.BLDate.Name = "BLDate"
        Me.BLDate.Size = New System.Drawing.Size(272, 35)
        Me.BLDate.Style = MetroFramework.MetroColorStyle.Lime
        Me.BLDate.TabIndex = 12
        '
        'MetroLabel19
        '
        Me.MetroLabel19.AutoSize = True
        Me.MetroLabel19.Location = New System.Drawing.Point(63, 146)
        Me.MetroLabel19.Name = "MetroLabel19"
        Me.MetroLabel19.Size = New System.Drawing.Size(76, 19)
        Me.MetroLabel19.TabIndex = 222
        Me.MetroLabel19.Text = "Crop order"
        '
        'CropOrderTextBox
        '
        Me.CropOrderTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CropOrderTextBox.CustomButton.Image = Nothing
        Me.CropOrderTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.CropOrderTextBox.CustomButton.Name = ""
        Me.CropOrderTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CropOrderTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CropOrderTextBox.CustomButton.TabIndex = 1
        Me.CropOrderTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CropOrderTextBox.CustomButton.UseSelectable = True
        Me.CropOrderTextBox.CustomButton.Visible = False
        Me.CropOrderTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CropOrderTextBox.Lines = New String(-1) {}
        Me.CropOrderTextBox.Location = New System.Drawing.Point(144, 126)
        Me.CropOrderTextBox.MaxLength = 4
        Me.CropOrderTextBox.Name = "CropOrderTextBox"
        Me.CropOrderTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CropOrderTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CropOrderTextBox.SelectedText = ""
        Me.CropOrderTextBox.SelectionLength = 0
        Me.CropOrderTextBox.SelectionStart = 0
        Me.CropOrderTextBox.ShortcutsEnabled = True
        Me.CropOrderTextBox.Size = New System.Drawing.Size(272, 40)
        Me.CropOrderTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropOrderTextBox.TabIndex = 0
        Me.CropOrderTextBox.UseSelectable = True
        Me.CropOrderTextBox.UseStyleColors = True
        Me.CropOrderTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CropOrderTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'CurrentStockTextBox
        '
        Me.CurrentStockTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CurrentStockTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CurrentStockTextBox.CustomButton.Image = Nothing
        Me.CurrentStockTextBox.CustomButton.Location = New System.Drawing.Point(68, 2)
        Me.CurrentStockTextBox.CustomButton.Name = ""
        Me.CurrentStockTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CurrentStockTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CurrentStockTextBox.CustomButton.TabIndex = 1
        Me.CurrentStockTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CurrentStockTextBox.CustomButton.UseSelectable = True
        Me.CurrentStockTextBox.CustomButton.Visible = False
        Me.CurrentStockTextBox.Enabled = False
        Me.CurrentStockTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CurrentStockTextBox.FontWeight = MetroFramework.MetroTextBoxWeight.Bold
        Me.CurrentStockTextBox.Lines = New String(-1) {}
        Me.CurrentStockTextBox.Location = New System.Drawing.Point(144, 488)
        Me.CurrentStockTextBox.MaxLength = 0
        Me.CurrentStockTextBox.Name = "CurrentStockTextBox"
        Me.CurrentStockTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CurrentStockTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CurrentStockTextBox.SelectedText = ""
        Me.CurrentStockTextBox.SelectionLength = 0
        Me.CurrentStockTextBox.SelectionStart = 0
        Me.CurrentStockTextBox.ShortcutsEnabled = True
        Me.CurrentStockTextBox.Size = New System.Drawing.Size(106, 40)
        Me.CurrentStockTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CurrentStockTextBox.TabIndex = 225
        Me.CurrentStockTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CurrentStockTextBox.UseCustomBackColor = True
        Me.CurrentStockTextBox.UseCustomForeColor = True
        Me.CurrentStockTextBox.UseSelectable = True
        Me.CurrentStockTextBox.UseStyleColors = True
        Me.CurrentStockTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CurrentStockTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel20
        '
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.Location = New System.Drawing.Point(53, 503)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(86, 19)
        Me.MetroLabel20.TabIndex = 226
        Me.MetroLabel20.Text = "Current stock"
        '
        'CurrentWeightTextBox
        '
        Me.CurrentWeightTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CurrentWeightTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CurrentWeightTextBox.CustomButton.Image = Nothing
        Me.CurrentWeightTextBox.CustomButton.Location = New System.Drawing.Point(116, 2)
        Me.CurrentWeightTextBox.CustomButton.Name = ""
        Me.CurrentWeightTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CurrentWeightTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CurrentWeightTextBox.CustomButton.TabIndex = 1
        Me.CurrentWeightTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CurrentWeightTextBox.CustomButton.UseSelectable = True
        Me.CurrentWeightTextBox.CustomButton.Visible = False
        Me.CurrentWeightTextBox.Enabled = False
        Me.CurrentWeightTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CurrentWeightTextBox.FontWeight = MetroFramework.MetroTextBoxWeight.Bold
        Me.CurrentWeightTextBox.Lines = New String(-1) {}
        Me.CurrentWeightTextBox.Location = New System.Drawing.Point(262, 488)
        Me.CurrentWeightTextBox.MaxLength = 0
        Me.CurrentWeightTextBox.Name = "CurrentWeightTextBox"
        Me.CurrentWeightTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CurrentWeightTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CurrentWeightTextBox.SelectedText = ""
        Me.CurrentWeightTextBox.SelectionLength = 0
        Me.CurrentWeightTextBox.SelectionStart = 0
        Me.CurrentWeightTextBox.ShortcutsEnabled = True
        Me.CurrentWeightTextBox.Size = New System.Drawing.Size(154, 40)
        Me.CurrentWeightTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CurrentWeightTextBox.TabIndex = 227
        Me.CurrentWeightTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CurrentWeightTextBox.UseCustomBackColor = True
        Me.CurrentWeightTextBox.UseCustomForeColor = True
        Me.CurrentWeightTextBox.UseSelectable = True
        Me.CurrentWeightTextBox.UseStyleColors = True
        Me.CurrentWeightTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CurrentWeightTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'InfoTile
        '
        Me.InfoTile.ActiveControl = Nothing
        Me.InfoTile.AutoSize = True
        Me.InfoTile.BackColor = System.Drawing.Color.White
        Me.InfoTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.InfoTile.Location = New System.Drawing.Point(422, 406)
        Me.InfoTile.Name = "InfoTile"
        Me.InfoTile.Size = New System.Drawing.Size(36, 35)
        Me.InfoTile.Style = MetroFramework.MetroColorStyle.White
        Me.InfoTile.TabIndex = 228
        Me.InfoTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.InfoTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Info32
        Me.InfoTile.TileImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.InfoTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.InfoTile.UseSelectable = True
        Me.InfoTile.UseTileImage = True
        '
        'NetRequestTextBox
        '
        Me.NetRequestTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.NetRequestTextBox.CustomButton.Image = Nothing
        Me.NetRequestTextBox.CustomButton.Location = New System.Drawing.Point(133, 2)
        Me.NetRequestTextBox.CustomButton.Name = ""
        Me.NetRequestTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.NetRequestTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NetRequestTextBox.CustomButton.TabIndex = 1
        Me.NetRequestTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NetRequestTextBox.CustomButton.UseSelectable = True
        Me.NetRequestTextBox.CustomButton.Visible = False
        Me.NetRequestTextBox.Enabled = False
        Me.NetRequestTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.NetRequestTextBox.Lines = New String(-1) {}
        Me.NetRequestTextBox.Location = New System.Drawing.Point(646, 126)
        Me.NetRequestTextBox.MaxLength = 32767
        Me.NetRequestTextBox.Name = "NetRequestTextBox"
        Me.NetRequestTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NetRequestTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NetRequestTextBox.SelectedText = ""
        Me.NetRequestTextBox.SelectionLength = 0
        Me.NetRequestTextBox.SelectionStart = 0
        Me.NetRequestTextBox.ShortcutsEnabled = True
        Me.NetRequestTextBox.Size = New System.Drawing.Size(171, 40)
        Me.NetRequestTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.NetRequestTextBox.TabIndex = 266
        Me.NetRequestTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NetRequestTextBox.UseCustomBackColor = True
        Me.NetRequestTextBox.UseCustomForeColor = True
        Me.NetRequestTextBox.UseSelectable = True
        Me.NetRequestTextBox.UseStyleColors = True
        Me.NetRequestTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NetRequestTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel21
        '
        Me.MetroLabel21.AutoSize = True
        Me.MetroLabel21.Location = New System.Drawing.Point(522, 146)
        Me.MetroLabel21.Name = "MetroLabel21"
        Me.MetroLabel21.Size = New System.Drawing.Size(110, 19)
        Me.MetroLabel21.TabIndex = 267
        Me.MetroLabel21.Text = "Net/Case request"
        '
        'ReceivedDateDatetime
        '
        Me.ReceivedDateDatetime.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.ReceivedDateDatetime.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ReceivedDateDatetime.Location = New System.Drawing.Point(144, 355)
        Me.ReceivedDateDatetime.MinimumSize = New System.Drawing.Size(0, 35)
        Me.ReceivedDateDatetime.Name = "ReceivedDateDatetime"
        Me.ReceivedDateDatetime.Size = New System.Drawing.Size(272, 35)
        Me.ReceivedDateDatetime.Style = MetroFramework.MetroColorStyle.Lime
        Me.ReceivedDateDatetime.TabIndex = 3
        '
        'MetroLabel22
        '
        Me.MetroLabel22.AutoSize = True
        Me.MetroLabel22.Location = New System.Drawing.Point(103, 370)
        Me.MetroLabel22.Name = "MetroLabel22"
        Me.MetroLabel22.Size = New System.Drawing.Size(36, 19)
        Me.MetroLabel22.TabIndex = 269
        Me.MetroLabel22.Text = "Date"
        '
        'CustomerTextBox
        '
        Me.CustomerTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CustomerTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CustomerTextBox.CustomButton.Image = Nothing
        Me.CustomerTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.CustomerTextBox.CustomButton.Name = ""
        Me.CustomerTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CustomerTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CustomerTextBox.CustomButton.TabIndex = 1
        Me.CustomerTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CustomerTextBox.CustomButton.UseSelectable = True
        Me.CustomerTextBox.CustomButton.Visible = False
        Me.CustomerTextBox.Enabled = False
        Me.CustomerTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CustomerTextBox.Lines = New String(-1) {}
        Me.CustomerTextBox.Location = New System.Drawing.Point(144, 396)
        Me.CustomerTextBox.MaxLength = 0
        Me.CustomerTextBox.Name = "CustomerTextBox"
        Me.CustomerTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CustomerTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CustomerTextBox.SelectedText = ""
        Me.CustomerTextBox.SelectionLength = 0
        Me.CustomerTextBox.SelectionStart = 0
        Me.CustomerTextBox.ShortcutsEnabled = True
        Me.CustomerTextBox.Size = New System.Drawing.Size(272, 40)
        Me.CustomerTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerTextBox.TabIndex = 270
        Me.CustomerTextBox.UseCustomBackColor = True
        Me.CustomerTextBox.UseCustomForeColor = True
        Me.CustomerTextBox.UseSelectable = True
        Me.CustomerTextBox.UseStyleColors = True
        Me.CustomerTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CustomerTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'PackedGradeTextBox
        '
        Me.PackedGradeTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.PackedGradeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.PackedGradeTextBox.CustomButton.Image = Nothing
        Me.PackedGradeTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.PackedGradeTextBox.CustomButton.Name = ""
        Me.PackedGradeTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.PackedGradeTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.PackedGradeTextBox.CustomButton.TabIndex = 1
        Me.PackedGradeTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.PackedGradeTextBox.CustomButton.UseSelectable = True
        Me.PackedGradeTextBox.CustomButton.Visible = False
        Me.PackedGradeTextBox.Enabled = False
        Me.PackedGradeTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.PackedGradeTextBox.Lines = New String(-1) {}
        Me.PackedGradeTextBox.Location = New System.Drawing.Point(144, 442)
        Me.PackedGradeTextBox.MaxLength = 0
        Me.PackedGradeTextBox.Name = "PackedGradeTextBox"
        Me.PackedGradeTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.PackedGradeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.PackedGradeTextBox.SelectedText = ""
        Me.PackedGradeTextBox.SelectionLength = 0
        Me.PackedGradeTextBox.SelectionStart = 0
        Me.PackedGradeTextBox.ShortcutsEnabled = True
        Me.PackedGradeTextBox.Size = New System.Drawing.Size(272, 40)
        Me.PackedGradeTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.PackedGradeTextBox.TabIndex = 271
        Me.PackedGradeTextBox.UseCustomBackColor = True
        Me.PackedGradeTextBox.UseCustomForeColor = True
        Me.PackedGradeTextBox.UseSelectable = True
        Me.PackedGradeTextBox.UseStyleColors = True
        Me.PackedGradeTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.PackedGradeTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ShippingDataSetBindingSource
        '
        Me.ShippingDataSetBindingSource.DataSource = Me.ShippingDataSet
        Me.ShippingDataSetBindingSource.Position = 0
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_GETMAX_ShippingOrderNoBindingSource
        '
        Me.Sp_Shipping_GETMAX_ShippingOrderNoBindingSource.DataMember = "sp_Shipping_GETMAX_ShippingOrderNo"
        Me.Sp_Shipping_GETMAX_ShippingOrderNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_GETMAX_ShippingOrderNoTableAdapter
        '
        Me.Sp_Shipping_GETMAX_ShippingOrderNoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Shipping_SEL_PackedgradeByGradersBindingSource
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByGraders"
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_PackedgradeByGradersTableAdapter
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_OrderBindingSource
        '
        Me.Sp_Shipping_SEL_OrderBindingSource.DataMember = "sp_Shipping_SEL_Order"
        Me.Sp_Shipping_SEL_OrderBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_OrderTableAdapter
        '
        Me.Sp_Shipping_SEL_OrderTableAdapter.ClearBeforeFill = True
        '
        'TotalWeightCustomer
        '
        Me.TotalWeightCustomer.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TotalWeightCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TotalWeightCustomer.CustomButton.Image = Nothing
        Me.TotalWeightCustomer.CustomButton.Location = New System.Drawing.Point(133, 2)
        Me.TotalWeightCustomer.CustomButton.Name = ""
        Me.TotalWeightCustomer.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.TotalWeightCustomer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalWeightCustomer.CustomButton.TabIndex = 1
        Me.TotalWeightCustomer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalWeightCustomer.CustomButton.UseSelectable = True
        Me.TotalWeightCustomer.CustomButton.Visible = False
        Me.TotalWeightCustomer.Enabled = False
        Me.TotalWeightCustomer.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TotalWeightCustomer.Lines = New String(-1) {}
        Me.TotalWeightCustomer.Location = New System.Drawing.Point(646, 310)
        Me.TotalWeightCustomer.MaxLength = 0
        Me.TotalWeightCustomer.Name = "TotalWeightCustomer"
        Me.TotalWeightCustomer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalWeightCustomer.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalWeightCustomer.SelectedText = ""
        Me.TotalWeightCustomer.SelectionLength = 0
        Me.TotalWeightCustomer.SelectionStart = 0
        Me.TotalWeightCustomer.ShortcutsEnabled = True
        Me.TotalWeightCustomer.Size = New System.Drawing.Size(171, 40)
        Me.TotalWeightCustomer.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalWeightCustomer.TabIndex = 273
        Me.TotalWeightCustomer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TotalWeightCustomer.UseCustomBackColor = True
        Me.TotalWeightCustomer.UseCustomForeColor = True
        Me.TotalWeightCustomer.UseSelectable = True
        Me.TotalWeightCustomer.UseStyleColors = True
        Me.TotalWeightCustomer.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalWeightCustomer.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TotalCaseCustomer
        '
        Me.TotalCaseCustomer.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TotalCaseCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TotalCaseCustomer.CustomButton.Image = Nothing
        Me.TotalCaseCustomer.CustomButton.Location = New System.Drawing.Point(133, 2)
        Me.TotalCaseCustomer.CustomButton.Name = ""
        Me.TotalCaseCustomer.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.TotalCaseCustomer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalCaseCustomer.CustomButton.TabIndex = 1
        Me.TotalCaseCustomer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalCaseCustomer.CustomButton.UseSelectable = True
        Me.TotalCaseCustomer.CustomButton.Visible = False
        Me.TotalCaseCustomer.Enabled = False
        Me.TotalCaseCustomer.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TotalCaseCustomer.Lines = New String(-1) {}
        Me.TotalCaseCustomer.Location = New System.Drawing.Point(646, 172)
        Me.TotalCaseCustomer.MaxLength = 0
        Me.TotalCaseCustomer.Name = "TotalCaseCustomer"
        Me.TotalCaseCustomer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalCaseCustomer.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalCaseCustomer.SelectedText = ""
        Me.TotalCaseCustomer.SelectionLength = 0
        Me.TotalCaseCustomer.SelectionStart = 0
        Me.TotalCaseCustomer.ShortcutsEnabled = True
        Me.TotalCaseCustomer.Size = New System.Drawing.Size(171, 40)
        Me.TotalCaseCustomer.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalCaseCustomer.TabIndex = 275
        Me.TotalCaseCustomer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TotalCaseCustomer.UseCustomBackColor = True
        Me.TotalCaseCustomer.UseCustomForeColor = True
        Me.TotalCaseCustomer.UseSelectable = True
        Me.TotalCaseCustomer.UseStyleColors = True
        Me.TotalCaseCustomer.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalCaseCustomer.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Sp_Shipping_SEL_ShippingOrderBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingOrderBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrder"
        Me.Sp_Shipping_SEL_ShippingOrderBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingOrderTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter.ClearBeforeFill = True
        '
        'TotalCaseRequestTextBox
        '
        Me.TotalCaseRequestTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TotalCaseRequestTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TotalCaseRequestTextBox.CustomButton.Image = Nothing
        Me.TotalCaseRequestTextBox.CustomButton.Location = New System.Drawing.Point(133, 2)
        Me.TotalCaseRequestTextBox.CustomButton.Name = ""
        Me.TotalCaseRequestTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.TotalCaseRequestTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalCaseRequestTextBox.CustomButton.TabIndex = 1
        Me.TotalCaseRequestTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalCaseRequestTextBox.CustomButton.UseSelectable = True
        Me.TotalCaseRequestTextBox.CustomButton.Visible = False
        Me.TotalCaseRequestTextBox.Enabled = False
        Me.TotalCaseRequestTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TotalCaseRequestTextBox.Lines = New String(-1) {}
        Me.TotalCaseRequestTextBox.Location = New System.Drawing.Point(646, 218)
        Me.TotalCaseRequestTextBox.MaxLength = 0
        Me.TotalCaseRequestTextBox.Name = "TotalCaseRequestTextBox"
        Me.TotalCaseRequestTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalCaseRequestTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalCaseRequestTextBox.SelectedText = ""
        Me.TotalCaseRequestTextBox.SelectionLength = 0
        Me.TotalCaseRequestTextBox.SelectionStart = 0
        Me.TotalCaseRequestTextBox.ShortcutsEnabled = True
        Me.TotalCaseRequestTextBox.Size = New System.Drawing.Size(171, 40)
        Me.TotalCaseRequestTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalCaseRequestTextBox.TabIndex = 276
        Me.TotalCaseRequestTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TotalCaseRequestTextBox.UseCustomBackColor = True
        Me.TotalCaseRequestTextBox.UseCustomForeColor = True
        Me.TotalCaseRequestTextBox.UseSelectable = True
        Me.TotalCaseRequestTextBox.UseStyleColors = True
        Me.TotalCaseRequestTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalCaseRequestTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(488, 187)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(142, 19)
        Me.MetroLabel1.TabIndex = 277
        Me.MetroLabel1.Text = "Customer case request"
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(510, 227)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(123, 19)
        Me.MetroLabel5.TabIndex = 278
        Me.MetroLabel5.Text = "Total case to shiped"
        '
        'TotalWeightRequestTextBox
        '
        Me.TotalWeightRequestTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TotalWeightRequestTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TotalWeightRequestTextBox.CustomButton.Image = Nothing
        Me.TotalWeightRequestTextBox.CustomButton.Location = New System.Drawing.Point(133, 2)
        Me.TotalWeightRequestTextBox.CustomButton.Name = ""
        Me.TotalWeightRequestTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.TotalWeightRequestTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalWeightRequestTextBox.CustomButton.TabIndex = 1
        Me.TotalWeightRequestTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalWeightRequestTextBox.CustomButton.UseSelectable = True
        Me.TotalWeightRequestTextBox.CustomButton.Visible = False
        Me.TotalWeightRequestTextBox.Enabled = False
        Me.TotalWeightRequestTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TotalWeightRequestTextBox.Lines = New String(-1) {}
        Me.TotalWeightRequestTextBox.Location = New System.Drawing.Point(646, 356)
        Me.TotalWeightRequestTextBox.MaxLength = 0
        Me.TotalWeightRequestTextBox.Name = "TotalWeightRequestTextBox"
        Me.TotalWeightRequestTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalWeightRequestTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalWeightRequestTextBox.SelectedText = ""
        Me.TotalWeightRequestTextBox.SelectionLength = 0
        Me.TotalWeightRequestTextBox.SelectionStart = 0
        Me.TotalWeightRequestTextBox.ShortcutsEnabled = True
        Me.TotalWeightRequestTextBox.Size = New System.Drawing.Size(171, 40)
        Me.TotalWeightRequestTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalWeightRequestTextBox.TabIndex = 279
        Me.TotalWeightRequestTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TotalWeightRequestTextBox.UseCustomBackColor = True
        Me.TotalWeightRequestTextBox.UseCustomForeColor = True
        Me.TotalWeightRequestTextBox.UseSelectable = True
        Me.TotalWeightRequestTextBox.UseStyleColors = True
        Me.TotalWeightRequestTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalWeightRequestTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel23
        '
        Me.MetroLabel23.AutoSize = True
        Me.MetroLabel23.Location = New System.Drawing.Point(490, 417)
        Me.MetroLabel23.Name = "MetroLabel23"
        Me.MetroLabel23.Size = New System.Drawing.Size(139, 19)
        Me.MetroLabel23.TabIndex = 280
        Me.MetroLabel23.Text = "current weight request"
        '
        'MetroLabel24
        '
        Me.MetroLabel24.AutoSize = True
        Me.MetroLabel24.Location = New System.Drawing.Point(497, 376)
        Me.MetroLabel24.Name = "MetroLabel24"
        Me.MetroLabel24.Size = New System.Drawing.Size(136, 19)
        Me.MetroLabel24.TabIndex = 281
        Me.MetroLabel24.Text = "Total weight to shiped"
        '
        'Sp_Shipping_SEL_CustomerBindingSource
        '
        Me.Sp_Shipping_SEL_CustomerBindingSource.DataMember = "sp_Shipping_SEL_Customer"
        Me.Sp_Shipping_SEL_CustomerBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_CustomerTableAdapter
        '
        Me.Sp_Shipping_SEL_CustomerTableAdapter.ClearBeforeFill = True
        '
        'LTLInvoiceNoTextBox
        '
        '
        '
        '
        Me.LTLInvoiceNoTextBox.CustomButton.Image = Nothing
        Me.LTLInvoiceNoTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.LTLInvoiceNoTextBox.CustomButton.Name = ""
        Me.LTLInvoiceNoTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.LTLInvoiceNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.LTLInvoiceNoTextBox.CustomButton.TabIndex = 1
        Me.LTLInvoiceNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LTLInvoiceNoTextBox.CustomButton.UseSelectable = True
        Me.LTLInvoiceNoTextBox.CustomButton.Visible = False
        Me.LTLInvoiceNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.LTLInvoiceNoTextBox.Lines = New String(-1) {}
        Me.LTLInvoiceNoTextBox.Location = New System.Drawing.Point(144, 309)
        Me.LTLInvoiceNoTextBox.MaxLength = 32767
        Me.LTLInvoiceNoTextBox.Name = "LTLInvoiceNoTextBox"
        Me.LTLInvoiceNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.LTLInvoiceNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.LTLInvoiceNoTextBox.SelectedText = ""
        Me.LTLInvoiceNoTextBox.SelectionLength = 0
        Me.LTLInvoiceNoTextBox.SelectionStart = 0
        Me.LTLInvoiceNoTextBox.ShortcutsEnabled = True
        Me.LTLInvoiceNoTextBox.Size = New System.Drawing.Size(272, 40)
        Me.LTLInvoiceNoTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.LTLInvoiceNoTextBox.TabIndex = 282
        Me.LTLInvoiceNoTextBox.UseSelectable = True
        Me.LTLInvoiceNoTextBox.UseStyleColors = True
        Me.LTLInvoiceNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.LTLInvoiceNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel25
        '
        Me.MetroLabel25.AutoSize = True
        Me.MetroLabel25.ForeColor = System.Drawing.Color.Maroon
        Me.MetroLabel25.Location = New System.Drawing.Point(15, 321)
        Me.MetroLabel25.Name = "MetroLabel25"
        Me.MetroLabel25.Size = New System.Drawing.Size(124, 19)
        Me.MetroLabel25.TabIndex = 283
        Me.MetroLabel25.Text = "LTL Invoice No.(Ref)"
        Me.MetroLabel25.UseCustomForeColor = True
        '
        'FrmAddOrder
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1239, 609)
        Me.Controls.Add(Me.LTLInvoiceNoTextBox)
        Me.Controls.Add(Me.MetroLabel25)
        Me.Controls.Add(Me.MetroLabel24)
        Me.Controls.Add(Me.MetroLabel23)
        Me.Controls.Add(Me.TotalWeightRequestTextBox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.TotalCaseRequestTextBox)
        Me.Controls.Add(Me.TotalCaseCustomer)
        Me.Controls.Add(Me.TotalWeightCustomer)
        Me.Controls.Add(Me.PackedGradeTextBox)
        Me.Controls.Add(Me.CustomerTextBox)
        Me.Controls.Add(Me.ReceivedDateDatetime)
        Me.Controls.Add(Me.MetroLabel22)
        Me.Controls.Add(Me.NetRequestTextBox)
        Me.Controls.Add(Me.MetroLabel21)
        Me.Controls.Add(Me.InfoTile)
        Me.Controls.Add(Me.CurrentWeightTextBox)
        Me.Controls.Add(Me.CurrentStockTextBox)
        Me.Controls.Add(Me.MetroLabel20)
        Me.Controls.Add(Me.CropOrderTextBox)
        Me.Controls.Add(Me.MetroLabel19)
        Me.Controls.Add(Me.BLDate)
        Me.Controls.Add(Me.ETDDate)
        Me.Controls.Add(Me.ETADate)
        Me.Controls.Add(Me.StuffDate)
        Me.Controls.Add(Me.MOPITextBox)
        Me.Controls.Add(Me.BLNoTextBox)
        Me.Controls.Add(Me.ContainerTextbox)
        Me.Controls.Add(Me.NewCaseTextBox)
        Me.Controls.Add(Me.NewWeightTextBox)
        Me.Controls.Add(Me.CustomerGradeTextbox)
        Me.Controls.Add(Me.MetroLabel18)
        Me.Controls.Add(Me.MetroLabel17)
        Me.Controls.Add(Me.MetroLabel16)
        Me.Controls.Add(Me.MetroLabel15)
        Me.Controls.Add(Me.MetroLabel14)
        Me.Controls.Add(Me.MetroLabel13)
        Me.Controls.Add(Me.MetroLabel12)
        Me.Controls.Add(Me.MetroLabel11)
        Me.Controls.Add(Me.MetroLabel10)
        Me.Controls.Add(Me.MetroLabel9)
        Me.Controls.Add(Me.InvNoTextbox)
        Me.Controls.Add(Me.SINOTextbox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.MetroLabel6)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.OrderNoTextbox)
        Me.Controls.Add(Me.MetroLabel8)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmAddOrder"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Add Order"
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.ShippingDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_GETMAX_ShippingOrderNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_OrderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_CustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents AddTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents InvNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents SINOTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents OrderNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel15 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel16 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel17 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel18 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CustomerGradeTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents NewWeightTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents NewCaseTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents ContainerTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents BLNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MOPITextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents StuffDate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents ETADate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents ETDDate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents BLDate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents ShippingDataSetBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_GETMAX_ShippingOrderNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_GETMAX_ShippingOrderNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_GETMAX_ShippingOrderNoTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents MetroLabel19 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropOrderTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents CurrentStockTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter
    Friend WithEvents CurrentWeightTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents InfoTile As MetroFramework.Controls.MetroTile
    Friend WithEvents NetRequestTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel21 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivedDateDatetime As MetroFramework.Controls.MetroDateTime
    Friend WithEvents MetroLabel22 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CustomerTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents PackedGradeTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Sp_Shipping_SEL_OrderBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_OrderTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_OrderTableAdapter
    Friend WithEvents TotalWeightCustomer As MetroFramework.Controls.MetroTextBox
    Friend WithEvents TotalCaseCustomer As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter
    Friend WithEvents TotalCaseRequestTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalWeightRequestTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel23 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel24 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_CustomerBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_CustomerTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_CustomerTableAdapter
    Friend WithEvents LTLInvoiceNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel25 As MetroFramework.Controls.MetroLabel
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmOrder
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim MetroTile2 As MetroFramework.Controls.MetroTile
        Dim MetroTile3 As MetroFramework.Controls.MetroTile
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.OrderNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SINoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceivedDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalWeightRequestDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalCaseRequestDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BLNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ShippedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PackedGrade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELShippingOrderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.ShowBarcodeListMetroTile = New MetroFramework.Controls.MetroTile()
        Me.ShowStockTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.UnfinishedTile = New MetroFramework.Controls.MetroTile()
        Me.UnfinishedLabel = New MetroFramework.Controls.MetroLabel()
        Me.FinishedTile = New MetroFramework.Controls.MetroTile()
        Me.finishLable = New MetroFramework.Controls.MetroLabel()
        Me.DeleteTile = New MetroFramework.Controls.MetroTile()
        Me.AddTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.SpShippingSELShippingOrderByCropBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingOrderByCropTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByCropTableAdapter()
        Me.StatusCombobox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel2 = New MetroFramework.Controls.MetroPanel()
        Me.NetTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.WeightTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.CaseTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.GradeLabel = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.CustomerLabel = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.OrderNoCustomerLabel = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.XOrderDetailNoLabel = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_OrderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_OrderTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_OrderTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.MetroPanel3 = New MetroFramework.Controls.MetroPanel()
        Me.CaseTotalTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel19 = New MetroFramework.Controls.MetroLabel()
        Me.WeightTotalTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        MetroTile2 = New MetroFramework.Controls.MetroTile()
        MetroTile3 = New MetroFramework.Controls.MetroTile()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.SpShippingSELShippingOrderByCropBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel2.SuspendLayout()
        CType(Me.Sp_Shipping_SEL_OrderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroTile2
        '
        MetroTile2.ActiveControl = Nothing
        MetroTile2.AutoSize = True
        MetroTile2.Dock = System.Windows.Forms.DockStyle.Top
        MetroTile2.Location = New System.Drawing.Point(0, 0)
        MetroTile2.Name = "MetroTile2"
        MetroTile2.Size = New System.Drawing.Size(261, 38)
        MetroTile2.Style = MetroFramework.MetroColorStyle.Lime
        MetroTile2.TabIndex = 2
        MetroTile2.Text = "Customer order"
        MetroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        MetroTile2.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        MetroTile2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        MetroTile2.UseSelectable = True
        '
        'MetroTile3
        '
        MetroTile3.ActiveControl = Nothing
        MetroTile3.AutoSize = True
        MetroTile3.Dock = System.Windows.Forms.DockStyle.Top
        MetroTile3.Location = New System.Drawing.Point(0, 0)
        MetroTile3.Name = "MetroTile3"
        MetroTile3.Size = New System.Drawing.Size(261, 38)
        MetroTile3.Style = MetroFramework.MetroColorStyle.Lime
        MetroTile3.TabIndex = 2
        MetroTile3.Text = "Shipping order "
        MetroTile3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        MetroTile3.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        MetroTile3.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        MetroTile3.UseSelectable = True
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OrderNoDataGridViewTextBoxColumn, Me.SINoDataGridViewTextBoxColumn, Me.InvNoDataGridViewTextBoxColumn, Me.ReceivedDateDataGridViewTextBoxColumn, Me.TotalWeightRequestDataGridViewTextBoxColumn, Me.TotalCaseRequestDataGridViewTextBoxColumn, Me.BLNo, Me.StatusName, Me.Status, Me.ShippedDate, Me.PackedGrade})
        Me.MetroGrid.DataSource = Me.SpShippingSELShippingOrderBindingSource
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle7
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(270, 125)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 28
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(935, 595)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 187
        '
        'OrderNoDataGridViewTextBoxColumn
        '
        Me.OrderNoDataGridViewTextBoxColumn.DataPropertyName = "OrderNo"
        Me.OrderNoDataGridViewTextBoxColumn.HeaderText = "OrderNo"
        Me.OrderNoDataGridViewTextBoxColumn.Name = "OrderNoDataGridViewTextBoxColumn"
        Me.OrderNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.OrderNoDataGridViewTextBoxColumn.Width = 87
        '
        'SINoDataGridViewTextBoxColumn
        '
        Me.SINoDataGridViewTextBoxColumn.DataPropertyName = "SINo"
        Me.SINoDataGridViewTextBoxColumn.HeaderText = "SINo"
        Me.SINoDataGridViewTextBoxColumn.Name = "SINoDataGridViewTextBoxColumn"
        Me.SINoDataGridViewTextBoxColumn.ReadOnly = True
        Me.SINoDataGridViewTextBoxColumn.Width = 61
        '
        'InvNoDataGridViewTextBoxColumn
        '
        Me.InvNoDataGridViewTextBoxColumn.DataPropertyName = "InvNo"
        Me.InvNoDataGridViewTextBoxColumn.HeaderText = "InvNo"
        Me.InvNoDataGridViewTextBoxColumn.Name = "InvNoDataGridViewTextBoxColumn"
        Me.InvNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.InvNoDataGridViewTextBoxColumn.Width = 69
        '
        'ReceivedDateDataGridViewTextBoxColumn
        '
        Me.ReceivedDateDataGridViewTextBoxColumn.DataPropertyName = "ReceivedDate"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.ReceivedDateDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.ReceivedDateDataGridViewTextBoxColumn.HeaderText = "Received"
        Me.ReceivedDateDataGridViewTextBoxColumn.Name = "ReceivedDateDataGridViewTextBoxColumn"
        Me.ReceivedDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReceivedDateDataGridViewTextBoxColumn.Width = 91
        '
        'TotalWeightRequestDataGridViewTextBoxColumn
        '
        Me.TotalWeightRequestDataGridViewTextBoxColumn.DataPropertyName = "TotalWeightRequest"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TotalWeightRequestDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.TotalWeightRequestDataGridViewTextBoxColumn.HeaderText = "Weight"
        Me.TotalWeightRequestDataGridViewTextBoxColumn.Name = "TotalWeightRequestDataGridViewTextBoxColumn"
        Me.TotalWeightRequestDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotalWeightRequestDataGridViewTextBoxColumn.Width = 78
        '
        'TotalCaseRequestDataGridViewTextBoxColumn
        '
        Me.TotalCaseRequestDataGridViewTextBoxColumn.DataPropertyName = "TotalCaseRequest"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.TotalCaseRequestDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.TotalCaseRequestDataGridViewTextBoxColumn.HeaderText = "Case"
        Me.TotalCaseRequestDataGridViewTextBoxColumn.Name = "TotalCaseRequestDataGridViewTextBoxColumn"
        Me.TotalCaseRequestDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotalCaseRequestDataGridViewTextBoxColumn.Width = 64
        '
        'BLNo
        '
        Me.BLNo.DataPropertyName = "BLNo"
        Me.BLNo.HeaderText = "BLNo"
        Me.BLNo.Name = "BLNo"
        Me.BLNo.ReadOnly = True
        Me.BLNo.Width = 66
        '
        'StatusName
        '
        Me.StatusName.DataPropertyName = "StatusName"
        Me.StatusName.HeaderText = "Status"
        Me.StatusName.Name = "StatusName"
        Me.StatusName.ReadOnly = True
        Me.StatusName.Width = 72
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        Me.Status.Visible = False
        Me.Status.Width = 66
        '
        'ShippedDate
        '
        Me.ShippedDate.DataPropertyName = "ShippedDate"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Format = "g"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.ShippedDate.DefaultCellStyle = DataGridViewCellStyle6
        Me.ShippedDate.HeaderText = "Shipped"
        Me.ShippedDate.Name = "ShippedDate"
        Me.ShippedDate.ReadOnly = True
        Me.ShippedDate.Width = 83
        '
        'PackedGrade
        '
        Me.PackedGrade.DataPropertyName = "PackedGrade"
        Me.PackedGrade.HeaderText = "PackedGrade"
        Me.PackedGrade.Name = "PackedGrade"
        Me.PackedGrade.ReadOnly = True
        Me.PackedGrade.Visible = False
        Me.PackedGrade.Width = 145
        '
        'SpShippingSELShippingOrderBindingSource
        '
        Me.SpShippingSELShippingOrderBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrder"
        Me.SpShippingSELShippingOrderBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.MetroLabel13)
        Me.MetroPanel1.Controls.Add(Me.ShowBarcodeListMetroTile)
        Me.MetroPanel1.Controls.Add(Me.ShowStockTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.UnfinishedTile)
        Me.MetroPanel1.Controls.Add(Me.UnfinishedLabel)
        Me.MetroPanel1.Controls.Add(Me.FinishedTile)
        Me.MetroPanel1.Controls.Add(Me.finishLable)
        Me.MetroPanel1.Controls.Add(Me.DeleteTile)
        Me.MetroPanel1.Controls.Add(Me.AddTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 48)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1202, 62)
        Me.MetroPanel1.TabIndex = 186
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.Location = New System.Drawing.Point(252, 21)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(110, 19)
        Me.MetroLabel13.TabIndex = 139
        Me.MetroLabel13.Text = "show barcode list"
        '
        'ShowBarcodeListMetroTile
        '
        Me.ShowBarcodeListMetroTile.ActiveControl = Nothing
        Me.ShowBarcodeListMetroTile.AutoSize = True
        Me.ShowBarcodeListMetroTile.BackColor = System.Drawing.Color.White
        Me.ShowBarcodeListMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ShowBarcodeListMetroTile.Location = New System.Drawing.Point(210, 15)
        Me.ShowBarcodeListMetroTile.Name = "ShowBarcodeListMetroTile"
        Me.ShowBarcodeListMetroTile.Size = New System.Drawing.Size(36, 35)
        Me.ShowBarcodeListMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.ShowBarcodeListMetroTile.TabIndex = 138
        Me.ShowBarcodeListMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowBarcodeListMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.UseLogin1
        Me.ShowBarcodeListMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowBarcodeListMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ShowBarcodeListMetroTile.UseSelectable = True
        Me.ShowBarcodeListMetroTile.UseTileImage = True
        '
        'ShowStockTile
        '
        Me.ShowStockTile.ActiveControl = Nothing
        Me.ShowStockTile.AutoSize = True
        Me.ShowStockTile.BackColor = System.Drawing.Color.White
        Me.ShowStockTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ShowStockTile.Location = New System.Drawing.Point(383, 15)
        Me.ShowStockTile.Name = "ShowStockTile"
        Me.ShowStockTile.Size = New System.Drawing.Size(36, 35)
        Me.ShowStockTile.Style = MetroFramework.MetroColorStyle.White
        Me.ShowStockTile.TabIndex = 137
        Me.ShowStockTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowStockTile.TileImage = Global.ShippingSystem.My.Resources.Resources.UseLogin1
        Me.ShowStockTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowStockTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ShowStockTile.UseSelectable = True
        Me.ShowStockTile.UseTileImage = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(425, 21)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(71, 19)
        Me.MetroLabel4.TabIndex = 136
        Me.MetroLabel4.Text = "show stock"
        '
        'UnfinishedTile
        '
        Me.UnfinishedTile.ActiveControl = Nothing
        Me.UnfinishedTile.AutoSize = True
        Me.UnfinishedTile.BackColor = System.Drawing.Color.White
        Me.UnfinishedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.UnfinishedTile.Location = New System.Drawing.Point(856, 13)
        Me.UnfinishedTile.Name = "UnfinishedTile"
        Me.UnfinishedTile.Size = New System.Drawing.Size(36, 35)
        Me.UnfinishedTile.Style = MetroFramework.MetroColorStyle.White
        Me.UnfinishedTile.TabIndex = 135
        Me.UnfinishedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnfinishedTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Unlock321
        Me.UnfinishedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnfinishedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.UnfinishedTile.UseSelectable = True
        Me.UnfinishedTile.UseTileImage = True
        '
        'UnfinishedLabel
        '
        Me.UnfinishedLabel.AutoSize = True
        Me.UnfinishedLabel.Location = New System.Drawing.Point(898, 23)
        Me.UnfinishedLabel.Name = "UnfinishedLabel"
        Me.UnfinishedLabel.Size = New System.Drawing.Size(52, 19)
        Me.UnfinishedLabel.TabIndex = 134
        Me.UnfinishedLabel.Text = "unfinish"
        '
        'FinishedTile
        '
        Me.FinishedTile.ActiveControl = Nothing
        Me.FinishedTile.AutoSize = True
        Me.FinishedTile.BackColor = System.Drawing.Color.White
        Me.FinishedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FinishedTile.Location = New System.Drawing.Point(744, 14)
        Me.FinishedTile.Name = "FinishedTile"
        Me.FinishedTile.Size = New System.Drawing.Size(36, 35)
        Me.FinishedTile.Style = MetroFramework.MetroColorStyle.White
        Me.FinishedTile.TabIndex = 133
        Me.FinishedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FinishedTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Lock321
        Me.FinishedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FinishedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FinishedTile.UseSelectable = True
        Me.FinishedTile.UseTileImage = True
        '
        'finishLable
        '
        Me.finishLable.AutoSize = True
        Me.finishLable.Location = New System.Drawing.Point(786, 24)
        Me.finishLable.Name = "finishLable"
        Me.finishLable.Size = New System.Drawing.Size(38, 19)
        Me.finishLable.TabIndex = 132
        Me.finishLable.Text = "finish"
        '
        'DeleteTile
        '
        Me.DeleteTile.ActiveControl = Nothing
        Me.DeleteTile.AutoSize = True
        Me.DeleteTile.BackColor = System.Drawing.Color.White
        Me.DeleteTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteTile.Location = New System.Drawing.Point(634, 13)
        Me.DeleteTile.Name = "DeleteTile"
        Me.DeleteTile.Size = New System.Drawing.Size(36, 35)
        Me.DeleteTile.Style = MetroFramework.MetroColorStyle.White
        Me.DeleteTile.TabIndex = 131
        Me.DeleteTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DeleteTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Cancel32
        Me.DeleteTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DeleteTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.DeleteTile.UseSelectable = True
        Me.DeleteTile.UseTileImage = True
        '
        'AddTile
        '
        Me.AddTile.ActiveControl = Nothing
        Me.AddTile.AutoSize = True
        Me.AddTile.BackColor = System.Drawing.Color.White
        Me.AddTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddTile.Location = New System.Drawing.Point(524, 16)
        Me.AddTile.Name = "AddTile"
        Me.AddTile.Size = New System.Drawing.Size(36, 35)
        Me.AddTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddTile.TabIndex = 130
        Me.AddTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Plus_32
        Me.AddTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddTile.UseSelectable = True
        Me.AddTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(566, 22)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel2.TabIndex = 129
        Me.MetroLabel2.Text = "create"
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(676, 21)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel5.TabIndex = 128
        Me.MetroLabel5.Text = "delete"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(1002, 13)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 126
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(1044, 23)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel1.TabIndex = 118
        Me.MetroLabel1.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1143, 7)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'SpShippingSELShippingOrderByCropBindingSource
        '
        Me.SpShippingSELShippingOrderByCropBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrderByCrop"
        Me.SpShippingSELShippingOrderByCropBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingOrderByCropTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderByCropTableAdapter.ClearBeforeFill = True
        '
        'StatusCombobox
        '
        Me.StatusCombobox.FormattingEnabled = True
        Me.StatusCombobox.ItemHeight = 23
        Me.StatusCombobox.Location = New System.Drawing.Point(85, 370)
        Me.StatusCombobox.Name = "StatusCombobox"
        Me.StatusCombobox.Size = New System.Drawing.Size(173, 29)
        Me.StatusCombobox.Style = MetroFramework.MetroColorStyle.Orange
        Me.StatusCombobox.TabIndex = 192
        Me.StatusCombobox.UseSelectable = True
        '
        'Sp_Shipping_SEL_ShippingOrderTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter.ClearBeforeFill = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(33, 375)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(41, 19)
        Me.MetroLabel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel3.TabIndex = 194
        Me.MetroLabel3.Text = "status"
        '
        'MetroPanel2
        '
        Me.MetroPanel2.Controls.Add(Me.NetTextbox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel12)
        Me.MetroPanel2.Controls.Add(Me.WeightTextBox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel9)
        Me.MetroPanel2.Controls.Add(Me.CaseTextBox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel7)
        Me.MetroPanel2.Controls.Add(Me.GradeLabel)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel2.Controls.Add(Me.StatusCombobox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel3)
        Me.MetroPanel2.Controls.Add(Me.CustomerLabel)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel8)
        Me.MetroPanel2.Controls.Add(Me.OrderNoCustomerLabel)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel10)
        Me.MetroPanel2.Controls.Add(Me.XOrderDetailNoLabel)
        Me.MetroPanel2.Controls.Add(MetroTile2)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel11)
        Me.MetroPanel2.HorizontalScrollbarBarColor = True
        Me.MetroPanel2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.HorizontalScrollbarSize = 10
        Me.MetroPanel2.Location = New System.Drawing.Point(3, 125)
        Me.MetroPanel2.Name = "MetroPanel2"
        Me.MetroPanel2.Size = New System.Drawing.Size(261, 422)
        Me.MetroPanel2.TabIndex = 198
        Me.MetroPanel2.VerticalScrollbarBarColor = True
        Me.MetroPanel2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.VerticalScrollbarSize = 10
        '
        'NetTextbox
        '
        Me.NetTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.NetTextbox.CustomButton.Image = Nothing
        Me.NetTextbox.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.NetTextbox.CustomButton.Name = ""
        Me.NetTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.NetTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NetTextbox.CustomButton.TabIndex = 1
        Me.NetTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NetTextbox.CustomButton.UseSelectable = True
        Me.NetTextbox.CustomButton.Visible = False
        Me.NetTextbox.Enabled = False
        Me.NetTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.NetTextbox.Lines = New String(-1) {}
        Me.NetTextbox.Location = New System.Drawing.Point(111, 320)
        Me.NetTextbox.MaxLength = 0
        Me.NetTextbox.Name = "NetTextbox"
        Me.NetTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NetTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NetTextbox.SelectedText = ""
        Me.NetTextbox.SelectionLength = 0
        Me.NetTextbox.SelectionStart = 0
        Me.NetTextbox.ShortcutsEnabled = True
        Me.NetTextbox.Size = New System.Drawing.Size(147, 40)
        Me.NetTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.NetTextbox.TabIndex = 202
        Me.NetTextbox.UseCustomBackColor = True
        Me.NetTextbox.UseSelectable = True
        Me.NetTextbox.UseStyleColors = True
        Me.NetTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NetTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(38, 331)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(63, 19)
        Me.MetroLabel12.TabIndex = 201
        Me.MetroLabel12.Text = "Net/Case"
        '
        'WeightTextBox
        '
        Me.WeightTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.WeightTextBox.CustomButton.Image = Nothing
        Me.WeightTextBox.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.WeightTextBox.CustomButton.Name = ""
        Me.WeightTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.WeightTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.WeightTextBox.CustomButton.TabIndex = 1
        Me.WeightTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.WeightTextBox.CustomButton.UseSelectable = True
        Me.WeightTextBox.CustomButton.Visible = False
        Me.WeightTextBox.Enabled = False
        Me.WeightTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.WeightTextBox.Lines = New String(-1) {}
        Me.WeightTextBox.Location = New System.Drawing.Point(111, 274)
        Me.WeightTextBox.MaxLength = 0
        Me.WeightTextBox.Name = "WeightTextBox"
        Me.WeightTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.WeightTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.WeightTextBox.SelectedText = ""
        Me.WeightTextBox.SelectionLength = 0
        Me.WeightTextBox.SelectionStart = 0
        Me.WeightTextBox.ShortcutsEnabled = True
        Me.WeightTextBox.Size = New System.Drawing.Size(147, 40)
        Me.WeightTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.WeightTextBox.TabIndex = 200
        Me.WeightTextBox.UseCustomBackColor = True
        Me.WeightTextBox.UseSelectable = True
        Me.WeightTextBox.UseStyleColors = True
        Me.WeightTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.WeightTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(53, 285)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(50, 19)
        Me.MetroLabel9.TabIndex = 199
        Me.MetroLabel9.Text = "Weight"
        '
        'CaseTextBox
        '
        Me.CaseTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.CaseTextBox.CustomButton.Image = Nothing
        Me.CaseTextBox.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.CaseTextBox.CustomButton.Name = ""
        Me.CaseTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CaseTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CaseTextBox.CustomButton.TabIndex = 1
        Me.CaseTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CaseTextBox.CustomButton.UseSelectable = True
        Me.CaseTextBox.CustomButton.Visible = False
        Me.CaseTextBox.Enabled = False
        Me.CaseTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.CaseTextBox.Lines = New String(-1) {}
        Me.CaseTextBox.Location = New System.Drawing.Point(111, 228)
        Me.CaseTextBox.MaxLength = 0
        Me.CaseTextBox.Name = "CaseTextBox"
        Me.CaseTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CaseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CaseTextBox.SelectedText = ""
        Me.CaseTextBox.SelectionLength = 0
        Me.CaseTextBox.SelectionStart = 0
        Me.CaseTextBox.ShortcutsEnabled = True
        Me.CaseTextBox.Size = New System.Drawing.Size(147, 40)
        Me.CaseTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CaseTextBox.TabIndex = 198
        Me.CaseTextBox.UseCustomBackColor = True
        Me.CaseTextBox.UseSelectable = True
        Me.CaseTextBox.UseStyleColors = True
        Me.CaseTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CaseTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(60, 239)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(42, 19)
        Me.MetroLabel7.TabIndex = 197
        Me.MetroLabel7.Text = "Cases"
        '
        'GradeLabel
        '
        Me.GradeLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.GradeLabel.CustomButton.Image = Nothing
        Me.GradeLabel.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.GradeLabel.CustomButton.Name = ""
        Me.GradeLabel.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.GradeLabel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.GradeLabel.CustomButton.TabIndex = 1
        Me.GradeLabel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.GradeLabel.CustomButton.UseSelectable = True
        Me.GradeLabel.CustomButton.Visible = False
        Me.GradeLabel.Enabled = False
        Me.GradeLabel.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.GradeLabel.Lines = New String(-1) {}
        Me.GradeLabel.Location = New System.Drawing.Point(111, 182)
        Me.GradeLabel.MaxLength = 0
        Me.GradeLabel.Name = "GradeLabel"
        Me.GradeLabel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.GradeLabel.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.GradeLabel.SelectedText = ""
        Me.GradeLabel.SelectionLength = 0
        Me.GradeLabel.SelectionStart = 0
        Me.GradeLabel.ShortcutsEnabled = True
        Me.GradeLabel.Size = New System.Drawing.Size(147, 40)
        Me.GradeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.GradeLabel.TabIndex = 196
        Me.GradeLabel.UseCustomBackColor = True
        Me.GradeLabel.UseSelectable = True
        Me.GradeLabel.UseStyleColors = True
        Me.GradeLabel.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.GradeLabel.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(14, 193)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(86, 19)
        Me.MetroLabel6.TabIndex = 195
        Me.MetroLabel6.Text = "PackedGrade"
        '
        'CustomerLabel
        '
        Me.CustomerLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.CustomerLabel.CustomButton.Image = Nothing
        Me.CustomerLabel.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.CustomerLabel.CustomButton.Name = ""
        Me.CustomerLabel.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CustomerLabel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CustomerLabel.CustomButton.TabIndex = 1
        Me.CustomerLabel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CustomerLabel.CustomButton.UseSelectable = True
        Me.CustomerLabel.CustomButton.Visible = False
        Me.CustomerLabel.Enabled = False
        Me.CustomerLabel.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.CustomerLabel.Lines = New String(-1) {}
        Me.CustomerLabel.Location = New System.Drawing.Point(111, 136)
        Me.CustomerLabel.MaxLength = 0
        Me.CustomerLabel.Name = "CustomerLabel"
        Me.CustomerLabel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CustomerLabel.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CustomerLabel.SelectedText = ""
        Me.CustomerLabel.SelectionLength = 0
        Me.CustomerLabel.SelectionStart = 0
        Me.CustomerLabel.ShortcutsEnabled = True
        Me.CustomerLabel.Size = New System.Drawing.Size(147, 40)
        Me.CustomerLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerLabel.TabIndex = 172
        Me.CustomerLabel.UseCustomBackColor = True
        Me.CustomerLabel.UseSelectable = True
        Me.CustomerLabel.UseStyleColors = True
        Me.CustomerLabel.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CustomerLabel.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(36, 142)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(66, 19)
        Me.MetroLabel8.TabIndex = 171
        Me.MetroLabel8.Text = "Customer"
        '
        'OrderNoCustomerLabel
        '
        Me.OrderNoCustomerLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.OrderNoCustomerLabel.CustomButton.Image = Nothing
        Me.OrderNoCustomerLabel.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.OrderNoCustomerLabel.CustomButton.Name = ""
        Me.OrderNoCustomerLabel.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.OrderNoCustomerLabel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.OrderNoCustomerLabel.CustomButton.TabIndex = 1
        Me.OrderNoCustomerLabel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OrderNoCustomerLabel.CustomButton.UseSelectable = True
        Me.OrderNoCustomerLabel.CustomButton.Visible = False
        Me.OrderNoCustomerLabel.Enabled = False
        Me.OrderNoCustomerLabel.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.OrderNoCustomerLabel.Lines = New String(-1) {}
        Me.OrderNoCustomerLabel.Location = New System.Drawing.Point(111, 44)
        Me.OrderNoCustomerLabel.MaxLength = 0
        Me.OrderNoCustomerLabel.Name = "OrderNoCustomerLabel"
        Me.OrderNoCustomerLabel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.OrderNoCustomerLabel.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.OrderNoCustomerLabel.SelectedText = ""
        Me.OrderNoCustomerLabel.SelectionLength = 0
        Me.OrderNoCustomerLabel.SelectionStart = 0
        Me.OrderNoCustomerLabel.ShortcutsEnabled = True
        Me.OrderNoCustomerLabel.Size = New System.Drawing.Size(147, 40)
        Me.OrderNoCustomerLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.OrderNoCustomerLabel.TabIndex = 170
        Me.OrderNoCustomerLabel.UseCustomBackColor = True
        Me.OrderNoCustomerLabel.UseSelectable = True
        Me.OrderNoCustomerLabel.UseStyleColors = True
        Me.OrderNoCustomerLabel.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.OrderNoCustomerLabel.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(12, 90)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(91, 19)
        Me.MetroLabel10.TabIndex = 169
        Me.MetroLabel10.Text = "Ord detail no."
        '
        'XOrderDetailNoLabel
        '
        Me.XOrderDetailNoLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.XOrderDetailNoLabel.CustomButton.Image = Nothing
        Me.XOrderDetailNoLabel.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.XOrderDetailNoLabel.CustomButton.Name = ""
        Me.XOrderDetailNoLabel.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.XOrderDetailNoLabel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.XOrderDetailNoLabel.CustomButton.TabIndex = 1
        Me.XOrderDetailNoLabel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.XOrderDetailNoLabel.CustomButton.UseSelectable = True
        Me.XOrderDetailNoLabel.CustomButton.Visible = False
        Me.XOrderDetailNoLabel.Enabled = False
        Me.XOrderDetailNoLabel.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.XOrderDetailNoLabel.Lines = New String(-1) {}
        Me.XOrderDetailNoLabel.Location = New System.Drawing.Point(111, 90)
        Me.XOrderDetailNoLabel.MaxLength = 0
        Me.XOrderDetailNoLabel.Name = "XOrderDetailNoLabel"
        Me.XOrderDetailNoLabel.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.XOrderDetailNoLabel.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.XOrderDetailNoLabel.SelectedText = ""
        Me.XOrderDetailNoLabel.SelectionLength = 0
        Me.XOrderDetailNoLabel.SelectionStart = 0
        Me.XOrderDetailNoLabel.ShortcutsEnabled = True
        Me.XOrderDetailNoLabel.Size = New System.Drawing.Size(147, 40)
        Me.XOrderDetailNoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.XOrderDetailNoLabel.TabIndex = 168
        Me.XOrderDetailNoLabel.UseCustomBackColor = True
        Me.XOrderDetailNoLabel.UseSelectable = True
        Me.XOrderDetailNoLabel.UseStyleColors = True
        Me.XOrderDetailNoLabel.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.XOrderDetailNoLabel.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.Location = New System.Drawing.Point(36, 50)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(67, 19)
        Me.MetroLabel11.TabIndex = 167
        Me.MetroLabel11.Text = "Order no."
        '
        'Sp_Shipping_SEL_OrderBindingSource
        '
        Me.Sp_Shipping_SEL_OrderBindingSource.DataMember = "sp_Shipping_SEL_Order"
        Me.Sp_Shipping_SEL_OrderBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_OrderTableAdapter
        '
        Me.Sp_Shipping_SEL_OrderTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'MetroPanel3
        '
        Me.MetroPanel3.Controls.Add(Me.CaseTotalTextBox)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel19)
        Me.MetroPanel3.Controls.Add(Me.WeightTotalTextBox)
        Me.MetroPanel3.Controls.Add(MetroTile3)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel20)
        Me.MetroPanel3.HorizontalScrollbarBarColor = True
        Me.MetroPanel3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.HorizontalScrollbarSize = 10
        Me.MetroPanel3.Location = New System.Drawing.Point(3, 553)
        Me.MetroPanel3.Name = "MetroPanel3"
        Me.MetroPanel3.Size = New System.Drawing.Size(261, 152)
        Me.MetroPanel3.TabIndex = 199
        Me.MetroPanel3.VerticalScrollbarBarColor = True
        Me.MetroPanel3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.VerticalScrollbarSize = 10
        '
        'CaseTotalTextBox
        '
        Me.CaseTotalTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.CaseTotalTextBox.CustomButton.Image = Nothing
        Me.CaseTotalTextBox.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.CaseTotalTextBox.CustomButton.Name = ""
        Me.CaseTotalTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CaseTotalTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CaseTotalTextBox.CustomButton.TabIndex = 1
        Me.CaseTotalTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CaseTotalTextBox.CustomButton.UseSelectable = True
        Me.CaseTotalTextBox.CustomButton.Visible = False
        Me.CaseTotalTextBox.Enabled = False
        Me.CaseTotalTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.CaseTotalTextBox.Lines = New String(-1) {}
        Me.CaseTotalTextBox.Location = New System.Drawing.Point(111, 44)
        Me.CaseTotalTextBox.MaxLength = 0
        Me.CaseTotalTextBox.Name = "CaseTotalTextBox"
        Me.CaseTotalTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CaseTotalTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CaseTotalTextBox.SelectedText = ""
        Me.CaseTotalTextBox.SelectionLength = 0
        Me.CaseTotalTextBox.SelectionStart = 0
        Me.CaseTotalTextBox.ShortcutsEnabled = True
        Me.CaseTotalTextBox.Size = New System.Drawing.Size(147, 40)
        Me.CaseTotalTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CaseTotalTextBox.TabIndex = 170
        Me.CaseTotalTextBox.UseCustomBackColor = True
        Me.CaseTotalTextBox.UseSelectable = True
        Me.CaseTotalTextBox.UseStyleColors = True
        Me.CaseTotalTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CaseTotalTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel19
        '
        Me.MetroLabel19.AutoSize = True
        Me.MetroLabel19.Location = New System.Drawing.Point(20, 100)
        Me.MetroLabel19.Name = "MetroLabel19"
        Me.MetroLabel19.Size = New System.Drawing.Size(80, 19)
        Me.MetroLabel19.TabIndex = 169
        Me.MetroLabel19.Text = "Weight total"
        '
        'WeightTotalTextBox
        '
        Me.WeightTotalTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.WeightTotalTextBox.CustomButton.Image = Nothing
        Me.WeightTotalTextBox.CustomButton.Location = New System.Drawing.Point(109, 2)
        Me.WeightTotalTextBox.CustomButton.Name = ""
        Me.WeightTotalTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.WeightTotalTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.WeightTotalTextBox.CustomButton.TabIndex = 1
        Me.WeightTotalTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.WeightTotalTextBox.CustomButton.UseSelectable = True
        Me.WeightTotalTextBox.CustomButton.Visible = False
        Me.WeightTotalTextBox.Enabled = False
        Me.WeightTotalTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.WeightTotalTextBox.Lines = New String(-1) {}
        Me.WeightTotalTextBox.Location = New System.Drawing.Point(111, 90)
        Me.WeightTotalTextBox.MaxLength = 0
        Me.WeightTotalTextBox.Name = "WeightTotalTextBox"
        Me.WeightTotalTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.WeightTotalTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.WeightTotalTextBox.SelectedText = ""
        Me.WeightTotalTextBox.SelectionLength = 0
        Me.WeightTotalTextBox.SelectionStart = 0
        Me.WeightTotalTextBox.ShortcutsEnabled = True
        Me.WeightTotalTextBox.Size = New System.Drawing.Size(147, 40)
        Me.WeightTotalTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.WeightTotalTextBox.TabIndex = 168
        Me.WeightTotalTextBox.UseCustomBackColor = True
        Me.WeightTotalTextBox.UseSelectable = True
        Me.WeightTotalTextBox.UseStyleColors = True
        Me.WeightTotalTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.WeightTotalTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel20
        '
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.Location = New System.Drawing.Point(30, 55)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(72, 19)
        Me.MetroLabel20.TabIndex = 167
        Me.MetroLabel20.Text = "Cases total"
        '
        'FrmOrder
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1213, 740)
        Me.Controls.Add(Me.MetroPanel3)
        Me.Controls.Add(Me.MetroPanel2)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmOrder"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Order"
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.SpShippingSELShippingOrderByCropBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel2.ResumeLayout(False)
        Me.MetroPanel2.PerformLayout()
        CType(Me.Sp_Shipping_SEL_OrderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel3.ResumeLayout(False)
        Me.MetroPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents SpShippingSELShippingOrderByCropBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByCropTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByCropTableAdapter
    Friend WithEvents StatusCombobox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents SpShippingSELShippingOrderBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter
    Friend WithEvents DeleteTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FinishedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents finishLable As MetroFramework.Controls.MetroLabel
    Friend WithEvents UnfinishedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents UnfinishedLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShowStockTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CustomerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PackedGradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerGradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MetroPanel2 As MetroFramework.Controls.MetroPanel
    Friend WithEvents CustomerLabel As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents OrderNoCustomerLabel As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents XOrderDetailNoLabel As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_OrderBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_OrderTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_OrderTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents GradeLabel As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents NetTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents WeightTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CaseTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel3 As MetroFramework.Controls.MetroPanel
    Friend WithEvents CaseTotalTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel19 As MetroFramework.Controls.MetroLabel
    Friend WithEvents WeightTotalTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents OrderNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SINoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents InvNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceivedDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalWeightRequestDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalCaseRequestDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BLNo As DataGridViewTextBoxColumn
    Friend WithEvents StatusName As DataGridViewTextBoxColumn
    Friend WithEvents Status As DataGridViewCheckBoxColumn
    Friend WithEvents ShippedDate As DataGridViewTextBoxColumn
    Friend WithEvents PackedGrade As DataGridViewTextBoxColumn
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShowBarcodeListMetroTile As MetroFramework.Controls.MetroTile
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmShowPackedGradeDetail
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.SpShippingSELPackedgradeByGradersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.CaseNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.GradersLabel = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter()
        Me.GradersDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BarcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CasenoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetRealDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductionTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToCustomerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Warehouse = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bay = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementStatusName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullMigration = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.pdremark = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PdRemarkParent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELPackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GradersDataGridViewTextBoxColumn, Me.GradeDataGridViewTextBoxColumn, Me.BarcodeDataGridViewTextBoxColumn, Me.CasenoDataGridViewTextBoxColumn, Me.NetDataGridViewTextBoxColumn, Me.NetRealDataGridViewTextBoxColumn, Me.ProductionTypeDataGridViewTextBoxColumn, Me.ToCustomerDataGridViewTextBoxColumn, Me.Warehouse, Me.bay, Me.MovementStatusName, Me.FullMigration, Me.pdremark, Me.PdRemarkParent})
        Me.MetroGrid.DataSource = Me.SpShippingSELPackedgradeByGradersBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(1, 114)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 28
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(1269, 601)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 199
        '
        'SpShippingSELPackedgradeByGradersBindingSource
        '
        Me.SpShippingSELPackedgradeByGradersBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByGraders"
        Me.SpShippingSELPackedgradeByGradersBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.CaseNoLabel)
        Me.MetroPanel1.Controls.Add(Me.GradersLabel)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(1, 46)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1269, 62)
        Me.MetroPanel1.TabIndex = 198
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MetroLabel2.Location = New System.Drawing.Point(285, 23)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(116, 25)
        Me.MetroLabel2.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel2.TabIndex = 129
        Me.MetroLabel2.Text = "Packed Grade"
        Me.MetroLabel2.UseCustomForeColor = True
        '
        'CaseNoLabel
        '
        Me.CaseNoLabel.AutoSize = True
        Me.CaseNoLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.CaseNoLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CaseNoLabel.Location = New System.Drawing.Point(687, 23)
        Me.CaseNoLabel.Name = "CaseNoLabel"
        Me.CaseNoLabel.Size = New System.Drawing.Size(89, 25)
        Me.CaseNoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CaseNoLabel.TabIndex = 128
        Me.CaseNoLabel.Text = "Username"
        Me.CaseNoLabel.UseCustomForeColor = True
        '
        'GradersLabel
        '
        Me.GradersLabel.AutoSize = True
        Me.GradersLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.GradersLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.GradersLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GradersLabel.Location = New System.Drawing.Point(412, 23)
        Me.GradersLabel.Name = "GradersLabel"
        Me.GradersLabel.Size = New System.Drawing.Size(97, 25)
        Me.GradersLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.GradersLabel.TabIndex = 127
        Me.GradersLabel.Text = "Username"
        Me.GradersLabel.UseCustomForeColor = True
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(1002, 13)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 126
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(1044, 23)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel1.TabIndex = 118
        Me.MetroLabel1.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1189, 7)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'Sp_Shipping_SEL_PackedgradeByGradersTableAdapter
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.ClearBeforeFill = True
        '
        'GradersDataGridViewTextBoxColumn
        '
        Me.GradersDataGridViewTextBoxColumn.DataPropertyName = "Graders"
        Me.GradersDataGridViewTextBoxColumn.HeaderText = "Graders"
        Me.GradersDataGridViewTextBoxColumn.Name = "GradersDataGridViewTextBoxColumn"
        Me.GradersDataGridViewTextBoxColumn.ReadOnly = True
        Me.GradersDataGridViewTextBoxColumn.Width = 83
        '
        'GradeDataGridViewTextBoxColumn
        '
        Me.GradeDataGridViewTextBoxColumn.DataPropertyName = "Grade"
        Me.GradeDataGridViewTextBoxColumn.HeaderText = "Grade"
        Me.GradeDataGridViewTextBoxColumn.Name = "GradeDataGridViewTextBoxColumn"
        Me.GradeDataGridViewTextBoxColumn.ReadOnly = True
        Me.GradeDataGridViewTextBoxColumn.Width = 71
        '
        'BarcodeDataGridViewTextBoxColumn
        '
        Me.BarcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode"
        Me.BarcodeDataGridViewTextBoxColumn.HeaderText = "Barcode"
        Me.BarcodeDataGridViewTextBoxColumn.Name = "BarcodeDataGridViewTextBoxColumn"
        Me.BarcodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.BarcodeDataGridViewTextBoxColumn.Width = 85
        '
        'CasenoDataGridViewTextBoxColumn
        '
        Me.CasenoDataGridViewTextBoxColumn.DataPropertyName = "Caseno"
        Me.CasenoDataGridViewTextBoxColumn.HeaderText = "Caseno"
        Me.CasenoDataGridViewTextBoxColumn.Name = "CasenoDataGridViewTextBoxColumn"
        Me.CasenoDataGridViewTextBoxColumn.ReadOnly = True
        Me.CasenoDataGridViewTextBoxColumn.Width = 80
        '
        'NetDataGridViewTextBoxColumn
        '
        Me.NetDataGridViewTextBoxColumn.DataPropertyName = "Net"
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.NetDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.NetDataGridViewTextBoxColumn.HeaderText = "Net"
        Me.NetDataGridViewTextBoxColumn.Name = "NetDataGridViewTextBoxColumn"
        Me.NetDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetDataGridViewTextBoxColumn.Width = 55
        '
        'NetRealDataGridViewTextBoxColumn
        '
        Me.NetRealDataGridViewTextBoxColumn.DataPropertyName = "NetReal"
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.NetRealDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.NetRealDataGridViewTextBoxColumn.HeaderText = "NetReal"
        Me.NetRealDataGridViewTextBoxColumn.Name = "NetRealDataGridViewTextBoxColumn"
        Me.NetRealDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetRealDataGridViewTextBoxColumn.Width = 84
        '
        'ProductionTypeDataGridViewTextBoxColumn
        '
        Me.ProductionTypeDataGridViewTextBoxColumn.DataPropertyName = "ProductionType"
        Me.ProductionTypeDataGridViewTextBoxColumn.HeaderText = "Pro.Type"
        Me.ProductionTypeDataGridViewTextBoxColumn.Name = "ProductionTypeDataGridViewTextBoxColumn"
        Me.ProductionTypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.ProductionTypeDataGridViewTextBoxColumn.Width = 86
        '
        'ToCustomerDataGridViewTextBoxColumn
        '
        Me.ToCustomerDataGridViewTextBoxColumn.DataPropertyName = "ToCustomer"
        Me.ToCustomerDataGridViewTextBoxColumn.HeaderText = "ToCustomer"
        Me.ToCustomerDataGridViewTextBoxColumn.Name = "ToCustomerDataGridViewTextBoxColumn"
        Me.ToCustomerDataGridViewTextBoxColumn.ReadOnly = True
        Me.ToCustomerDataGridViewTextBoxColumn.Width = 109
        '
        'Warehouse
        '
        Me.Warehouse.DataPropertyName = "Warehouse"
        Me.Warehouse.HeaderText = "Warehouse"
        Me.Warehouse.Name = "Warehouse"
        Me.Warehouse.ReadOnly = True
        Me.Warehouse.Width = 105
        '
        'bay
        '
        Me.bay.DataPropertyName = "bay"
        Me.bay.HeaderText = "bay"
        Me.bay.Name = "bay"
        Me.bay.ReadOnly = True
        Me.bay.Width = 55
        '
        'MovementStatusName
        '
        Me.MovementStatusName.DataPropertyName = "MovementStatusName"
        Me.MovementStatusName.HeaderText = "Status"
        Me.MovementStatusName.Name = "MovementStatusName"
        Me.MovementStatusName.ReadOnly = True
        Me.MovementStatusName.Width = 72
        '
        'FullMigration
        '
        Me.FullMigration.DataPropertyName = "FullMigration"
        Me.FullMigration.HeaderText = "FullMigration"
        Me.FullMigration.Name = "FullMigration"
        Me.FullMigration.ReadOnly = True
        Me.FullMigration.Width = 99
        '
        'pdremark
        '
        Me.pdremark.DataPropertyName = "pdremark"
        Me.pdremark.HeaderText = "pdremark"
        Me.pdremark.Name = "pdremark"
        Me.pdremark.ReadOnly = True
        Me.pdremark.Width = 93
        '
        'PdRemarkParent
        '
        Me.PdRemarkParent.DataPropertyName = "PdRemarkParent"
        Me.PdRemarkParent.HeaderText = "PdRemarkParent"
        Me.PdRemarkParent.Name = "PdRemarkParent"
        Me.PdRemarkParent.ReadOnly = True
        Me.PdRemarkParent.Width = 139
        '
        'FrmShowPackedGradeDetail
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1352, 773)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmShowPackedGradeDetail"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELPackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SpShippingSELPackedgradeByGradersBindingSource As BindingSource
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter
    Friend WithEvents GradersLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CaseNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents GradersDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BarcodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CasenoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetRealDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProductionTypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ToCustomerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Warehouse As DataGridViewTextBoxColumn
    Friend WithEvents bay As DataGridViewTextBoxColumn
    Friend WithEvents MovementStatusName As DataGridViewTextBoxColumn
    Friend WithEvents FullMigration As DataGridViewCheckBoxColumn
    Friend WithEvents pdremark As DataGridViewTextBoxColumn
    Friend WithEvents PdRemarkParent As DataGridViewTextBoxColumn
End Class

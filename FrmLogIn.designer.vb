﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmLogIn
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ClearButton = New MetroFramework.Controls.MetroButton()
        Me.LoginButton = New MetroFramework.Controls.MetroButton()
        Me.PasswordTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.UsernameTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.ReceivingDataSet = New ShippingSystem.ShippingDataSet()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_DecodePasswordBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_DecodePasswordTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClearButton
        '
        Me.ClearButton.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.ClearButton.Location = New System.Drawing.Point(156, 3)
        Me.ClearButton.Name = "ClearButton"
        Me.ClearButton.Size = New System.Drawing.Size(130, 32)
        Me.ClearButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.ClearButton.TabIndex = 13
        Me.ClearButton.Text = "Clear"
        Me.ClearButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.ClearButton.UseSelectable = True
        Me.ClearButton.UseStyleColors = True
        '
        'LoginButton
        '
        Me.LoginButton.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.LoginButton.Location = New System.Drawing.Point(0, 3)
        Me.LoginButton.Margin = New System.Windows.Forms.Padding(0, 3, 3, 3)
        Me.LoginButton.Name = "LoginButton"
        Me.LoginButton.Size = New System.Drawing.Size(150, 32)
        Me.LoginButton.Style = MetroFramework.MetroColorStyle.Lime
        Me.LoginButton.TabIndex = 12
        Me.LoginButton.Text = "Login"
        Me.LoginButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LoginButton.UseSelectable = True
        Me.LoginButton.UseStyleColors = True
        '
        'PasswordTextBox
        '
        '
        '
        '
        Me.PasswordTextBox.CustomButton.Image = Nothing
        Me.PasswordTextBox.CustomButton.Location = New System.Drawing.Point(256, 2)
        Me.PasswordTextBox.CustomButton.Name = ""
        Me.PasswordTextBox.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.PasswordTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.PasswordTextBox.CustomButton.TabIndex = 1
        Me.PasswordTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.PasswordTextBox.CustomButton.UseSelectable = True
        Me.PasswordTextBox.CustomButton.Visible = False
        Me.PasswordTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.PasswordTextBox.FontWeight = MetroFramework.MetroTextBoxWeight.Bold
        Me.PasswordTextBox.Lines = New String(-1) {}
        Me.PasswordTextBox.Location = New System.Drawing.Point(3, 100)
        Me.PasswordTextBox.MaxLength = 32767
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.PasswordTextBox.PromptText = "Password"
        Me.PasswordTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.PasswordTextBox.SelectedText = ""
        Me.PasswordTextBox.SelectionLength = 0
        Me.PasswordTextBox.SelectionStart = 0
        Me.PasswordTextBox.ShortcutsEnabled = True
        Me.PasswordTextBox.Size = New System.Drawing.Size(286, 32)
        Me.PasswordTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.PasswordTextBox.TabIndex = 9
        Me.PasswordTextBox.UseSelectable = True
        Me.PasswordTextBox.UseStyleColors = True
        Me.PasswordTextBox.UseSystemPasswordChar = True
        Me.PasswordTextBox.WaterMark = "Password"
        Me.PasswordTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.PasswordTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'UsernameTextBox
        '
        Me.UsernameTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.UsernameTextBox.CustomButton.Image = Nothing
        Me.UsernameTextBox.CustomButton.Location = New System.Drawing.Point(256, 2)
        Me.UsernameTextBox.CustomButton.Name = ""
        Me.UsernameTextBox.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.UsernameTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.UsernameTextBox.CustomButton.TabIndex = 1
        Me.UsernameTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.UsernameTextBox.CustomButton.UseSelectable = True
        Me.UsernameTextBox.CustomButton.Visible = False
        Me.UsernameTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.UsernameTextBox.FontWeight = MetroFramework.MetroTextBoxWeight.Bold
        Me.UsernameTextBox.Lines = New String(-1) {}
        Me.UsernameTextBox.Location = New System.Drawing.Point(3, 62)
        Me.UsernameTextBox.MaxLength = 32767
        Me.UsernameTextBox.Name = "UsernameTextBox"
        Me.UsernameTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.UsernameTextBox.PromptText = "Username"
        Me.UsernameTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.UsernameTextBox.SelectedText = ""
        Me.UsernameTextBox.SelectionLength = 0
        Me.UsernameTextBox.SelectionStart = 0
        Me.UsernameTextBox.ShortcutsEnabled = True
        Me.UsernameTextBox.Size = New System.Drawing.Size(286, 32)
        Me.UsernameTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameTextBox.TabIndex = 8
        Me.UsernameTextBox.UseSelectable = True
        Me.UsernameTextBox.UseStyleColors = True
        Me.UsernameTextBox.WaterMark = "Username"
        Me.UsernameTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.UsernameTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ReceivingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_DecodePasswordBindingSource
        '
        Me.Sp_Receiving_DecodePasswordBindingSource.DataMember = "sp_Receiving_DecodePassword"
        Me.Sp_Receiving_DecodePasswordBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_DecodePasswordTableAdapter
        '
        Me.Sp_Receiving_DecodePasswordTableAdapter.ClearBeforeFill = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel3.ForeColor = System.Drawing.Color.SaddleBrown
        Me.MetroLabel3.Location = New System.Drawing.Point(0, 3)
        Me.MetroLabel3.Margin = New System.Windows.Forms.Padding(0, 3, 3, 3)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(153, 25)
        Me.MetroLabel3.TabIndex = 14
        Me.MetroLabel3.Text = "Shipping System"
        Me.MetroLabel3.UseCustomForeColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel3)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.UsernameTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.PasswordTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(424, 269)
        Me.FlowLayoutPanel1.TabIndex = 133
        Me.FlowLayoutPanel1.WrapContents = False
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel1.ForeColor = System.Drawing.Color.SaddleBrown
        Me.MetroLabel1.Location = New System.Drawing.Point(0, 34)
        Me.MetroLabel1.Margin = New System.Windows.Forms.Padding(0, 3, 3, 10)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(83, 15)
        Me.MetroLabel1.TabIndex = 135
        Me.MetroLabel1.Text = "Version 2023-1"
        Me.MetroLabel1.UseCustomForeColor = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.LoginButton)
        Me.FlowLayoutPanel2.Controls.Add(Me.ClearButton)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 138)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(289, 38)
        Me.FlowLayoutPanel2.TabIndex = 134
        '
        'FrmLogIn
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(464, 349)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.MaximizeBox = False
        Me.Name = "FrmLogIn"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Login"
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ClearButton As MetroFramework.Controls.MetroButton
    Friend WithEvents LoginButton As MetroFramework.Controls.MetroButton
    Friend WithEvents PasswordTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents UsernameTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents ReceivingDataSet As ShippingSystem.ShippingDataSet
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As ShippingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_DecodePasswordBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_DecodePasswordTableAdapter As ShippingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
End Class

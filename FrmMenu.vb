﻿Public Class FrmMenu
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private Sub FrmMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.security' table. You can move, or remove it, as needed.
        Try
            UsernameMetroLabel.Text = XUsername
            Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub SettingTile_Click(sender As Object, e As EventArgs)
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim securityRow As ShippingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
            securityRow = Me.ShippingDataSet.security.FindByuname(XUsernameApprove)
            If securityRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not securityRow Is Nothing Then

                If securityRow.uname = XUsernameApprove And securityRow.level = "Admin" Then
                    Dim frm As New FrmAdminSetup
                    frm.ShowDialog()
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้หากต้องการเพิ่มสิทธิ์นี้กรุณาติดต่อ Admin(แผนก IT)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If


        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BtnReceived_Click(sender As Object, e As EventArgs) Handles BtnReceived.Click
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim baleBarcodeRow As ShippingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
            baleBarcodeRow = Me.ShippingDataSet.security.FindByuname(XUsernameApprove)
            If baleBarcodeRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not baleBarcodeRow Is Nothing Then
                If baleBarcodeRow.uname = XUsernameApprove And (baleBarcodeRow.depart = "Shipping" Or baleBarcodeRow.depart = "Processing" Or baleBarcodeRow.level = "Admin" Or baleBarcodeRow.depart = "Leaf Accounting") Then
                    Dim frm As New FrmReceived
                    frm.ShowDialog()
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้หากต้องการเพิ่มสิทธิ์นี้กรุณาติดต่อ Admin(แผนก IT)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BtnOrder_Click(sender As Object, e As EventArgs) Handles BtnOrder.Click
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim securityRow As ShippingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
            securityRow = Me.ShippingDataSet.security.FindByuname(XUsernameApprove)
            If securityRow Is Nothing Then
                MessageBox.Show("ไม่พบ Username นี้ในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not securityRow Is Nothing Then
                If securityRow.uname = XUsernameApprove And
                (securityRow.pdtransfer = True Or
                (securityRow.shipping = True And
                securityRow.level = "Supervisor") Or
                securityRow.level = "Admin" Or
                securityRow.depart = "Leaf Accounting") Then
                    Dim frm As New FrmOrderMaster
                    frm.ShowDialog()
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้หากต้องการเพิ่มสิทธิ์นี้กรุณาติดต่อ Admin(แผนก IT)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BtnBayControl_Click(sender As Object, e As EventArgs) Handles BtnBayControl.Click
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim securityRow As ShippingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
            securityRow = Me.ShippingDataSet.security.FindByuname(XUsernameApprove)
            If securityRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not securityRow Is Nothing Then
                If securityRow.uname = XUsernameApprove And (securityRow.depart = "Shipping" Or securityRow.level = "Admin") Then
                    Dim frm As New FrmBayControl
                    frm.ShowDialog()
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้หากต้องการเพิ่มสิทธิ์นี้กรุณาติดต่อ Admin(แผนก IT)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If


        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BtnMovement_Click(sender As Object, e As EventArgs) Handles BtnMovement.Click
        Try
            Dim frm As New FrmMovement
            frm.ShowDialog()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FrmMenu_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub

    Private Sub ShippingButton_Click(sender As Object, e As EventArgs) Handles SettingButton.Click, ShippingButton.Click
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim securityRow As ShippingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
            securityRow = Me.ShippingDataSet.security.FindByuname(XUsernameApprove)
            If securityRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not securityRow Is Nothing Then
                If securityRow.uname = XUsernameApprove And (securityRow.depart = "Shipping" Or securityRow.level = "Admin" Or securityRow.depart = "Leaf Accounting") Then
                    Dim frm As New FrmShipping
                    frm.ShowDialog()
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้หากต้องการเพิ่มสิทธิ์นี้กรุณาติดต่อ Admin(แผนก IT)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
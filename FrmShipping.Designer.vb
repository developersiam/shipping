﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmShipping
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.ShippedTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.StatusCombobox = New MetroFramework.Controls.MetroComboBox()
        Me.SpShippingSELShippingOrderByCropBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.OrderNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceivedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Customer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PackedGrade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerGrade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalWeightRequest = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalCaseRequest = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ShippingContainer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ShippedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SINoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalWeightRequestDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalCaseRequestDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StuffDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BLNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MOPIDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ETDDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ETADateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BLdateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceivedUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinishedUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.StatusNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ShippedUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CropDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContainerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELShippingOrderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingOrderByCropTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByCropTableAdapter()
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.SpShippingSELShippingOrderByCropBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.ShippedTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 63)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1169, 47)
        Me.MetroPanel1.TabIndex = 187
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'ShippedTile
        '
        Me.ShippedTile.ActiveControl = Nothing
        Me.ShippedTile.AutoSize = True
        Me.ShippedTile.BackColor = System.Drawing.Color.White
        Me.ShippedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ShippedTile.Location = New System.Drawing.Point(821, 9)
        Me.ShippedTile.Name = "ShippedTile"
        Me.ShippedTile.Size = New System.Drawing.Size(58, 35)
        Me.ShippedTile.Style = MetroFramework.MetroColorStyle.White
        Me.ShippedTile.TabIndex = 130
        Me.ShippedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShippedTile.TileImage = Global.ShippingSystem.My.Resources.Resources.InTransit321
        Me.ShippedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShippedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ShippedTile.UseSelectable = True
        Me.ShippedTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(874, 17)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel2.TabIndex = 129
        Me.MetroLabel2.Text = "shipped."
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(963, 9)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 126
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(1005, 17)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel1.TabIndex = 118
        Me.MetroLabel1.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1119, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 39)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(54, 38)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(63, 17)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(5, 129)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(55, 25)
        Me.MetroLabel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel3.TabIndex = 199
        Me.MetroLabel3.Text = "status"
        '
        'StatusCombobox
        '
        Me.StatusCombobox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.StatusCombobox.FormattingEnabled = True
        Me.StatusCombobox.ItemHeight = 29
        Me.StatusCombobox.Location = New System.Drawing.Point(68, 120)
        Me.StatusCombobox.Name = "StatusCombobox"
        Me.StatusCombobox.Size = New System.Drawing.Size(146, 35)
        Me.StatusCombobox.Style = MetroFramework.MetroColorStyle.Orange
        Me.StatusCombobox.TabIndex = 197
        Me.StatusCombobox.UseSelectable = True
        '
        'SpShippingSELShippingOrderByCropBindingSource
        '
        Me.SpShippingSELShippingOrderByCropBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrderByCrop"
        Me.SpShippingSELShippingOrderByCropBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OrderNo, Me.InvNoDataGridViewTextBoxColumn, Me.ReceivedDate, Me.Customer, Me.PackedGrade, Me.CustomerGrade, Me.TotalWeightRequest, Me.TotalCaseRequest, Me.ShippingContainer, Me.StatusName, Me.ShippedDate, Me.SINoDataGridViewTextBoxColumn, Me.TotalWeightRequestDataGridViewTextBoxColumn, Me.TotalCaseRequestDataGridViewTextBoxColumn, Me.StuffDateDataGridViewTextBoxColumn, Me.BLNoDataGridViewTextBoxColumn, Me.MOPIDataGridViewTextBoxColumn, Me.ETDDateDataGridViewTextBoxColumn, Me.ETADateDataGridViewTextBoxColumn, Me.BLdateDataGridViewTextBoxColumn, Me.ReceivedUserDataGridViewTextBoxColumn, Me.FinishedUserDataGridViewTextBoxColumn, Me.StatusDataGridViewCheckBoxColumn, Me.StatusNameDataGridViewTextBoxColumn, Me.ShippedUserDataGridViewTextBoxColumn, Me.CropDataGridViewTextBoxColumn, Me.ContainerDataGridViewTextBoxColumn})
        Me.MetroGrid.DataSource = Me.SpShippingSELShippingOrderBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(3, 161)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 28
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(1168, 558)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 195
        '
        'OrderNo
        '
        Me.OrderNo.DataPropertyName = "OrderNo"
        Me.OrderNo.HeaderText = "OrderNo"
        Me.OrderNo.Name = "OrderNo"
        Me.OrderNo.ReadOnly = True
        Me.OrderNo.Width = 87
        '
        'InvNoDataGridViewTextBoxColumn
        '
        Me.InvNoDataGridViewTextBoxColumn.DataPropertyName = "InvNo"
        Me.InvNoDataGridViewTextBoxColumn.HeaderText = "Invoice No"
        Me.InvNoDataGridViewTextBoxColumn.Name = "InvNoDataGridViewTextBoxColumn"
        Me.InvNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ReceivedDate
        '
        Me.ReceivedDate.DataPropertyName = "ReceivedDate"
        Me.ReceivedDate.HeaderText = "ReceivedDate"
        Me.ReceivedDate.Name = "ReceivedDate"
        Me.ReceivedDate.ReadOnly = True
        Me.ReceivedDate.Width = 122
        '
        'Customer
        '
        Me.Customer.DataPropertyName = "Customer"
        Me.Customer.HeaderText = "Customer"
        Me.Customer.Name = "Customer"
        Me.Customer.ReadOnly = True
        Me.Customer.Width = 94
        '
        'PackedGrade
        '
        Me.PackedGrade.DataPropertyName = "PackedGrade"
        Me.PackedGrade.HeaderText = "PackedGrade"
        Me.PackedGrade.Name = "PackedGrade"
        Me.PackedGrade.ReadOnly = True
        Me.PackedGrade.Width = 116
        '
        'CustomerGrade
        '
        Me.CustomerGrade.DataPropertyName = "CustomerGrade"
        Me.CustomerGrade.HeaderText = "CustomerGrade"
        Me.CustomerGrade.Name = "CustomerGrade"
        Me.CustomerGrade.ReadOnly = True
        Me.CustomerGrade.Width = 133
        '
        'TotalWeightRequest
        '
        Me.TotalWeightRequest.DataPropertyName = "TotalWeightRequest"
        DataGridViewCellStyle3.Format = "N1"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.TotalWeightRequest.DefaultCellStyle = DataGridViewCellStyle3
        Me.TotalWeightRequest.HeaderText = "Weight"
        Me.TotalWeightRequest.Name = "TotalWeightRequest"
        Me.TotalWeightRequest.ReadOnly = True
        Me.TotalWeightRequest.Width = 78
        '
        'TotalCaseRequest
        '
        Me.TotalCaseRequest.DataPropertyName = "TotalCaseRequest"
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TotalCaseRequest.DefaultCellStyle = DataGridViewCellStyle4
        Me.TotalCaseRequest.HeaderText = "Case"
        Me.TotalCaseRequest.Name = "TotalCaseRequest"
        Me.TotalCaseRequest.ReadOnly = True
        Me.TotalCaseRequest.Width = 64
        '
        'ShippingContainer
        '
        Me.ShippingContainer.DataPropertyName = "Container"
        Me.ShippingContainer.HeaderText = "Container"
        Me.ShippingContainer.Name = "ShippingContainer"
        Me.ShippingContainer.ReadOnly = True
        Me.ShippingContainer.Width = 95
        '
        'StatusName
        '
        Me.StatusName.DataPropertyName = "StatusName"
        Me.StatusName.HeaderText = "Status"
        Me.StatusName.Name = "StatusName"
        Me.StatusName.ReadOnly = True
        Me.StatusName.Width = 72
        '
        'ShippedDate
        '
        Me.ShippedDate.DataPropertyName = "ShippedDate"
        Me.ShippedDate.HeaderText = "ShippedDate"
        Me.ShippedDate.Name = "ShippedDate"
        Me.ShippedDate.ReadOnly = True
        Me.ShippedDate.Width = 114
        '
        'SINoDataGridViewTextBoxColumn
        '
        Me.SINoDataGridViewTextBoxColumn.DataPropertyName = "SINo"
        Me.SINoDataGridViewTextBoxColumn.HeaderText = "SINo"
        Me.SINoDataGridViewTextBoxColumn.Name = "SINoDataGridViewTextBoxColumn"
        Me.SINoDataGridViewTextBoxColumn.ReadOnly = True
        Me.SINoDataGridViewTextBoxColumn.Width = 61
        '
        'TotalWeightRequestDataGridViewTextBoxColumn
        '
        Me.TotalWeightRequestDataGridViewTextBoxColumn.DataPropertyName = "TotalWeightRequest"
        Me.TotalWeightRequestDataGridViewTextBoxColumn.HeaderText = "TotalWeightRequest"
        Me.TotalWeightRequestDataGridViewTextBoxColumn.Name = "TotalWeightRequestDataGridViewTextBoxColumn"
        Me.TotalWeightRequestDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotalWeightRequestDataGridViewTextBoxColumn.Width = 163
        '
        'TotalCaseRequestDataGridViewTextBoxColumn
        '
        Me.TotalCaseRequestDataGridViewTextBoxColumn.DataPropertyName = "TotalCaseRequest"
        Me.TotalCaseRequestDataGridViewTextBoxColumn.HeaderText = "TotalCaseRequest"
        Me.TotalCaseRequestDataGridViewTextBoxColumn.Name = "TotalCaseRequestDataGridViewTextBoxColumn"
        Me.TotalCaseRequestDataGridViewTextBoxColumn.ReadOnly = True
        Me.TotalCaseRequestDataGridViewTextBoxColumn.Width = 149
        '
        'StuffDateDataGridViewTextBoxColumn
        '
        Me.StuffDateDataGridViewTextBoxColumn.DataPropertyName = "StuffDate"
        Me.StuffDateDataGridViewTextBoxColumn.HeaderText = "StuffDate"
        Me.StuffDateDataGridViewTextBoxColumn.Name = "StuffDateDataGridViewTextBoxColumn"
        Me.StuffDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.StuffDateDataGridViewTextBoxColumn.Width = 92
        '
        'BLNoDataGridViewTextBoxColumn
        '
        Me.BLNoDataGridViewTextBoxColumn.DataPropertyName = "BLNo"
        Me.BLNoDataGridViewTextBoxColumn.HeaderText = "BLNo"
        Me.BLNoDataGridViewTextBoxColumn.Name = "BLNoDataGridViewTextBoxColumn"
        Me.BLNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.BLNoDataGridViewTextBoxColumn.Width = 66
        '
        'MOPIDataGridViewTextBoxColumn
        '
        Me.MOPIDataGridViewTextBoxColumn.DataPropertyName = "MOPI"
        Me.MOPIDataGridViewTextBoxColumn.HeaderText = "MOPI"
        Me.MOPIDataGridViewTextBoxColumn.Name = "MOPIDataGridViewTextBoxColumn"
        Me.MOPIDataGridViewTextBoxColumn.ReadOnly = True
        Me.MOPIDataGridViewTextBoxColumn.Width = 68
        '
        'ETDDateDataGridViewTextBoxColumn
        '
        Me.ETDDateDataGridViewTextBoxColumn.DataPropertyName = "ETDDate"
        Me.ETDDateDataGridViewTextBoxColumn.HeaderText = "ETDDate"
        Me.ETDDateDataGridViewTextBoxColumn.Name = "ETDDateDataGridViewTextBoxColumn"
        Me.ETDDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.ETDDateDataGridViewTextBoxColumn.Width = 89
        '
        'ETADateDataGridViewTextBoxColumn
        '
        Me.ETADateDataGridViewTextBoxColumn.DataPropertyName = "ETADate"
        Me.ETADateDataGridViewTextBoxColumn.HeaderText = "ETADate"
        Me.ETADateDataGridViewTextBoxColumn.Name = "ETADateDataGridViewTextBoxColumn"
        Me.ETADateDataGridViewTextBoxColumn.ReadOnly = True
        Me.ETADateDataGridViewTextBoxColumn.Width = 87
        '
        'BLdateDataGridViewTextBoxColumn
        '
        Me.BLdateDataGridViewTextBoxColumn.DataPropertyName = "BLdate"
        Me.BLdateDataGridViewTextBoxColumn.HeaderText = "BLdate"
        Me.BLdateDataGridViewTextBoxColumn.Name = "BLdateDataGridViewTextBoxColumn"
        Me.BLdateDataGridViewTextBoxColumn.ReadOnly = True
        Me.BLdateDataGridViewTextBoxColumn.Width = 77
        '
        'ReceivedUserDataGridViewTextBoxColumn
        '
        Me.ReceivedUserDataGridViewTextBoxColumn.DataPropertyName = "ReceivedUser"
        Me.ReceivedUserDataGridViewTextBoxColumn.HeaderText = "ReceivedUser"
        Me.ReceivedUserDataGridViewTextBoxColumn.Name = "ReceivedUserDataGridViewTextBoxColumn"
        Me.ReceivedUserDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReceivedUserDataGridViewTextBoxColumn.Width = 121
        '
        'FinishedUserDataGridViewTextBoxColumn
        '
        Me.FinishedUserDataGridViewTextBoxColumn.DataPropertyName = "FinishedUser"
        Me.FinishedUserDataGridViewTextBoxColumn.HeaderText = "FinishedUser"
        Me.FinishedUserDataGridViewTextBoxColumn.Name = "FinishedUserDataGridViewTextBoxColumn"
        Me.FinishedUserDataGridViewTextBoxColumn.ReadOnly = True
        Me.FinishedUserDataGridViewTextBoxColumn.Width = 116
        '
        'StatusDataGridViewCheckBoxColumn
        '
        Me.StatusDataGridViewCheckBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewCheckBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewCheckBoxColumn.Name = "StatusDataGridViewCheckBoxColumn"
        Me.StatusDataGridViewCheckBoxColumn.ReadOnly = True
        Me.StatusDataGridViewCheckBoxColumn.Width = 53
        '
        'StatusNameDataGridViewTextBoxColumn
        '
        Me.StatusNameDataGridViewTextBoxColumn.DataPropertyName = "StatusName"
        Me.StatusNameDataGridViewTextBoxColumn.HeaderText = "StatusName"
        Me.StatusNameDataGridViewTextBoxColumn.Name = "StatusNameDataGridViewTextBoxColumn"
        Me.StatusNameDataGridViewTextBoxColumn.ReadOnly = True
        Me.StatusNameDataGridViewTextBoxColumn.Width = 110
        '
        'ShippedUserDataGridViewTextBoxColumn
        '
        Me.ShippedUserDataGridViewTextBoxColumn.DataPropertyName = "ShippedUser"
        Me.ShippedUserDataGridViewTextBoxColumn.HeaderText = "ShippedUser"
        Me.ShippedUserDataGridViewTextBoxColumn.Name = "ShippedUserDataGridViewTextBoxColumn"
        Me.ShippedUserDataGridViewTextBoxColumn.ReadOnly = True
        Me.ShippedUserDataGridViewTextBoxColumn.Width = 113
        '
        'CropDataGridViewTextBoxColumn
        '
        Me.CropDataGridViewTextBoxColumn.DataPropertyName = "crop"
        Me.CropDataGridViewTextBoxColumn.HeaderText = "crop"
        Me.CropDataGridViewTextBoxColumn.Name = "CropDataGridViewTextBoxColumn"
        Me.CropDataGridViewTextBoxColumn.ReadOnly = True
        Me.CropDataGridViewTextBoxColumn.Width = 60
        '
        'ContainerDataGridViewTextBoxColumn
        '
        Me.ContainerDataGridViewTextBoxColumn.DataPropertyName = "Container"
        Me.ContainerDataGridViewTextBoxColumn.HeaderText = "Container"
        Me.ContainerDataGridViewTextBoxColumn.Name = "ContainerDataGridViewTextBoxColumn"
        Me.ContainerDataGridViewTextBoxColumn.ReadOnly = True
        Me.ContainerDataGridViewTextBoxColumn.Width = 95
        '
        'SpShippingSELShippingOrderBindingSource
        '
        Me.SpShippingSELShippingOrderBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrder"
        Me.SpShippingSELShippingOrderBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingOrderByCropTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderByCropTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_ShippingOrderTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter.ClearBeforeFill = True
        '
        'FrmShipping
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1199, 744)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.StatusCombobox)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmShipping"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Shipping"
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.SpShippingSELShippingOrderByCropBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents ShippedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents StatusCombobox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents SpShippingSELShippingOrderByCropBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByCropTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByCropTableAdapter
    Friend WithEvents SpShippingSELShippingOrderBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter
    Friend WithEvents OrderNo As DataGridViewTextBoxColumn
    Friend WithEvents InvNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceivedDate As DataGridViewTextBoxColumn
    Friend WithEvents Customer As DataGridViewTextBoxColumn
    Friend WithEvents PackedGrade As DataGridViewTextBoxColumn
    Friend WithEvents CustomerGrade As DataGridViewTextBoxColumn
    Friend WithEvents TotalWeightRequest As DataGridViewTextBoxColumn
    Friend WithEvents TotalCaseRequest As DataGridViewTextBoxColumn
    Friend WithEvents ShippingContainer As DataGridViewTextBoxColumn
    Friend WithEvents StatusName As DataGridViewTextBoxColumn
    Friend WithEvents ShippedDate As DataGridViewTextBoxColumn
    Friend WithEvents SINoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalWeightRequestDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalCaseRequestDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StuffDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BLNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MOPIDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ETDDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ETADateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BLdateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceivedUserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FinishedUserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents StatusNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ShippedUserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CropDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ContainerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class

﻿Public Class FrmShipping
    Inherits MetroFramework.Forms.MetroForm
    Dim XOrderNo1 As String

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            UsernameMetroLabel.Text = XUsername
            Sp_Shipping_SEL_ShippingOrderByCropTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrderByCrop)
            StatusCombobox.Items.Clear()
            StatusCombobox.Items.Add("Pending")
            StatusCombobox.Items.Add("Complete")
            StatusCombobox.SelectedIndex = 0
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub YearComboBox_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If StatusCombobox.Text <> "" Then
                Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, "", 2)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub StatusCombobox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles StatusCombobox.SelectedIndexChanged
        Try
            If StatusCombobox.Text <> "" Then
                Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, "", 2)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid.CellMouseClick
        Try
            XOrderNo1 = MetroGrid.Item(0, MetroGrid.CurrentCell.RowIndex).Value
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ShippedTile_Click(sender As Object, e As EventArgs) Handles ShippedTile.Click
        Try
            If XOrderNo1 = "" Then
                MessageBox.Show("กรูณาเลือกแถวที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            Dim frm As New FrmAddShipping
            frm.XOrderNo = XOrderNo1
            frm.ShowDialog()
            frm.Dispose()
            If StatusCombobox.Text <> "" Then
                Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, "", 2)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            If StatusCombobox.Text <> "" Then
                Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, "", 2)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub YearCheckBox_CheckedChanged(sender As Object, e As EventArgs)
        Try

            If StatusCombobox.Text <> "" Then
                Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, StatusCombobox.SelectedIndex, "", 2)
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


End Class

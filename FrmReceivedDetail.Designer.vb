﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmReceivedDetail
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim MetroTile2 As MetroFramework.Controls.MetroTile
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.BCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CASENODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradersDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WHDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetdefDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELShippingMovementBCByMovementNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.MetroPanel2 = New MetroFramework.Controls.MetroPanel()
        Me.TruckNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.CCase = New MetroFramework.Controls.MetroTextBox()
        Me.FromTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MovementNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.ReceivedLabel = New MetroFramework.Controls.MetroLabel()
        Me.ReceivedTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel15 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter()
        Me.RCaseNo = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.BCTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_BarcodeDetailFromPCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_BarcodeDetailFromPCTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_BarcodeDetailFromPCTableAdapter()
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementByMovementNoWHTableAdapter()
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_FromBayToTableAdapter1 = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayToTableAdapter()
        Me.ToBayComboBox = New MetroFramework.Controls.MetroComboBox()
        MetroTile2 = New MetroFramework.Controls.MetroTile()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELShippingMovementBCByMovementNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel2.SuspendLayout()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.Sp_Shipping_SEL_ShippingMovementByMovementNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_BarcodeDetailFromPCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroTile2
        '
        MetroTile2.ActiveControl = Nothing
        MetroTile2.AutoSize = True
        MetroTile2.Dock = System.Windows.Forms.DockStyle.Top
        MetroTile2.Location = New System.Drawing.Point(0, 0)
        MetroTile2.Name = "MetroTile2"
        MetroTile2.Size = New System.Drawing.Size(277, 41)
        MetroTile2.Style = MetroFramework.MetroColorStyle.Lime
        MetroTile2.TabIndex = 2
        MetroTile2.Text = "Movement information"
        MetroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        MetroTile2.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        MetroTile2.UseSelectable = True
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Calibri", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle11
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Calibri", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BCDataGridViewTextBoxColumn, Me.CASENODataGridViewTextBoxColumn, Me.GradersDataGridViewTextBoxColumn, Me.BAYDataGridViewTextBoxColumn, Me.WHDataGridViewTextBoxColumn, Me.NetdefDataGridViewTextBoxColumn, Me.MovementNoDataGridViewTextBoxColumn})
        Me.MetroGrid.DataSource = Me.SpShippingSELShippingMovementBCByMovementNoBindingSource
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Calibri", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle14
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(293, 164)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Calibri", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 28
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(893, 496)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 185
        '
        'BCDataGridViewTextBoxColumn
        '
        Me.BCDataGridViewTextBoxColumn.DataPropertyName = "BC"
        Me.BCDataGridViewTextBoxColumn.HeaderText = "BC"
        Me.BCDataGridViewTextBoxColumn.Name = "BCDataGridViewTextBoxColumn"
        Me.BCDataGridViewTextBoxColumn.ReadOnly = True
        Me.BCDataGridViewTextBoxColumn.Width = 53
        '
        'CASENODataGridViewTextBoxColumn
        '
        Me.CASENODataGridViewTextBoxColumn.DataPropertyName = "CASENO"
        Me.CASENODataGridViewTextBoxColumn.HeaderText = "Case"
        Me.CASENODataGridViewTextBoxColumn.Name = "CASENODataGridViewTextBoxColumn"
        Me.CASENODataGridViewTextBoxColumn.ReadOnly = True
        Me.CASENODataGridViewTextBoxColumn.Width = 69
        '
        'GradersDataGridViewTextBoxColumn
        '
        Me.GradersDataGridViewTextBoxColumn.DataPropertyName = "graders"
        Me.GradersDataGridViewTextBoxColumn.HeaderText = "Grade"
        Me.GradersDataGridViewTextBoxColumn.Name = "GradersDataGridViewTextBoxColumn"
        Me.GradersDataGridViewTextBoxColumn.ReadOnly = True
        Me.GradersDataGridViewTextBoxColumn.Width = 80
        '
        'BAYDataGridViewTextBoxColumn
        '
        Me.BAYDataGridViewTextBoxColumn.DataPropertyName = "BAY"
        Me.BAYDataGridViewTextBoxColumn.HeaderText = "BAY"
        Me.BAYDataGridViewTextBoxColumn.Name = "BAYDataGridViewTextBoxColumn"
        Me.BAYDataGridViewTextBoxColumn.ReadOnly = True
        Me.BAYDataGridViewTextBoxColumn.Width = 62
        '
        'WHDataGridViewTextBoxColumn
        '
        Me.WHDataGridViewTextBoxColumn.DataPropertyName = "WH"
        Me.WHDataGridViewTextBoxColumn.HeaderText = "WH"
        Me.WHDataGridViewTextBoxColumn.Name = "WHDataGridViewTextBoxColumn"
        Me.WHDataGridViewTextBoxColumn.ReadOnly = True
        Me.WHDataGridViewTextBoxColumn.Width = 62
        '
        'NetdefDataGridViewTextBoxColumn
        '
        Me.NetdefDataGridViewTextBoxColumn.DataPropertyName = "netdef"
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.NetdefDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle13
        Me.NetdefDataGridViewTextBoxColumn.HeaderText = "netdef"
        Me.NetdefDataGridViewTextBoxColumn.Name = "NetdefDataGridViewTextBoxColumn"
        Me.NetdefDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetdefDataGridViewTextBoxColumn.Width = 83
        '
        'MovementNoDataGridViewTextBoxColumn
        '
        Me.MovementNoDataGridViewTextBoxColumn.DataPropertyName = "MovementNo"
        Me.MovementNoDataGridViewTextBoxColumn.HeaderText = "Move No."
        Me.MovementNoDataGridViewTextBoxColumn.Name = "MovementNoDataGridViewTextBoxColumn"
        Me.MovementNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.MovementNoDataGridViewTextBoxColumn.Width = 108
        '
        'SpShippingSELShippingMovementBCByMovementNoBindingSource
        '
        Me.SpShippingSELShippingMovementBCByMovementNoBindingSource.DataMember = "sp_Shipping_SEL_ShippingMovementBCByMovementNo"
        Me.SpShippingSELShippingMovementBCByMovementNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroPanel2
        '
        Me.MetroPanel2.Controls.Add(Me.TruckNoTextbox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel3)
        Me.MetroPanel2.Controls.Add(Me.CCase)
        Me.MetroPanel2.Controls.Add(Me.FromTextbox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel9)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel2.Controls.Add(Me.MovementNoTextbox)
        Me.MetroPanel2.Controls.Add(MetroTile2)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel2.HorizontalScrollbarBarColor = True
        Me.MetroPanel2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.HorizontalScrollbarSize = 10
        Me.MetroPanel2.Location = New System.Drawing.Point(10, 107)
        Me.MetroPanel2.Name = "MetroPanel2"
        Me.MetroPanel2.Size = New System.Drawing.Size(277, 239)
        Me.MetroPanel2.TabIndex = 184
        Me.MetroPanel2.VerticalScrollbarBarColor = True
        Me.MetroPanel2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.VerticalScrollbarSize = 10
        '
        'TruckNoTextbox
        '
        '
        '
        '
        Me.TruckNoTextbox.CustomButton.Image = Nothing
        Me.TruckNoTextbox.CustomButton.Location = New System.Drawing.Point(82, 2)
        Me.TruckNoTextbox.CustomButton.Name = ""
        Me.TruckNoTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.TruckNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoTextbox.CustomButton.TabIndex = 1
        Me.TruckNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoTextbox.CustomButton.UseSelectable = True
        Me.TruckNoTextbox.CustomButton.Visible = False
        Me.TruckNoTextbox.Enabled = False
        Me.TruckNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TruckNoTextbox.Lines = New String(-1) {}
        Me.TruckNoTextbox.Location = New System.Drawing.Point(147, 143)
        Me.TruckNoTextbox.MaxLength = 2
        Me.TruckNoTextbox.Name = "TruckNoTextbox"
        Me.TruckNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoTextbox.SelectedText = ""
        Me.TruckNoTextbox.SelectionLength = 0
        Me.TruckNoTextbox.SelectionStart = 0
        Me.TruckNoTextbox.ShortcutsEnabled = True
        Me.TruckNoTextbox.Size = New System.Drawing.Size(120, 40)
        Me.TruckNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckNoTextbox.TabIndex = 172
        Me.TruckNoTextbox.UseSelectable = True
        Me.TruckNoTextbox.UseStyleColors = True
        Me.TruckNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(11, 154)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(80, 25)
        Me.MetroLabel3.TabIndex = 171
        Me.MetroLabel3.Text = "Truck no."
        '
        'CCase
        '
        '
        '
        '
        Me.CCase.CustomButton.Image = Nothing
        Me.CCase.CustomButton.Location = New System.Drawing.Point(82, 2)
        Me.CCase.CustomButton.Name = ""
        Me.CCase.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CCase.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CCase.CustomButton.TabIndex = 1
        Me.CCase.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CCase.CustomButton.UseSelectable = True
        Me.CCase.CustomButton.Visible = False
        Me.CCase.Enabled = False
        Me.CCase.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CCase.Lines = New String(-1) {}
        Me.CCase.Location = New System.Drawing.Point(147, 189)
        Me.CCase.MaxLength = 2
        Me.CCase.Name = "CCase"
        Me.CCase.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CCase.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CCase.SelectedText = ""
        Me.CCase.SelectionLength = 0
        Me.CCase.SelectionStart = 0
        Me.CCase.ShortcutsEnabled = True
        Me.CCase.Size = New System.Drawing.Size(120, 40)
        Me.CCase.Style = MetroFramework.MetroColorStyle.Lime
        Me.CCase.TabIndex = 168
        Me.CCase.UseSelectable = True
        Me.CCase.UseStyleColors = True
        Me.CCase.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CCase.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FromTextbox
        '
        '
        '
        '
        Me.FromTextbox.CustomButton.Image = Nothing
        Me.FromTextbox.CustomButton.Location = New System.Drawing.Point(82, 2)
        Me.FromTextbox.CustomButton.Name = ""
        Me.FromTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.FromTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.FromTextbox.CustomButton.TabIndex = 1
        Me.FromTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromTextbox.CustomButton.UseSelectable = True
        Me.FromTextbox.CustomButton.Visible = False
        Me.FromTextbox.Enabled = False
        Me.FromTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.FromTextbox.Lines = New String(-1) {}
        Me.FromTextbox.Location = New System.Drawing.Point(147, 97)
        Me.FromTextbox.MaxLength = 2
        Me.FromTextbox.Name = "FromTextbox"
        Me.FromTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.FromTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.FromTextbox.SelectedText = ""
        Me.FromTextbox.SelectionLength = 0
        Me.FromTextbox.SelectionStart = 0
        Me.FromTextbox.ShortcutsEnabled = True
        Me.FromTextbox.Size = New System.Drawing.Size(120, 40)
        Me.FromTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.FromTextbox.TabIndex = 170
        Me.FromTextbox.UseSelectable = True
        Me.FromTextbox.UseStyleColors = True
        Me.FromTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FromTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel9.Location = New System.Drawing.Point(11, 200)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(116, 25)
        Me.MetroLabel9.TabIndex = 167
        Me.MetroLabel9.Text = "Count of case"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(11, 108)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(51, 25)
        Me.MetroLabel2.TabIndex = 169
        Me.MetroLabel2.Text = "From"
        '
        'MovementNoTextbox
        '
        '
        '
        '
        Me.MovementNoTextbox.CustomButton.Image = Nothing
        Me.MovementNoTextbox.CustomButton.Location = New System.Drawing.Point(82, 2)
        Me.MovementNoTextbox.CustomButton.Name = ""
        Me.MovementNoTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.MovementNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MovementNoTextbox.CustomButton.TabIndex = 1
        Me.MovementNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MovementNoTextbox.CustomButton.UseSelectable = True
        Me.MovementNoTextbox.CustomButton.Visible = False
        Me.MovementNoTextbox.Enabled = False
        Me.MovementNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.MovementNoTextbox.Lines = New String(-1) {}
        Me.MovementNoTextbox.Location = New System.Drawing.Point(147, 51)
        Me.MovementNoTextbox.MaxLength = 2
        Me.MovementNoTextbox.Name = "MovementNoTextbox"
        Me.MovementNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MovementNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MovementNoTextbox.SelectedText = ""
        Me.MovementNoTextbox.SelectionLength = 0
        Me.MovementNoTextbox.SelectionStart = 0
        Me.MovementNoTextbox.ShortcutsEnabled = True
        Me.MovementNoTextbox.Size = New System.Drawing.Size(120, 40)
        Me.MovementNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.MovementNoTextbox.TabIndex = 168
        Me.MovementNoTextbox.UseSelectable = True
        Me.MovementNoTextbox.UseStyleColors = True
        Me.MovementNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MovementNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel1.Location = New System.Drawing.Point(11, 62)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(122, 25)
        Me.MetroLabel1.TabIndex = 167
        Me.MetroLabel1.Text = "Movement no."
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.ReceivedLabel)
        Me.MetroPanel1.Controls.Add(Me.ReceivedTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel15)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel8)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(7, 29)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1179, 73)
        Me.MetroPanel1.TabIndex = 183
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'ReceivedLabel
        '
        Me.ReceivedLabel.AutoSize = True
        Me.ReceivedLabel.BackColor = System.Drawing.Color.LightGray
        Me.ReceivedLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.ReceivedLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.ReceivedLabel.ForeColor = System.Drawing.Color.Red
        Me.ReceivedLabel.Location = New System.Drawing.Point(436, 19)
        Me.ReceivedLabel.Name = "ReceivedLabel"
        Me.ReceivedLabel.Size = New System.Drawing.Size(89, 25)
        Me.ReceivedLabel.TabIndex = 184
        Me.ReceivedLabel.Text = "Received"
        Me.ReceivedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReceivedLabel.UseCustomBackColor = True
        Me.ReceivedLabel.UseCustomForeColor = True
        Me.ReceivedLabel.UseStyleColors = True
        Me.ReceivedLabel.Visible = False
        '
        'ReceivedTile
        '
        Me.ReceivedTile.ActiveControl = Nothing
        Me.ReceivedTile.AutoSize = True
        Me.ReceivedTile.BackColor = System.Drawing.Color.White
        Me.ReceivedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ReceivedTile.Enabled = False
        Me.ReceivedTile.Location = New System.Drawing.Point(582, 2)
        Me.ReceivedTile.Name = "ReceivedTile"
        Me.ReceivedTile.Size = New System.Drawing.Size(66, 66)
        Me.ReceivedTile.Style = MetroFramework.MetroColorStyle.White
        Me.ReceivedTile.TabIndex = 127
        Me.ReceivedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReceivedTile.TileImage = Global.ShippingSystem.My.Resources.Resources.AcceptDatabase64
        Me.ReceivedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReceivedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ReceivedTile.UseSelectable = True
        Me.ReceivedTile.UseTileImage = True
        '
        'MetroLabel15
        '
        Me.MetroLabel15.AutoSize = True
        Me.MetroLabel15.Location = New System.Drawing.Point(654, 23)
        Me.MetroLabel15.Name = "MetroLabel15"
        Me.MetroLabel15.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel15.TabIndex = 126
        Me.MetroLabel15.Text = "received"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(737, 3)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(66, 58)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh64
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(809, 24)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel8.TabIndex = 116
        Me.MetroLabel8.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1052, 2)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(63, 56)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft264
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'Sp_Shipping_SEL_ShippingMovementByMovementNoBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoBindingSource.DataMember = "sp_Shipping_SEL_ShippingMovementByMovementNo"
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter.ClearBeforeFill = True
        '
        'RCaseNo
        '
        '
        '
        '
        Me.RCaseNo.CustomButton.Image = Nothing
        Me.RCaseNo.CustomButton.Location = New System.Drawing.Point(83, 1)
        Me.RCaseNo.CustomButton.Name = ""
        Me.RCaseNo.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.RCaseNo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.RCaseNo.CustomButton.TabIndex = 1
        Me.RCaseNo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.RCaseNo.CustomButton.UseSelectable = True
        Me.RCaseNo.CustomButton.Visible = False
        Me.RCaseNo.Enabled = False
        Me.RCaseNo.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.RCaseNo.Lines = New String(-1) {}
        Me.RCaseNo.Location = New System.Drawing.Point(774, 123)
        Me.RCaseNo.MaxLength = 2
        Me.RCaseNo.Name = "RCaseNo"
        Me.RCaseNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.RCaseNo.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.RCaseNo.SelectedText = ""
        Me.RCaseNo.SelectionLength = 0
        Me.RCaseNo.SelectionStart = 0
        Me.RCaseNo.ShortcutsEnabled = True
        Me.RCaseNo.Size = New System.Drawing.Size(117, 35)
        Me.RCaseNo.Style = MetroFramework.MetroColorStyle.Lime
        Me.RCaseNo.TabIndex = 174
        Me.RCaseNo.UseSelectable = True
        Me.RCaseNo.UseStyleColors = True
        Me.RCaseNo.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.RCaseNo.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel10.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel10.Location = New System.Drawing.Point(774, 106)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(86, 15)
        Me.MetroLabel10.TabIndex = 173
        Me.MetroLabel10.Text = "Received case"
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel5.Location = New System.Drawing.Point(418, 106)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(53, 15)
        Me.MetroLabel5.TabIndex = 167
        Me.MetroLabel5.Text = "Barcode"
        '
        'BCTextbox
        '
        '
        '
        '
        Me.BCTextbox.CustomButton.Image = Nothing
        Me.BCTextbox.CustomButton.Location = New System.Drawing.Point(316, 1)
        Me.BCTextbox.CustomButton.Name = ""
        Me.BCTextbox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.BCTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BCTextbox.CustomButton.TabIndex = 1
        Me.BCTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BCTextbox.CustomButton.UseSelectable = True
        Me.BCTextbox.CustomButton.Visible = False
        Me.BCTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BCTextbox.Lines = New String(-1) {}
        Me.BCTextbox.Location = New System.Drawing.Point(418, 123)
        Me.BCTextbox.MaxLength = 100
        Me.BCTextbox.Name = "BCTextbox"
        Me.BCTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BCTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BCTextbox.SelectedText = ""
        Me.BCTextbox.SelectionLength = 0
        Me.BCTextbox.SelectionStart = 0
        Me.BCTextbox.ShortcutsEnabled = True
        Me.BCTextbox.Size = New System.Drawing.Size(350, 35)
        Me.BCTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.BCTextbox.TabIndex = 168
        Me.BCTextbox.UseSelectable = True
        Me.BCTextbox.UseStyleColors = True
        Me.BCTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BCTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel7.Location = New System.Drawing.Point(293, 106)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(27, 15)
        Me.MetroLabel7.TabIndex = 167
        Me.MetroLabel7.Text = "Bay"
        '
        'Sp_Shipping_SEL_BarcodeDetailFromPCBindingSource
        '
        Me.Sp_Shipping_SEL_BarcodeDetailFromPCBindingSource.DataMember = "sp_Shipping_SEL_BarcodeDetailFromPC"
        Me.Sp_Shipping_SEL_BarcodeDetailFromPCBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_BarcodeDetailFromPCTableAdapter
        '
        Me.Sp_Shipping_SEL_BarcodeDetailFromPCTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_ShippingMovementByMovementNoWHTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource.DataMember = "sp_Shipping_SEL_ShippingMovementByMovementNoWH"
        Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_FromBayToTableAdapter1
        '
        Me.Sp_Shipping_SEL_FromBayToTableAdapter1.ClearBeforeFill = True
        '
        'ToBayComboBox
        '
        Me.ToBayComboBox.DisplayMember = "BAY"
        Me.ToBayComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.ToBayComboBox.FormattingEnabled = True
        Me.ToBayComboBox.ItemHeight = 29
        Me.ToBayComboBox.Location = New System.Drawing.Point(293, 123)
        Me.ToBayComboBox.Name = "ToBayComboBox"
        Me.ToBayComboBox.Size = New System.Drawing.Size(119, 35)
        Me.ToBayComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.ToBayComboBox.TabIndex = 199
        Me.ToBayComboBox.UseSelectable = True
        Me.ToBayComboBox.ValueMember = "BAY"
        '
        'FrmReceivedDetail
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1564, 684)
        Me.Controls.Add(Me.ToBayComboBox)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.RCaseNo)
        Me.Controls.Add(Me.MetroLabel10)
        Me.Controls.Add(Me.MetroPanel2)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.BCTextbox)
        Me.Name = "FrmReceivedDetail"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELShippingMovementBCByMovementNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel2.ResumeLayout(False)
        Me.MetroPanel2.PerformLayout()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.Sp_Shipping_SEL_ShippingMovementByMovementNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_BarcodeDetailFromPCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroPanel2 As MetroFramework.Controls.MetroPanel
    Friend WithEvents TruckNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CCase As MetroFramework.Controls.MetroTextBox
    Friend WithEvents FromTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MovementNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents ReceivedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel15 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementByMovementNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    'Friend WithEvents Sp_Shipping_SEL_PDByMovementNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PDByMovementNoTableAdapter
    Friend WithEvents ReceivedLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SpShippingSELShippingMovementBCByMovementNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter
    Friend WithEvents BCDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CASENODataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GradersDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BAYDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents WHDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetdefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MovementNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BCTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RCaseNo As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_FromBayToTableAdapter1 As ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayToTableAdapter
    Friend WithEvents Sp_Shipping_SEL_BarcodeDetailFromPCTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_BarcodeDetailFromPCTableAdapter
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementByMovementNoWHTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementByMovementNoWHTableAdapter
    Friend WithEvents Sp_Shipping_SEL_BarcodeDetailFromPCBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource As BindingSource
    Friend WithEvents ToBayComboBox As MetroFramework.Controls.MetroComboBox
End Class

﻿Public Class FrmUpdateShippingLocation
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private Sub FrmUpdateShippingLocation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            'LocIDTextbox.Text = ""
            LocNameTextbox.Text = ""
            Me.WareHouseTableAdapter.Fill(Me.ShippingDataSet.WareHouse)

            'Get data from matrc
            Dim XInfoRow As ShippingDataSet.sp_Shipping_SEL_ShippingLocationRow
            Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingLocation)
            XInfoRow = Me.ShippingDataSet.sp_Shipping_SEL_ShippingLocation.FindByLocNo(LocIDTextbox.Text)

            If XInfoRow Is Nothing Then
                MessageBox.Show("ไม่พบเลขนี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            LocNameTextbox.Text = XInfoRow.LocName

            If Not XInfoRow Is Nothing And Not XInfoRow.IsStationNull Then
                StationComboBox.Text = XInfoRow.Station
            End If


            If XInfoRow.Status = True Then
                Status.Text = "Active"
                Status.Checked = True
            ElseIf XInfoRow.Status = False Then
                Status.Text = "Unactive"
                Status.Checked = False
            End If


        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If LocIDTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Location ID", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If LocNameTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Location name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim XStatus As Boolean
            If Status.Checked = True Then
                XStatus = True
            Else
                XStatus = False
            End If

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการแก้ไข Location ID นี้ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                db.sp_Shipping_UPD_ShippingLocation(LocIDTextbox.Text, LocNameTextbox.Text, XStatus, StationComboBox.Text)

                MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
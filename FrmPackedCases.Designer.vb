﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPackedCases
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CropCombobox = New MetroFramework.Controls.MetroComboBox()
        Me.GradeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.RefreshButton = New MetroFramework.Controls.MetroButton()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.pdMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.SpPDSELPackedCasesByGradeResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.crop = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.caseno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.graders = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.customer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.customerrs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.netdef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.netreal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.packingmat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bay = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pdtype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.packingdate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.packingtime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fromprgrade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.frompdno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.frompddate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.frompdhour = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.issued = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.issuedto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.issueddate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.toprgrade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.topddate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.topdno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.topdhour = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ShippingOrderNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.spno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderDetailNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SINo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BLNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MOPI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.pdMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpPDSELPackedCasesByGradeResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CropCombobox
        '
        Me.CropCombobox.FormattingEnabled = True
        Me.CropCombobox.ItemHeight = 23
        Me.CropCombobox.Location = New System.Drawing.Point(3, 3)
        Me.CropCombobox.Name = "CropCombobox"
        Me.CropCombobox.PromptText = "crop"
        Me.CropCombobox.Size = New System.Drawing.Size(100, 29)
        Me.CropCombobox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropCombobox.TabIndex = 200
        Me.CropCombobox.UseSelectable = True
        '
        'GradeComboBox
        '
        Me.GradeComboBox.DisplayMember = "packedgrade1"
        Me.GradeComboBox.FormattingEnabled = True
        Me.GradeComboBox.ItemHeight = 23
        Me.GradeComboBox.Location = New System.Drawing.Point(3, 3)
        Me.GradeComboBox.Name = "GradeComboBox"
        Me.GradeComboBox.PromptText = "grade"
        Me.GradeComboBox.Size = New System.Drawing.Size(200, 29)
        Me.GradeComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.GradeComboBox.TabIndex = 202
        Me.GradeComboBox.UseSelectable = True
        Me.GradeComboBox.ValueMember = "packedgrade1"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.CropCombobox)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(106, 35)
        Me.FlowLayoutPanel1.TabIndex = 204
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.GradeComboBox)
        Me.FlowLayoutPanel2.Controls.Add(Me.RefreshButton)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(115, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(266, 35)
        Me.FlowLayoutPanel2.TabIndex = 205
        '
        'RefreshButton
        '
        Me.RefreshButton.AutoSize = True
        Me.RefreshButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.RefreshButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RefreshButton.Location = New System.Drawing.Point(209, 3)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(54, 29)
        Me.RefreshButton.TabIndex = 208
        Me.RefreshButton.Text = "Refresh"
        Me.RefreshButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.RefreshButton.UseSelectable = True
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1154, 41)
        Me.FlowLayoutPanel3.TabIndex = 206
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.FlowLayoutPanel3)
        Me.FlowLayoutPanel4.Controls.Add(Me.pdMetroGrid)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(1160, 664)
        Me.FlowLayoutPanel4.TabIndex = 207
        '
        'pdMetroGrid
        '
        Me.pdMetroGrid.AllowUserToAddRows = False
        Me.pdMetroGrid.AllowUserToDeleteRows = False
        Me.pdMetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.pdMetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.pdMetroGrid.AutoGenerateColumns = False
        Me.pdMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.pdMetroGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.pdMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pdMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.pdMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.pdMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.pdMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.pdMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.pdMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.crop, Me.type, Me.bc, Me.caseno, Me.grade, Me.graders, Me.customer, Me.customerrs, Me.netdef, Me.netreal, Me.packingmat, Me.bay, Me.pdtype, Me.wh, Me.packingdate, Me.packingtime, Me.fromprgrade, Me.frompdno, Me.frompddate, Me.frompdhour, Me.issued, Me.issuedto, Me.issueddate, Me.toprgrade, Me.topddate, Me.topdno, Me.topdhour, Me.ShippingOrderNo, Me.spno, Me.OrderNo, Me.InvoiceNo, Me.OrderDetailNo, Me.SINo, Me.BLNo, Me.MOPI})
        Me.pdMetroGrid.DataSource = Me.SpPDSELPackedCasesByGradeResultBindingSource
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.pdMetroGrid.DefaultCellStyle = DataGridViewCellStyle7
        Me.pdMetroGrid.EnableHeadersVisualStyles = False
        Me.pdMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.pdMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.pdMetroGrid.Location = New System.Drawing.Point(3, 50)
        Me.pdMetroGrid.Name = "pdMetroGrid"
        Me.pdMetroGrid.ReadOnly = True
        Me.pdMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.pdMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.pdMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.pdMetroGrid.RowTemplate.Height = 28
        Me.pdMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.pdMetroGrid.Size = New System.Drawing.Size(1154, 611)
        Me.pdMetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.pdMetroGrid.TabIndex = 207
        '
        'SpPDSELPackedCasesByGradeResultBindingSource
        '
        Me.SpPDSELPackedCasesByGradeResultBindingSource.DataSource = GetType(FactoryEntities.sp_PD_SEL_PackedCasesByGrade_Result)
        '
        'crop
        '
        Me.crop.DataPropertyName = "crop"
        Me.crop.Frozen = True
        Me.crop.HeaderText = "crop"
        Me.crop.Name = "crop"
        Me.crop.ReadOnly = True
        Me.crop.Width = 53
        '
        'type
        '
        Me.type.DataPropertyName = "type"
        Me.type.HeaderText = "type"
        Me.type.Name = "type"
        Me.type.ReadOnly = True
        Me.type.Width = 52
        '
        'bc
        '
        Me.bc.DataPropertyName = "bc"
        Me.bc.HeaderText = "bc"
        Me.bc.Name = "bc"
        Me.bc.ReadOnly = True
        Me.bc.Width = 42
        '
        'caseno
        '
        Me.caseno.DataPropertyName = "caseno"
        Me.caseno.HeaderText = "caseno"
        Me.caseno.Name = "caseno"
        Me.caseno.ReadOnly = True
        Me.caseno.Width = 66
        '
        'grade
        '
        Me.grade.DataPropertyName = "grade"
        Me.grade.HeaderText = "grade"
        Me.grade.Name = "grade"
        Me.grade.ReadOnly = True
        Me.grade.Width = 60
        '
        'graders
        '
        Me.graders.DataPropertyName = "graders"
        Me.graders.HeaderText = "graders"
        Me.graders.Name = "graders"
        Me.graders.ReadOnly = True
        Me.graders.Width = 69
        '
        'customer
        '
        Me.customer.DataPropertyName = "customer"
        Me.customer.HeaderText = "customer"
        Me.customer.Name = "customer"
        Me.customer.ReadOnly = True
        Me.customer.Width = 77
        '
        'customerrs
        '
        Me.customerrs.DataPropertyName = "customerrs"
        Me.customerrs.HeaderText = "customerrs"
        Me.customerrs.Name = "customerrs"
        Me.customerrs.ReadOnly = True
        Me.customerrs.Width = 86
        '
        'netdef
        '
        Me.netdef.DataPropertyName = "netdef"
        DataGridViewCellStyle3.Format = "N1"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.netdef.DefaultCellStyle = DataGridViewCellStyle3
        Me.netdef.HeaderText = "netdef"
        Me.netdef.Name = "netdef"
        Me.netdef.ReadOnly = True
        Me.netdef.Width = 64
        '
        'netreal
        '
        Me.netreal.DataPropertyName = "netreal"
        DataGridViewCellStyle4.Format = "N1"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.netreal.DefaultCellStyle = DataGridViewCellStyle4
        Me.netreal.HeaderText = "netreal"
        Me.netreal.Name = "netreal"
        Me.netreal.ReadOnly = True
        Me.netreal.Width = 66
        '
        'packingmat
        '
        Me.packingmat.DataPropertyName = "packingmat"
        Me.packingmat.HeaderText = "packingmat"
        Me.packingmat.Name = "packingmat"
        Me.packingmat.ReadOnly = True
        Me.packingmat.Width = 90
        '
        'bay
        '
        Me.bay.DataPropertyName = "bay"
        Me.bay.HeaderText = "bay"
        Me.bay.Name = "bay"
        Me.bay.ReadOnly = True
        Me.bay.Width = 48
        '
        'pdtype
        '
        Me.pdtype.DataPropertyName = "pdtype"
        Me.pdtype.HeaderText = "pdtype"
        Me.pdtype.Name = "pdtype"
        Me.pdtype.ReadOnly = True
        Me.pdtype.Width = 66
        '
        'wh
        '
        Me.wh.DataPropertyName = "wh"
        Me.wh.HeaderText = "wh"
        Me.wh.Name = "wh"
        Me.wh.ReadOnly = True
        Me.wh.Width = 46
        '
        'packingdate
        '
        Me.packingdate.DataPropertyName = "packingdate"
        DataGridViewCellStyle5.Format = "d"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.packingdate.DefaultCellStyle = DataGridViewCellStyle5
        Me.packingdate.HeaderText = "packingdate"
        Me.packingdate.Name = "packingdate"
        Me.packingdate.ReadOnly = True
        Me.packingdate.Width = 94
        '
        'packingtime
        '
        Me.packingtime.DataPropertyName = "packingtime"
        DataGridViewCellStyle6.Format = "T"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.packingtime.DefaultCellStyle = DataGridViewCellStyle6
        Me.packingtime.HeaderText = "packingtime"
        Me.packingtime.Name = "packingtime"
        Me.packingtime.ReadOnly = True
        Me.packingtime.Width = 93
        '
        'fromprgrade
        '
        Me.fromprgrade.DataPropertyName = "fromprgrade"
        Me.fromprgrade.HeaderText = "fromprgrade"
        Me.fromprgrade.Name = "fromprgrade"
        Me.fromprgrade.ReadOnly = True
        Me.fromprgrade.Width = 95
        '
        'frompdno
        '
        Me.frompdno.DataPropertyName = "frompdno"
        Me.frompdno.HeaderText = "frompdno"
        Me.frompdno.Name = "frompdno"
        Me.frompdno.ReadOnly = True
        Me.frompdno.Width = 82
        '
        'frompddate
        '
        Me.frompddate.DataPropertyName = "frompddate"
        Me.frompddate.HeaderText = "frompddate"
        Me.frompddate.Name = "frompddate"
        Me.frompddate.ReadOnly = True
        Me.frompddate.Width = 91
        '
        'frompdhour
        '
        Me.frompdhour.DataPropertyName = "frompdhour"
        Me.frompdhour.HeaderText = "frompdhour"
        Me.frompdhour.Name = "frompdhour"
        Me.frompdhour.ReadOnly = True
        Me.frompdhour.Width = 93
        '
        'issued
        '
        Me.issued.DataPropertyName = "issued"
        Me.issued.HeaderText = "issued"
        Me.issued.Name = "issued"
        Me.issued.ReadOnly = True
        Me.issued.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.issued.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.issued.Width = 63
        '
        'issuedto
        '
        Me.issuedto.DataPropertyName = "issuedto"
        Me.issuedto.HeaderText = "issuedto"
        Me.issuedto.Name = "issuedto"
        Me.issuedto.ReadOnly = True
        Me.issuedto.Width = 74
        '
        'issueddate
        '
        Me.issueddate.DataPropertyName = "issueddate"
        Me.issueddate.HeaderText = "issueddate"
        Me.issueddate.Name = "issueddate"
        Me.issueddate.ReadOnly = True
        Me.issueddate.Width = 86
        '
        'toprgrade
        '
        Me.toprgrade.DataPropertyName = "toprgrade"
        Me.toprgrade.HeaderText = "toprgrade"
        Me.toprgrade.Name = "toprgrade"
        Me.toprgrade.ReadOnly = True
        Me.toprgrade.Width = 82
        '
        'topddate
        '
        Me.topddate.DataPropertyName = "topddate"
        Me.topddate.HeaderText = "topddate"
        Me.topddate.Name = "topddate"
        Me.topddate.ReadOnly = True
        Me.topddate.Width = 78
        '
        'topdno
        '
        Me.topdno.DataPropertyName = "topdno"
        Me.topdno.HeaderText = "topdno"
        Me.topdno.Name = "topdno"
        Me.topdno.ReadOnly = True
        Me.topdno.Width = 69
        '
        'topdhour
        '
        Me.topdhour.DataPropertyName = "topdhour"
        Me.topdhour.HeaderText = "topdhour"
        Me.topdhour.Name = "topdhour"
        Me.topdhour.ReadOnly = True
        Me.topdhour.Width = 80
        '
        'ShippingOrderNo
        '
        Me.ShippingOrderNo.DataPropertyName = "ShippingOrderNo"
        Me.ShippingOrderNo.HeaderText = "ShippingOrderNo"
        Me.ShippingOrderNo.Name = "ShippingOrderNo"
        Me.ShippingOrderNo.ReadOnly = True
        Me.ShippingOrderNo.Width = 122
        '
        'spno
        '
        Me.spno.DataPropertyName = "spno"
        Me.spno.HeaderText = "spno"
        Me.spno.Name = "spno"
        Me.spno.ReadOnly = True
        Me.spno.Width = 56
        '
        'OrderNo
        '
        Me.OrderNo.DataPropertyName = "OrderNo"
        Me.OrderNo.HeaderText = "OrderNo"
        Me.OrderNo.Name = "OrderNo"
        Me.OrderNo.ReadOnly = True
        Me.OrderNo.Width = 75
        '
        'InvoiceNo
        '
        Me.InvoiceNo.DataPropertyName = "InvoiceNo"
        Me.InvoiceNo.HeaderText = "InvoiceNo"
        Me.InvoiceNo.Name = "InvoiceNo"
        Me.InvoiceNo.ReadOnly = True
        Me.InvoiceNo.Width = 81
        '
        'OrderDetailNo
        '
        Me.OrderDetailNo.DataPropertyName = "OrderDetailNo"
        Me.OrderDetailNo.HeaderText = "OrderDetailNo"
        Me.OrderDetailNo.Name = "OrderDetailNo"
        Me.OrderDetailNo.ReadOnly = True
        Me.OrderDetailNo.Width = 105
        '
        'SINo
        '
        Me.SINo.DataPropertyName = "SINo"
        Me.SINo.HeaderText = "SINo"
        Me.SINo.Name = "SINo"
        Me.SINo.ReadOnly = True
        Me.SINo.Width = 54
        '
        'BLNo
        '
        Me.BLNo.DataPropertyName = "BLNo"
        Me.BLNo.HeaderText = "BLNo"
        Me.BLNo.Name = "BLNo"
        Me.BLNo.ReadOnly = True
        Me.BLNo.Width = 56
        '
        'MOPI
        '
        Me.MOPI.DataPropertyName = "MOPI"
        Me.MOPI.HeaderText = "MOPI"
        Me.MOPI.Name = "MOPI"
        Me.MOPI.ReadOnly = True
        Me.MOPI.Width = 58
        '
        'FrmPackedCases
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1200, 744)
        Me.Controls.Add(Me.FlowLayoutPanel4)
        Me.Name = "FrmPackedCases"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Barcode Detail List"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        CType(Me.pdMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpPDSELPackedCasesByGradeResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CropCombobox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents GradeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents RefreshButton As MetroFramework.Controls.MetroButton
    Friend WithEvents pdMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents SpPDSELPackedCasesByGradeResultBindingSource As BindingSource
    Friend WithEvents crop As DataGridViewTextBoxColumn
    Friend WithEvents type As DataGridViewTextBoxColumn
    Friend WithEvents bc As DataGridViewTextBoxColumn
    Friend WithEvents caseno As DataGridViewTextBoxColumn
    Friend WithEvents grade As DataGridViewTextBoxColumn
    Friend WithEvents graders As DataGridViewTextBoxColumn
    Friend WithEvents customer As DataGridViewTextBoxColumn
    Friend WithEvents customerrs As DataGridViewTextBoxColumn
    Friend WithEvents netdef As DataGridViewTextBoxColumn
    Friend WithEvents netreal As DataGridViewTextBoxColumn
    Friend WithEvents packingmat As DataGridViewTextBoxColumn
    Friend WithEvents bay As DataGridViewTextBoxColumn
    Friend WithEvents pdtype As DataGridViewTextBoxColumn
    Friend WithEvents wh As DataGridViewTextBoxColumn
    Friend WithEvents packingdate As DataGridViewTextBoxColumn
    Friend WithEvents packingtime As DataGridViewTextBoxColumn
    Friend WithEvents fromprgrade As DataGridViewTextBoxColumn
    Friend WithEvents frompdno As DataGridViewTextBoxColumn
    Friend WithEvents frompddate As DataGridViewTextBoxColumn
    Friend WithEvents frompdhour As DataGridViewTextBoxColumn
    Friend WithEvents issued As DataGridViewCheckBoxColumn
    Friend WithEvents issuedto As DataGridViewTextBoxColumn
    Friend WithEvents issueddate As DataGridViewTextBoxColumn
    Friend WithEvents toprgrade As DataGridViewTextBoxColumn
    Friend WithEvents topddate As DataGridViewTextBoxColumn
    Friend WithEvents topdno As DataGridViewTextBoxColumn
    Friend WithEvents topdhour As DataGridViewTextBoxColumn
    Friend WithEvents ShippingOrderNo As DataGridViewTextBoxColumn
    Friend WithEvents spno As DataGridViewTextBoxColumn
    Friend WithEvents OrderNo As DataGridViewTextBoxColumn
    Friend WithEvents InvoiceNo As DataGridViewTextBoxColumn
    Friend WithEvents OrderDetailNo As DataGridViewTextBoxColumn
    Friend WithEvents SINo As DataGridViewTextBoxColumn
    Friend WithEvents BLNo As DataGridViewTextBoxColumn
    Friend WithEvents MOPI As DataGridViewTextBoxColumn
End Class

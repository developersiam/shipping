﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmBayControl
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Sp_Shipping_SEL_FromBayToBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_SEL_FromBayFromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_FromBayFromTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayFromTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Shipping_SEL_FromBayToTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayToTableAdapter()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.ToBayComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Shipping_SEL_ShippingBayToBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.FromBayComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Shipping_SEL_ShippingBayFromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.MetroGridFrom = New MetroFramework.Controls.MetroGrid()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_ShippingBayFromTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingBayFromTableAdapter()
        Me.Sp_Shipping_SEL_ShippingBayToTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingBayToTableAdapter()
        Me.MetroGridTo = New MetroFramework.Controls.MetroGrid()
        Me.MoveTile = New MetroFramework.Controls.MetroTile()
        Me.CBalesToMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.CBalesFromMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MixGradeCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.Sp_Shipping_SEL_GradeByBayBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_GradeByBayTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_GradeByBayTableAdapter()
        Me.FullMigrationCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.Rows1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradeDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetdefDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullMigration = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Rows1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.FullMigrationDate = New MetroFramework.Controls.MetroDateTime()
        Me.SaveTile = New MetroFramework.Controls.MetroTile()
        CType(Me.Sp_Shipping_SEL_FromBayToBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_FromBayFromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingBayToBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingBayFromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MetroGridFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.MetroGridTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_GradeByBayBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Sp_Shipping_SEL_FromBayToBindingSource
        '
        Me.Sp_Shipping_SEL_FromBayToBindingSource.DataMember = "sp_Shipping_SEL_FromBayTo"
        Me.Sp_Shipping_SEL_FromBayToBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_SEL_FromBayFromBindingSource
        '
        Me.Sp_Shipping_SEL_FromBayFromBindingSource.DataMember = "sp_Shipping_SEL_FromBayFrom"
        Me.Sp_Shipping_SEL_FromBayFromBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_FromBayFromTableAdapter
        '
        Me.Sp_Shipping_SEL_FromBayFromTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Shipping_SEL_FromBayToTableAdapter
        '
        Me.Sp_Shipping_SEL_FromBayToTableAdapter.ClearBeforeFill = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'ToBayComboBox
        '
        Me.ToBayComboBox.DataSource = Me.Sp_Shipping_SEL_ShippingBayToBindingSource
        Me.ToBayComboBox.DisplayMember = "BAY"
        Me.ToBayComboBox.FormattingEnabled = True
        Me.ToBayComboBox.ItemHeight = 24
        Me.ToBayComboBox.Location = New System.Drawing.Point(728, 133)
        Me.ToBayComboBox.Name = "ToBayComboBox"
        Me.ToBayComboBox.Size = New System.Drawing.Size(110, 30)
        Me.ToBayComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.ToBayComboBox.TabIndex = 199
        Me.ToBayComboBox.UseSelectable = True
        Me.ToBayComboBox.ValueMember = "BAY"
        '
        'Sp_Shipping_SEL_ShippingBayToBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingBayToBindingSource.DataMember = "sp_Shipping_SEL_ShippingBayTo"
        Me.Sp_Shipping_SEL_ShippingBayToBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(681, 136)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(63, 25)
        Me.MetroLabel1.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel1.TabIndex = 198
        Me.MetroLabel1.Text = "To Bay"
        '
        'FromBayComboBox
        '
        Me.FromBayComboBox.DataSource = Me.Sp_Shipping_SEL_ShippingBayFromBindingSource
        Me.FromBayComboBox.DisplayMember = "BAY"
        Me.FromBayComboBox.FormattingEnabled = True
        Me.FromBayComboBox.ItemHeight = 24
        Me.FromBayComboBox.Location = New System.Drawing.Point(96, 133)
        Me.FromBayComboBox.Name = "FromBayComboBox"
        Me.FromBayComboBox.Size = New System.Drawing.Size(106, 30)
        Me.FromBayComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.FromBayComboBox.TabIndex = 197
        Me.FromBayComboBox.UseSelectable = True
        Me.FromBayComboBox.ValueMember = "BAY"
        '
        'Sp_Shipping_SEL_ShippingBayFromBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingBayFromBindingSource.DataMember = "sp_Shipping_SEL_ShippingBayFrom"
        Me.Sp_Shipping_SEL_ShippingBayFromBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(23, 143)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(67, 20)
        Me.MetroLabel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel3.TabIndex = 196
        Me.MetroLabel3.Text = "From Bay"
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(811, 25)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel8.TabIndex = 116
        Me.MetroLabel8.Text = "refresh"
        '
        'MetroGridFrom
        '
        Me.MetroGridFrom.AllowUserToAddRows = False
        Me.MetroGridFrom.AllowUserToDeleteRows = False
        Me.MetroGridFrom.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGridFrom.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGridFrom.AutoGenerateColumns = False
        Me.MetroGridFrom.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGridFrom.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGridFrom.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGridFrom.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGridFrom.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGridFrom.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGridFrom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGridFrom.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rows1, Me.DataGridViewTextBoxColumn5, Me.GradeDataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn6, Me.NetdefDataGridViewTextBoxColumn1, Me.FullMigration})
        Me.MetroGridFrom.DataSource = Me.Sp_Shipping_SEL_FromBayFromBindingSource
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGridFrom.DefaultCellStyle = DataGridViewCellStyle4
        Me.MetroGridFrom.EnableHeadersVisualStyles = False
        Me.MetroGridFrom.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGridFrom.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGridFrom.Location = New System.Drawing.Point(23, 169)
        Me.MetroGridFrom.Name = "MetroGridFrom"
        Me.MetroGridFrom.ReadOnly = True
        Me.MetroGridFrom.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGridFrom.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.MetroGridFrom.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGridFrom.RowTemplate.Height = 28
        Me.MetroGridFrom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGridFrom.Size = New System.Drawing.Size(556, 558)
        Me.MetroGridFrom.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGridFrom.TabIndex = 194
        Me.MetroGridFrom.UseCustomBackColor = True
        Me.MetroGridFrom.UseCustomForeColor = True
        Me.MetroGridFrom.UseStyleColors = True
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel8)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(23, 48)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1237, 67)
        Me.MetroPanel1.TabIndex = 193
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(769, 20)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 125
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1069, 11)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 124
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(66, 26)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'Sp_Shipping_SEL_ShippingBayFromTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingBayFromTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_ShippingBayToTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingBayToTableAdapter.ClearBeforeFill = True
        '
        'MetroGridTo
        '
        Me.MetroGridTo.AllowDrop = True
        Me.MetroGridTo.AllowUserToAddRows = False
        Me.MetroGridTo.AllowUserToDeleteRows = False
        Me.MetroGridTo.AllowUserToResizeRows = False
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGridTo.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.MetroGridTo.AutoGenerateColumns = False
        Me.MetroGridTo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGridTo.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGridTo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGridTo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGridTo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGridTo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.MetroGridTo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGridTo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rows1DataGridViewTextBoxColumn, Me.Column1, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewCheckBoxColumn1})
        Me.MetroGridTo.DataSource = Me.Sp_Shipping_SEL_FromBayToBindingSource
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGridTo.DefaultCellStyle = DataGridViewCellStyle9
        Me.MetroGridTo.EnableHeadersVisualStyles = False
        Me.MetroGridTo.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGridTo.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGridTo.Location = New System.Drawing.Point(681, 169)
        Me.MetroGridTo.Name = "MetroGridTo"
        Me.MetroGridTo.ReadOnly = True
        Me.MetroGridTo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGridTo.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.MetroGridTo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGridTo.RowTemplate.Height = 28
        Me.MetroGridTo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGridTo.Size = New System.Drawing.Size(573, 558)
        Me.MetroGridTo.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGridTo.TabIndex = 200
        '
        'MoveTile
        '
        Me.MoveTile.ActiveControl = Nothing
        Me.MoveTile.AutoSize = True
        Me.MoveTile.BackColor = System.Drawing.Color.White
        Me.MoveTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MoveTile.Location = New System.Drawing.Point(597, 341)
        Me.MoveTile.Name = "MoveTile"
        Me.MoveTile.Size = New System.Drawing.Size(67, 61)
        Me.MoveTile.Style = MetroFramework.MetroColorStyle.White
        Me.MoveTile.TabIndex = 201
        Me.MoveTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MoveTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledChevron_Right64
        Me.MoveTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MoveTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MoveTile.UseSelectable = True
        Me.MoveTile.UseTileImage = True
        '
        'CBalesToMetroLabel
        '
        Me.CBalesToMetroLabel.AutoSize = True
        Me.CBalesToMetroLabel.BackColor = System.Drawing.Color.White
        Me.CBalesToMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.CBalesToMetroLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CBalesToMetroLabel.Location = New System.Drawing.Point(844, 136)
        Me.CBalesToMetroLabel.Name = "CBalesToMetroLabel"
        Me.CBalesToMetroLabel.Size = New System.Drawing.Size(63, 25)
        Me.CBalesToMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CBalesToMetroLabel.TabIndex = 202
        Me.CBalesToMetroLabel.Text = "To Bay"
        Me.CBalesToMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBalesToMetroLabel.UseCustomBackColor = True
        Me.CBalesToMetroLabel.UseCustomForeColor = True
        '
        'CBalesFromMetroLabel
        '
        Me.CBalesFromMetroLabel.AutoSize = True
        Me.CBalesFromMetroLabel.BackColor = System.Drawing.Color.White
        Me.CBalesFromMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.CBalesFromMetroLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CBalesFromMetroLabel.Location = New System.Drawing.Point(208, 138)
        Me.CBalesFromMetroLabel.Name = "CBalesFromMetroLabel"
        Me.CBalesFromMetroLabel.Size = New System.Drawing.Size(63, 25)
        Me.CBalesFromMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CBalesFromMetroLabel.TabIndex = 203
        Me.CBalesFromMetroLabel.Text = "To Bay"
        Me.CBalesFromMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBalesFromMetroLabel.UseCustomBackColor = True
        Me.CBalesFromMetroLabel.UseCustomForeColor = True
        '
        'MixGradeCheckBox
        '
        Me.MixGradeCheckBox.AutoSize = True
        Me.MixGradeCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.MixGradeCheckBox.Location = New System.Drawing.Point(1092, 136)
        Me.MixGradeCheckBox.Name = "MixGradeCheckBox"
        Me.MixGradeCheckBox.Size = New System.Drawing.Size(92, 20)
        Me.MixGradeCheckBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.MixGradeCheckBox.TabIndex = 204
        Me.MixGradeCheckBox.Text = "Mix grade"
        Me.MixGradeCheckBox.UseSelectable = True
        '
        'Sp_Shipping_SEL_GradeByBayBindingSource
        '
        Me.Sp_Shipping_SEL_GradeByBayBindingSource.DataMember = "sp_Shipping_SEL_GradeByBay"
        Me.Sp_Shipping_SEL_GradeByBayBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_GradeByBayTableAdapter
        '
        Me.Sp_Shipping_SEL_GradeByBayTableAdapter.ClearBeforeFill = True
        '
        'FullMigrationCheckBox
        '
        Me.FullMigrationCheckBox.AutoSize = True
        Me.FullMigrationCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.FullMigrationCheckBox.Location = New System.Drawing.Point(299, 141)
        Me.FullMigrationCheckBox.Name = "FullMigrationCheckBox"
        Me.FullMigrationCheckBox.Size = New System.Drawing.Size(117, 20)
        Me.FullMigrationCheckBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.FullMigrationCheckBox.TabIndex = 205
        Me.FullMigrationCheckBox.Text = "Full migration"
        Me.FullMigrationCheckBox.UseSelectable = True
        '
        'Rows1
        '
        Me.Rows1.DataPropertyName = "Rows1"
        Me.Rows1.HeaderText = "No."
        Me.Rows1.Name = "Rows1"
        Me.Rows1.ReadOnly = True
        Me.Rows1.Width = 66
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "BC"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Barcode"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 105
        '
        'GradeDataGridViewTextBoxColumn1
        '
        Me.GradeDataGridViewTextBoxColumn1.DataPropertyName = "grade"
        Me.GradeDataGridViewTextBoxColumn1.HeaderText = "Grade"
        Me.GradeDataGridViewTextBoxColumn1.Name = "GradeDataGridViewTextBoxColumn1"
        Me.GradeDataGridViewTextBoxColumn1.ReadOnly = True
        Me.GradeDataGridViewTextBoxColumn1.Width = 88
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "CASENO"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Case"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 76
        '
        'NetdefDataGridViewTextBoxColumn1
        '
        Me.NetdefDataGridViewTextBoxColumn1.DataPropertyName = "netdef"
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.NetdefDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle3
        Me.NetdefDataGridViewTextBoxColumn1.HeaderText = "Net"
        Me.NetdefDataGridViewTextBoxColumn1.Name = "NetdefDataGridViewTextBoxColumn1"
        Me.NetdefDataGridViewTextBoxColumn1.ReadOnly = True
        Me.NetdefDataGridViewTextBoxColumn1.Width = 67
        '
        'FullMigration
        '
        Me.FullMigration.DataPropertyName = "FullMigration"
        Me.FullMigration.HeaderText = "FullMigration"
        Me.FullMigration.Name = "FullMigration"
        Me.FullMigration.ReadOnly = True
        Me.FullMigration.Width = 125
        '
        'Rows1DataGridViewTextBoxColumn
        '
        Me.Rows1DataGridViewTextBoxColumn.DataPropertyName = "Rows1"
        Me.Rows1DataGridViewTextBoxColumn.HeaderText = "No."
        Me.Rows1DataGridViewTextBoxColumn.Name = "Rows1DataGridViewTextBoxColumn"
        Me.Rows1DataGridViewTextBoxColumn.ReadOnly = True
        Me.Rows1DataGridViewTextBoxColumn.Width = 66
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "bc"
        Me.Column1.HeaderText = "Barcode"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 105
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "grade"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Grade"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 88
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "caseno"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Case"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 76
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "netdef"
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn3.HeaderText = "Net"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 67
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "FullMigration"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "FullMigration"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.ReadOnly = True
        Me.DataGridViewCheckBoxColumn1.Width = 125
        '
        'FullMigrationDate
        '
        Me.FullMigrationDate.FontSize = MetroFramework.MetroDateTimeSize.Small
        Me.FullMigrationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FullMigrationDate.Location = New System.Drawing.Point(422, 136)
        Me.FullMigrationDate.MinimumSize = New System.Drawing.Size(0, 27)
        Me.FullMigrationDate.Name = "FullMigrationDate"
        Me.FullMigrationDate.Size = New System.Drawing.Size(106, 27)
        Me.FullMigrationDate.Style = MetroFramework.MetroColorStyle.Lime
        Me.FullMigrationDate.TabIndex = 206
        Me.FullMigrationDate.Visible = False
        '
        'SaveTile
        '
        Me.SaveTile.ActiveControl = Nothing
        Me.SaveTile.AutoSize = True
        Me.SaveTile.BackColor = System.Drawing.Color.White
        Me.SaveTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveTile.Location = New System.Drawing.Point(534, 133)
        Me.SaveTile.Name = "SaveTile"
        Me.SaveTile.Size = New System.Drawing.Size(36, 35)
        Me.SaveTile.Style = MetroFramework.MetroColorStyle.White
        Me.SaveTile.TabIndex = 207
        Me.SaveTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save32
        Me.SaveTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SaveTile.UseSelectable = True
        Me.SaveTile.UseTileImage = True
        Me.SaveTile.Visible = False
        '
        'FrmBayControl
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1541, 743)
        Me.Controls.Add(Me.SaveTile)
        Me.Controls.Add(Me.FullMigrationDate)
        Me.Controls.Add(Me.FullMigrationCheckBox)
        Me.Controls.Add(Me.MixGradeCheckBox)
        Me.Controls.Add(Me.CBalesFromMetroLabel)
        Me.Controls.Add(Me.CBalesToMetroLabel)
        Me.Controls.Add(Me.MoveTile)
        Me.Controls.Add(Me.MetroGridTo)
        Me.Controls.Add(Me.ToBayComboBox)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.FromBayComboBox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroGridFrom)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmBayControl"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        CType(Me.Sp_Shipping_SEL_FromBayToBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_FromBayFromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingBayToBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingBayFromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MetroGridFrom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.MetroGridTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_GradeByBayBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Sp_Shipping_SEL_FromBayToBindingSource As BindingSource
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_FromBayFromBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_FromBayFromTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayFromTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Shipping_SEL_FromBayToTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayToTableAdapter
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents ToBayComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FromBayComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroGridFrom As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_ShippingBayFromBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingBayFromTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingBayFromTableAdapter
    Friend WithEvents Sp_Shipping_SEL_ShippingBayToBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingBayToTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingBayToTableAdapter
    Friend WithEvents MetroGridTo As MetroFramework.Controls.MetroGrid
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MoveTile As MetroFramework.Controls.MetroTile
    Friend WithEvents BcDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CropDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PackingmatDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FormDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CustomerDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CasenoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents GrossdefDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TaredefDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents GrossrealDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MalcamDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HearsonDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BrabenderDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents NicDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents SugDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ClDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BayDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents WhDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents RcfromDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents IsnoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents IssuedDataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents IssuedtoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents IssueddateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents IssuedtimeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents IssuedreasonDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TopdtimeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents SpnoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PackingdateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PackingtimeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FeedingdateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FeedingtimeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DtrecordDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PackinguserDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BlendinguserDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents RepackeduserDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FrompddateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FromprgradeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FrompdnoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FrompdhourDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TopddateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ToprgradeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TopdnoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TopdhourDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PdtypeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BoxDataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents SpdateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents SptimeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents SpuserDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CustomercasenoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CustomergradeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents GradersDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CustomerrsDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PriceDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CropdefDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CompanyDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CasenotempDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BfnoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BftimesDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PackcostDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TransportcostDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents RedrycostDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ThrDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents RdyDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PendDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BoxtareDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PdremarkDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FrompdremarkDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TopdremarkDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PdremarkrsDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FullMigrationDataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents CropDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PackingmatDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FormDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GrossdefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TaredefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GrossrealDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MalcamDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents HearsonDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BrabenderDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NicDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SugDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ClDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BayDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents WhDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RcfromDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IsnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IssuedDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents IssuedtoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IssueddateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IssuedtimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IssuedreasonDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TopdtimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SpnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PackingdateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PackingtimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FeedingdateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FeedingtimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DtrecordDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PackinguserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BlendinguserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RepackeduserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FrompddateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FromprgradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FrompdnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FrompdhourDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TopddateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ToprgradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TopdnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TopdhourDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PdtypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BoxDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents SpdateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SptimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SpuserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomergradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GradersDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PriceDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CropdefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CompanyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CasenotempDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BfnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BftimesDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PackcostDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TransportcostDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RedrycostDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ThrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RdyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PendDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BoxtareDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PdremarkDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FrompdremarkDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TopdremarkDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PdremarkrsDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FullMigrationDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents CBalesToMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CBalesFromMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MixGradeCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents Sp_Shipping_SEL_GradeByBayBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_GradeByBayTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_GradeByBayTableAdapter
    Friend WithEvents FullMigrationCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents Rows1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents GradeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents NetdefDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents FullMigration As DataGridViewCheckBoxColumn
    Friend WithEvents Rows1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents FullMigrationDate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents SaveTile As MetroFramework.Controls.MetroTile
End Class

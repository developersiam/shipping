﻿Imports System.ComponentModel

Public Class FrmReceivedDetail
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Dim _movement As ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNoRow
    Public _movementNo As String

    Private Sub FrmReceivedDetail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername
            ReceivedLabel.Visible = False
            RCaseNo.Text = ""

            Me.Sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNo, _movementNo)
            _movement = ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNo.FindByMovementNo(_movementNo)
            If _movement Is Nothing Then
                MessageBox.Show("ไม่พบเลข Movement No. นี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _movement.MovementFromFinished = False Then
                MessageBox.Show("Movement no. นี้ได้ยังไม่ได้ Finished, กรุณาแจ้งต้นทางก่อนการ Received", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _movement.IsDateReceivedNull Then
                ReceivedLabel.Visible = False
            Else
                ReceivedLabel.Visible = True
            End If

            MovementNoTextbox.Text = _movement.MovementNo
            FromTextbox.Text = _movement.FromLocName
            TruckNoTextbox.Text = _movement.TruckNo

            BayBinding()
            DataGridBinding()

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BayBinding()
        Try
            Dim _bayTableAdapter As New ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingBayToTableAdapter
            Dim _bayDataTabale As New ShippingDataSet.sp_Shipping_SEL_ShippingBayToDataTable
            Dim bayList As New ShippingDataSet.sp_Shipping_SEL_ShippingBayToDataTable

            'กรณี destination เป็น Rodchana และ STEC จะให้แสดงตัวเลือกเฉพาะ bay 0 เท่านั้น
            'กรณี destination เป็น Bankru จะไม่โชว์ Bay 0

            _bayTableAdapter.Fill(_bayDataTabale)

            If Not _movement.ToStation.Contains("Baan") Then
                Dim row As DataRow = bayList.NewRow
                row("BAY") = 0
                bayList.Rows.Add(row)
            Else
                For Each row As DataRow In _bayDataTabale.Rows
                    If _movement.ToStation.Contains("Baan") Then
                        If row("BAY").ToString() <> "0" Then
                            Dim newRow = bayList.NewRow
                            newRow("BAY") = row("BAY")
                            bayList.Rows.Add(newRow)
                        End If
                    End If
                Next
            End If
            ToBayComboBox.DataSource = bayList
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Close()
    End Sub

    Private Sub ReceivedTile_Click(sender As Object, e As EventArgs) Handles ReceivedTile.Click
        Try
            If _movement.MovementNo = "" Then
                MessageBox.Show("ไม่มีข้อมูล Movement no.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim XInfoRow As ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNoRow
            Me.Sp_Shipping_SEL_ShippingMovementByMovementNoTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNo, _movement.MovementNo)
            XInfoRow = Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNo.FindByMovementNo(_movement.MovementNo)
            If XInfoRow Is Nothing Then
                MessageBox.Show("ไม่พบเลขMovement no.นี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not XInfoRow Is Nothing And Not XInfoRow.IsDateReceivedNull Then
                MessageBox.Show("Movement no. นี้ได้ Received ไปแล้ว ไม่สามารถทำซ้ำได้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการ Received Movement no. นี้ ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                db.sp_Shipping_MovementReceived(_movement.MovementNo, XUsername, _movement.FromStation, _movement.ToStation)
                MessageBox.Show("Received Movement No. นี้เรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ReceivedLabel.Visible = True
            End If

            'Me.Sp_Shipping_SEL_PDByMovementNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PDByMovementNo, MovementNoTextbox.Text)
            'CCase.Text = SpShippingSELPDByMovementNoBindingSource.Count

            'fill data to grid
            Me.Sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementBCByMovementNo, _movement.MovementNo)
            'Count  Bale  From ShippingMovementTransaction where MovementNo
            CCase.Text = SpShippingSELShippingMovementBCByMovementNoBindingSource.Count
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BCTextbox_KeyDown(sender As Object, e As KeyEventArgs) Handles BCTextbox.KeyDown

        If e.KeyCode = Keys.Enter Then

            'Check BC in Movement List
            Dim bcInfo As ShippingDataSet.sp_Shipping_SEL_BarcodeDetailFromPCRow
            Me.Sp_Shipping_SEL_BarcodeDetailFromPCTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_BarcodeDetailFromPC, BCTextbox.Text)
            bcInfo = Me.ShippingDataSet.sp_Shipping_SEL_BarcodeDetailFromPC.FirstOrDefault()
            If bcInfo Is Nothing Then
                MessageBox.Show("ไม่พบเลข Barcode กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            '**เงื่อนไขการรับยาเข้าระบบ
            '1.ต้นทางจะต้อง Finish movement ก่อนทุกครั้งเพื่อแจ้งให้ผู้รับทราบว่าข้อมูลยาที่จะส่งมาให้ผู้รับนั้นถูกต้องแล้ว
            '2.สถานะรหัส movement จะต้องไม่เป็น Received (ถูกรับแล้ว)
            '3.บาร์โค้ตที่จะสแกนรับจะต้องอยู่ในรหัส movement เดียวกันเท่านั้น รับยาข้าม movement ไม่ได้
            '4.กรณีรับยาที่ STEC 1 ไม่ต้องระบุ Bay

            If bcInfo.MovementNo <> _movement.MovementNo Then
                MessageBox.Show("รหัส Movement No. ไม่ตรงกัน ไม่สามารถรับเข้าได้ ที่ถูกต้องคือ #" + bcInfo.MovementNo,
                                "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _movement.MovementFromFinished <> True Then
                MessageBox.Show("Movement no. นี้ได้ยังไม่ได้ Finished, กรุณาแจ้งต้นทางก่อนการ Received",
                                "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _movement.IsDateReceivedNull = False Then
                MessageBox.Show("สถานะ Movement นี้ถูกเปลี่ยนเป็น Received (รับครบแล้ว) เมื่อ " + _movement.DateReceived,
                                "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _movement.ToStation = "STEC" And ToBayComboBox.Text <> "0" Then
                MessageBox.Show("กรณีการรับยาที่ STEC1 ให้เลือก Bay = 0",
                                "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'Update WH and Bay where by BC and Movement No.
            If ToBayComboBox.Text = "0" Then
                db.sp_Shipping_MovementReceivedByBC(BCTextbox.Text, "A000", _movement.ToStation)
            Else
                db.sp_Shipping_MovementReceivedByBC(BCTextbox.Text, ToBayComboBox.Text, _movement.ToStation)
            End If

            DataGridBinding()

            'Count if Total case = updated case
            '--- call old sp
            If RCaseNo.Text = CCase.Text Then
                db.sp_Shipping_MovementReceived(_movement.MovementNo, XUsername, _movement.FromStation, _movement.ToStation)
                MessageBox.Show("Received Movement No. นี้เรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ReceivedLabel.Visible = True
            End If

            BCTextbox.Text = ""
            BCTextbox.Focus()
        End If
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        DataGridBinding()
    End Sub

    Private Sub DataGridBinding()
        Try
            'fill data to grid
            Me.Sp_Shipping_SEL_ShippingMovementBCByMovementNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementBCByMovementNo, _movement.MovementNo)
            'Count  Bale  From ShippingMovementTransaction where MovementNo
            CCase.Text = SpShippingSELShippingMovementBCByMovementNoBindingSource.Count

            MetroGrid.Sort(MetroGrid.Columns(3), ListSortDirection.Descending)

            '---Count  Bale  From ShippingMovementTransaction which already moved by MovementNo
            Me.Sp_Shipping_SEL_ShippingMovementByMovementNoWHTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementNoWH, _movement.MovementNo, _movement.ToStation)
            If Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource.Count >= 0 Then
                RCaseNo.Text = Sp_Shipping_SEL_ShippingMovementByMovementNoWHBindingSource.Count
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
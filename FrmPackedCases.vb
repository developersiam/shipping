﻿Imports FactoryBL
Imports FactoryEntities
Imports FastMember

Public Class FrmPackedCases
    Inherits MetroFramework.Forms.MetroForm
    Dim _pdList As List(Of sp_PD_SEL_PackedCasesByGrade_Result)
    Dim _packedgradeList As List(Of packedgrade)
    Dim _cropList As List(Of Int32)

    Private Sub PackedGradeBinding()
        Try
            If String.IsNullOrEmpty(CropCombobox.Text) Then
                Return
            End If

            _packedgradeList = Facade.packedgradeBL().GetByCrop(Convert.ToInt16(CropCombobox.Text)).OrderBy(Function(x) x.packedgrade1).ToList()
            GradeComboBox.DataSource = _packedgradeList
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub pdListBinding()
        Try
            If String.IsNullOrEmpty(GradeComboBox.Text) Then
                Return
            End If

            _pdList = Facade.pdBL().GetByGradeSP(GradeComboBox.Text).OrderBy(Function(x) x.caseno).ToList()

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_pdList)
            dt.Load(reader)
            pdMetroGrid.DataSource = dt
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FrmPackedCases_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            _cropList = Facade.packedgradeBL().GetPackedCrop().OrderByDescending(Function(x) x).ToList()
            CropCombobox.DataSource = _cropList
            pdMetroGrid.AutoGenerateColumns = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub GradeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GradeComboBox.SelectedIndexChanged
        pdListBinding()
    End Sub

    Private Sub CropCombobox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CropCombobox.SelectedIndexChanged
        PackedGradeBinding()
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        pdListBinding()
    End Sub
End Class
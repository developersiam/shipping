﻿Imports System.Text.RegularExpressions

Public Class FrmEditOrderMaster
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Dim XNetOld As Int32
    Dim XtotalCaseOld As Int32
    Dim XtotalWeightOld As Double
    Private Sub FrmEditOrderMaster_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            Me.Sp_Shipping_SEL_CustomerTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Customer)
            Me.CropFromPackedGradeTableAdapter.Fill(Me.ShippingDataSet.CropFromPackedGrade)
            Me.TypeTableAdapter.Fill(Me.ShippingDataSet.type)

            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 1, XOrderDetailNoTextBox.Text)
            OrderNoTextBox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).orderno
            ReceivedDate.Value = ShippingDataSet.sp_Shipping_SEL_Order.Item(0)._date
            SupplierComboBox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).supplier
            InvoicingCompComBoBox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).invoicingcom
            CustomerTextBox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).customer
            PackedGradeTextBox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).grade
            CustomerGradeTextbox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).CustomerGrade
            TotalWeightRequestTextbox.Text = Convert.ToDecimal(ShippingDataSet.sp_Shipping_SEL_Order.Item(0).nett).ToString("N2")
            XtotalWeightOld = Convert.ToDouble(Val(ShippingDataSet.sp_Shipping_SEL_Order.Item(0).nett))
            TotalCaseRequestTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_Order.Item(0).cases)
            XtotalCaseOld = Convert.ToDouble(Val(ShippingDataSet.sp_Shipping_SEL_Order.Item(0).cases))
            NetRequestTextBox.Text = String.Format("{0:0.0,#}", ShippingDataSet.sp_Shipping_SEL_Order.Item(0).net)
            XNetOld = Convert.ToDouble(Val(ShippingDataSet.sp_Shipping_SEL_Order.Item(0).net))


            If PackedGradeTextBox.Text <> "" Then
                Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, PackedGradeTextBox.Text, CustomerTextBox.Text)
                CurrentStockTextBox.Text = Sp_Shipping_SEL_PackedgradeByGradersBindingSource.Count

                Dim XSumOfCurrentWeight As Double
                XSumOfCurrentWeight = IIf(IsDBNull(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' ")), 0, Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' "))
                CurrentWeightTextBox.Text = XSumOfCurrentWeight.ToString("N2")
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub InfoTile_Click(sender As Object, e As EventArgs) Handles InfoTile.Click
        Try
            Dim XGrader As String
            Dim XCustomer2 As String

            XGrader = PackedGradeTextBox.Text
            XCustomer2 = CustomerTextBox.Text
            Dim xfrm As New FrmShowPackedGradeDetail
            xfrm.XGraders = XGrader
            xfrm.XCustomer = XCustomer2
            xfrm.ShowDialog()
            xfrm.Dispose()


            If PackedGradeTextBox.Text <> "" Then
                Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, PackedGradeTextBox.Text, CustomerTextBox.Text)
                CurrentStockTextBox.Text = Sp_Shipping_SEL_PackedgradeByGradersBindingSource.Count

                Dim XSumOfCurrentWeight As Double
                XSumOfCurrentWeight = IIf(IsDBNull(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' ")), 0, Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' "))
                CurrentWeightTextBox.Text = XSumOfCurrentWeight.ToString("N2")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub TotalCaseRequestTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles TotalCaseRequestTextBox.KeyUp
        Try
            If NetRequestTextBox.Text <> "" AndAlso TotalCaseRequestTextBox.Text <> "" Then
                TotalWeightRequestTextbox.Text = Convert.ToDouble(NetRequestTextBox.Text) * Convert.ToDouble(TotalCaseRequestTextBox.Text)
            Else
                TotalWeightRequestTextbox.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub TotalCaseRequestTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TotalCaseRequestTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TotalWeightRequestTextbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TotalWeightRequestTextbox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NetRequestTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NetRequestTextBox.KeyPress
        Try
            'If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AddTile_Click(sender As Object, e As EventArgs) Handles AddTile.Click
        Try
            If _security Is Nothing Then
                Throw New Exception("ไม่พบ user account นี้ในระบบ โปรดทำการ login อีกครั้ง")
            End If

            If _security.pdtransfer = False And _security.level <> "Admin" Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            Dim XChkOrder As Boolean = True
            'Get data from movement  
            Dim XInfoRow As ShippingDataSet.sp_Shipping_SEL_CustomerRow
            'Me.Sp_Shipping_SEL_CustomerTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Customer)
            XInfoRow = ShippingDataSet.sp_Shipping_SEL_Customer.FindByCODE(CustomerTextBox.Text)
            If XInfoRow Is Nothing Then
                MessageBox.Show("ไม่พบลูกค้านี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not XInfoRow Is Nothing Then
                XChkOrder = XInfoRow.UseOrders
            End If

            Dim XChkWeight As Boolean = True
            Dim XChkCase As Boolean = True

            'ถ้า XchkOrder = true จะต้องระบุ Order from sale, case ,weight  แต่ถ้าเป็น false ไม่ต้องระบุ3ค่านี้ก็ได้
            If XChkOrder = True Then
                If OrderNoTextBox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ order from sales !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If NetRequestTextBox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ net request !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If TotalWeightRequestTextbox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Total weight from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Val(TotalWeightRequestTextbox.Text) > Val(CurrentWeightTextBox.Text) Then
                    Dim result As DialogResult
                    result = MessageBox.Show("น้ำหนักที่ลูกค้าต้องการมีมากกว่าน้ำหนักสินค้าที่เรามีอยู่ ต้องการบันทึกข้อมูลหรือไม่?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    If result = DialogResult.No Then
                        XChkWeight = False
                    End If
                End If

                If TotalCaseRequestTextBox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Total case from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Val(TotalCaseRequestTextBox.Text) > Val(CurrentStockTextBox.Text) Then
                    Dim result As DialogResult
                    result = MessageBox.Show("จำนวนกล่องที่ลูกค้าต้องการมีมากกว่าที่เรามีอยู่ ต้องการบันทึกข้อมูลหรือไม่?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    If result = DialogResult.No Then
                        XChkCase = False
                    End If
                End If
            End If



            If CustomerTextBox.Text = "" Then
                MessageBox.Show("กรุณาระบุ Customer !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If PackedGradeTextBox.Text = "" Then
                MessageBox.Show("กรุณาระบุ PackedGrade(Internal grade) !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'Dim XChkWeight As Boolean = True
            'If TotalWeightRequestTextbox.Text = "" Then
            '    MessageBox.Show("กรุณาระบุ Total weight from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    Return
            'ElseIf Val(TotalWeightRequestTextbox.Text) > Val(CurrentWeightTextBox.Text) Then
            '    Dim result As DialogResult
            '    result = MessageBox.Show("น้ำหนักที่ลูกค้าต้องการมีมากกว่าน้ำหนักสินค้าที่เรามีอยู่ ต้องการบันทึกข้อมูลหรือไม่?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            '    If result = DialogResult.No Then
            '        XChkWeight = False
            '    End If
            'End If

            'Dim XChkCase As Boolean = True
            'If TotalCaseRequestTextBox.Text = "" Then
            '    MessageBox.Show("กรุณาระบุ Total case from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    Return
            'ElseIf Val(TotalCaseRequestTextBox.Text) > Val(CurrentStockTextBox.Text) Then
            '    Dim result As DialogResult
            '    result = MessageBox.Show("จำนวนกล่องที่ลูกค้าต้องการมีมากกว่าที่เรามีอยู่ ต้องการบันทึกข้อมูลหรือไม่?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            '    If result = DialogResult.No Then
            '        XChkCase = False
            '    End If
            'End If

            'โปรแกรมจะบันทึกข้อมูลให้ก็ต่อเมื่อ น้ำหนักสินค้าเรามีพอสำหรับลูกค้าหรือผู้บันทึก Accept และจำนวนกล่องเรามีพอสำหรับลูกค้าหรือผู้บันทึก Accept เท่านั้น
            If XChkWeight = True AndAlso XChkCase = True Then
                Dim XOrderDetailNo As String
                'Get OrderNo from database 
                Sp_Shipping_GETMAX_OrderDetailNoTableAdapter.Fill(ShippingDataSet.sp_Shipping_GETMAX_OrderDetailNo)
                XOrderDetailNo = ShippingDataSet.sp_Shipping_GETMAX_OrderDetailNo.Item(0).OrderDetailNo

                db.sp_Shipping_UPD_Order(OrderNoTextBox.Text, ReceivedDate.Value, SupplierComboBox.Text, InvoicingCompComBoBox.Text, CustomerTextBox.Text _
                                         , PackedGradeTextBox.Text, Convert.ToDouble(Val(TotalCaseRequestTextBox.Text)), Convert.ToDouble(Val(TotalWeightRequestTextbox.Text)) _
                                         , XUsername, Convert.ToDouble(Val(NetRequestTextBox.Text)), XOrderDetailNoTextBox.Text _
                                         , CustomerGradeTextbox.Text, Convert.ToDouble(Val(XtotalCaseOld)), Convert.ToDouble(Val(XtotalWeightOld)), Convert.ToDouble(Val(XNetOld)))


                MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NetRequestTextBox_Leave(sender As Object, e As EventArgs) Handles NetRequestTextBox.Leave
        Try
            Dim regex As Regex = New Regex("^[0-9]*(\.[0-9]{1,4})?$")
            Dim match As Match = regex.Match(NetRequestTextBox.Text)
            NetRequestTextBox.Text = If(match.Success, NetRequestTextBox.Text, "")
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
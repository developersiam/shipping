﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAddMovementTruck
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.DriverTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.TruckNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.TruckIDTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.SuspendLayout()
        '
        'DriverTextbox
        '
        '
        '
        '
        Me.DriverTextbox.CustomButton.Image = Nothing
        Me.DriverTextbox.CustomButton.Location = New System.Drawing.Point(383, 2)
        Me.DriverTextbox.CustomButton.Name = ""
        Me.DriverTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.DriverTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.DriverTextbox.CustomButton.TabIndex = 1
        Me.DriverTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.DriverTextbox.CustomButton.UseSelectable = True
        Me.DriverTextbox.CustomButton.Visible = False
        Me.DriverTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.DriverTextbox.Lines = New String(-1) {}
        Me.DriverTextbox.Location = New System.Drawing.Point(335, 330)
        Me.DriverTextbox.MaxLength = 32767
        Me.DriverTextbox.Name = "DriverTextbox"
        Me.DriverTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.DriverTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.DriverTextbox.SelectedText = ""
        Me.DriverTextbox.SelectionLength = 0
        Me.DriverTextbox.SelectionStart = 0
        Me.DriverTextbox.Size = New System.Drawing.Size(411, 30)
        Me.DriverTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.DriverTextbox.TabIndex = 2
        Me.DriverTextbox.UseSelectable = True
        Me.DriverTextbox.UseStyleColors = True
        Me.DriverTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.DriverTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(236, 330)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(47, 20)
        Me.MetroLabel4.TabIndex = 100
        Me.MetroLabel4.Text = "Driver"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(823, 81)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 99
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(748, 81)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 98
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save50
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'TruckNoTextbox
        '
        '
        '
        '
        Me.TruckNoTextbox.CustomButton.Image = Nothing
        Me.TruckNoTextbox.CustomButton.Location = New System.Drawing.Point(383, 2)
        Me.TruckNoTextbox.CustomButton.Name = ""
        Me.TruckNoTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TruckNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoTextbox.CustomButton.TabIndex = 1
        Me.TruckNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoTextbox.CustomButton.UseSelectable = True
        Me.TruckNoTextbox.CustomButton.Visible = False
        Me.TruckNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TruckNoTextbox.Lines = New String(-1) {}
        Me.TruckNoTextbox.Location = New System.Drawing.Point(335, 294)
        Me.TruckNoTextbox.MaxLength = 32767
        Me.TruckNoTextbox.Name = "TruckNoTextbox"
        Me.TruckNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoTextbox.SelectedText = ""
        Me.TruckNoTextbox.SelectionLength = 0
        Me.TruckNoTextbox.SelectionStart = 0
        Me.TruckNoTextbox.Size = New System.Drawing.Size(411, 30)
        Me.TruckNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckNoTextbox.TabIndex = 1
        Me.TruckNoTextbox.UseSelectable = True
        Me.TruckNoTextbox.UseStyleColors = True
        Me.TruckNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(236, 294)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(68, 20)
        Me.MetroLabel2.TabIndex = 94
        Me.MetroLabel2.Text = "Truck No."
        '
        'TruckIDTextbox
        '
        Me.TruckIDTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TruckIDTextbox.CustomButton.Image = Nothing
        Me.TruckIDTextbox.CustomButton.Location = New System.Drawing.Point(76, 2)
        Me.TruckIDTextbox.CustomButton.Name = ""
        Me.TruckIDTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TruckIDTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckIDTextbox.CustomButton.TabIndex = 1
        Me.TruckIDTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckIDTextbox.CustomButton.UseSelectable = True
        Me.TruckIDTextbox.CustomButton.Visible = False
        Me.TruckIDTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TruckIDTextbox.Lines = New String(-1) {}
        Me.TruckIDTextbox.Location = New System.Drawing.Point(335, 258)
        Me.TruckIDTextbox.MaxLength = 4
        Me.TruckIDTextbox.Name = "TruckIDTextbox"
        Me.TruckIDTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckIDTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckIDTextbox.SelectedText = ""
        Me.TruckIDTextbox.SelectionLength = 0
        Me.TruckIDTextbox.SelectionStart = 0
        Me.TruckIDTextbox.Size = New System.Drawing.Size(104, 30)
        Me.TruckIDTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckIDTextbox.TabIndex = 0
        Me.TruckIDTextbox.UseSelectable = True
        Me.TruckIDTextbox.UseStyleColors = True
        Me.TruckIDTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckIDTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(236, 258)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(61, 20)
        Me.MetroLabel1.TabIndex = 92
        Me.MetroLabel1.Text = "Truck ID."
        '
        'FrmAddMovementTruck
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1090, 589)
        Me.Controls.Add(Me.DriverTextbox)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.AddMetroTile)
        Me.Controls.Add(Me.TruckNoTextbox)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.TruckIDTextbox)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "FrmAddMovementTruck"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Add  new movement truck"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DriverTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents TruckNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TruckIDTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
End Class

﻿Module MessageBoxModule
    Public Sub Warning(ByVal message As String)
        MessageBox.Show(message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    End Sub

    Public Sub Info(ByVal message As String)
        MessageBox.Show(message, "info", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Sub Danger(ByVal ex As Exception)
        If IsDBNull(ex.InnerException) Then
            MessageBox.Show(ex.Message, "error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show(ex.InnerException.Message, "error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Public Function Question(ByVal message As String) As MessageBoxOptions
        Return MessageBox.Show(message, "warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
    End Function

    Public Function Exception(ByVal ex As Exception) As MessageBoxOptions
        Dim message = ""
        If ex.InnerException IsNot Nothing Then
            message = ex.InnerException.Message
        Else
            message = ex.Message
        End If
        Return MessageBox.Show(message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    End Function
End Module

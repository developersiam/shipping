﻿Public Class FrmReceived
    Inherits MetroFramework.Forms.MetroForm
    Dim XMovementStatusID As Int16
    Dim XMovementNo As String
    Private Sub FrmReceived_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername


            'Show ShippingLocation and filter  where Status = 1 only.
            Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingLocation)
            SpShippingSELShippingLocationBindingSource.Filter = "Status = 1"


            MovementStatusCombobox.SelectedIndex = 0
            XMovementStatusID = MovementStatusCombobox.SelectedIndex
            Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementStatusID, XMovementStatusID, ShippingLocationComboBox.SelectedValue)

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub ShippingLocationComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ShippingLocationComboBox.SelectedIndexChanged
        Try
            XMovementStatusID = MovementStatusCombobox.SelectedIndex
            Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementStatusID, XMovementStatusID, ShippingLocationComboBox.SelectedValue)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MovementStatusCombobox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MovementStatusCombobox.SelectedIndexChanged
        Try
            XMovementStatusID = MovementStatusCombobox.SelectedIndex
            Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementStatusID, XMovementStatusID, ShippingLocationComboBox.SelectedValue)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            XMovementStatusID = MovementStatusCombobox.SelectedIndex
            Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementStatusID, XMovementStatusID, ShippingLocationComboBox.SelectedValue)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub DetailTile_Click(sender As Object, e As EventArgs) Handles DetailTile.Click
        Try
            If XMovementNo = "" Then
                MessageBox.Show("กรุณาเลือกแถวที่ต้องการจากตาราง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            Dim xfrm As New FrmReceivedDetail
            xfrm._movementNo = XMovementNo
            xfrm.ShowDialog()

            XMovementStatusID = MovementStatusCombobox.SelectedIndex
            Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementByMovementStatusID, XMovementStatusID, ShippingLocationComboBox.SelectedValue)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid.CellMouseClick
        Try
            XMovementNo = MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
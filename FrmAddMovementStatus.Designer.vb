﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAddMovementStatus
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.StatusNameTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.StatusIDTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.SuspendLayout()
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(779, 78)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 107
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(704, 78)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 106
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save50
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'StatusNameTextbox
        '
        '
        '
        '
        Me.StatusNameTextbox.CustomButton.Image = Nothing
        Me.StatusNameTextbox.CustomButton.Location = New System.Drawing.Point(383, 2)
        Me.StatusNameTextbox.CustomButton.Name = ""
        Me.StatusNameTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.StatusNameTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.StatusNameTextbox.CustomButton.TabIndex = 1
        Me.StatusNameTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.StatusNameTextbox.CustomButton.UseSelectable = True
        Me.StatusNameTextbox.CustomButton.Visible = False
        Me.StatusNameTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.StatusNameTextbox.Lines = New String(-1) {}
        Me.StatusNameTextbox.Location = New System.Drawing.Point(291, 291)
        Me.StatusNameTextbox.MaxLength = 32767
        Me.StatusNameTextbox.Name = "StatusNameTextbox"
        Me.StatusNameTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.StatusNameTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.StatusNameTextbox.SelectedText = ""
        Me.StatusNameTextbox.SelectionLength = 0
        Me.StatusNameTextbox.SelectionStart = 0
        Me.StatusNameTextbox.Size = New System.Drawing.Size(411, 30)
        Me.StatusNameTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.StatusNameTextbox.TabIndex = 1
        Me.StatusNameTextbox.UseSelectable = True
        Me.StatusNameTextbox.UseStyleColors = True
        Me.StatusNameTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.StatusNameTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(192, 291)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(84, 20)
        Me.MetroLabel2.TabIndex = 105
        Me.MetroLabel2.Text = "Status name"
        '
        'StatusIDTextbox
        '
        Me.StatusIDTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.StatusIDTextbox.CustomButton.Image = Nothing
        Me.StatusIDTextbox.CustomButton.Location = New System.Drawing.Point(76, 2)
        Me.StatusIDTextbox.CustomButton.Name = ""
        Me.StatusIDTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.StatusIDTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.StatusIDTextbox.CustomButton.TabIndex = 1
        Me.StatusIDTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.StatusIDTextbox.CustomButton.UseSelectable = True
        Me.StatusIDTextbox.CustomButton.Visible = False
        Me.StatusIDTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.StatusIDTextbox.Lines = New String(-1) {}
        Me.StatusIDTextbox.Location = New System.Drawing.Point(291, 255)
        Me.StatusIDTextbox.MaxLength = 2
        Me.StatusIDTextbox.Name = "StatusIDTextbox"
        Me.StatusIDTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.StatusIDTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.StatusIDTextbox.SelectedText = ""
        Me.StatusIDTextbox.SelectionLength = 0
        Me.StatusIDTextbox.SelectionStart = 0
        Me.StatusIDTextbox.Size = New System.Drawing.Size(104, 30)
        Me.StatusIDTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.StatusIDTextbox.TabIndex = 0
        Me.StatusIDTextbox.UseSelectable = True
        Me.StatusIDTextbox.UseStyleColors = True
        Me.StatusIDTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.StatusIDTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(192, 255)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(62, 20)
        Me.MetroLabel1.TabIndex = 104
        Me.MetroLabel1.Text = "Stauts ID"
        '
        'FrmAddMovementStatus
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1026, 557)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.AddMetroTile)
        Me.Controls.Add(Me.StatusNameTextbox)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.StatusIDTextbox)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "FrmAddMovementStatus"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Add new movement status"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents StatusNameTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents StatusIDTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmUpdateShippingLocation
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LocNameTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.LocIDTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.LocIDlable = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.Status = New MetroFramework.Controls.MetroCheckBox()
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_SEL_ShippingLocationBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingLocationTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingLocationTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.StationComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.WareHouseBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.WareHouseTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.WareHouseTableAdapter()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingLocationBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WareHouseBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LocNameTextbox
        '
        '
        '
        '
        Me.LocNameTextbox.CustomButton.Image = Nothing
        Me.LocNameTextbox.CustomButton.Location = New System.Drawing.Point(383, 2)
        Me.LocNameTextbox.CustomButton.Name = ""
        Me.LocNameTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.LocNameTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.LocNameTextbox.CustomButton.TabIndex = 1
        Me.LocNameTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LocNameTextbox.CustomButton.UseSelectable = True
        Me.LocNameTextbox.CustomButton.Visible = False
        Me.LocNameTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.LocNameTextbox.Lines = New String(-1) {}
        Me.LocNameTextbox.Location = New System.Drawing.Point(174, 261)
        Me.LocNameTextbox.MaxLength = 32767
        Me.LocNameTextbox.Name = "LocNameTextbox"
        Me.LocNameTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.LocNameTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.LocNameTextbox.SelectedText = ""
        Me.LocNameTextbox.SelectionLength = 0
        Me.LocNameTextbox.SelectionStart = 0
        Me.LocNameTextbox.Size = New System.Drawing.Size(411, 30)
        Me.LocNameTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.LocNameTextbox.TabIndex = 127
        Me.LocNameTextbox.UseSelectable = True
        Me.LocNameTextbox.UseStyleColors = True
        Me.LocNameTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.LocNameTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(75, 261)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(84, 20)
        Me.MetroLabel3.TabIndex = 129
        Me.MetroLabel3.Text = "Status name"
        '
        'LocIDTextbox
        '
        Me.LocIDTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.LocIDTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.LocIDTextbox.CustomButton.Image = Nothing
        Me.LocIDTextbox.CustomButton.Location = New System.Drawing.Point(76, 2)
        Me.LocIDTextbox.CustomButton.Name = ""
        Me.LocIDTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.LocIDTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.LocIDTextbox.CustomButton.TabIndex = 1
        Me.LocIDTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LocIDTextbox.CustomButton.UseSelectable = True
        Me.LocIDTextbox.CustomButton.Visible = False
        Me.LocIDTextbox.Enabled = False
        Me.LocIDTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.LocIDTextbox.Lines = New String(-1) {}
        Me.LocIDTextbox.Location = New System.Drawing.Point(174, 225)
        Me.LocIDTextbox.MaxLength = 2
        Me.LocIDTextbox.Name = "LocIDTextbox"
        Me.LocIDTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.LocIDTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.LocIDTextbox.SelectedText = ""
        Me.LocIDTextbox.SelectionLength = 0
        Me.LocIDTextbox.SelectionStart = 0
        Me.LocIDTextbox.Size = New System.Drawing.Size(104, 30)
        Me.LocIDTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.LocIDTextbox.TabIndex = 126
        Me.LocIDTextbox.UseCustomBackColor = True
        Me.LocIDTextbox.UseSelectable = True
        Me.LocIDTextbox.UseStyleColors = True
        Me.LocIDTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.LocIDTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LocIDlable
        '
        Me.LocIDlable.AutoSize = True
        Me.LocIDlable.Location = New System.Drawing.Point(75, 225)
        Me.LocIDlable.Name = "LocIDlable"
        Me.LocIDlable.Size = New System.Drawing.Size(62, 20)
        Me.LocIDlable.TabIndex = 128
        Me.LocIDlable.Text = "Stauts ID"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(653, 67)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 124
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(578, 67)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 123
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save50
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'Status
        '
        Me.Status.AutoSize = True
        Me.Status.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.Status.Location = New System.Drawing.Point(174, 354)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(137, 20)
        Me.Status.Style = MetroFramework.MetroColorStyle.Orange
        Me.Status.TabIndex = 125
        Me.Status.Text = "MetroCheckBox1"
        Me.Status.UseSelectable = True
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_SEL_ShippingLocationBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingLocationBindingSource.DataMember = "sp_Shipping_SEL_ShippingLocation"
        Me.Sp_Shipping_SEL_ShippingLocationBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingLocationTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'StationComboBox
        '
        Me.StationComboBox.BackColor = System.Drawing.Color.White
        Me.StationComboBox.DataSource = Me.WareHouseBindingSource
        Me.StationComboBox.DisplayMember = "wh"
        Me.StationComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.StationComboBox.FormattingEnabled = True
        Me.StationComboBox.ItemHeight = 29
        Me.StationComboBox.Location = New System.Drawing.Point(174, 297)
        Me.StationComboBox.Name = "StationComboBox"
        Me.StationComboBox.Size = New System.Drawing.Size(224, 35)
        Me.StationComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.StationComboBox.TabIndex = 193
        Me.StationComboBox.UseCustomBackColor = True
        Me.StationComboBox.UseSelectable = True
        Me.StationComboBox.ValueMember = "wh"
        '
        'WareHouseBindingSource
        '
        Me.WareHouseBindingSource.DataMember = "WareHouse"
        Me.WareHouseBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(118, 312)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(50, 20)
        Me.MetroLabel7.TabIndex = 194
        Me.MetroLabel7.Text = "Station"
        '
        'WareHouseTableAdapter
        '
        Me.WareHouseTableAdapter.ClearBeforeFill = True
        '
        'FrmUpdateShippingLocation
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(989, 636)
        Me.Controls.Add(Me.StationComboBox)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.LocNameTextbox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.LocIDTextbox)
        Me.Controls.Add(Me.LocIDlable)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.AddMetroTile)
        Me.Controls.Add(Me.Status)
        Me.Name = "FrmUpdateShippingLocation"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Update  shipping location"
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingLocationBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WareHouseBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LocNameTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents LocIDTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LocIDlable As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents Status As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_ShippingLocationBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingLocationTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingLocationTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents StationComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents WareHouseBindingSource As BindingSource
    Friend WithEvents WareHouseTableAdapter As ShippingDataSetTableAdapters.WareHouseTableAdapter
End Class

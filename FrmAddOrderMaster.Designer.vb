﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAddOrderMaster
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.AddTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel23 = New MetroFramework.Controls.MetroLabel()
        Me.ReceivedDate = New MetroFramework.Controls.MetroDateTime()
        Me.MetroLabel22 = New MetroFramework.Controls.MetroLabel()
        Me.NetRequestTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel21 = New MetroFramework.Controls.MetroLabel()
        Me.InfoTile = New MetroFramework.Controls.MetroTile()
        Me.CurrentWeightTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.CurrentStockTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        Me.SearchTile = New MetroFramework.Controls.MetroTile()
        Me.OrderNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel19 = New MetroFramework.Controls.MetroLabel()
        Me.CropComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.CropFromPackedGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.TotalCaseRequestTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.TotalWeightRequestTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.CustomerGradeTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.PackedGradeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Shipping_SEL_PackedgradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Shipping_SEL_CustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_CustomerTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_CustomerTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.TypeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.typeTableAdapter()
        Me.CropFromPackedGradeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.CropFromPackedGradeTableAdapter()
        Me.Sp_Shipping_SEL_PackedgradeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeTableAdapter()
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter()
        Me.Sp_Shipping_GETMAX_OrderDetailNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_GETMAX_OrderDetailNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_GETMAX_OrderDetailNoTableAdapter()
        Me.SupplierComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.InvoicingCompComBoBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.CropFromPackedGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_CustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_GETMAX_OrderDetailNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.AddTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(23, 63)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(425, 62)
        Me.MetroPanel1.TabIndex = 188
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'AddTile
        '
        Me.AddTile.ActiveControl = Nothing
        Me.AddTile.AutoSize = True
        Me.AddTile.BackColor = System.Drawing.Color.White
        Me.AddTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddTile.Location = New System.Drawing.Point(179, 16)
        Me.AddTile.Name = "AddTile"
        Me.AddTile.Size = New System.Drawing.Size(36, 35)
        Me.AddTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddTile.TabIndex = 130
        Me.AddTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save32
        Me.AddTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddTile.UseSelectable = True
        Me.AddTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(221, 22)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(34, 19)
        Me.MetroLabel2.TabIndex = 129
        Me.MetroLabel2.Text = "save"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(370, 7)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroLabel23
        '
        Me.MetroLabel23.AutoSize = True
        Me.MetroLabel23.Location = New System.Drawing.Point(108, 243)
        Me.MetroLabel23.Name = "MetroLabel23"
        Me.MetroLabel23.Size = New System.Drawing.Size(57, 19)
        Me.MetroLabel23.TabIndex = 318
        Me.MetroLabel23.Text = "Supplier"
        '
        'ReceivedDate
        '
        Me.ReceivedDate.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.ReceivedDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ReceivedDate.Location = New System.Drawing.Point(176, 192)
        Me.ReceivedDate.MinimumSize = New System.Drawing.Size(0, 35)
        Me.ReceivedDate.Name = "ReceivedDate"
        Me.ReceivedDate.Size = New System.Drawing.Size(272, 35)
        Me.ReceivedDate.Style = MetroFramework.MetroColorStyle.Lime
        Me.ReceivedDate.TabIndex = 1
        '
        'MetroLabel22
        '
        Me.MetroLabel22.AutoSize = True
        Me.MetroLabel22.Location = New System.Drawing.Point(120, 207)
        Me.MetroLabel22.Name = "MetroLabel22"
        Me.MetroLabel22.Size = New System.Drawing.Size(36, 19)
        Me.MetroLabel22.TabIndex = 316
        Me.MetroLabel22.Text = "Date"
        '
        'NetRequestTextBox
        '
        '
        '
        '
        Me.NetRequestTextBox.CustomButton.Image = Nothing
        Me.NetRequestTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.NetRequestTextBox.CustomButton.Name = ""
        Me.NetRequestTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.NetRequestTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NetRequestTextBox.CustomButton.TabIndex = 1
        Me.NetRequestTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NetRequestTextBox.CustomButton.UseSelectable = True
        Me.NetRequestTextBox.CustomButton.Visible = False
        Me.NetRequestTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.NetRequestTextBox.Lines = New String(-1) {}
        Me.NetRequestTextBox.Location = New System.Drawing.Point(176, 535)
        Me.NetRequestTextBox.MaxLength = 32767
        Me.NetRequestTextBox.Name = "NetRequestTextBox"
        Me.NetRequestTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NetRequestTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NetRequestTextBox.SelectedText = ""
        Me.NetRequestTextBox.SelectionLength = 0
        Me.NetRequestTextBox.SelectionStart = 0
        Me.NetRequestTextBox.ShortcutsEnabled = True
        Me.NetRequestTextBox.Size = New System.Drawing.Size(272, 40)
        Me.NetRequestTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.NetRequestTextBox.TabIndex = 9
        Me.NetRequestTextBox.UseSelectable = True
        Me.NetRequestTextBox.UseStyleColors = True
        Me.NetRequestTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NetRequestTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel21
        '
        Me.MetroLabel21.AutoSize = True
        Me.MetroLabel21.Location = New System.Drawing.Point(49, 544)
        Me.MetroLabel21.Name = "MetroLabel21"
        Me.MetroLabel21.Size = New System.Drawing.Size(110, 19)
        Me.MetroLabel21.TabIndex = 314
        Me.MetroLabel21.Text = "Net/Case request"
        '
        'InfoTile
        '
        Me.InfoTile.ActiveControl = Nothing
        Me.InfoTile.AutoSize = True
        Me.InfoTile.BackColor = System.Drawing.Color.White
        Me.InfoTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.InfoTile.Location = New System.Drawing.Point(406, 402)
        Me.InfoTile.Name = "InfoTile"
        Me.InfoTile.Size = New System.Drawing.Size(36, 35)
        Me.InfoTile.Style = MetroFramework.MetroColorStyle.White
        Me.InfoTile.TabIndex = 312
        Me.InfoTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.InfoTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Info32
        Me.InfoTile.TileImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.InfoTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.InfoTile.UseSelectable = True
        Me.InfoTile.UseTileImage = True
        '
        'CurrentWeightTextBox
        '
        Me.CurrentWeightTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CurrentWeightTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CurrentWeightTextBox.CustomButton.Image = Nothing
        Me.CurrentWeightTextBox.CustomButton.Location = New System.Drawing.Point(116, 2)
        Me.CurrentWeightTextBox.CustomButton.Name = ""
        Me.CurrentWeightTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CurrentWeightTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CurrentWeightTextBox.CustomButton.TabIndex = 1
        Me.CurrentWeightTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CurrentWeightTextBox.CustomButton.UseSelectable = True
        Me.CurrentWeightTextBox.CustomButton.Visible = False
        Me.CurrentWeightTextBox.Enabled = False
        Me.CurrentWeightTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CurrentWeightTextBox.FontWeight = MetroFramework.MetroTextBoxWeight.Bold
        Me.CurrentWeightTextBox.Lines = New String(-1) {}
        Me.CurrentWeightTextBox.Location = New System.Drawing.Point(294, 443)
        Me.CurrentWeightTextBox.MaxLength = 0
        Me.CurrentWeightTextBox.Name = "CurrentWeightTextBox"
        Me.CurrentWeightTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CurrentWeightTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CurrentWeightTextBox.SelectedText = ""
        Me.CurrentWeightTextBox.SelectionLength = 0
        Me.CurrentWeightTextBox.SelectionStart = 0
        Me.CurrentWeightTextBox.ShortcutsEnabled = True
        Me.CurrentWeightTextBox.Size = New System.Drawing.Size(154, 40)
        Me.CurrentWeightTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CurrentWeightTextBox.TabIndex = 311
        Me.CurrentWeightTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CurrentWeightTextBox.UseCustomBackColor = True
        Me.CurrentWeightTextBox.UseCustomForeColor = True
        Me.CurrentWeightTextBox.UseSelectable = True
        Me.CurrentWeightTextBox.UseStyleColors = True
        Me.CurrentWeightTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CurrentWeightTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'CurrentStockTextBox
        '
        Me.CurrentStockTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CurrentStockTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CurrentStockTextBox.CustomButton.Image = Nothing
        Me.CurrentStockTextBox.CustomButton.Location = New System.Drawing.Point(68, 2)
        Me.CurrentStockTextBox.CustomButton.Name = ""
        Me.CurrentStockTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CurrentStockTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CurrentStockTextBox.CustomButton.TabIndex = 1
        Me.CurrentStockTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CurrentStockTextBox.CustomButton.UseSelectable = True
        Me.CurrentStockTextBox.CustomButton.Visible = False
        Me.CurrentStockTextBox.Enabled = False
        Me.CurrentStockTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CurrentStockTextBox.FontWeight = MetroFramework.MetroTextBoxWeight.Bold
        Me.CurrentStockTextBox.Lines = New String(-1) {}
        Me.CurrentStockTextBox.Location = New System.Drawing.Point(176, 443)
        Me.CurrentStockTextBox.MaxLength = 0
        Me.CurrentStockTextBox.Name = "CurrentStockTextBox"
        Me.CurrentStockTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CurrentStockTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CurrentStockTextBox.SelectedText = ""
        Me.CurrentStockTextBox.SelectionLength = 0
        Me.CurrentStockTextBox.SelectionStart = 0
        Me.CurrentStockTextBox.ShortcutsEnabled = True
        Me.CurrentStockTextBox.Size = New System.Drawing.Size(106, 40)
        Me.CurrentStockTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CurrentStockTextBox.TabIndex = 309
        Me.CurrentStockTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CurrentStockTextBox.UseCustomBackColor = True
        Me.CurrentStockTextBox.UseCustomForeColor = True
        Me.CurrentStockTextBox.UseSelectable = True
        Me.CurrentStockTextBox.UseStyleColors = True
        Me.CurrentStockTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CurrentStockTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel20
        '
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.Location = New System.Drawing.Point(82, 458)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(86, 19)
        Me.MetroLabel20.TabIndex = 310
        Me.MetroLabel20.Text = "Current stock"
        '
        'SearchTile
        '
        Me.SearchTile.ActiveControl = Nothing
        Me.SearchTile.AutoSize = True
        Me.SearchTile.BackColor = System.Drawing.Color.White
        Me.SearchTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SearchTile.Location = New System.Drawing.Point(412, 361)
        Me.SearchTile.Name = "SearchTile"
        Me.SearchTile.Size = New System.Drawing.Size(36, 35)
        Me.SearchTile.Style = MetroFramework.MetroColorStyle.White
        Me.SearchTile.TabIndex = 308
        Me.SearchTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SearchTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Search32
        Me.SearchTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SearchTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SearchTile.UseSelectable = True
        Me.SearchTile.UseTileImage = True
        '
        'OrderNoTextBox
        '
        Me.OrderNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.OrderNoTextBox.CustomButton.Image = Nothing
        Me.OrderNoTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.OrderNoTextBox.CustomButton.Name = ""
        Me.OrderNoTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.OrderNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.OrderNoTextBox.CustomButton.TabIndex = 1
        Me.OrderNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OrderNoTextBox.CustomButton.UseSelectable = True
        Me.OrderNoTextBox.CustomButton.Visible = False
        Me.OrderNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.OrderNoTextBox.Lines = New String(-1) {}
        Me.OrderNoTextBox.Location = New System.Drawing.Point(176, 141)
        Me.OrderNoTextBox.MaxLength = 0
        Me.OrderNoTextBox.Name = "OrderNoTextBox"
        Me.OrderNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.OrderNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.OrderNoTextBox.SelectedText = ""
        Me.OrderNoTextBox.SelectionLength = 0
        Me.OrderNoTextBox.SelectionStart = 0
        Me.OrderNoTextBox.ShortcutsEnabled = True
        Me.OrderNoTextBox.Size = New System.Drawing.Size(272, 40)
        Me.OrderNoTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.OrderNoTextBox.TabIndex = 0
        Me.OrderNoTextBox.UseSelectable = True
        Me.OrderNoTextBox.UseStyleColors = True
        Me.OrderNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.OrderNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel19
        '
        Me.MetroLabel19.AutoSize = True
        Me.MetroLabel19.Location = New System.Drawing.Point(27, 161)
        Me.MetroLabel19.Name = "MetroLabel19"
        Me.MetroLabel19.Size = New System.Drawing.Size(138, 19)
        Me.MetroLabel19.TabIndex = 306
        Me.MetroLabel19.Text = "Order No.(from sales)"
        '
        'CropComboBox
        '
        Me.CropComboBox.DataSource = Me.CropFromPackedGradeBindingSource
        Me.CropComboBox.DisplayMember = "crop"
        Me.CropComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.CropComboBox.FormattingEnabled = True
        Me.CropComboBox.ItemHeight = 29
        Me.CropComboBox.Location = New System.Drawing.Point(176, 361)
        Me.CropComboBox.Name = "CropComboBox"
        Me.CropComboBox.Size = New System.Drawing.Size(100, 35)
        Me.CropComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropComboBox.TabIndex = 5
        Me.CropComboBox.UseSelectable = True
        Me.CropComboBox.ValueMember = "crop"
        '
        'CropFromPackedGradeBindingSource
        '
        Me.CropFromPackedGradeBindingSource.DataMember = "CropFromPackedGrade"
        Me.CropFromPackedGradeBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TypeComboBox
        '
        Me.TypeComboBox.DataSource = Me.TypeBindingSource
        Me.TypeComboBox.DisplayMember = "type"
        Me.TypeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 29
        Me.TypeComboBox.Location = New System.Drawing.Point(327, 361)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(73, 35)
        Me.TypeComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TypeComboBox.TabIndex = 6
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type"
        '
        'TypeBindingSource
        '
        Me.TypeBindingSource.DataMember = "type"
        Me.TypeBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(282, 376)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(36, 19)
        Me.MetroLabel5.TabIndex = 305
        Me.MetroLabel5.Text = "Type"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(131, 376)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(39, 19)
        Me.MetroLabel1.TabIndex = 304
        Me.MetroLabel1.Text = "Crop"
        '
        'TotalCaseRequestTextBox
        '
        '
        '
        '
        Me.TotalCaseRequestTextBox.CustomButton.Image = Nothing
        Me.TotalCaseRequestTextBox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.TotalCaseRequestTextBox.CustomButton.Name = ""
        Me.TotalCaseRequestTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.TotalCaseRequestTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalCaseRequestTextBox.CustomButton.TabIndex = 1
        Me.TotalCaseRequestTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalCaseRequestTextBox.CustomButton.UseSelectable = True
        Me.TotalCaseRequestTextBox.CustomButton.Visible = False
        Me.TotalCaseRequestTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TotalCaseRequestTextBox.Lines = New String(-1) {}
        Me.TotalCaseRequestTextBox.Location = New System.Drawing.Point(176, 581)
        Me.TotalCaseRequestTextBox.MaxLength = 32767
        Me.TotalCaseRequestTextBox.Name = "TotalCaseRequestTextBox"
        Me.TotalCaseRequestTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalCaseRequestTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalCaseRequestTextBox.SelectedText = ""
        Me.TotalCaseRequestTextBox.SelectionLength = 0
        Me.TotalCaseRequestTextBox.SelectionStart = 0
        Me.TotalCaseRequestTextBox.ShortcutsEnabled = True
        Me.TotalCaseRequestTextBox.Size = New System.Drawing.Size(272, 40)
        Me.TotalCaseRequestTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalCaseRequestTextBox.TabIndex = 10
        Me.TotalCaseRequestTextBox.UseSelectable = True
        Me.TotalCaseRequestTextBox.UseStyleColors = True
        Me.TotalCaseRequestTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalCaseRequestTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TotalWeightRequestTextbox
        '
        '
        '
        '
        Me.TotalWeightRequestTextbox.CustomButton.Image = Nothing
        Me.TotalWeightRequestTextbox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.TotalWeightRequestTextbox.CustomButton.Name = ""
        Me.TotalWeightRequestTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.TotalWeightRequestTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalWeightRequestTextbox.CustomButton.TabIndex = 1
        Me.TotalWeightRequestTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalWeightRequestTextbox.CustomButton.UseSelectable = True
        Me.TotalWeightRequestTextbox.CustomButton.Visible = False
        Me.TotalWeightRequestTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TotalWeightRequestTextbox.Lines = New String(-1) {}
        Me.TotalWeightRequestTextbox.Location = New System.Drawing.Point(176, 627)
        Me.TotalWeightRequestTextbox.MaxLength = 32767
        Me.TotalWeightRequestTextbox.Name = "TotalWeightRequestTextbox"
        Me.TotalWeightRequestTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalWeightRequestTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalWeightRequestTextbox.SelectedText = ""
        Me.TotalWeightRequestTextbox.SelectionLength = 0
        Me.TotalWeightRequestTextbox.SelectionStart = 0
        Me.TotalWeightRequestTextbox.ShortcutsEnabled = True
        Me.TotalWeightRequestTextbox.Size = New System.Drawing.Size(272, 40)
        Me.TotalWeightRequestTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalWeightRequestTextbox.TabIndex = 11
        Me.TotalWeightRequestTextbox.UseSelectable = True
        Me.TotalWeightRequestTextbox.UseStyleColors = True
        Me.TotalWeightRequestTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalWeightRequestTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'CustomerGradeTextbox
        '
        '
        '
        '
        Me.CustomerGradeTextbox.CustomButton.Image = Nothing
        Me.CustomerGradeTextbox.CustomButton.Location = New System.Drawing.Point(234, 2)
        Me.CustomerGradeTextbox.CustomButton.Name = ""
        Me.CustomerGradeTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CustomerGradeTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CustomerGradeTextbox.CustomButton.TabIndex = 1
        Me.CustomerGradeTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CustomerGradeTextbox.CustomButton.UseSelectable = True
        Me.CustomerGradeTextbox.CustomButton.Visible = False
        Me.CustomerGradeTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CustomerGradeTextbox.Lines = New String(-1) {}
        Me.CustomerGradeTextbox.Location = New System.Drawing.Point(176, 489)
        Me.CustomerGradeTextbox.MaxLength = 32767
        Me.CustomerGradeTextbox.Name = "CustomerGradeTextbox"
        Me.CustomerGradeTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CustomerGradeTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CustomerGradeTextbox.SelectedText = ""
        Me.CustomerGradeTextbox.SelectionLength = 0
        Me.CustomerGradeTextbox.SelectionStart = 0
        Me.CustomerGradeTextbox.ShortcutsEnabled = True
        Me.CustomerGradeTextbox.Size = New System.Drawing.Size(272, 40)
        Me.CustomerGradeTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerGradeTextbox.TabIndex = 8
        Me.CustomerGradeTextbox.UseSelectable = True
        Me.CustomerGradeTextbox.UseStyleColors = True
        Me.CustomerGradeTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CustomerGradeTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.Location = New System.Drawing.Point(50, 590)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(112, 19)
        Me.MetroLabel11.TabIndex = 296
        Me.MetroLabel11.Text = "Total case request"
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(37, 638)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(125, 19)
        Me.MetroLabel10.TabIndex = 295
        Me.MetroLabel10.Text = "Total weight request"
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(61, 498)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(105, 19)
        Me.MetroLabel9.TabIndex = 294
        Me.MetroLabel9.Text = "Customer grade"
        '
        'PackedGradeComboBox
        '
        Me.PackedGradeComboBox.BackColor = System.Drawing.Color.White
        Me.PackedGradeComboBox.DataSource = Me.Sp_Shipping_SEL_PackedgradeBindingSource
        Me.PackedGradeComboBox.DisplayMember = "graders"
        Me.PackedGradeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.PackedGradeComboBox.FormattingEnabled = True
        Me.PackedGradeComboBox.ItemHeight = 29
        Me.PackedGradeComboBox.Location = New System.Drawing.Point(176, 402)
        Me.PackedGradeComboBox.Name = "PackedGradeComboBox"
        Me.PackedGradeComboBox.Size = New System.Drawing.Size(224, 35)
        Me.PackedGradeComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.PackedGradeComboBox.TabIndex = 7
        Me.PackedGradeComboBox.UseCustomBackColor = True
        Me.PackedGradeComboBox.UseSelectable = True
        Me.PackedGradeComboBox.ValueMember = "graders"
        '
        'Sp_Shipping_SEL_PackedgradeBindingSource
        '
        Me.Sp_Shipping_SEL_PackedgradeBindingSource.DataMember = "sp_Shipping_SEL_Packedgrade"
        Me.Sp_Shipping_SEL_PackedgradeBindingSource.DataSource = Me.ShippingDataSet
        '
        'CustomerComboBox
        '
        Me.CustomerComboBox.DataSource = Me.Sp_Shipping_SEL_CustomerBindingSource
        Me.CustomerComboBox.DisplayMember = "CODE"
        Me.CustomerComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.CustomerComboBox.FormattingEnabled = True
        Me.CustomerComboBox.ItemHeight = 29
        Me.CustomerComboBox.Location = New System.Drawing.Point(176, 315)
        Me.CustomerComboBox.Name = "CustomerComboBox"
        Me.CustomerComboBox.Size = New System.Drawing.Size(272, 35)
        Me.CustomerComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.CustomerComboBox.TabIndex = 4
        Me.CustomerComboBox.UseSelectable = True
        Me.CustomerComboBox.ValueMember = "CODE"
        '
        'Sp_Shipping_SEL_CustomerBindingSource
        '
        Me.Sp_Shipping_SEL_CustomerBindingSource.DataMember = "sp_Shipping_SEL_Customer"
        Me.Sp_Shipping_SEL_CustomerBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Enabled = False
        Me.MetroLabel4.Location = New System.Drawing.Point(101, 330)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(66, 19)
        Me.MetroLabel4.TabIndex = 292
        Me.MetroLabel4.Text = "Customer"
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(97, 417)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(70, 19)
        Me.MetroLabel7.TabIndex = 290
        Me.MetroLabel7.Text = "Packgrade"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Enabled = False
        Me.MetroLabel3.Location = New System.Drawing.Point(64, 288)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(101, 19)
        Me.MetroLabel3.TabIndex = 320
        Me.MetroLabel3.Text = "Invoicing comp."
        '
        'Sp_Shipping_SEL_CustomerTableAdapter
        '
        Me.Sp_Shipping_SEL_CustomerTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Me.TypeTableAdapter
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'TypeTableAdapter
        '
        Me.TypeTableAdapter.ClearBeforeFill = True
        '
        'CropFromPackedGradeTableAdapter
        '
        Me.CropFromPackedGradeTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_PackedgradeTableAdapter
        '
        Me.Sp_Shipping_SEL_PackedgradeTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_PackedgradeByGradersBindingSource
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByGraders"
        Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_PackedgradeByGradersTableAdapter
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_GETMAX_OrderDetailNoBindingSource
        '
        Me.Sp_Shipping_GETMAX_OrderDetailNoBindingSource.DataMember = "sp_Shipping_GETMAX_OrderDetailNo"
        Me.Sp_Shipping_GETMAX_OrderDetailNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_GETMAX_OrderDetailNoTableAdapter
        '
        Me.Sp_Shipping_GETMAX_OrderDetailNoTableAdapter.ClearBeforeFill = True
        '
        'SupplierComboBox
        '
        Me.SupplierComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.SupplierComboBox.FormattingEnabled = True
        Me.SupplierComboBox.ItemHeight = 29
        Me.SupplierComboBox.Items.AddRange(New Object() {"STEC", "Thepawong"})
        Me.SupplierComboBox.Location = New System.Drawing.Point(176, 233)
        Me.SupplierComboBox.Name = "SupplierComboBox"
        Me.SupplierComboBox.Size = New System.Drawing.Size(272, 35)
        Me.SupplierComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.SupplierComboBox.TabIndex = 2
        Me.SupplierComboBox.UseSelectable = True
        '
        'InvoicingCompComBoBox
        '
        Me.InvoicingCompComBoBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.InvoicingCompComBoBox.FormattingEnabled = True
        Me.InvoicingCompComBoBox.ItemHeight = 29
        Me.InvoicingCompComBoBox.Items.AddRange(New Object() {"TCLTC", "Thepawong"})
        Me.InvoicingCompComBoBox.Location = New System.Drawing.Point(176, 274)
        Me.InvoicingCompComBoBox.Name = "InvoicingCompComBoBox"
        Me.InvoicingCompComBoBox.Size = New System.Drawing.Size(272, 35)
        Me.InvoicingCompComBoBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.InvoicingCompComBoBox.TabIndex = 3
        Me.InvoicingCompComBoBox.UseSelectable = True
        '
        'FrmAddOrderMaster
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(525, 706)
        Me.Controls.Add(Me.InvoicingCompComBoBox)
        Me.Controls.Add(Me.SupplierComboBox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel23)
        Me.Controls.Add(Me.ReceivedDate)
        Me.Controls.Add(Me.MetroLabel22)
        Me.Controls.Add(Me.NetRequestTextBox)
        Me.Controls.Add(Me.MetroLabel21)
        Me.Controls.Add(Me.InfoTile)
        Me.Controls.Add(Me.CurrentWeightTextBox)
        Me.Controls.Add(Me.CurrentStockTextBox)
        Me.Controls.Add(Me.MetroLabel20)
        Me.Controls.Add(Me.SearchTile)
        Me.Controls.Add(Me.OrderNoTextBox)
        Me.Controls.Add(Me.MetroLabel19)
        Me.Controls.Add(Me.CropComboBox)
        Me.Controls.Add(Me.TypeComboBox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.TotalCaseRequestTextBox)
        Me.Controls.Add(Me.TotalWeightRequestTextbox)
        Me.Controls.Add(Me.CustomerGradeTextbox)
        Me.Controls.Add(Me.MetroLabel11)
        Me.Controls.Add(Me.MetroLabel10)
        Me.Controls.Add(Me.MetroLabel9)
        Me.Controls.Add(Me.PackedGradeComboBox)
        Me.Controls.Add(Me.CustomerComboBox)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmAddOrderMaster"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Add Order Master"
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.CropFromPackedGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_CustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_PackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_GETMAX_OrderDetailNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents AddTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel23 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivedDate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents MetroLabel22 As MetroFramework.Controls.MetroLabel
    Friend WithEvents NetRequestTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel21 As MetroFramework.Controls.MetroLabel
    Friend WithEvents InfoTile As MetroFramework.Controls.MetroTile
    Friend WithEvents CurrentWeightTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents CurrentStockTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SearchTile As MetroFramework.Controls.MetroTile
    Friend WithEvents OrderNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel19 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalCaseRequestTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents TotalWeightRequestTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents CustomerGradeTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PackedGradeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents CustomerComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_CustomerBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_CustomerTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_CustomerTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents CropFromPackedGradeBindingSource As BindingSource
    Friend WithEvents CropFromPackedGradeTableAdapter As ShippingDataSetTableAdapters.CropFromPackedGradeTableAdapter
    Friend WithEvents TypeTableAdapter As ShippingDataSetTableAdapters.typeTableAdapter
    Friend WithEvents TypeBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeTableAdapter
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter
    Friend WithEvents Sp_Shipping_GETMAX_OrderDetailNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_GETMAX_OrderDetailNoTableAdapter
    Friend WithEvents Sp_Shipping_GETMAX_OrderDetailNoBindingSource As BindingSource
    Friend WithEvents SupplierComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents InvoicingCompComBoBox As MetroFramework.Controls.MetroComboBox
End Class

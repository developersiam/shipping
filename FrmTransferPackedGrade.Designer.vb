﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTransferPackedGrade
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.CaseNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.GradersLabel = New MetroFramework.Controls.MetroLabel()
        Me.AddTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.Graders = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Grade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Barcode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Caseno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Net = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToCustomer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Warehouse = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bay = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PdremarkDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PdRemarkParent = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementStatusName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELPackedgradeByGradersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.CropComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.CropFromPackedGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.PackedGradeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CustomerComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpShippingSELCustomerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.CropForCustomerTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.CustomerRadio = New MetroFramework.Controls.MetroRadioButton()
        Me.InStockRadio = New MetroFramework.Controls.MetroRadioButton()
        Me.CustomerPackedGradeTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.PdRemarkParentTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.CropFromPackedGradeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.CropFromPackedGradeTableAdapter()
        Me.TypeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.typeTableAdapter()
        Me.Sp_Shipping_SEL_CustomerTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_CustomerTableAdapter()
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter()
        Me.Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter()
        Me.Sp_Shipping_GETMAX_TransferNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_GETMAX_TransferNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_GETMAX_TransferNoTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELPackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CropFromPackedGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELPackedgradeByInternalGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELCustomerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Sp_Shipping_GETMAX_TransferNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.CaseNoLabel)
        Me.MetroPanel1.Controls.Add(Me.GradersLabel)
        Me.MetroPanel1.Controls.Add(Me.AddTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(1, 46)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1269, 62)
        Me.MetroPanel1.TabIndex = 188
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MetroLabel4.Location = New System.Drawing.Point(282, 19)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(116, 25)
        Me.MetroLabel4.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel4.TabIndex = 135
        Me.MetroLabel4.Text = "Packed Grade"
        Me.MetroLabel4.UseCustomForeColor = True
        '
        'CaseNoLabel
        '
        Me.CaseNoLabel.AutoSize = True
        Me.CaseNoLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.CaseNoLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CaseNoLabel.Location = New System.Drawing.Point(603, 19)
        Me.CaseNoLabel.Name = "CaseNoLabel"
        Me.CaseNoLabel.Size = New System.Drawing.Size(89, 25)
        Me.CaseNoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CaseNoLabel.TabIndex = 134
        Me.CaseNoLabel.Text = "Username"
        Me.CaseNoLabel.UseCustomForeColor = True
        '
        'GradersLabel
        '
        Me.GradersLabel.AutoSize = True
        Me.GradersLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.GradersLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.GradersLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GradersLabel.Location = New System.Drawing.Point(409, 19)
        Me.GradersLabel.Name = "GradersLabel"
        Me.GradersLabel.Size = New System.Drawing.Size(97, 25)
        Me.GradersLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.GradersLabel.TabIndex = 133
        Me.GradersLabel.Text = "Username"
        Me.GradersLabel.UseCustomForeColor = True
        '
        'AddTile
        '
        Me.AddTile.ActiveControl = Nothing
        Me.AddTile.AutoSize = True
        Me.AddTile.BackColor = System.Drawing.Color.White
        Me.AddTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddTile.Location = New System.Drawing.Point(885, 13)
        Me.AddTile.Name = "AddTile"
        Me.AddTile.Size = New System.Drawing.Size(36, 35)
        Me.AddTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddTile.TabIndex = 132
        Me.AddTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save32
        Me.AddTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddTile.UseSelectable = True
        Me.AddTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(927, 19)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(37, 20)
        Me.MetroLabel2.TabIndex = 131
        Me.MetroLabel2.Text = "save"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(1002, 13)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 126
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(1044, 23)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel1.TabIndex = 118
        Me.MetroLabel1.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1189, 7)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Graders, Me.Grade, Me.Barcode, Me.Caseno, Me.Net, Me.ToCustomer, Me.Warehouse, Me.bay, Me.PdremarkDataGridViewTextBoxColumn, Me.PdRemarkParent, Me.MovementStatusName})
        Me.MetroGrid.DataSource = Me.SpShippingSELPackedgradeByGradersBindingSource
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle4
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(1, 114)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.MetroGrid.RowHeadersWidth = 51
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 28
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(898, 558)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 198
        '
        'Graders
        '
        Me.Graders.DataPropertyName = "Graders"
        Me.Graders.HeaderText = "Parent Grade"
        Me.Graders.MinimumWidth = 6
        Me.Graders.Name = "Graders"
        Me.Graders.ReadOnly = True
        Me.Graders.Width = 148
        '
        'Grade
        '
        Me.Grade.DataPropertyName = "Grade"
        Me.Grade.HeaderText = "First Grade"
        Me.Grade.MinimumWidth = 6
        Me.Grade.Name = "Grade"
        Me.Grade.ReadOnly = True
        Me.Grade.Width = 129
        '
        'Barcode
        '
        Me.Barcode.DataPropertyName = "Barcode"
        Me.Barcode.HeaderText = "Barcode"
        Me.Barcode.MinimumWidth = 6
        Me.Barcode.Name = "Barcode"
        Me.Barcode.ReadOnly = True
        Me.Barcode.Width = 105
        '
        'Caseno
        '
        Me.Caseno.DataPropertyName = "Caseno"
        Me.Caseno.HeaderText = "Caseno"
        Me.Caseno.MinimumWidth = 6
        Me.Caseno.Name = "Caseno"
        Me.Caseno.ReadOnly = True
        Me.Caseno.Width = 98
        '
        'Net
        '
        Me.Net.DataPropertyName = "Net"
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Net.DefaultCellStyle = DataGridViewCellStyle3
        Me.Net.HeaderText = "Net Def."
        Me.Net.MinimumWidth = 6
        Me.Net.Name = "Net"
        Me.Net.ReadOnly = True
        Me.Net.Width = 104
        '
        'ToCustomer
        '
        Me.ToCustomer.DataPropertyName = "ToCustomer"
        Me.ToCustomer.HeaderText = "Customer"
        Me.ToCustomer.MinimumWidth = 6
        Me.ToCustomer.Name = "ToCustomer"
        Me.ToCustomer.ReadOnly = True
        Me.ToCustomer.Width = 118
        '
        'Warehouse
        '
        Me.Warehouse.DataPropertyName = "Warehouse"
        Me.Warehouse.HeaderText = "Warehouse"
        Me.Warehouse.MinimumWidth = 6
        Me.Warehouse.Name = "Warehouse"
        Me.Warehouse.ReadOnly = True
        Me.Warehouse.Width = 132
        '
        'bay
        '
        Me.bay.DataPropertyName = "bay"
        Me.bay.HeaderText = "bay"
        Me.bay.MinimumWidth = 6
        Me.bay.Name = "bay"
        Me.bay.ReadOnly = True
        Me.bay.Width = 67
        '
        'PdremarkDataGridViewTextBoxColumn
        '
        Me.PdremarkDataGridViewTextBoxColumn.DataPropertyName = "pdremark"
        Me.PdremarkDataGridViewTextBoxColumn.HeaderText = "pdremark"
        Me.PdremarkDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.PdremarkDataGridViewTextBoxColumn.Name = "PdremarkDataGridViewTextBoxColumn"
        Me.PdremarkDataGridViewTextBoxColumn.ReadOnly = True
        Me.PdremarkDataGridViewTextBoxColumn.Width = 118
        '
        'PdRemarkParent
        '
        Me.PdRemarkParent.DataPropertyName = "PdRemarkParent"
        Me.PdRemarkParent.HeaderText = "PdRemarkParent"
        Me.PdRemarkParent.MinimumWidth = 6
        Me.PdRemarkParent.Name = "PdRemarkParent"
        Me.PdRemarkParent.ReadOnly = True
        Me.PdRemarkParent.Width = 176
        '
        'MovementStatusName
        '
        Me.MovementStatusName.DataPropertyName = "MovementStatusName"
        Me.MovementStatusName.HeaderText = "MovementStatus"
        Me.MovementStatusName.MinimumWidth = 6
        Me.MovementStatusName.Name = "MovementStatusName"
        Me.MovementStatusName.ReadOnly = True
        Me.MovementStatusName.Width = 180
        '
        'SpShippingSELPackedgradeByGradersBindingSource
        '
        Me.SpShippingSELPackedgradeByGradersBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByGraders"
        Me.SpShippingSELPackedgradeByGradersBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CropComboBox
        '
        Me.CropComboBox.DataSource = Me.CropFromPackedGradeBindingSource
        Me.CropComboBox.DisplayMember = "crop"
        Me.CropComboBox.Enabled = False
        Me.CropComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.CropComboBox.FormattingEnabled = True
        Me.CropComboBox.ItemHeight = 29
        Me.CropComboBox.Location = New System.Drawing.Point(173, 68)
        Me.CropComboBox.Name = "CropComboBox"
        Me.CropComboBox.Size = New System.Drawing.Size(180, 35)
        Me.CropComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropComboBox.TabIndex = 221
        Me.CropComboBox.UseSelectable = True
        Me.CropComboBox.ValueMember = "crop"
        '
        'CropFromPackedGradeBindingSource
        '
        Me.CropFromPackedGradeBindingSource.DataMember = "CropFromPackedGrade"
        Me.CropFromPackedGradeBindingSource.DataSource = Me.ShippingDataSet
        '
        'TypeComboBox
        '
        Me.TypeComboBox.DataSource = Me.TypeBindingSource
        Me.TypeComboBox.DisplayMember = "type"
        Me.TypeComboBox.Enabled = False
        Me.TypeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 29
        Me.TypeComboBox.Location = New System.Drawing.Point(173, 109)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(180, 35)
        Me.TypeComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TypeComboBox.TabIndex = 222
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type"
        '
        'TypeBindingSource
        '
        Me.TypeBindingSource.DataMember = "type"
        Me.TypeBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(128, 124)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel5.TabIndex = 226
        Me.MetroLabel5.Text = "Type"
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(128, 83)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel7.TabIndex = 225
        Me.MetroLabel7.Text = "Crop"
        '
        'PackedGradeComboBox
        '
        Me.PackedGradeComboBox.DataSource = Me.SpShippingSELPackedgradeByInternalGradeBindingSource
        Me.PackedGradeComboBox.DisplayMember = "packedgrade"
        Me.PackedGradeComboBox.Enabled = False
        Me.PackedGradeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.PackedGradeComboBox.FormattingEnabled = True
        Me.PackedGradeComboBox.ItemHeight = 29
        Me.PackedGradeComboBox.Location = New System.Drawing.Point(173, 150)
        Me.PackedGradeComboBox.Name = "PackedGradeComboBox"
        Me.PackedGradeComboBox.Size = New System.Drawing.Size(180, 35)
        Me.PackedGradeComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.PackedGradeComboBox.TabIndex = 223
        Me.PackedGradeComboBox.UseSelectable = True
        Me.PackedGradeComboBox.ValueMember = "packedgrade"
        '
        'SpShippingSELPackedgradeByInternalGradeBindingSource
        '
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource.DataMember = "sp_Shipping_SEL_PackedgradeByInternalGrade"
        Me.SpShippingSELPackedgradeByInternalGradeBindingSource.DataSource = Me.ShippingDataSet
        '
        'CustomerComboBox
        '
        Me.CustomerComboBox.DataSource = Me.SpShippingSELCustomerBindingSource
        Me.CustomerComboBox.DisplayMember = "CODE"
        Me.CustomerComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.CustomerComboBox.FormattingEnabled = True
        Me.CustomerComboBox.ItemHeight = 29
        Me.CustomerComboBox.Location = New System.Drawing.Point(1035, 469)
        Me.CustomerComboBox.Name = "CustomerComboBox"
        Me.CustomerComboBox.Size = New System.Drawing.Size(235, 35)
        Me.CustomerComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.CustomerComboBox.TabIndex = 227
        Me.CustomerComboBox.UseSelectable = True
        Me.CustomerComboBox.ValueMember = "CODE"
        '
        'SpShippingSELCustomerBindingSource
        '
        Me.SpShippingSELCustomerBindingSource.DataMember = "sp_Shipping_SEL_Customer"
        Me.SpShippingSELCustomerBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(905, 484)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(124, 20)
        Me.MetroLabel9.TabIndex = 228
        Me.MetroLabel9.Text = "Trans. to Customer"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.MetroLabel11)
        Me.GroupBox1.Controls.Add(Me.MetroLabel10)
        Me.GroupBox1.Controls.Add(Me.MetroLabel8)
        Me.GroupBox1.Controls.Add(Me.CropForCustomerTextbox)
        Me.GroupBox1.Controls.Add(Me.CustomerRadio)
        Me.GroupBox1.Controls.Add(Me.TypeComboBox)
        Me.GroupBox1.Controls.Add(Me.CropComboBox)
        Me.GroupBox1.Controls.Add(Me.MetroLabel5)
        Me.GroupBox1.Controls.Add(Me.InStockRadio)
        Me.GroupBox1.Controls.Add(Me.CustomerPackedGradeTextbox)
        Me.GroupBox1.Controls.Add(Me.MetroLabel7)
        Me.GroupBox1.Controls.Add(Me.PackedGradeComboBox)
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(905, 114)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(365, 349)
        Me.GroupBox1.TabIndex = 231
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Trans. to Packed Grade"
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.Location = New System.Drawing.Point(58, 275)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(109, 20)
        Me.MetroLabel11.TabIndex = 237
        Me.MetroLabel11.Text = "Customer grade"
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(72, 165)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(95, 20)
        Me.MetroLabel10.TabIndex = 236
        Me.MetroLabel10.Text = "Packed Grade"
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(128, 321)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel8.TabIndex = 235
        Me.MetroLabel8.Text = "Crop"
        '
        'CropForCustomerTextbox
        '
        '
        '
        '
        Me.CropForCustomerTextbox.CustomButton.Image = Nothing
        Me.CropForCustomerTextbox.CustomButton.Location = New System.Drawing.Point(142, 2)
        Me.CropForCustomerTextbox.CustomButton.Name = ""
        Me.CropForCustomerTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CropForCustomerTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CropForCustomerTextbox.CustomButton.TabIndex = 1
        Me.CropForCustomerTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CropForCustomerTextbox.CustomButton.UseSelectable = True
        Me.CropForCustomerTextbox.CustomButton.Visible = False
        Me.CropForCustomerTextbox.Enabled = False
        Me.CropForCustomerTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CropForCustomerTextbox.Lines = New String(-1) {}
        Me.CropForCustomerTextbox.Location = New System.Drawing.Point(173, 301)
        Me.CropForCustomerTextbox.MaxLength = 32767
        Me.CropForCustomerTextbox.Name = "CropForCustomerTextbox"
        Me.CropForCustomerTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CropForCustomerTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CropForCustomerTextbox.SelectedText = ""
        Me.CropForCustomerTextbox.SelectionLength = 0
        Me.CropForCustomerTextbox.SelectionStart = 0
        Me.CropForCustomerTextbox.ShortcutsEnabled = True
        Me.CropForCustomerTextbox.Size = New System.Drawing.Size(180, 40)
        Me.CropForCustomerTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropForCustomerTextbox.TabIndex = 234
        Me.CropForCustomerTextbox.UseSelectable = True
        Me.CropForCustomerTextbox.UseStyleColors = True
        Me.CropForCustomerTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CropForCustomerTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'CustomerRadio
        '
        Me.CustomerRadio.AutoSize = True
        Me.CustomerRadio.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.CustomerRadio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CustomerRadio.Location = New System.Drawing.Point(6, 213)
        Me.CustomerRadio.Name = "CustomerRadio"
        Me.CustomerRadio.Size = New System.Drawing.Size(131, 20)
        Me.CustomerRadio.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerRadio.TabIndex = 233
        Me.CustomerRadio.Text = "Customer grade"
        Me.CustomerRadio.UseCustomForeColor = True
        Me.CustomerRadio.UseSelectable = True
        '
        'InStockRadio
        '
        Me.InStockRadio.AutoSize = True
        Me.InStockRadio.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.InStockRadio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.InStockRadio.Location = New System.Drawing.Point(11, 42)
        Me.InStockRadio.Name = "InStockRadio"
        Me.InStockRadio.Size = New System.Drawing.Size(169, 20)
        Me.InStockRadio.Style = MetroFramework.MetroColorStyle.Lime
        Me.InStockRadio.TabIndex = 232
        Me.InStockRadio.Text = "Packed Grade in stock"
        Me.InStockRadio.UseCustomForeColor = True
        Me.InStockRadio.UseSelectable = True
        '
        'CustomerPackedGradeTextbox
        '
        Me.CustomerPackedGradeTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CustomerPackedGradeTextbox.CustomButton.Image = Nothing
        Me.CustomerPackedGradeTextbox.CustomButton.Location = New System.Drawing.Point(142, 2)
        Me.CustomerPackedGradeTextbox.CustomButton.Name = ""
        Me.CustomerPackedGradeTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CustomerPackedGradeTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CustomerPackedGradeTextbox.CustomButton.TabIndex = 1
        Me.CustomerPackedGradeTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CustomerPackedGradeTextbox.CustomButton.UseSelectable = True
        Me.CustomerPackedGradeTextbox.CustomButton.Visible = False
        Me.CustomerPackedGradeTextbox.Enabled = False
        Me.CustomerPackedGradeTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CustomerPackedGradeTextbox.Lines = New String(-1) {}
        Me.CustomerPackedGradeTextbox.Location = New System.Drawing.Point(173, 255)
        Me.CustomerPackedGradeTextbox.MaxLength = 32767
        Me.CustomerPackedGradeTextbox.Name = "CustomerPackedGradeTextbox"
        Me.CustomerPackedGradeTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CustomerPackedGradeTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CustomerPackedGradeTextbox.SelectedText = ""
        Me.CustomerPackedGradeTextbox.SelectionLength = 0
        Me.CustomerPackedGradeTextbox.SelectionStart = 0
        Me.CustomerPackedGradeTextbox.ShortcutsEnabled = True
        Me.CustomerPackedGradeTextbox.Size = New System.Drawing.Size(180, 40)
        Me.CustomerPackedGradeTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerPackedGradeTextbox.TabIndex = 231
        Me.CustomerPackedGradeTextbox.UseSelectable = True
        Me.CustomerPackedGradeTextbox.UseStyleColors = True
        Me.CustomerPackedGradeTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CustomerPackedGradeTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(916, 530)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(156, 20)
        Me.MetroLabel3.TabIndex = 237
        Me.MetroLabel3.Text = "Remark of parent grade"
        '
        'PdRemarkParentTextBox
        '
        '
        '
        '
        Me.PdRemarkParentTextBox.CustomButton.Image = Nothing
        Me.PdRemarkParentTextBox.CustomButton.Location = New System.Drawing.Point(154, 2)
        Me.PdRemarkParentTextBox.CustomButton.Name = ""
        Me.PdRemarkParentTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.PdRemarkParentTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.PdRemarkParentTextBox.CustomButton.TabIndex = 1
        Me.PdRemarkParentTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.PdRemarkParentTextBox.CustomButton.UseSelectable = True
        Me.PdRemarkParentTextBox.CustomButton.Visible = False
        Me.PdRemarkParentTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.PdRemarkParentTextBox.Lines = New String(-1) {}
        Me.PdRemarkParentTextBox.Location = New System.Drawing.Point(1078, 510)
        Me.PdRemarkParentTextBox.MaxLength = 32767
        Me.PdRemarkParentTextBox.Name = "PdRemarkParentTextBox"
        Me.PdRemarkParentTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.PdRemarkParentTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.PdRemarkParentTextBox.SelectedText = ""
        Me.PdRemarkParentTextBox.SelectionLength = 0
        Me.PdRemarkParentTextBox.SelectionStart = 0
        Me.PdRemarkParentTextBox.ShortcutsEnabled = True
        Me.PdRemarkParentTextBox.Size = New System.Drawing.Size(192, 40)
        Me.PdRemarkParentTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.PdRemarkParentTextBox.TabIndex = 236
        Me.PdRemarkParentTextBox.UseSelectable = True
        Me.PdRemarkParentTextBox.UseStyleColors = True
        Me.PdRemarkParentTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.PdRemarkParentTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'CropFromPackedGradeTableAdapter
        '
        Me.CropFromPackedGradeTableAdapter.ClearBeforeFill = True
        '
        'TypeTableAdapter
        '
        Me.TypeTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_CustomerTableAdapter
        '
        Me.Sp_Shipping_SEL_CustomerTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_PackedgradeByGradersTableAdapter
        '
        Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter
        '
        Me.Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_GETMAX_TransferNoBindingSource
        '
        Me.Sp_Shipping_GETMAX_TransferNoBindingSource.DataMember = "sp_Shipping_GETMAX_TransferNo"
        Me.Sp_Shipping_GETMAX_TransferNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_GETMAX_TransferNoTableAdapter
        '
        Me.Sp_Shipping_GETMAX_TransferNoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Me.TypeTableAdapter
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'FrmTransferPackedGrade
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1286, 769)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.PdRemarkParentTextBox)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CustomerComboBox)
        Me.Controls.Add(Me.MetroLabel9)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmTransferPackedGrade"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELPackedgradeByGradersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CropFromPackedGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELPackedgradeByInternalGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELCustomerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Sp_Shipping_GETMAX_TransferNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents AddTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CaseNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents GradersLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PackedGradeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents CustomerComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents CropFromPackedGradeBindingSource As BindingSource
    Friend WithEvents CropFromPackedGradeTableAdapter As ShippingDataSetTableAdapters.CropFromPackedGradeTableAdapter
    Friend WithEvents TypeBindingSource As BindingSource
    Friend WithEvents TypeTableAdapter As ShippingDataSetTableAdapters.typeTableAdapter
    Friend WithEvents SpShippingSELCustomerBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_CustomerTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_CustomerTableAdapter
    Friend WithEvents SpShippingSELPackedgradeByGradersBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByGradersTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByGradersTableAdapter
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents CustomerPackedGradeTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents CustomerRadio As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents InStockRadio As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropForCustomerTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SpShippingSELPackedgradeByInternalGradeBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeByInternalGradeTableAdapter
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PdRemarkParentTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Sp_Shipping_GETMAX_TransferNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_GETMAX_TransferNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_GETMAX_TransferNoTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Graders As DataGridViewTextBoxColumn
    Friend WithEvents Grade As DataGridViewTextBoxColumn
    Friend WithEvents Barcode As DataGridViewTextBoxColumn
    Friend WithEvents Caseno As DataGridViewTextBoxColumn
    Friend WithEvents Net As DataGridViewTextBoxColumn
    Friend WithEvents ToCustomer As DataGridViewTextBoxColumn
    Friend WithEvents Warehouse As DataGridViewTextBoxColumn
    Friend WithEvents bay As DataGridViewTextBoxColumn
    Friend WithEvents PdremarkDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PdRemarkParent As DataGridViewTextBoxColumn
    Friend WithEvents MovementStatusName As DataGridViewTextBoxColumn
End Class

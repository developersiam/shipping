﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmShippingLocation
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.CreateTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MatRcMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.LocNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LocNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.SpShippingSELShippingLocationBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_SEL_ShippingLocationTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingLocationTableAdapter()
        Me.Station = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.MatRcMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELShippingLocationBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.CreateTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(6, 28)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1179, 54)
        Me.MetroPanel1.TabIndex = 129
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(698, 16)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'CreateTile
        '
        Me.CreateTile.ActiveControl = Nothing
        Me.CreateTile.AutoSize = True
        Me.CreateTile.BackColor = System.Drawing.Color.White
        Me.CreateTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CreateTile.Location = New System.Drawing.Point(587, 16)
        Me.CreateTile.Name = "CreateTile"
        Me.CreateTile.Size = New System.Drawing.Size(38, 35)
        Me.CreateTile.Style = MetroFramework.MetroColorStyle.White
        Me.CreateTile.TabIndex = 120
        Me.CreateTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CreateTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Plus_32
        Me.CreateTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CreateTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.CreateTile.UseSelectable = True
        Me.CreateTile.UseTileImage = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(631, 23)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(48, 20)
        Me.MetroLabel5.TabIndex = 117
        Me.MetroLabel5.Text = "create"
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(740, 23)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1125, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MatRcMetroGrid
        '
        Me.MatRcMetroGrid.AllowUserToAddRows = False
        Me.MatRcMetroGrid.AllowUserToDeleteRows = False
        Me.MatRcMetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MatRcMetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MatRcMetroGrid.AutoGenerateColumns = False
        Me.MatRcMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MatRcMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRcMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MatRcMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MatRcMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRcMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MatRcMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MatRcMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LocNoDataGridViewTextBoxColumn, Me.LocNameDataGridViewTextBoxColumn, Me.StatusDataGridViewCheckBoxColumn, Me.Station})
        Me.MatRcMetroGrid.DataSource = Me.SpShippingSELShippingLocationBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MatRcMetroGrid.DefaultCellStyle = DataGridViewCellStyle3
        Me.MatRcMetroGrid.EnableHeadersVisualStyles = False
        Me.MatRcMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MatRcMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRcMetroGrid.Location = New System.Drawing.Point(6, 98)
        Me.MatRcMetroGrid.Name = "MatRcMetroGrid"
        Me.MatRcMetroGrid.ReadOnly = True
        Me.MatRcMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRcMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.MatRcMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MatRcMetroGrid.RowTemplate.Height = 24
        Me.MatRcMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MatRcMetroGrid.Size = New System.Drawing.Size(1176, 574)
        Me.MatRcMetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MatRcMetroGrid.TabIndex = 130
        '
        'LocNoDataGridViewTextBoxColumn
        '
        Me.LocNoDataGridViewTextBoxColumn.DataPropertyName = "LocNo"
        Me.LocNoDataGridViewTextBoxColumn.HeaderText = "LocNo"
        Me.LocNoDataGridViewTextBoxColumn.Name = "LocNoDataGridViewTextBoxColumn"
        Me.LocNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.LocNoDataGridViewTextBoxColumn.Width = 88
        '
        'LocNameDataGridViewTextBoxColumn
        '
        Me.LocNameDataGridViewTextBoxColumn.DataPropertyName = "LocName"
        Me.LocNameDataGridViewTextBoxColumn.HeaderText = "LocName"
        Me.LocNameDataGridViewTextBoxColumn.Name = "LocNameDataGridViewTextBoxColumn"
        Me.LocNameDataGridViewTextBoxColumn.ReadOnly = True
        Me.LocNameDataGridViewTextBoxColumn.Width = 113
        '
        'StatusDataGridViewCheckBoxColumn
        '
        Me.StatusDataGridViewCheckBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewCheckBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewCheckBoxColumn.Name = "StatusDataGridViewCheckBoxColumn"
        Me.StatusDataGridViewCheckBoxColumn.ReadOnly = True
        Me.StatusDataGridViewCheckBoxColumn.Width = 66
        '
        'SpShippingSELShippingLocationBindingSource
        '
        Me.SpShippingSELShippingLocationBindingSource.DataMember = "sp_Shipping_SEL_ShippingLocation"
        Me.SpShippingSELShippingLocationBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_SEL_ShippingLocationTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.ClearBeforeFill = True
        '
        'Station
        '
        Me.Station.DataPropertyName = "Station"
        Me.Station.HeaderText = "Station"
        Me.Station.Name = "Station"
        Me.Station.ReadOnly = True
        Me.Station.Width = 96
        '
        'FrmShippingLocation
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1354, 697)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.MatRcMetroGrid)
        Me.Name = "FrmShippingLocation"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.MatRcMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELShippingLocationBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents CreateTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MatRcMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents SpShippingSELShippingLocationBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingLocationTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingLocationTableAdapter
    Friend WithEvents LocNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LocNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents Station As DataGridViewTextBoxColumn
End Class

﻿Public Class FrmAdminSetup
    Inherits MetroFramework.Forms.MetroForm
    Private Sub FrmAdminSetup_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername
            'Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    'Private Sub BtnMovementTruck_Click(sender As Object, e As EventArgs) Handles BtnMovementTruck.Click
    '    Try
    '        Dim frmMovementTruck As New FrmMovementTruck
    '        frmMovementTruck.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    'Private Sub BtnTransportCompany_Click(sender As Object, e As EventArgs) Handles BtnTransportCompany.Click
    '    Try
    '        Dim frmMovementStatus As New FrmMovementStatus
    '        frmMovementStatus.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    'Private Sub BtnShippingLocation_Click(sender As Object, e As EventArgs) Handles BtnShippingLocation.Click
    '    Try
    '        Dim frmShippingLocation As New FrmShippingLocation
    '        frmShippingLocation.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    Private Sub BtnUnFinishedShipped_Click(sender As Object, e As EventArgs) Handles BtnUnFinishedShipped.Click
        Try

            Dim frm As New FrmUnFinishShipped
            frm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
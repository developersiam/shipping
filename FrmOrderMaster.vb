﻿Imports System.Threading

Public Class FrmOrderMaster
    Inherits MetroFramework.Forms.MetroForm
    Dim XOrderDetailNo As String
    Dim XOrderNo As String
    Dim XPackedGrade As String
    Dim db As New ShippingDataClassesDataContext

    Private Sub FrmOrder2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.security' table. You can move, or remove it, as needed.
        Me.SecurityTableAdapter.Fill(Me.ShippingDataSet.security)
        Try
            UsernameMetroLabel.Text = XUsername
            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
            'MetroGrid.Columns["Amount"].DefaultCellStyle.Format = "#.##0";
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddTile_Click(sender As Object, e As EventArgs) Handles AddTile.Click
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            If _security Is Nothing Then
                Throw New Exception("ไม่พบ user account นี้ในระบบ โปรดทำการ login อีกครั้ง")
            End If

            If _security.pdtransfer = False And _security.level <> "Admin" Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            Dim frm As New FrmAddOrderMaster
            frm.ShowDialog()
            frm.Dispose()
            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            If (GradeTextBox.Text <> "") Then
                Me.SpShippingSELOrderBindingSource.Filter = "Grade = " & "'" & GradeTextBox.Text & "'"
            Else
                Me.SpShippingSELOrderBindingSource.RemoveFilter()
                Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub ShippingOrderTile_Click(sender As Object, e As EventArgs) Handles ShippingOrderTile.Click
        Try
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            If Not IsDBNull(MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value) Then
                XOrderDetailNo = MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value
            End If

            If XOrderDetailNo = "" Then
                Throw New Exception("กรุณาเลือกรายการที่ต้องการจากตาราง")
            End If

            If _security Is Nothing Then
                Throw New Exception("ไม่พบ user account นี้ในระบบ โปรดทำการ login อีกครั้ง")
            End If

            If (Not (_security.shipping = True And _security.level = "Supervisor")) And
                _security.level <> "Admin" And
                _security.depart <> "Leaf Accounting" Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            Dim frm As New FrmOrder
            frm.XOrderDetailNoLabel.Text = XOrderDetailNo
            frm.ShowDialog()
            frm.Dispose()

            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid.CellMouseClick
        Try
            XOrderNo = MetroGrid.Item(0, MetroGrid.CurrentCell.RowIndex).Value
            XPackedGrade = MetroGrid.Item(7, MetroGrid.CurrentCell.RowIndex).Value
            If Not IsDBNull(MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value) Then
                XOrderDetailNo = MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MetroGrid_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles MetroGrid.CellDoubleClick
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            If Not IsDBNull(MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value) Then
                XOrderDetailNo = MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value
            End If

            If XOrderDetailNo = "" Then
                Throw New Exception("กรุณาเลือกรายการที่ต้องการจากตาราง")
            End If

            If _security Is Nothing Then
                Throw New Exception("ไม่พบ user account นี้ในระบบ โปรดทำการ login อีกครั้ง")
            End If

            If _security.pdtransfer = False And
                _security.level <> "Admin" And
                _security.depart <> "Leaf Accounting" Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            Dim frm As New FrmEditOrderMaster
            frm.XOrderDetailNoTextBox.Text = XOrderDetailNo
            frm.ShowDialog()
            frm.Dispose()

            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub DeleteTile_Click(sender As Object, e As EventArgs) Handles DeleteTile.Click
        Try
            'Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            If _security Is Nothing Then
                Throw New Exception("ไม่พบ user account นี้ในระบบ โปรดทำการ login อีกครั้ง")
            End If

            If _security.pdtransfer = False And _security.level <> "Admin" Then
                Throw New Exception("You don't have a permission for access to this function.")
            End If

            'เช็คว่าหากมีการนำเอาorderนี้ไปใช้แล้วหรือยัง
            If Not IsDBNull(MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value) Then
                XOrderDetailNo = MetroGrid.Item(1, MetroGrid.CurrentCell.RowIndex).Value
                'Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, XOrderDetailNo)
                Me.Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, False, XOrderDetailNo, 1)

                'ถ้าใน ShippingOrder มีข้อมูลแล้ว ไม่อนุญาตให้ลบ
                If Sp_Shipping_SEL_ShippingOrderBindingSource.Count > 0 Then
                    Throw New Exception("Orderนี้ได้ถูกนำไปShippingแล้ว ไม่อนุญาตให้ลบข้อมูล หากต้องการลบกรุณาแจ้ง Admin")
                End If
            Else
                Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, "")

                'ถ้าใน ShippingOrder มีข้อมูลแล้ว ไม่อนุญาตให้ลบ
                If Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource.Count > 0 Then
                    Throw New Exception("Orderนี้ได้ถูกนำไปShippingแล้ว ไม่อนุญาตให้ลบข้อมูล หากต้องการลบกรุณาแจ้ง Admin")
                End If
            End If

            Dim result = MessageBoxModule.Question("ต้องการลบ Order " & XOrderNo & " นี้ใช่หรือไม่ ?")
            If result = DialogResult.Yes Then
                db.sp_Shipping_DEL_Order(XOrderNo, XOrderDetailNo, XUsername, XPackedGrade)
                MessageBoxModule.Info("ลบOrder No. นี้เรียบร้อยแล้ว")
            End If

            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub ShowStockTile_Click(sender As Object, e As EventArgs) Handles ShowStockTile.Click
        Try
            Dim frm As New FrmShowPackedGrade
            frm.ShowDialog()
            frm.Dispose()
            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub SearchTile_Click(sender As Object, e As EventArgs) Handles SearchTile.Click
        Try
            If (GradeTextBox.Text <> "") Then
                Me.SpShippingSELOrderBindingSource.Filter = "Grade = " & "'" & GradeTextBox.Text & "'"
            Else
                Me.SpShippingSELOrderBindingSource.RemoveFilter()
                'Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 0, "")
            End If

        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub GradeTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles GradeTextBox.KeyDown
        Try
            If (e.KeyCode = Keys.Enter) Then
                SearchTile.PerformClick()
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub ShowBarcodeListMetroTile_Click(sender As Object, e As EventArgs) Handles ShowBarcodeListMetroTile.Click
        Try
            Dim frm As New FrmPackedCases
            frm.ShowDialog()
            frm.Close()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
﻿Public Class FrmBayControl
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private Sub FrmBayControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername
            CBalesFromMetroLabel.Text = ""
            CBalesToMetroLabel.Text = ""
            MixGradeCheckBox.Checked = False

            Me.Sp_Shipping_SEL_ShippingBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingBayFrom)
            Me.Sp_Shipping_SEL_ShippingBayToTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingBayTo)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub FromBayComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FromBayComboBox.SelectedIndexChanged
        Try
            If FromBayComboBox.Text <> "" Then
                Me.Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                CBalesFromMetroLabel.Text = Sp_Shipping_SEL_FromBayFromBindingSource.Count & " Cases"
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ToBayComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ToBayComboBox.SelectedIndexChanged
        Try
            If ToBayComboBox.Text <> "" Then
                Me.Sp_Shipping_SEL_FromBayToTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayTo, ToBayComboBox.Text)
                CBalesToMetroLabel.Text = Sp_Shipping_SEL_FromBayToBindingSource.Count & " Cases"
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            Me.Sp_Shipping_SEL_ShippingBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingBayFrom)
            Me.Sp_Shipping_SEL_ShippingBayToTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingBayTo)

            If FromBayComboBox.Text <> "" Then
                Me.Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                CBalesFromMetroLabel.Text = Sp_Shipping_SEL_FromBayFromBindingSource.Count & " Cases"
            End If

            If ToBayComboBox.Text <> "" Then
                Me.Sp_Shipping_SEL_FromBayToTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayTo, ToBayComboBox.Text)
                CBalesToMetroLabel.Text = Sp_Shipping_SEL_FromBayToBindingSource.Count & " Cases"
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub MoveTile_Click(sender As Object, e As EventArgs) Handles MoveTile.Click
        Try
            If ToBayComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก To Bay.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If ToBayComboBox.Text = FromBayComboBox.Text Then
                MessageBox.Show("Bayทั้งสองคือค่าเดียวกัน กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'SelectedRowCount เก็บค่าที่ทำไฮไลท์สีเขียว
            Dim SelectedRowCount As Integer
            SelectedRowCount = MetroGridFrom.Rows.GetRowCount(DataGridViewElementStates.Selected)

            'XBC เก็บค่า Barcodeในแถวที่ทำไฮไลท์สีเขียว
            Dim XBC As String


            'เช็คว่าถ้าไม่ได้เลือก Mix grade และเกรดไม่ตรงใน Bay ให้แจ้ง Message box ให้เลือก Mix grade
            '-------------
            If MixGradeCheckBox.Checked = False Then
                Dim XResult As Boolean = True
                Dim XGrade As String
                If (SelectedRowCount > 0) Then
                    For i As Integer = 0 To SelectedRowCount - 1
                        XGrade = MetroGridFrom.SelectedRows(i).Cells(2).Value.ToString()
                        Me.Sp_Shipping_SEL_GradeByBayTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_GradeByBay, ToBayComboBox.Text, XGrade)
                        If Sp_Shipping_SEL_GradeByBayBindingSource.Count <= 0 Then XResult = False
                    Next
                    If XResult = False Then
                        MessageBox.Show("เนื่องจากเกรดไม่ตรงกับที่มีอยู่แล้วใน Bay " & ToBayComboBox.Text & " กรุณาตรวจสอบข้อมูลหรือเลือก Mix grade เพื่ออนุญาตให้ใส่ข้อมูลได้", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If

                End If
            End If
            '-------------

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการย้ายBarcodeจำนวน " & SelectedRowCount & " รายการ ไป " & ToBayComboBox.Text & " ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                'ถ้าเกิน 99 ลุก จะขึ้น messagebox ถาม
                If Val(CBalesToMetroLabel.Text) + SelectedRowCount > 99 Then
                    Dim result2 As DialogResult
                    result2 = MessageBox.Show("Bay " & ToBayComboBox.Text & "  จะเกิน 99 ลูก คุณต้องการบันทึกข้อมูลหรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    If result2 = DialogResult.Yes Then
                        If (SelectedRowCount > 0) Then
                            For i As Integer = 0 To SelectedRowCount - 1
                                XBC = MetroGridFrom.SelectedRows(i).Cells(1).Value.ToString()
                                db.sp_Shipping_UPD_BayByBC(XBC, ToBayComboBox.Text)
                            Next
                            MessageBox.Show("ย้าย Barcode ในแถวสีเขียวเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                Else
                    'ถ้าไม่เกิน ให้บันทึกข้อมูลได้เลย
                    If (SelectedRowCount > 0) Then
                        For i As Integer = 0 To SelectedRowCount - 1
                            XBC = MetroGridFrom.SelectedRows(i).Cells(1).Value.ToString()
                            db.sp_Shipping_UPD_BayByBC(XBC, ToBayComboBox.Text)
                        Next
                        MessageBox.Show("ย้าย Barcode ในแถวสีเขียวเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If

                If FromBayComboBox.Text <> "" Then
                    Me.Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                    CBalesFromMetroLabel.Text = Sp_Shipping_SEL_FromBayFromBindingSource.Count & " Cases"
                End If

                If ToBayComboBox.Text <> "" Then
                    Me.Sp_Shipping_SEL_FromBayToTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayTo, ToBayComboBox.Text)
                    CBalesToMetroLabel.Text = Sp_Shipping_SEL_FromBayToBindingSource.Count & " Cases"
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub FullMigrationCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles FullMigrationCheckBox.CheckedChanged
        Try
            If FullMigrationCheckBox.Checked = True Then
                FullMigrationDate.Value = Now
                FullMigrationDate.Visible = True
                SaveTile.Visible = True

            ElseIf FullMigrationCheckBox.Checked = False Then
                FullMigrationDate.Value = Now
                FullMigrationDate.Visible = False
                SaveTile.Visible = False
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SaveTile_Click(sender As Object, e As EventArgs) Handles SaveTile.Click
        Try
            If FullMigrationDate.Value > Now Then
                MessageBox.Show("คุณระบุวันที่ผิด คุณไม่สามารถระบุวันที่FullMigrationล่วงหน้าได้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการบันทึกการFullMigration Bay " & FromBayComboBox.Text & " จำนวน " & CBalesFromMetroLabel.Text & " ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                db.sp_Shipping_UPD_Fullmigration(FromBayComboBox.Text, True, FullMigrationDate.Value, XUsername)
                MessageBox.Show("บันทึกการFullMigrationเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            If FromBayComboBox.Text <> "" Then
                Me.Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                CBalesFromMetroLabel.Text = Sp_Shipping_SEL_FromBayFromBindingSource.Count & " Cases"
            End If

            If ToBayComboBox.Text <> "" Then
                Me.Sp_Shipping_SEL_FromBayToTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayTo, ToBayComboBox.Text)
                CBalesToMetroLabel.Text = Sp_Shipping_SEL_FromBayToBindingSource.Count & " Cases"
            End If

            FullMigrationDate.Value = Now
            FullMigrationDate.Visible = False
            SaveTile.Visible = False
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
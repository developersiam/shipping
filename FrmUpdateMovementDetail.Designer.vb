﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUpdateMovementDetail
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim MetroTile2 As MetroFramework.Controls.MetroTile
        Me.MetroPanel2 = New MetroFramework.Controls.MetroPanel()
        Me.TruckNoComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.CCaseTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.FromTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MovementNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.CancelTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.ClearTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.SaveTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        MetroTile2 = New MetroFramework.Controls.MetroTile()
        Me.MetroPanel2.SuspendLayout()
        Me.MetroPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroTile2
        '
        MetroTile2.ActiveControl = Nothing
        MetroTile2.AutoSize = True
        MetroTile2.Dock = System.Windows.Forms.DockStyle.Top
        MetroTile2.Location = New System.Drawing.Point(0, 0)
        MetroTile2.Name = "MetroTile2"
        MetroTile2.Size = New System.Drawing.Size(360, 41)
        MetroTile2.Style = MetroFramework.MetroColorStyle.Lime
        MetroTile2.TabIndex = 2
        MetroTile2.Text = "Movement Document"
        MetroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        MetroTile2.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        MetroTile2.UseSelectable = True
        '
        'MetroPanel2
        '
        Me.MetroPanel2.Controls.Add(Me.TruckNoComboBox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel3)
        Me.MetroPanel2.Controls.Add(Me.CCaseTextbox)
        Me.MetroPanel2.Controls.Add(Me.FromTextbox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel9)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel2.Controls.Add(Me.MovementNoTextbox)
        Me.MetroPanel2.Controls.Add(MetroTile2)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel2.HorizontalScrollbarBarColor = True
        Me.MetroPanel2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.HorizontalScrollbarSize = 10
        Me.MetroPanel2.Location = New System.Drawing.Point(10, 100)
        Me.MetroPanel2.Name = "MetroPanel2"
        Me.MetroPanel2.Size = New System.Drawing.Size(360, 237)
        Me.MetroPanel2.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel2.TabIndex = 180
        Me.MetroPanel2.VerticalScrollbarBarColor = True
        Me.MetroPanel2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.VerticalScrollbarSize = 10
        '
        'TruckNoComboBox
        '
        Me.TruckNoComboBox.FormattingEnabled = True
        Me.TruckNoComboBox.ItemHeight = 24
        Me.TruckNoComboBox.Location = New System.Drawing.Point(147, 147)
        Me.TruckNoComboBox.Name = "TruckNoComboBox"
        Me.TruckNoComboBox.Size = New System.Drawing.Size(200, 30)
        Me.TruckNoComboBox.TabIndex = 172
        Me.TruckNoComboBox.UseSelectable = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(10, 152)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(81, 25)
        Me.MetroLabel3.TabIndex = 171
        Me.MetroLabel3.Text = "Truck no."
        '
        'CCaseTextbox
        '
        Me.CCaseTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.CCaseTextbox.CustomButton.Image = Nothing
        Me.CCaseTextbox.CustomButton.Location = New System.Drawing.Point(162, 2)
        Me.CCaseTextbox.CustomButton.Name = ""
        Me.CCaseTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CCaseTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CCaseTextbox.CustomButton.TabIndex = 1
        Me.CCaseTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CCaseTextbox.CustomButton.UseSelectable = True
        Me.CCaseTextbox.CustomButton.Visible = False
        Me.CCaseTextbox.Enabled = False
        Me.CCaseTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CCaseTextbox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CCaseTextbox.Lines = New String(-1) {}
        Me.CCaseTextbox.Location = New System.Drawing.Point(147, 187)
        Me.CCaseTextbox.MaxLength = 2
        Me.CCaseTextbox.Name = "CCaseTextbox"
        Me.CCaseTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CCaseTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CCaseTextbox.SelectedText = ""
        Me.CCaseTextbox.SelectionLength = 0
        Me.CCaseTextbox.SelectionStart = 0
        Me.CCaseTextbox.ShortcutsEnabled = True
        Me.CCaseTextbox.Size = New System.Drawing.Size(200, 40)
        Me.CCaseTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CCaseTextbox.TabIndex = 168
        Me.CCaseTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CCaseTextbox.UseCustomBackColor = True
        Me.CCaseTextbox.UseCustomForeColor = True
        Me.CCaseTextbox.UseSelectable = True
        Me.CCaseTextbox.UseStyleColors = True
        Me.CCaseTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CCaseTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FromTextbox
        '
        Me.FromTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.FromTextbox.CustomButton.Image = Nothing
        Me.FromTextbox.CustomButton.Location = New System.Drawing.Point(162, 2)
        Me.FromTextbox.CustomButton.Name = ""
        Me.FromTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.FromTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.FromTextbox.CustomButton.TabIndex = 1
        Me.FromTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromTextbox.CustomButton.UseSelectable = True
        Me.FromTextbox.CustomButton.Visible = False
        Me.FromTextbox.Enabled = False
        Me.FromTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.FromTextbox.Lines = New String(-1) {}
        Me.FromTextbox.Location = New System.Drawing.Point(147, 95)
        Me.FromTextbox.MaxLength = 2
        Me.FromTextbox.Name = "FromTextbox"
        Me.FromTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.FromTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.FromTextbox.SelectedText = ""
        Me.FromTextbox.SelectionLength = 0
        Me.FromTextbox.SelectionStart = 0
        Me.FromTextbox.ShortcutsEnabled = True
        Me.FromTextbox.Size = New System.Drawing.Size(200, 40)
        Me.FromTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.FromTextbox.TabIndex = 170
        Me.FromTextbox.UseCustomBackColor = True
        Me.FromTextbox.UseSelectable = True
        Me.FromTextbox.UseStyleColors = True
        Me.FromTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FromTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel9.Location = New System.Drawing.Point(10, 198)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(120, 25)
        Me.MetroLabel9.TabIndex = 167
        Me.MetroLabel9.Text = "Count of case"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(10, 106)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(54, 25)
        Me.MetroLabel2.TabIndex = 169
        Me.MetroLabel2.Text = "From"
        '
        'MovementNoTextbox
        '
        Me.MovementNoTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.MovementNoTextbox.CustomButton.Image = Nothing
        Me.MovementNoTextbox.CustomButton.Location = New System.Drawing.Point(162, 2)
        Me.MovementNoTextbox.CustomButton.Name = ""
        Me.MovementNoTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.MovementNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MovementNoTextbox.CustomButton.TabIndex = 1
        Me.MovementNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MovementNoTextbox.CustomButton.UseSelectable = True
        Me.MovementNoTextbox.CustomButton.Visible = False
        Me.MovementNoTextbox.Enabled = False
        Me.MovementNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.MovementNoTextbox.Lines = New String(-1) {}
        Me.MovementNoTextbox.Location = New System.Drawing.Point(147, 49)
        Me.MovementNoTextbox.MaxLength = 2
        Me.MovementNoTextbox.Name = "MovementNoTextbox"
        Me.MovementNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MovementNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MovementNoTextbox.SelectedText = ""
        Me.MovementNoTextbox.SelectionLength = 0
        Me.MovementNoTextbox.SelectionStart = 0
        Me.MovementNoTextbox.ShortcutsEnabled = True
        Me.MovementNoTextbox.Size = New System.Drawing.Size(200, 40)
        Me.MovementNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.MovementNoTextbox.TabIndex = 168
        Me.MovementNoTextbox.UseCustomBackColor = True
        Me.MovementNoTextbox.UseSelectable = True
        Me.MovementNoTextbox.UseStyleColors = True
        Me.MovementNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MovementNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel1.Location = New System.Drawing.Point(10, 60)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(130, 25)
        Me.MetroLabel1.TabIndex = 167
        Me.MetroLabel1.Text = "Movement no."
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.CancelTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel14)
        Me.MetroPanel1.Controls.Add(Me.ClearTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.SaveTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel7)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(10, 29)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(360, 60)
        Me.MetroPanel1.TabIndex = 181
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'CancelTile
        '
        Me.CancelTile.ActiveControl = Nothing
        Me.CancelTile.AutoSize = True
        Me.CancelTile.BackColor = System.Drawing.Color.White
        Me.CancelTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CancelTile.Location = New System.Drawing.Point(257, 4)
        Me.CancelTile.Name = "CancelTile"
        Me.CancelTile.Size = New System.Drawing.Size(55, 55)
        Me.CancelTile.Style = MetroFramework.MetroColorStyle.White
        Me.CancelTile.TabIndex = 125
        Me.CancelTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CancelTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Cancel64
        Me.CancelTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CancelTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.CancelTile.UseSelectable = True
        Me.CancelTile.UseTileImage = True
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.Location = New System.Drawing.Point(307, 23)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(51, 20)
        Me.MetroLabel14.TabIndex = 124
        Me.MetroLabel14.Text = "Cancel"
        '
        'ClearTile
        '
        Me.ClearTile.ActiveControl = Nothing
        Me.ClearTile.AutoSize = True
        Me.ClearTile.BackColor = System.Drawing.Color.White
        Me.ClearTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearTile.Location = New System.Drawing.Point(803, 4)
        Me.ClearTile.Name = "ClearTile"
        Me.ClearTile.Size = New System.Drawing.Size(55, 55)
        Me.ClearTile.Style = MetroFramework.MetroColorStyle.White
        Me.ClearTile.TabIndex = 123
        Me.ClearTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CancelFile64
        Me.ClearTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ClearTile.UseSelectable = True
        Me.ClearTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(858, 23)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel6.TabIndex = 122
        Me.MetroLabel6.Text = "clear"
        '
        'SaveTile
        '
        Me.SaveTile.ActiveControl = Nothing
        Me.SaveTile.AutoSize = True
        Me.SaveTile.BackColor = System.Drawing.Color.White
        Me.SaveTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveTile.Enabled = False
        Me.SaveTile.Location = New System.Drawing.Point(167, 4)
        Me.SaveTile.Name = "SaveTile"
        Me.SaveTile.Size = New System.Drawing.Size(50, 50)
        Me.SaveTile.Style = MetroFramework.MetroColorStyle.White
        Me.SaveTile.TabIndex = 120
        Me.SaveTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save64
        Me.SaveTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SaveTile.UseSelectable = True
        Me.SaveTile.UseTileImage = True
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(217, 23)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(38, 20)
        Me.MetroLabel7.TabIndex = 117
        Me.MetroLabel7.Text = "Save"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1076, 4)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(63, 56)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft264
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'FrmUpdateMovementDetail
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(380, 369)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.MetroPanel2)
        Me.Name = "FrmUpdateMovementDetail"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel2.ResumeLayout(False)
        Me.MetroPanel2.PerformLayout()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MetroPanel2 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CCaseTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents FromTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MovementNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents CancelTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ClearTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SaveTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents TruckNoComboBox As MetroFramework.Controls.MetroComboBox
End Class

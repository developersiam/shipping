﻿Public Class FrmAddOrder
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Public XCropOrder As String
    Public XOrderDetailNo As String
    Private Sub FrmAddOrder_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.sp_Shipping_SEL_Customer' table. You can move, or remove it, as needed.
        Me.Sp_Shipping_SEL_CustomerTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Customer)
        Try
            UsernameMetroLabel.Text = XUsername


            CropOrderTextBox.Text = XCropOrder

            'Me.Sp_Shipping_SEL_CustomerTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Customer)
            'Me.CropFromPackedGradeTableAdapter.Fill(Me.ShippingDataSet.CropFromPackedGrade)
            'Me.TypeTableAdapter.Fill(Me.ShippingDataSet.type)
            'If CropComboBox.Text = "All" Then
            '    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
            'Else
            '    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
            'End If
            Me.Sp_Shipping_SEL_OrderTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Order, 1, XOrderDetailNo)
            CustomerTextBox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).customer
            PackedGradeTextBox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).grade
            CustomerGradeTextbox.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).CustomerGrade
            NetRequestTextBox.Text = String.Format("{0:0.0,#}", ShippingDataSet.sp_Shipping_SEL_Order.Item(0).net)
            TotalWeightCustomer.Text = String.Format("{0:0.0,#}", ShippingDataSet.sp_Shipping_SEL_Order.Item(0).nett)
            TotalCaseCustomer.Text = ShippingDataSet.sp_Shipping_SEL_Order.Item(0).cases
            If PackedGradeTextBox.Text <> "" Then
                Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, PackedGradeTextBox.Text, CustomerTextBox.Text)
                CurrentStockTextBox.Text = Sp_Shipping_SEL_PackedgradeByGradersBindingSource.Count

                Dim XSumOfCurrentWeight As Double
                XSumOfCurrentWeight = IIf(IsDBNull(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' ")), 0, Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & PackedGradeTextBox.Text & "' "))
                CurrentWeightTextBox.Text = XSumOfCurrentWeight.ToString("N2")
            End If

            ReceivedDateDatetime.Value = Now
            StuffDate.Value = Now
            ETADate.Value = Now
            ETDDate.Value = Now
            BLDate.Value = Now

            'หาผลรวมที่ทำ Shipping orderไปแล้ว
            Sp_Shipping_SEL_ShippingOrderTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_ShippingOrder, 1, XOrderDetailNo, 1)
            TotalCaseRequestTextBox.Text = String.Format("{0:#,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalcaserequest)", String.Empty))
            TotalWeightRequestTextBox.Text = String.Format("{0:0.0,#}", ShippingDataSet.sp_Shipping_SEL_ShippingOrder.Compute("SUM(totalweightrequest)", String.Empty))


        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Try
            Me.Dispose()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try

    End Sub

    Private Sub AddTile_Click(sender As Object, e As EventArgs) Handles AddTile.Click
        Try
            Dim XChkOrder As Boolean = True
            'Get data from movement  
            Dim XInfoRow As ShippingDataSet.sp_Shipping_SEL_CustomerRow
            Me.Sp_Shipping_SEL_CustomerTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_Customer)
            XInfoRow = ShippingDataSet.sp_Shipping_SEL_Customer.FindByCODE(CustomerTextBox.Text)
            If XInfoRow Is Nothing Then
                MessageBox.Show("ไม่พบลูกค้านี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not XInfoRow Is Nothing Then
                XChkOrder = XInfoRow.UseOrders
            End If

            If TotalCaseRequestTextBox.Text = "" Then TotalCaseRequestTextBox.Text = 0
            If TotalWeightRequestTextBox.Text = "" Then TotalWeightRequestTextBox.Text = 0

            'ถ้า XchkOrder = true จะต้องระบุ Order from sale, case ,weight  แต่ถ้าเป็น false ไม่ต้องระบุ3ค่านี้ก็ได้
            If XChkOrder = True Then

                If NewCaseTextBox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Total case from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewCaseTextBox.Text) > Convert.ToDouble(TotalCaseCustomer.Text) Then
                    MessageBox.Show("จำนวนกล่องที่คุณระบุมากกว่าที่กำหนดให้ลูกค้ารายนี้ กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewCaseTextBox.Text) > Convert.ToDouble(CurrentStockTextBox.Text) Then
                    MessageBox.Show("จำนวนกล่องที่คุณระบุมากกว่าที่มีในสต๊อก กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewCaseTextBox.Text) > (Convert.ToDouble(TotalCaseCustomer.Text) - Convert.ToDouble(TotalCaseRequestTextBox.Text)) Then
                    MessageBox.Show("จำนวนกล่องที่คุณระบุมากกว่ายอดคงค้างที่ต้องส่งให้ลูกค้า กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If



                If NewWeightTextBox.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Total weight from customer orders !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewWeightTextBox.Text) > Convert.ToDouble(TotalWeightCustomer.Text) Then
                    MessageBox.Show("น้ำหนักที่คุณระบุมากกว่าที่กำหนดให้ลูกค้ารายนี้ กรุณาตรวจสอบข้อมูล!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewWeightTextBox.Text) > Convert.ToDouble(CurrentWeightTextBox.Text) Then
                    MessageBox.Show("น้ำหนักที่คุณระบุมากกว่าที่มีในสต๊อก กรุณาตรวจสอบข้อมูล!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                ElseIf Convert.ToDouble(NewWeightTextBox.Text) > (Convert.ToDouble(TotalWeightCustomer.Text) - Convert.ToDouble(TotalWeightRequestTextBox.Text)) Then
                    MessageBox.Show("น้ำหนักที่คุณระบุมากกว่ายอดคงค้างที่ต้องส่งให้ลูกค้า กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            Else
                If Convert.ToDouble(NewCaseTextBox.Text) > Convert.ToDouble(CurrentStockTextBox.Text) Then
                    MessageBox.Show("จำนวนกล่องที่คุณระบุมากกว่าที่มีในสต๊อก กรุณาตรวจอสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If Convert.ToDouble(NewWeightTextBox.Text) > Convert.ToDouble(CurrentWeightTextBox.Text) Then
                    MessageBox.Show("น้ำหนักที่คุณระบุมากกว่าที่มีในสต๊อก กรุณาตรวจสอบข้อมูล!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If

            If ContainerTextbox.Text = "" Then
                MessageBox.Show("กรุณาระบุรายละเอียด Container !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'โปรแกรมจะบันทึกข้อมูลให้ก็ต่อเมื่อ น้ำหนักสินค้าเรามีพอสำหรับลูกค้าหรือผู้บันทึก Accept และจำนวนกล่องเรามีพอสำหรับลูกค้าหรือผู้บันทึก Accept เท่านั้น
            'If XChkWeight = True AndAlso XChkCase = True Then
            'Get OrderNo from database 

            'จะบันทึก ShippingOrder ให้ก็ต่อเมื่อจะต้องมีสินค้าพอเท่านั้น
            Sp_Shipping_GETMAX_ShippingOrderNoTableAdapter.Fill(ShippingDataSet.sp_Shipping_GETMAX_ShippingOrderNo, CropOrderTextBox.Text)
            OrderNoTextbox.Text = ShippingDataSet.sp_Shipping_GETMAX_ShippingOrderNo.Item(0).OrderNo

            db.sp_Shipping_INS_ShippingOrder(XOrderDetailNo, OrderNoTextbox.Text, SINOTextbox.Text, InvNoTextbox.Text, ReceivedDateDatetime.Value _
                             , NewWeightTextBox.Text, NewCaseTextBox.Text, ContainerTextbox.Text _
                             , StuffDate.Value, BLNoTextBox.Text, MOPITextBox.Text, ETDDate.Value _
                             , ETADate.Value, BLDate.Value, XUsername, CropOrderTextBox.Text, LTLInvoiceNoTextBox.Text)
            MessageBoxModule.Info("บันทึกข้อมูลเรียบร้อยแล้ว")
            'End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub CropOrderTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CropOrderTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub InfoTile_Click(sender As Object, e As EventArgs) Handles InfoTile.Click
        Try
            Dim XGrader As String
            Dim XCustomer2 As String

            XGrader = PackedGradeTextBox.Text
            XCustomer2 = CustomerTextBox.Text
            Dim xfrm As New FrmShowPackedGradeDetail
            xfrm.XGraders = XGrader
            xfrm.XCustomer = XCustomer2
            xfrm.ShowDialog()
            xfrm.Dispose()
            'If CropComboBox.Text = "All" Then
            '    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, 0, TypeComboBox.Text, 0)
            'Else
            '    Sp_Shipping_SEL_PackedgradeTableAdapter.Fill(ShippingDataSet.sp_Shipping_SEL_Packedgrade, CropComboBox.Text, TypeComboBox.Text, 1)
            'End If

            'PackedGradeComboBox.BackColor = Color.White
            'PackedGradeComboBox.Enabled = True

            If PackedGradeTextBox.Text <> "" Then
                Me.Sp_Shipping_SEL_PackedgradeByGradersTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders, XGrader, XCustomer2)
                CurrentStockTextBox.Text = Sp_Shipping_SEL_PackedgradeByGradersBindingSource.Count

                Dim XSumOfCurrentWeight As Double
                XSumOfCurrentWeight = IIf(IsDBNull(Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & XGrader & "' ")), 0, Me.ShippingDataSet.sp_Shipping_SEL_PackedgradeByGraders.Compute("sum(net)", "Graders = '" & XGrader & "' "))
                CurrentWeightTextBox.Text = XSumOfCurrentWeight.ToString("N2")
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub


    Private Sub NewCaseTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NewCaseTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub NewCaseTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles NewCaseTextBox.KeyUp
        Try
            If NetRequestTextBox.Text <> "" AndAlso NewCaseTextBox.Text <> "" Then
                NewWeightTextBox.Text = Convert.ToDouble(NetRequestTextBox.Text) * Convert.ToDouble(NewCaseTextBox.Text)
            Else
                NewWeightTextBox.Text = ""
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub NewWeightTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NewWeightTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
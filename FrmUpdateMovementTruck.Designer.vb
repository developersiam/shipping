﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmUpdateMovementTruck
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DriverTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.TruckNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.TruckIDTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_SEL_ShippingTruckBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingTruckTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingTruckTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.Status = New MetroFramework.Controls.MetroCheckBox()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingTruckBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DriverTextbox
        '
        '
        '
        '
        Me.DriverTextbox.CustomButton.Image = Nothing
        Me.DriverTextbox.CustomButton.Location = New System.Drawing.Point(383, 2)
        Me.DriverTextbox.CustomButton.Name = ""
        Me.DriverTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.DriverTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.DriverTextbox.CustomButton.TabIndex = 1
        Me.DriverTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.DriverTextbox.CustomButton.UseSelectable = True
        Me.DriverTextbox.CustomButton.Visible = False
        Me.DriverTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.DriverTextbox.Lines = New String(-1) {}
        Me.DriverTextbox.Location = New System.Drawing.Point(275, 343)
        Me.DriverTextbox.MaxLength = 32767
        Me.DriverTextbox.Name = "DriverTextbox"
        Me.DriverTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.DriverTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.DriverTextbox.SelectedText = ""
        Me.DriverTextbox.SelectionLength = 0
        Me.DriverTextbox.SelectionStart = 0
        Me.DriverTextbox.Size = New System.Drawing.Size(411, 30)
        Me.DriverTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.DriverTextbox.TabIndex = 103
        Me.DriverTextbox.UseSelectable = True
        Me.DriverTextbox.UseStyleColors = True
        Me.DriverTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.DriverTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(176, 343)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(47, 20)
        Me.MetroLabel4.TabIndex = 108
        Me.MetroLabel4.Text = "Driver"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(763, 94)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 107
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(688, 94)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 106
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save50
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'TruckNoTextbox
        '
        Me.TruckNoTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        '
        '
        '
        Me.TruckNoTextbox.CustomButton.Image = Nothing
        Me.TruckNoTextbox.CustomButton.Location = New System.Drawing.Point(383, 2)
        Me.TruckNoTextbox.CustomButton.Name = ""
        Me.TruckNoTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TruckNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoTextbox.CustomButton.TabIndex = 1
        Me.TruckNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoTextbox.CustomButton.UseSelectable = True
        Me.TruckNoTextbox.CustomButton.Visible = False
        Me.TruckNoTextbox.Enabled = False
        Me.TruckNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TruckNoTextbox.Lines = New String(-1) {}
        Me.TruckNoTextbox.Location = New System.Drawing.Point(275, 307)
        Me.TruckNoTextbox.MaxLength = 32767
        Me.TruckNoTextbox.Name = "TruckNoTextbox"
        Me.TruckNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoTextbox.SelectedText = ""
        Me.TruckNoTextbox.SelectionLength = 0
        Me.TruckNoTextbox.SelectionStart = 0
        Me.TruckNoTextbox.Size = New System.Drawing.Size(411, 30)
        Me.TruckNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckNoTextbox.TabIndex = 102
        Me.TruckNoTextbox.UseCustomBackColor = True
        Me.TruckNoTextbox.UseSelectable = True
        Me.TruckNoTextbox.UseStyleColors = True
        Me.TruckNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(176, 307)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(68, 20)
        Me.MetroLabel2.TabIndex = 105
        Me.MetroLabel2.Text = "Truck No."
        '
        'TruckIDTextbox
        '
        Me.TruckIDTextbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TruckIDTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TruckIDTextbox.CustomButton.Image = Nothing
        Me.TruckIDTextbox.CustomButton.Location = New System.Drawing.Point(76, 2)
        Me.TruckIDTextbox.CustomButton.Name = ""
        Me.TruckIDTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TruckIDTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckIDTextbox.CustomButton.TabIndex = 1
        Me.TruckIDTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckIDTextbox.CustomButton.UseSelectable = True
        Me.TruckIDTextbox.CustomButton.Visible = False
        Me.TruckIDTextbox.Enabled = False
        Me.TruckIDTextbox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TruckIDTextbox.Lines = New String(-1) {}
        Me.TruckIDTextbox.Location = New System.Drawing.Point(275, 271)
        Me.TruckIDTextbox.MaxLength = 4
        Me.TruckIDTextbox.Name = "TruckIDTextbox"
        Me.TruckIDTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckIDTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckIDTextbox.SelectedText = ""
        Me.TruckIDTextbox.SelectionLength = 0
        Me.TruckIDTextbox.SelectionStart = 0
        Me.TruckIDTextbox.Size = New System.Drawing.Size(104, 30)
        Me.TruckIDTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckIDTextbox.TabIndex = 101
        Me.TruckIDTextbox.UseCustomBackColor = True
        Me.TruckIDTextbox.UseSelectable = True
        Me.TruckIDTextbox.UseStyleColors = True
        Me.TruckIDTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckIDTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(176, 271)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(61, 20)
        Me.MetroLabel1.TabIndex = 104
        Me.MetroLabel1.Text = "Truck ID."
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_SEL_ShippingTruckBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingTruckBindingSource.DataMember = "sp_Shipping_SEL_ShippingTruck"
        Me.Sp_Shipping_SEL_ShippingTruckBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingTruckTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingTruckTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Status
        '
        Me.Status.AutoSize = True
        Me.Status.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.Status.Location = New System.Drawing.Point(275, 388)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(137, 20)
        Me.Status.Style = MetroFramework.MetroColorStyle.Orange
        Me.Status.TabIndex = 109
        Me.Status.Text = "MetroCheckBox1"
        Me.Status.UseSelectable = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(23, 94)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 113
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(76, 113)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 114
        Me.UsernameMetroLabel.Text = "Username"
        '
        'FrmUpdateMovementTruck
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1010, 582)
        Me.Controls.Add(Me.MetroTile1)
        Me.Controls.Add(Me.UsernameMetroLabel)
        Me.Controls.Add(Me.Status)
        Me.Controls.Add(Me.DriverTextbox)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.AddMetroTile)
        Me.Controls.Add(Me.TruckNoTextbox)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.TruckIDTextbox)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "FrmUpdateMovementTruck"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Update movement truck"
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingTruckBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DriverTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents TruckNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TruckIDTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_ShippingTruckBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingTruckTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingTruckTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Status As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
End Class

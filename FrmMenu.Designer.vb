﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMenu
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.BtnOrder = New System.Windows.Forms.Button()
        Me.BtnMovement = New System.Windows.Forms.Button()
        Me.BtnBayControl = New System.Windows.Forms.Button()
        Me.SettingButton = New System.Windows.Forms.Button()
        Me.BtnReceived = New System.Windows.Forms.Button()
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ShippingButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(33, 0)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(68, 30)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 122
        Me.UsernameMetroLabel.Text = "Username"
        Me.UsernameMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnOrder
        '
        Me.BtnOrder.BackColor = System.Drawing.Color.DarkKhaki
        Me.BtnOrder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnOrder.FlatAppearance.BorderSize = 0
        Me.BtnOrder.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnOrder.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BtnOrder.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOrder.ForeColor = System.Drawing.Color.Black
        Me.BtnOrder.Image = Global.ShippingSystem.My.Resources.Resources.Natural_Food641
        Me.BtnOrder.Location = New System.Drawing.Point(315, 3)
        Me.BtnOrder.Name = "BtnOrder"
        Me.BtnOrder.Size = New System.Drawing.Size(150, 150)
        Me.BtnOrder.TabIndex = 10
        Me.BtnOrder.Text = "Order"
        Me.BtnOrder.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnOrder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BtnOrder.UseVisualStyleBackColor = False
        '
        'BtnMovement
        '
        Me.BtnMovement.BackColor = System.Drawing.Color.LimeGreen
        Me.BtnMovement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnMovement.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMovement.FlatAppearance.BorderSize = 0
        Me.BtnMovement.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnMovement.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BtnMovement.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnMovement.ForeColor = System.Drawing.Color.Black
        Me.BtnMovement.Image = Global.ShippingSystem.My.Resources.Resources.Warehouse52
        Me.BtnMovement.Location = New System.Drawing.Point(3, 3)
        Me.BtnMovement.Name = "BtnMovement"
        Me.BtnMovement.Size = New System.Drawing.Size(150, 150)
        Me.BtnMovement.TabIndex = 9
        Me.BtnMovement.Text = "Movement"
        Me.BtnMovement.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnMovement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BtnMovement.UseVisualStyleBackColor = False
        '
        'BtnBayControl
        '
        Me.BtnBayControl.BackColor = System.Drawing.Color.SkyBlue
        Me.BtnBayControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnBayControl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnBayControl.FlatAppearance.BorderSize = 0
        Me.BtnBayControl.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnBayControl.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BtnBayControl.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBayControl.ForeColor = System.Drawing.Color.Black
        Me.BtnBayControl.Image = Global.ShippingSystem.My.Resources.Resources.MoveStock64
        Me.BtnBayControl.Location = New System.Drawing.Point(471, 3)
        Me.BtnBayControl.Name = "BtnBayControl"
        Me.BtnBayControl.Size = New System.Drawing.Size(150, 150)
        Me.BtnBayControl.TabIndex = 8
        Me.BtnBayControl.Text = "Bay control"
        Me.BtnBayControl.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnBayControl.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BtnBayControl.UseVisualStyleBackColor = False
        '
        'SettingButton
        '
        Me.SettingButton.BackColor = System.Drawing.Color.LightGray
        Me.SettingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SettingButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SettingButton.FlatAppearance.BorderSize = 0
        Me.SettingButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SettingButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SettingButton.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SettingButton.ForeColor = System.Drawing.Color.Black
        Me.SettingButton.Image = Global.ShippingSystem.My.Resources.Resources.Settings50
        Me.SettingButton.Location = New System.Drawing.Point(159, 3)
        Me.SettingButton.Name = "SettingButton"
        Me.SettingButton.Size = New System.Drawing.Size(150, 150)
        Me.SettingButton.TabIndex = 5
        Me.SettingButton.Text = "Setting"
        Me.SettingButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.SettingButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.SettingButton.UseVisualStyleBackColor = False
        '
        'BtnReceived
        '
        Me.BtnReceived.BackColor = System.Drawing.Color.Gold
        Me.BtnReceived.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnReceived.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnReceived.FlatAppearance.BorderSize = 0
        Me.BtnReceived.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnReceived.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BtnReceived.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnReceived.ForeColor = System.Drawing.Color.Black
        Me.BtnReceived.Image = Global.ShippingSystem.My.Resources.Resources.CheckedCheckbox64
        Me.BtnReceived.Location = New System.Drawing.Point(159, 3)
        Me.BtnReceived.Name = "BtnReceived"
        Me.BtnReceived.Size = New System.Drawing.Size(150, 150)
        Me.BtnReceived.TabIndex = 3
        Me.BtnReceived.Text = "Received"
        Me.BtnReceived.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnReceived.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BtnReceived.UseVisualStyleBackColor = False
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ShippingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.PictureBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(624, 30)
        Me.FlowLayoutPanel1.TabIndex = 127
        '
        'PictureBox1
        '
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Image = Global.ShippingSystem.My.Resources.Resources.icons8_user_24
        Me.PictureBox1.InitialImage = Nothing
        Me.PictureBox1.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(24, 24)
        Me.PictureBox1.TabIndex = 130
        Me.PictureBox1.TabStop = False
        '
        'ShippingButton
        '
        Me.ShippingButton.BackColor = System.Drawing.Color.Pink
        Me.ShippingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ShippingButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ShippingButton.FlatAppearance.BorderSize = 0
        Me.ShippingButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.ShippingButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ShippingButton.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ShippingButton.ForeColor = System.Drawing.Color.Black
        Me.ShippingButton.Image = Global.ShippingSystem.My.Resources.Resources.Truck52
        Me.ShippingButton.Location = New System.Drawing.Point(3, 3)
        Me.ShippingButton.Name = "ShippingButton"
        Me.ShippingButton.Size = New System.Drawing.Size(150, 150)
        Me.ShippingButton.TabIndex = 11
        Me.ShippingButton.Text = "Shipping"
        Me.ShippingButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ShippingButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.ShippingButton.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.BtnMovement)
        Me.FlowLayoutPanel2.Controls.Add(Me.BtnReceived)
        Me.FlowLayoutPanel2.Controls.Add(Me.BtnOrder)
        Me.FlowLayoutPanel2.Controls.Add(Me.BtnBayControl)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 39)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(624, 156)
        Me.FlowLayoutPanel2.TabIndex = 128
        Me.FlowLayoutPanel2.WrapContents = False
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel4)
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(630, 360)
        Me.FlowLayoutPanel3.TabIndex = 129
        Me.FlowLayoutPanel3.WrapContents = False
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.ShippingButton)
        Me.FlowLayoutPanel4.Controls.Add(Me.SettingButton)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 201)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(624, 156)
        Me.FlowLayoutPanel4.TabIndex = 129
        '
        'FrmMenu
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(829, 537)
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Name = "FrmMenu"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Main Menu"
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SettingButton As System.Windows.Forms.Button
    Friend WithEvents BtnReceived As System.Windows.Forms.Button
    Friend WithEvents ShippingDataSet As ShippingSystem.ShippingDataSet
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents BtnBayControl As System.Windows.Forms.Button
    Friend WithEvents BtnMovement As System.Windows.Forms.Button
    Friend WithEvents BtnOrder As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents ShippingButton As Button
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents PictureBox1 As PictureBox
End Class

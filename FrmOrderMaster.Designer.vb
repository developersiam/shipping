﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmOrderMaster
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.ShowBarcodeListMetroTile = New MetroFramework.Controls.MetroTile()
        Me.ShowStockTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.DeleteTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.ShippingOrderTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.AddTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.OrdernoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrderDetailNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SupplierDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvoicingcomDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerGradeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CasesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NettDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELOrderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_SEL_OrderTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_OrderTableAdapter()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter()
        Me.Sp_Shipping_SEL_ShippingOrderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.SearchTile = New MetroFramework.Controls.MetroTile()
        Me.GradeTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELOrderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.MetroLabel13)
        Me.MetroPanel1.Controls.Add(Me.ShowBarcodeListMetroTile)
        Me.MetroPanel1.Controls.Add(Me.ShowStockTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.DeleteTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.ShippingOrderTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel3)
        Me.MetroPanel1.Controls.Add(Me.AddTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(2, 54)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1269, 62)
        Me.MetroPanel1.TabIndex = 196
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.Location = New System.Drawing.Point(314, 27)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(110, 19)
        Me.MetroLabel13.TabIndex = 141
        Me.MetroLabel13.Text = "show barcode list"
        '
        'ShowBarcodeListMetroTile
        '
        Me.ShowBarcodeListMetroTile.ActiveControl = Nothing
        Me.ShowBarcodeListMetroTile.AutoSize = True
        Me.ShowBarcodeListMetroTile.BackColor = System.Drawing.Color.White
        Me.ShowBarcodeListMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ShowBarcodeListMetroTile.Location = New System.Drawing.Point(272, 21)
        Me.ShowBarcodeListMetroTile.Name = "ShowBarcodeListMetroTile"
        Me.ShowBarcodeListMetroTile.Size = New System.Drawing.Size(36, 35)
        Me.ShowBarcodeListMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.ShowBarcodeListMetroTile.TabIndex = 140
        Me.ShowBarcodeListMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowBarcodeListMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.UseLogin1
        Me.ShowBarcodeListMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowBarcodeListMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ShowBarcodeListMetroTile.UseSelectable = True
        Me.ShowBarcodeListMetroTile.UseTileImage = True
        '
        'ShowStockTile
        '
        Me.ShowStockTile.ActiveControl = Nothing
        Me.ShowStockTile.AutoSize = True
        Me.ShowStockTile.BackColor = System.Drawing.Color.White
        Me.ShowStockTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ShowStockTile.Location = New System.Drawing.Point(434, 21)
        Me.ShowStockTile.Name = "ShowStockTile"
        Me.ShowStockTile.Size = New System.Drawing.Size(36, 35)
        Me.ShowStockTile.Style = MetroFramework.MetroColorStyle.White
        Me.ShowStockTile.TabIndex = 139
        Me.ShowStockTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowStockTile.TileImage = Global.ShippingSystem.My.Resources.Resources.UseLogin1
        Me.ShowStockTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowStockTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ShowStockTile.UseSelectable = True
        Me.ShowStockTile.UseTileImage = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(476, 27)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(71, 19)
        Me.MetroLabel4.TabIndex = 138
        Me.MetroLabel4.Text = "show stock"
        '
        'DeleteTile
        '
        Me.DeleteTile.ActiveControl = Nothing
        Me.DeleteTile.AutoSize = True
        Me.DeleteTile.BackColor = System.Drawing.Color.White
        Me.DeleteTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteTile.Location = New System.Drawing.Point(720, 21)
        Me.DeleteTile.Name = "DeleteTile"
        Me.DeleteTile.Size = New System.Drawing.Size(36, 35)
        Me.DeleteTile.Style = MetroFramework.MetroColorStyle.White
        Me.DeleteTile.TabIndex = 131
        Me.DeleteTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DeleteTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Cancel32
        Me.DeleteTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DeleteTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.DeleteTile.UseSelectable = True
        Me.DeleteTile.UseTileImage = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(762, 29)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel5.TabIndex = 128
        Me.MetroLabel5.Text = "delete"
        '
        'ShippingOrderTile
        '
        Me.ShippingOrderTile.ActiveControl = Nothing
        Me.ShippingOrderTile.AutoSize = True
        Me.ShippingOrderTile.BackColor = System.Drawing.Color.White
        Me.ShippingOrderTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ShippingOrderTile.Location = New System.Drawing.Point(840, 21)
        Me.ShippingOrderTile.Name = "ShippingOrderTile"
        Me.ShippingOrderTile.Size = New System.Drawing.Size(36, 35)
        Me.ShippingOrderTile.Style = MetroFramework.MetroColorStyle.White
        Me.ShippingOrderTile.TabIndex = 133
        Me.ShippingOrderTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShippingOrderTile.TileImage = Global.ShippingSystem.My.Resources.Resources.PurchaseOrder322
        Me.ShippingOrderTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShippingOrderTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ShippingOrderTile.UseSelectable = True
        Me.ShippingOrderTile.UseTileImage = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(882, 26)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(95, 19)
        Me.MetroLabel3.TabIndex = 132
        Me.MetroLabel3.Text = "shipping order"
        '
        'AddTile
        '
        Me.AddTile.ActiveControl = Nothing
        Me.AddTile.AutoSize = True
        Me.AddTile.BackColor = System.Drawing.Color.White
        Me.AddTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddTile.Location = New System.Drawing.Point(596, 21)
        Me.AddTile.Name = "AddTile"
        Me.AddTile.Size = New System.Drawing.Size(36, 35)
        Me.AddTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddTile.TabIndex = 130
        Me.AddTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Plus_32
        Me.AddTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddTile.UseSelectable = True
        Me.AddTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(638, 27)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(45, 19)
        Me.MetroLabel2.TabIndex = 129
        Me.MetroLabel2.Text = "create"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(1002, 18)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 126
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(1044, 28)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel1.TabIndex = 118
        Me.MetroLabel1.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1189, 12)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 9)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 28)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OrdernoDataGridViewTextBoxColumn, Me.OrderDetailNoDataGridViewTextBoxColumn, Me.DateDataGridViewTextBoxColumn, Me.SupplierDataGridViewTextBoxColumn, Me.InvoicingcomDataGridViewTextBoxColumn, Me.CustomerDataGridViewTextBoxColumn, Me.CustomerGradeDataGridViewTextBoxColumn, Me.GradeDataGridViewTextBoxColumn, Me.CasesDataGridViewTextBoxColumn, Me.NettDataGridViewTextBoxColumn, Me.NetDataGridViewTextBoxColumn})
        Me.MetroGrid.DataSource = Me.SpShippingSELOrderBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(5, 168)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 28
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(1266, 572)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 197
        '
        'OrdernoDataGridViewTextBoxColumn
        '
        Me.OrdernoDataGridViewTextBoxColumn.DataPropertyName = "orderno"
        Me.OrdernoDataGridViewTextBoxColumn.HeaderText = "orderno"
        Me.OrdernoDataGridViewTextBoxColumn.Name = "OrdernoDataGridViewTextBoxColumn"
        Me.OrdernoDataGridViewTextBoxColumn.ReadOnly = True
        Me.OrdernoDataGridViewTextBoxColumn.Width = 82
        '
        'OrderDetailNoDataGridViewTextBoxColumn
        '
        Me.OrderDetailNoDataGridViewTextBoxColumn.DataPropertyName = "OrderDetailNo"
        Me.OrderDetailNoDataGridViewTextBoxColumn.HeaderText = "OrderDetailNo"
        Me.OrderDetailNoDataGridViewTextBoxColumn.Name = "OrderDetailNoDataGridViewTextBoxColumn"
        Me.OrderDetailNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.OrderDetailNoDataGridViewTextBoxColumn.Width = 126
        '
        'DateDataGridViewTextBoxColumn
        '
        Me.DateDataGridViewTextBoxColumn.DataPropertyName = "date"
        Me.DateDataGridViewTextBoxColumn.HeaderText = "date"
        Me.DateDataGridViewTextBoxColumn.Name = "DateDataGridViewTextBoxColumn"
        Me.DateDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateDataGridViewTextBoxColumn.Width = 61
        '
        'SupplierDataGridViewTextBoxColumn
        '
        Me.SupplierDataGridViewTextBoxColumn.DataPropertyName = "supplier"
        Me.SupplierDataGridViewTextBoxColumn.HeaderText = "supplier"
        Me.SupplierDataGridViewTextBoxColumn.Name = "SupplierDataGridViewTextBoxColumn"
        Me.SupplierDataGridViewTextBoxColumn.ReadOnly = True
        Me.SupplierDataGridViewTextBoxColumn.Width = 84
        '
        'InvoicingcomDataGridViewTextBoxColumn
        '
        Me.InvoicingcomDataGridViewTextBoxColumn.DataPropertyName = "invoicingcom"
        Me.InvoicingcomDataGridViewTextBoxColumn.HeaderText = "invoicingcom"
        Me.InvoicingcomDataGridViewTextBoxColumn.Name = "InvoicingcomDataGridViewTextBoxColumn"
        Me.InvoicingcomDataGridViewTextBoxColumn.ReadOnly = True
        Me.InvoicingcomDataGridViewTextBoxColumn.Width = 117
        '
        'CustomerDataGridViewTextBoxColumn
        '
        Me.CustomerDataGridViewTextBoxColumn.DataPropertyName = "customer"
        Me.CustomerDataGridViewTextBoxColumn.HeaderText = "customer"
        Me.CustomerDataGridViewTextBoxColumn.Name = "CustomerDataGridViewTextBoxColumn"
        Me.CustomerDataGridViewTextBoxColumn.ReadOnly = True
        Me.CustomerDataGridViewTextBoxColumn.Width = 92
        '
        'CustomerGradeDataGridViewTextBoxColumn
        '
        Me.CustomerGradeDataGridViewTextBoxColumn.DataPropertyName = "CustomerGrade"
        Me.CustomerGradeDataGridViewTextBoxColumn.HeaderText = "CustomerGrade"
        Me.CustomerGradeDataGridViewTextBoxColumn.Name = "CustomerGradeDataGridViewTextBoxColumn"
        Me.CustomerGradeDataGridViewTextBoxColumn.ReadOnly = True
        Me.CustomerGradeDataGridViewTextBoxColumn.Width = 133
        '
        'GradeDataGridViewTextBoxColumn
        '
        Me.GradeDataGridViewTextBoxColumn.DataPropertyName = "grade"
        Me.GradeDataGridViewTextBoxColumn.HeaderText = "grade"
        Me.GradeDataGridViewTextBoxColumn.Name = "GradeDataGridViewTextBoxColumn"
        Me.GradeDataGridViewTextBoxColumn.ReadOnly = True
        Me.GradeDataGridViewTextBoxColumn.Width = 69
        '
        'CasesDataGridViewTextBoxColumn
        '
        Me.CasesDataGridViewTextBoxColumn.DataPropertyName = "cases"
        Me.CasesDataGridViewTextBoxColumn.HeaderText = "cases"
        Me.CasesDataGridViewTextBoxColumn.Name = "CasesDataGridViewTextBoxColumn"
        Me.CasesDataGridViewTextBoxColumn.ReadOnly = True
        Me.CasesDataGridViewTextBoxColumn.Width = 69
        '
        'NettDataGridViewTextBoxColumn
        '
        Me.NettDataGridViewTextBoxColumn.DataPropertyName = "nett"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.NettDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.NettDataGridViewTextBoxColumn.HeaderText = "nett"
        Me.NettDataGridViewTextBoxColumn.Name = "NettDataGridViewTextBoxColumn"
        Me.NettDataGridViewTextBoxColumn.ReadOnly = True
        Me.NettDataGridViewTextBoxColumn.Width = 58
        '
        'NetDataGridViewTextBoxColumn
        '
        Me.NetDataGridViewTextBoxColumn.DataPropertyName = "net"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.NetDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.NetDataGridViewTextBoxColumn.HeaderText = "net/case"
        Me.NetDataGridViewTextBoxColumn.Name = "NetDataGridViewTextBoxColumn"
        Me.NetDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetDataGridViewTextBoxColumn.Width = 89
        '
        'SpShippingSELOrderBindingSource
        '
        Me.SpShippingSELOrderBindingSource.DataMember = "sp_Shipping_SEL_Order"
        Me.SpShippingSELOrderBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_SEL_OrderTableAdapter
        '
        Me.Sp_Shipping_SEL_OrderTableAdapter.ClearBeforeFill = True
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ShippingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrderByOrderNo"
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_ShippingOrderBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingOrderBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrder"
        Me.Sp_Shipping_SEL_ShippingOrderBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingOrderTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderTableAdapter.ClearBeforeFill = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel6.Location = New System.Drawing.Point(940, 129)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(58, 25)
        Me.MetroLabel6.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel6.TabIndex = 198
        Me.MetroLabel6.Text = "Grade"
        '
        'SearchTile
        '
        Me.SearchTile.ActiveControl = Nothing
        Me.SearchTile.AutoSize = True
        Me.SearchTile.BackColor = System.Drawing.Color.White
        Me.SearchTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SearchTile.Location = New System.Drawing.Point(1200, 122)
        Me.SearchTile.Name = "SearchTile"
        Me.SearchTile.Size = New System.Drawing.Size(36, 35)
        Me.SearchTile.Style = MetroFramework.MetroColorStyle.White
        Me.SearchTile.TabIndex = 199
        Me.SearchTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SearchTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Search32
        Me.SearchTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SearchTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SearchTile.UseSelectable = True
        Me.SearchTile.UseTileImage = True
        '
        'GradeTextBox
        '
        Me.GradeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.GradeTextBox.CustomButton.Image = Nothing
        Me.GradeTextBox.CustomButton.Location = New System.Drawing.Point(152, 2)
        Me.GradeTextBox.CustomButton.Name = ""
        Me.GradeTextBox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.GradeTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.GradeTextBox.CustomButton.TabIndex = 1
        Me.GradeTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.GradeTextBox.CustomButton.UseSelectable = True
        Me.GradeTextBox.CustomButton.Visible = False
        Me.GradeTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.GradeTextBox.Lines = New String(-1) {}
        Me.GradeTextBox.Location = New System.Drawing.Point(1004, 122)
        Me.GradeTextBox.MaxLength = 32767
        Me.GradeTextBox.Name = "GradeTextBox"
        Me.GradeTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.GradeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.GradeTextBox.SelectedText = ""
        Me.GradeTextBox.SelectionLength = 0
        Me.GradeTextBox.SelectionStart = 0
        Me.GradeTextBox.ShortcutsEnabled = True
        Me.GradeTextBox.Size = New System.Drawing.Size(190, 40)
        Me.GradeTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.GradeTextBox.TabIndex = 200
        Me.GradeTextBox.UseSelectable = True
        Me.GradeTextBox.UseStyleColors = True
        Me.GradeTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.GradeTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FrmOrderMaster
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1299, 800)
        Me.Controls.Add(Me.GradeTextBox)
        Me.Controls.Add(Me.SearchTile)
        Me.Controls.Add(Me.MetroLabel6)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.MetroGrid)
        Me.Name = "FrmOrderMaster"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Order Master"
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELOrderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents DeleteTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents ShippingOrderTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents SpShippingSELOrderBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_OrderTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_OrderTableAdapter
    Friend WithEvents SecurityBindingSource As BindingSource
    Friend WithEvents SecurityTableAdapter As ShippingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ShowStockTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderTableAdapter
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SearchTile As MetroFramework.Controls.MetroTile
    Friend WithEvents GradeTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents OrdernoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrderDetailNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SupplierDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents InvoicingcomDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerGradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CasesDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NettDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShowBarcodeListMetroTile As MetroFramework.Controls.MetroTile
End Class

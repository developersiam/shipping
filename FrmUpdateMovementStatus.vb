﻿Public Class FrmUpdateMovementStatus
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private Sub FrmUpdateMovementStatus_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.sp_Shipping_SEL_ShippingMovementStatus' table. You can move, or remove it, as needed.

        Try
            Me.Size = New Size(1280, 768)
            'StatusIDTextbox.Text = ""
            StatusNameTextbox.Text = ""

            'Get data from matrc
            Dim XInfoRow As ShippingDataSet.sp_Shipping_SEL_ShippingMovementStatusRow
            Me.Sp_Shipping_SEL_ShippingMovementStatusTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementStatus)
            XInfoRow = Me.ShippingDataSet.sp_Shipping_SEL_ShippingMovementStatus.FindByMovementStatusID(StatusIDTextbox.Text)

            If XInfoRow Is Nothing Then
                MessageBox.Show("ไม่พบเลขนี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            StatusNameTextbox.Text = XInfoRow.MovementStatusName

            If XInfoRow.Status = True Then
                Status.Text = "Active"
                Status.Checked = True
            ElseIf XInfoRow.Status = False Then
                Status.Text = "Unactive"
                Status.Checked = False
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If StatusIDTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Status ID", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If StatusNameTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Status name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim XStatus As Boolean
            If Status.Checked = True Then
                XStatus = True
            Else
                XStatus = False
            End If

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการแก้ไข status ID นี้ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                db.sp_Shipping_UPD_ShippingMovementStatus(StatusIDTextbox.Text, StatusNameTextbox.Text, XStatus)

                MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
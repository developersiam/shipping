﻿Imports FactoryBL
Imports FactoryEntities

Public Class EditTruckNo
    Inherits MetroFramework.Forms.MetroForm

    Private _truckID As String
    Public Property TruckID() As String
        Get
            Return _truckID
        End Get
        Set(ByVal value As String)
            _truckID = value
        End Set
    End Property

    Private _truck As ShippingTruck
    Public Property Truck() As ShippingTruck
        Get
            Return _truck
        End Get
        Set(ByVal value As ShippingTruck)
            _truck = value
        End Set
    End Property

    Private Sub SaveMetroButton_Click(sender As Object, e As EventArgs) Handles SaveMetroButton.Click
        Try
            If String.IsNullOrEmpty(NewTruckNoMetroTextBox.Text) Then
                Throw New Exception("โปรดระบุทะเบียนรถบรรทุก")
            End If

            If _truck.TruckNo = NewTruckNoMetroTextBox.Text Then
                Throw New Exception("ทะเบียนรถที่ท่านต้องการแก้ไขนี้ ซ้ำกับข้อมูลเดิม")
            End If

            If MessageBoxModule.Question("ท่านต้องการเพิ่มรายการทะเบียนรถบรรทุกในระบบใช่หรือไม่?") = MsgBoxResult.No Then
                Return
            End If

            Facade.ShippingTruckBL().Edit(_truckID, NewTruckNoMetroTextBox.Text, XUsername)
            MessageBoxModule.Info("บันทึกข้อมูลสำเร็จ")
            Me.Close()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub EditTruckNo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            _truck = Facade.ShippingTruckBL().GetSingle(_truckID)
            If _truck Is Nothing Then
                Throw New Exception("ไม่พบข้อมูลทะเบียนรถ TruckID : " + _truckID + " นี้ในระบบ")
            End If

            OldTruckNoMetroTextBox.Text = _truck.TruckNo
            NewTruckNoMetroTextBox.Text = _truck.TruckNo
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
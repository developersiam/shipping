﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TruckList
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ShowAllMetroCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.AddMetroButton = New MetroFramework.Controls.MetroButton()
        Me.EditTruckNoMetroButton = New MetroFramework.Controls.MetroButton()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.TruckListMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.TruckID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TruckNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CreateDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ModifiedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.TotalRecordMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.TruckListMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.TruckListMetroGrid)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(752, 411)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.Controls.Add(Me.ShowAllMetroCheckBox)
        Me.FlowLayoutPanel3.Controls.Add(Me.AddMetroButton)
        Me.FlowLayoutPanel3.Controls.Add(Me.EditTruckNoMetroButton)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(746, 36)
        Me.FlowLayoutPanel3.TabIndex = 4
        '
        'ShowAllMetroCheckBox
        '
        Me.ShowAllMetroCheckBox.AutoSize = True
        Me.ShowAllMetroCheckBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.ShowAllMetroCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.ShowAllMetroCheckBox.Location = New System.Drawing.Point(3, 3)
        Me.ShowAllMetroCheckBox.Name = "ShowAllMetroCheckBox"
        Me.ShowAllMetroCheckBox.Size = New System.Drawing.Size(90, 30)
        Me.ShowAllMetroCheckBox.Style = MetroFramework.MetroColorStyle.Green
        Me.ShowAllMetroCheckBox.TabIndex = 2
        Me.ShowAllMetroCheckBox.Text = "แสดงทั้งหมด"
        Me.ShowAllMetroCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ShowAllMetroCheckBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.ShowAllMetroCheckBox.UseSelectable = True
        Me.ShowAllMetroCheckBox.UseStyleColors = True
        '
        'AddMetroButton
        '
        Me.AddMetroButton.Location = New System.Drawing.Point(99, 3)
        Me.AddMetroButton.Name = "AddMetroButton"
        Me.AddMetroButton.Size = New System.Drawing.Size(125, 30)
        Me.AddMetroButton.TabIndex = 0
        Me.AddMetroButton.Text = "เพิ่มทะเบียนรถบรรทุก"
        Me.AddMetroButton.UseSelectable = True
        '
        'EditTruckNoMetroButton
        '
        Me.EditTruckNoMetroButton.Location = New System.Drawing.Point(230, 3)
        Me.EditTruckNoMetroButton.Name = "EditTruckNoMetroButton"
        Me.EditTruckNoMetroButton.Size = New System.Drawing.Size(102, 30)
        Me.EditTruckNoMetroButton.TabIndex = 1
        Me.EditTruckNoMetroButton.Text = "แก้ไขทะเบียนรถ"
        Me.EditTruckNoMetroButton.UseSelectable = True
        Me.EditTruckNoMetroButton.Visible = False
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel2.LabelMode = MetroFramework.Controls.MetroLabelMode.Selectable
        Me.MetroLabel2.Location = New System.Drawing.Point(3, 42)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(213, 15)
        Me.MetroLabel2.Style = MetroFramework.MetroColorStyle.Green
        Me.MetroLabel2.TabIndex = 5
        Me.MetroLabel2.Text = "**คลิกที่แถวข้อมูลหากต้องการแก้ไขทะเบียนรถ"
        Me.MetroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroLabel2.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroLabel2.UseMnemonic = False
        '
        'TruckListMetroGrid
        '
        Me.TruckListMetroGrid.AllowUserToAddRows = False
        Me.TruckListMetroGrid.AllowUserToDeleteRows = False
        Me.TruckListMetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TruckListMetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.TruckListMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TruckListMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TruckListMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.TruckListMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(89, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TruckListMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.TruckListMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TruckListMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TruckID, Me.TruckNo, Me.Status, Me.CreateDate, Me.ModifiedDate})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TruckListMetroGrid.DefaultCellStyle = DataGridViewCellStyle3
        Me.TruckListMetroGrid.EnableHeadersVisualStyles = False
        Me.TruckListMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.TruckListMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TruckListMetroGrid.Location = New System.Drawing.Point(3, 60)
        Me.TruckListMetroGrid.Name = "TruckListMetroGrid"
        Me.TruckListMetroGrid.ReadOnly = True
        Me.TruckListMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(89, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TruckListMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.TruckListMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.TruckListMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.TruckListMetroGrid.Size = New System.Drawing.Size(746, 313)
        Me.TruckListMetroGrid.Style = MetroFramework.MetroColorStyle.Green
        Me.TruckListMetroGrid.TabIndex = 0
        Me.TruckListMetroGrid.Theme = MetroFramework.MetroThemeStyle.Light
        '
        'TruckID
        '
        Me.TruckID.DataPropertyName = "TruckID"
        Me.TruckID.HeaderText = "TruckID"
        Me.TruckID.Name = "TruckID"
        Me.TruckID.ReadOnly = True
        '
        'TruckNo
        '
        Me.TruckNo.DataPropertyName = "TruckNo"
        Me.TruckNo.HeaderText = "TruckNo"
        Me.TruckNo.Name = "TruckNo"
        Me.TruckNo.ReadOnly = True
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'CreateDate
        '
        Me.CreateDate.DataPropertyName = "CreateDate"
        Me.CreateDate.HeaderText = "CreateDate"
        Me.CreateDate.Name = "CreateDate"
        Me.CreateDate.ReadOnly = True
        '
        'ModifiedDate
        '
        Me.ModifiedDate.DataPropertyName = "ModifiedDate"
        Me.ModifiedDate.HeaderText = "ModifiedDate"
        Me.ModifiedDate.Name = "ModifiedDate"
        Me.ModifiedDate.ReadOnly = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel2.Controls.Add(Me.TotalRecordMetroLabel)
        Me.FlowLayoutPanel2.Controls.Add(Me.MetroLabel3)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 379)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(131, 19)
        Me.FlowLayoutPanel2.TabIndex = 3
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(29, 19)
        Me.MetroLabel1.TabIndex = 1
        Me.MetroLabel1.Text = "รวม"
        '
        'TotalRecordMetroLabel
        '
        Me.TotalRecordMetroLabel.AutoSize = True
        Me.TotalRecordMetroLabel.Location = New System.Drawing.Point(38, 0)
        Me.TotalRecordMetroLabel.Name = "TotalRecordMetroLabel"
        Me.TotalRecordMetroLabel.Size = New System.Drawing.Size(36, 19)
        Me.TotalRecordMetroLabel.TabIndex = 2
        Me.TotalRecordMetroLabel.Text = "x,xxx"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(80, 0)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel3.TabIndex = 3
        Me.MetroLabel3.Text = "รายการ"
        '
        'TruckList
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(792, 491)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "TruckList"
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "รายการข้อมูลทะเบียนรถบรรทุก"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        CType(Me.TruckListMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents TruckListMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalRecordMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents AddMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents EditTruckNoMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents ShowAllMetroCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TruckID As DataGridViewTextBoxColumn
    Friend WithEvents TruckNo As DataGridViewTextBoxColumn
    Friend WithEvents Status As DataGridViewCheckBoxColumn
    Friend WithEvents CreateDate As DataGridViewTextBoxColumn
    Friend WithEvents ModifiedDate As DataGridViewTextBoxColumn
End Class

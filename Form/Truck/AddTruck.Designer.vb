﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddTruck
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.TruckNoMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.AddMetroButton = New MetroFramework.Controls.MetroButton()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.TruckNoMetroTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.AddMetroButton)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(375, 137)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(119, 19)
        Me.MetroLabel1.TabIndex = 0
        Me.MetroLabel1.Text = "เลขทะเบียนรถบรรทุก"
        '
        'TruckNoMetroTextBox
        '
        '
        '
        '
        Me.TruckNoMetroTextBox.CustomButton.Image = Nothing
        Me.TruckNoMetroTextBox.CustomButton.Location = New System.Drawing.Point(174, 1)
        Me.TruckNoMetroTextBox.CustomButton.Name = ""
        Me.TruckNoMetroTextBox.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TruckNoMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoMetroTextBox.CustomButton.TabIndex = 1
        Me.TruckNoMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoMetroTextBox.CustomButton.UseSelectable = True
        Me.TruckNoMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TruckNoMetroTextBox.Lines = New String(-1) {}
        Me.TruckNoMetroTextBox.Location = New System.Drawing.Point(3, 22)
        Me.TruckNoMetroTextBox.MaxLength = 32767
        Me.TruckNoMetroTextBox.Name = "TruckNoMetroTextBox"
        Me.TruckNoMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckNoMetroTextBox.PromptText = "TruckNo"
        Me.TruckNoMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoMetroTextBox.SelectedText = ""
        Me.TruckNoMetroTextBox.SelectionLength = 0
        Me.TruckNoMetroTextBox.SelectionStart = 0
        Me.TruckNoMetroTextBox.ShortcutsEnabled = True
        Me.TruckNoMetroTextBox.ShowButton = True
        Me.TruckNoMetroTextBox.ShowClearButton = True
        Me.TruckNoMetroTextBox.Size = New System.Drawing.Size(196, 25)
        Me.TruckNoMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.TruckNoMetroTextBox.TabIndex = 1
        Me.TruckNoMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoMetroTextBox.UseSelectable = True
        Me.TruckNoMetroTextBox.WaterMark = "TruckNo"
        Me.TruckNoMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'AddMetroButton
        '
        Me.AddMetroButton.Location = New System.Drawing.Point(3, 53)
        Me.AddMetroButton.Name = "AddMetroButton"
        Me.AddMetroButton.Size = New System.Drawing.Size(100, 23)
        Me.AddMetroButton.Style = MetroFramework.MetroColorStyle.Green
        Me.AddMetroButton.TabIndex = 2
        Me.AddMetroButton.Text = "บันทึกข้อมูล"
        Me.AddMetroButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.AddMetroButton.UseSelectable = True
        '
        'AddTruck
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(415, 217)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "AddTruck"
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "เพิ่มทะเบียนรถบรรทุก"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TruckNoMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents AddMetroButton As MetroFramework.Controls.MetroButton
End Class

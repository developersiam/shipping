﻿Imports FactoryBL
Imports FactoryEntities
Imports FastMember

Public Class TruckList
    Inherits MetroFramework.Forms.MetroForm

    Private _truckList As List(Of ShippingTruck)
    Public Property TruckList() As List(Of ShippingTruck)
        Get
            Return _truckList
        End Get
        Set(ByVal value As List(Of ShippingTruck))
            _truckList = value
        End Set
    End Property

    Private _totalRecord As Integer
    Public Property TotalRecord() As Integer
        Get
            Return _totalRecord
        End Get
        Set(ByVal value As Integer)
            _totalRecord = value
        End Set
    End Property

    Private _isShowAll As Boolean
    Public Property IsShowAll() As Boolean
        Get
            Return _isShowAll
        End Get
        Set(ByVal value As Boolean)
            _isShowAll = value
        End Set
    End Property

    Private Sub TruckListBinding()
        Try

            EditTruckNoMetroButton.Visible = False

            _truckList = Facade.ShippingTruckBL().GetAll().OrderBy(Function(x) x.TruckNo).ToList()
            If _isShowAll = False Then
                _truckList = _truckList.Where(Function(x) x.Status = True).OrderBy(Function(x) x.TruckNo).ToList()
            End If
            _totalRecord = _truckList.Count()

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_truckList)
            dt.Load(reader)

            TruckListMetroGrid.AutoGenerateColumns = False
            TruckListMetroGrid.DataSource = dt
            TotalRecordMetroLabel.Text = _totalRecord.ToString("N0")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub TruckList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _isShowAll = False
        ShowAllMetroCheckBox.Checked = _isShowAll
        TruckListBinding()
    End Sub

    Private Sub ShowAllMetroCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles ShowAllMetroCheckBox.CheckedChanged
        _isShowAll = ShowAllMetroCheckBox.Checked
        TruckListBinding()
    End Sub

    Private Sub TruckListMetroGrid_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles TruckListMetroGrid.CellClick
        Try
            If e.RowIndex < 0 Then
                Return
            End If

            If e.ColumnIndex = 2 Then
                If MessageBoxModule.Question("ท่านต้องการเปลี่ยนสถานะทะเบียนรถบรรทุกใช่หรือไม่?") = MsgBoxResult.No Then
                    Return
                End If

                Dim truck = TruckListMetroGrid.Rows(e.RowIndex)
                Dim truckID = truck.Cells(0).Value
                Dim status = truck.Cells(2).Value
                Facade.ShippingTruckBL().ChangeStatus(truckID, Not status, "TestUser")
                TruckListBinding()
                MessageBoxModule.Info("เปลี่ยนสถานะทะเบียนรถบรรทุกสำเร็จ")
            End If
            EditTruckNoMetroButton.Visible = True
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub EditTruckNoMetroButton_Click(sender As Object, e As EventArgs) Handles EditTruckNoMetroButton.Click
        Try

            Dim selectedRowIndex = TruckListMetroGrid.CurrentRow.Index
            If selectedRowIndex < 1 Then
                Throw New Exception("โปรดเลือกรายการที่ต้องการแก้ไขจากตารางด้านล่าง")
            End If

            If MessageBoxModule.Question("ท่านต้องการแก้ไขทะเบียนรถบรรทุกคันนี้ใช่หรือไม่?") = MsgBoxResult.No Then
                Return
            End If

            Dim truckID = TruckListMetroGrid.Rows(selectedRowIndex).Cells(0).Value
            Dim window As New EditTruckNo
            window.TruckID = truckID
            window.ShowDialog()
            TruckListBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub AddMetroButton_Click(sender As Object, e As EventArgs) Handles AddMetroButton.Click
        Try
            Dim window As New AddTruck
            window.ShowDialog()
            TruckListBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
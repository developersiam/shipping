﻿Imports FactoryBL

Public Class AddTruck
    Inherits MetroFramework.Forms.MetroForm

    Private Sub AddMetroButton_Click(sender As Object, e As EventArgs) Handles AddMetroButton.Click
        Try
            If String.IsNullOrEmpty(TruckNoMetroTextBox.Text) Then
                MessageBoxModule.Warning("โปรดระบุทะเบียนรถบรรทุก")
            End If

            If MessageBoxModule.Question("ท่านต้องการเพิ่มรายการทะเบียนรถบรรทุกในระบบใช่หรือไม่?") = DialogResult.No Then
                Return
            End If

            Facade.ShippingTruckBL().Add(TruckNoMetroTextBox.Text, XUsername)
            MessageBoxModule.Info("บันทึกข้อมูลสำเร็จ")
            Me.Close()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EditTruckNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.OldTruckNoMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.NewTruckNoMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.SaveMetroButton = New MetroFramework.Controls.MetroButton()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.OldTruckNoMetroTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.NewTruckNoMetroTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.SaveMetroButton)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(375, 137)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(151, 19)
        Me.MetroLabel2.TabIndex = 3
        Me.MetroLabel2.Text = "เลขทะเบียนรถบรรทุก (เดิม)"
        '
        'OldTruckNoMetroTextBox
        '
        '
        '
        '
        Me.OldTruckNoMetroTextBox.CustomButton.Image = Nothing
        Me.OldTruckNoMetroTextBox.CustomButton.Location = New System.Drawing.Point(186, 1)
        Me.OldTruckNoMetroTextBox.CustomButton.Name = ""
        Me.OldTruckNoMetroTextBox.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.OldTruckNoMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.OldTruckNoMetroTextBox.CustomButton.TabIndex = 1
        Me.OldTruckNoMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OldTruckNoMetroTextBox.CustomButton.UseSelectable = True
        Me.OldTruckNoMetroTextBox.CustomButton.Visible = False
        Me.OldTruckNoMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.OldTruckNoMetroTextBox.Lines = New String(-1) {}
        Me.OldTruckNoMetroTextBox.Location = New System.Drawing.Point(3, 22)
        Me.OldTruckNoMetroTextBox.MaxLength = 32767
        Me.OldTruckNoMetroTextBox.Name = "OldTruckNoMetroTextBox"
        Me.OldTruckNoMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.OldTruckNoMetroTextBox.PromptText = "Old Truck No"
        Me.OldTruckNoMetroTextBox.ReadOnly = True
        Me.OldTruckNoMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.OldTruckNoMetroTextBox.SelectedText = ""
        Me.OldTruckNoMetroTextBox.SelectionLength = 0
        Me.OldTruckNoMetroTextBox.SelectionStart = 0
        Me.OldTruckNoMetroTextBox.ShortcutsEnabled = True
        Me.OldTruckNoMetroTextBox.Size = New System.Drawing.Size(208, 23)
        Me.OldTruckNoMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.OldTruckNoMetroTextBox.TabIndex = 4
        Me.OldTruckNoMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OldTruckNoMetroTextBox.UseSelectable = True
        Me.OldTruckNoMetroTextBox.WaterMark = "Old Truck No"
        Me.OldTruckNoMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.OldTruckNoMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 48)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(208, 19)
        Me.MetroLabel1.TabIndex = 0
        Me.MetroLabel1.Text = "เลขทะเบียนรถบรรทุก (ที่ต้องการแก้ไข)"
        '
        'NewTruckNoMetroTextBox
        '
        '
        '
        '
        Me.NewTruckNoMetroTextBox.CustomButton.Image = Nothing
        Me.NewTruckNoMetroTextBox.CustomButton.Location = New System.Drawing.Point(186, 1)
        Me.NewTruckNoMetroTextBox.CustomButton.Name = ""
        Me.NewTruckNoMetroTextBox.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.NewTruckNoMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NewTruckNoMetroTextBox.CustomButton.TabIndex = 1
        Me.NewTruckNoMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NewTruckNoMetroTextBox.CustomButton.UseSelectable = True
        Me.NewTruckNoMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.NewTruckNoMetroTextBox.Lines = New String(-1) {}
        Me.NewTruckNoMetroTextBox.Location = New System.Drawing.Point(3, 70)
        Me.NewTruckNoMetroTextBox.MaxLength = 32767
        Me.NewTruckNoMetroTextBox.Name = "NewTruckNoMetroTextBox"
        Me.NewTruckNoMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NewTruckNoMetroTextBox.PromptText = "New Truck No"
        Me.NewTruckNoMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NewTruckNoMetroTextBox.SelectedText = ""
        Me.NewTruckNoMetroTextBox.SelectionLength = 0
        Me.NewTruckNoMetroTextBox.SelectionStart = 0
        Me.NewTruckNoMetroTextBox.ShortcutsEnabled = True
        Me.NewTruckNoMetroTextBox.ShowButton = True
        Me.NewTruckNoMetroTextBox.ShowClearButton = True
        Me.NewTruckNoMetroTextBox.Size = New System.Drawing.Size(208, 23)
        Me.NewTruckNoMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.NewTruckNoMetroTextBox.TabIndex = 1
        Me.NewTruckNoMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NewTruckNoMetroTextBox.UseSelectable = True
        Me.NewTruckNoMetroTextBox.WaterMark = "New Truck No"
        Me.NewTruckNoMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NewTruckNoMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'SaveMetroButton
        '
        Me.SaveMetroButton.Location = New System.Drawing.Point(3, 99)
        Me.SaveMetroButton.Name = "SaveMetroButton"
        Me.SaveMetroButton.Size = New System.Drawing.Size(100, 23)
        Me.SaveMetroButton.Style = MetroFramework.MetroColorStyle.Green
        Me.SaveMetroButton.TabIndex = 2
        Me.SaveMetroButton.Text = "บันทึกข้อมูล"
        Me.SaveMetroButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SaveMetroButton.UseSelectable = True
        '
        'EditTruckNo
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(415, 217)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "EditTruckNo"
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "แก้ไขทะเบียนรถบรรทุก"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents NewTruckNoMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents SaveMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents OldTruckNoMetroTextBox As MetroFramework.Controls.MetroTextBox
End Class

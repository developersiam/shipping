﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAddMovementNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.BackMetroButton = New MetroFramework.Controls.MetroButton()
        Me.SaveMetroButton = New MetroFramework.Controls.MetroButton()
        Me.TruckMetroComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TruckDriverMetroComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.ToLocationMetroComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.FromLocationMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.SendDateMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.CropMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.AutoSize = True
        Me.FlowLayoutPanel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel7.Controls.Add(Me.SaveMetroButton)
        Me.FlowLayoutPanel7.Controls.Add(Me.BackMetroButton)
        Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(3, 243)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(343, 36)
        Me.FlowLayoutPanel7.TabIndex = 188
        '
        'BackMetroButton
        '
        Me.BackMetroButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackMetroButton.FontSize = MetroFramework.MetroButtonSize.Tall
        Me.BackMetroButton.Location = New System.Drawing.Point(109, 3)
        Me.BackMetroButton.Name = "BackMetroButton"
        Me.BackMetroButton.Size = New System.Drawing.Size(100, 30)
        Me.BackMetroButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BackMetroButton.TabIndex = 187
        Me.BackMetroButton.Text = "Back"
        Me.BackMetroButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BackMetroButton.UseSelectable = True
        Me.BackMetroButton.UseStyleColors = True
        '
        'SaveMetroButton
        '
        Me.SaveMetroButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SaveMetroButton.FontSize = MetroFramework.MetroButtonSize.Tall
        Me.SaveMetroButton.Location = New System.Drawing.Point(3, 3)
        Me.SaveMetroButton.Name = "SaveMetroButton"
        Me.SaveMetroButton.Size = New System.Drawing.Size(100, 30)
        Me.SaveMetroButton.Style = MetroFramework.MetroColorStyle.Lime
        Me.SaveMetroButton.TabIndex = 186
        Me.SaveMetroButton.Text = "Save"
        Me.SaveMetroButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SaveMetroButton.UseSelectable = True
        Me.SaveMetroButton.UseStyleColors = True
        '
        'TruckMetroComboBox
        '
        Me.TruckMetroComboBox.DisplayMember = "TruckNo"
        Me.TruckMetroComboBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.TruckMetroComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.TruckMetroComboBox.FormattingEnabled = True
        Me.TruckMetroComboBox.ItemHeight = 29
        Me.TruckMetroComboBox.Location = New System.Drawing.Point(3, 205)
        Me.TruckMetroComboBox.Name = "TruckMetroComboBox"
        Me.TruckMetroComboBox.PromptText = "Truck No."
        Me.TruckMetroComboBox.Size = New System.Drawing.Size(343, 35)
        Me.TruckMetroComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckMetroComboBox.TabIndex = 185
        Me.TruckMetroComboBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckMetroComboBox.UseSelectable = True
        Me.TruckMetroComboBox.UseStyleColors = True
        Me.TruckMetroComboBox.ValueMember = "TruckID"
        '
        'TruckDriverMetroComboBox
        '
        Me.TruckDriverMetroComboBox.DisplayMember = "FirstName"
        Me.TruckDriverMetroComboBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.TruckDriverMetroComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.TruckDriverMetroComboBox.FormattingEnabled = True
        Me.TruckDriverMetroComboBox.ItemHeight = 29
        Me.TruckDriverMetroComboBox.Location = New System.Drawing.Point(3, 167)
        Me.TruckDriverMetroComboBox.Name = "TruckDriverMetroComboBox"
        Me.TruckDriverMetroComboBox.PromptText = "Driver"
        Me.TruckDriverMetroComboBox.Size = New System.Drawing.Size(343, 35)
        Me.TruckDriverMetroComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckDriverMetroComboBox.TabIndex = 183
        Me.TruckDriverMetroComboBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckDriverMetroComboBox.UseSelectable = True
        Me.TruckDriverMetroComboBox.UseStyleColors = True
        Me.TruckDriverMetroComboBox.ValueMember = "PersonID"
        '
        'ToLocationMetroComboBox
        '
        Me.ToLocationMetroComboBox.DisplayMember = "LocName"
        Me.ToLocationMetroComboBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.ToLocationMetroComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.ToLocationMetroComboBox.FormattingEnabled = True
        Me.ToLocationMetroComboBox.ItemHeight = 29
        Me.ToLocationMetroComboBox.Location = New System.Drawing.Point(3, 126)
        Me.ToLocationMetroComboBox.Name = "ToLocationMetroComboBox"
        Me.ToLocationMetroComboBox.PromptText = "To Location"
        Me.ToLocationMetroComboBox.Size = New System.Drawing.Size(343, 35)
        Me.ToLocationMetroComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.ToLocationMetroComboBox.TabIndex = 184
        Me.ToLocationMetroComboBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.ToLocationMetroComboBox.UseSelectable = True
        Me.ToLocationMetroComboBox.UseStyleColors = True
        Me.ToLocationMetroComboBox.ValueMember = "LocNo"
        '
        'FromLocationMetroTextBox
        '
        '
        '
        '
        Me.FromLocationMetroTextBox.CustomButton.Image = Nothing
        Me.FromLocationMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.FromLocationMetroTextBox.CustomButton.Name = ""
        Me.FromLocationMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.FromLocationMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.FromLocationMetroTextBox.CustomButton.TabIndex = 1
        Me.FromLocationMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromLocationMetroTextBox.CustomButton.UseSelectable = True
        Me.FromLocationMetroTextBox.CustomButton.Visible = False
        Me.FromLocationMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.FromLocationMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.FromLocationMetroTextBox.Lines = New String(-1) {}
        Me.FromLocationMetroTextBox.Location = New System.Drawing.Point(3, 85)
        Me.FromLocationMetroTextBox.MaxLength = 32767
        Me.FromLocationMetroTextBox.Name = "FromLocationMetroTextBox"
        Me.FromLocationMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.FromLocationMetroTextBox.PromptText = "Form Location"
        Me.FromLocationMetroTextBox.ReadOnly = True
        Me.FromLocationMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.FromLocationMetroTextBox.SelectedText = ""
        Me.FromLocationMetroTextBox.SelectionLength = 0
        Me.FromLocationMetroTextBox.SelectionStart = 0
        Me.FromLocationMetroTextBox.ShortcutsEnabled = True
        Me.FromLocationMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.FromLocationMetroTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.FromLocationMetroTextBox.TabIndex = 182
        Me.FromLocationMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromLocationMetroTextBox.UseSelectable = True
        Me.FromLocationMetroTextBox.UseStyleColors = True
        Me.FromLocationMetroTextBox.WaterMark = "Form Location"
        Me.FromLocationMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FromLocationMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'SendDateMetroTextBox
        '
        '
        '
        '
        Me.SendDateMetroTextBox.CustomButton.Image = Nothing
        Me.SendDateMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.SendDateMetroTextBox.CustomButton.Name = ""
        Me.SendDateMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.SendDateMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.SendDateMetroTextBox.CustomButton.TabIndex = 1
        Me.SendDateMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SendDateMetroTextBox.CustomButton.UseSelectable = True
        Me.SendDateMetroTextBox.CustomButton.Visible = False
        Me.SendDateMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.SendDateMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.SendDateMetroTextBox.Lines = New String(-1) {}
        Me.SendDateMetroTextBox.Location = New System.Drawing.Point(3, 44)
        Me.SendDateMetroTextBox.MaxLength = 32767
        Me.SendDateMetroTextBox.Name = "SendDateMetroTextBox"
        Me.SendDateMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.SendDateMetroTextBox.PromptText = "Send Date"
        Me.SendDateMetroTextBox.ReadOnly = True
        Me.SendDateMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.SendDateMetroTextBox.SelectedText = ""
        Me.SendDateMetroTextBox.SelectionLength = 0
        Me.SendDateMetroTextBox.SelectionStart = 0
        Me.SendDateMetroTextBox.ShortcutsEnabled = True
        Me.SendDateMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.SendDateMetroTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.SendDateMetroTextBox.TabIndex = 181
        Me.SendDateMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SendDateMetroTextBox.UseSelectable = True
        Me.SendDateMetroTextBox.UseStyleColors = True
        Me.SendDateMetroTextBox.WaterMark = "Send Date"
        Me.SendDateMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.SendDateMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.CropMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.SendDateMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.FromLocationMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.ToLocationMetroComboBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.TruckDriverMetroComboBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.TruckMetroComboBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.FlowLayoutPanel7)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(367, 341)
        Me.FlowLayoutPanel6.TabIndex = 181
        '
        'CropMetroTextBox
        '
        '
        '
        '
        Me.CropMetroTextBox.CustomButton.Image = Nothing
        Me.CropMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.CropMetroTextBox.CustomButton.Name = ""
        Me.CropMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.CropMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CropMetroTextBox.CustomButton.TabIndex = 1
        Me.CropMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CropMetroTextBox.CustomButton.UseSelectable = True
        Me.CropMetroTextBox.CustomButton.Visible = False
        Me.CropMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.CropMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CropMetroTextBox.Lines = New String(-1) {}
        Me.CropMetroTextBox.Location = New System.Drawing.Point(3, 3)
        Me.CropMetroTextBox.MaxLength = 32767
        Me.CropMetroTextBox.Name = "CropMetroTextBox"
        Me.CropMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CropMetroTextBox.PromptText = "Crop"
        Me.CropMetroTextBox.ReadOnly = True
        Me.CropMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CropMetroTextBox.SelectedText = ""
        Me.CropMetroTextBox.SelectionLength = 0
        Me.CropMetroTextBox.SelectionStart = 0
        Me.CropMetroTextBox.ShortcutsEnabled = True
        Me.CropMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.CropMetroTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropMetroTextBox.TabIndex = 180
        Me.CropMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CropMetroTextBox.UseSelectable = True
        Me.CropMetroTextBox.UseStyleColors = True
        Me.CropMetroTextBox.WaterMark = "Crop"
        Me.CropMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CropMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FrmAddMovementNo
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(407, 421)
        Me.Controls.Add(Me.FlowLayoutPanel6)
        Me.Name = "FrmAddMovementNo"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Add Movement No"
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
    Friend WithEvents SaveMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents BackMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents TruckMetroComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TruckDriverMetroComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents ToLocationMetroComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents FromLocationMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents SendDateMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents CropMetroTextBox As MetroFramework.Controls.MetroTextBox
End Class

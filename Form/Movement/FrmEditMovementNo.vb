﻿Imports FactoryBL
Imports FactoryBL.Helper
Imports FactoryBL.Model

Public Class FrmEditMovementNo
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private _movementNo As String
    Public Property MovementNo() As String
        Get
            Return _movementNo
        End Get
        Set(ByVal value As String)
            _movementNo = value
        End Set
    End Property

    Private Sub FrmEditMovementNo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If String.IsNullOrEmpty(_movementNo) Then
                Throw New Exception("Movement no cannot be empty.")
            End If

            Dim movement = Facade.ShippingMovementBL().GetSingle(_movementNo)
            Dim locationList = Facade.ShippingLocationBL().GetAll().OrderBy(Function(x) x.LocName).ToList()

            MovementNoMetroTextBox.Text = movement.MovementNo
            CropMetroTextBox.Text = movement.Crop
            SendDateMetroTextBox.Text = Convert.ToDateTime(movement.DateStart).ToString("dd/MM/yyyy")
            FromMetroTextBox.Text = locationList.SingleOrDefault(Function(x) x.LocNo = movement.MovementFrom).LocName
            ToMetroTextBox.Text = locationList.SingleOrDefault(Function(x) x.LocNo = movement.MovementTo).LocName

            TruckDriverMetroComboBox.DataSource = Nothing
            TruckDriverMetroComboBox.DataSource = h_TruckDriver.GetByStatus(True).Select(Function(x) New m_TruckDriver With {.PersonID = x.PersonID, .FirstName = x.FirstName + " " + x.LastName}).OrderBy(Function(x) x.FirstName).ToList()

            TruckMetroComboBox.DataSource = Nothing
            TruckMetroComboBox.DataSource = Facade.ShippingTruckBL().GetAll().Where(Function(x) x.Status = True).OrderBy(Function(x) x.TruckNo).ToList()

            TruckDriverMetroComboBox.SelectedValue = movement.PersonID
            TruckMetroComboBox.SelectedValue = movement.TruckID
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroButton_Click(sender As Object, e As EventArgs) Handles BackMetroButton.Click
        Me.Close()
    End Sub

    Private Sub SaveMetroButton_Click(sender As Object, e As EventArgs) Handles SaveMetroButton.Click
        Try
            If MessageBoxModule.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") = DialogResult.No Then
                Return
            End If

            Facade.ShippingMovementBL().ChangeDriverAndTruck(_movementNo, TruckDriverMetroComboBox.SelectedValue, TruckMetroComboBox.SelectedValue, XUsername)
            MessageBoxModule.Info("แก้ไขข้อมูลสำเร็จ")
            Me.Close()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
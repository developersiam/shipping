﻿Imports FactoryBL
Imports FactoryBL.Helper
Imports FactoryBL.Model
Imports HRISSystemBL.BL.HRISBL

Public Class FrmAddMovementNo
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext

    Private _crop As Int16
    Public Property Crop() As Int16
        Get
            Return _crop
        End Get
        Set(ByVal value As Int16)
            _crop = value
        End Set
    End Property

    Private _fromLocationNo As String
    Public Property FromLocationNo() As String
        Get
            Return _fromLocationNo
        End Get
        Set(ByVal value As String)
            _fromLocationNo = value
        End Set
    End Property

    Private _fromLocationName As String
    Public Property FromLocationName() As String
        Get
            Return _fromLocationName
        End Get
        Set(ByVal value As String)
            _fromLocationName = value
        End Set
    End Property

    Private Sub FrmAddMovementNo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            CropMetroTextBox.Text = _crop
            SendDateMetroTextBox.Text = Now.ToString("dd/MM/yyyy")
            FromLocationMetroTextBox.Text = _fromLocationName

            TruckDriverMetroComboBox.DataSource = Nothing
            TruckDriverMetroComboBox.DataSource = h_TruckDriver.GetByStatus(True).Select(Function(x) New m_TruckDriver With {.PersonID = x.PersonID, .FirstName = x.FirstName + " " + x.LastName}).OrderBy(Function(x) x.FirstName).ToList()

            TruckMetroComboBox.DataSource = Nothing
            TruckMetroComboBox.DataSource = Facade.ShippingTruckBL().GetAll().Where(Function(x) x.Status = True).OrderBy(Function(x) x.TruckNo).ToList()

            ToLocationMetroComboBox.DataSource = Nothing
            ToLocationMetroComboBox.DataSource = Facade.ShippingLocationBL().GetAll().Where(Function(x) x.Status = True And x.LocNo <> _fromLocationNo).OrderBy(Function(x) x.LocName).ToList()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub SaveMetroButton_Click(sender As Object, e As EventArgs) Handles SaveMetroButton.Click
        Try
            If String.IsNullOrEmpty(_fromLocationNo) = True Then
                Throw New Exception("โปรดระบุสถานีต้นทาง (from location)")
            End If

            If String.IsNullOrEmpty(ToLocationMetroComboBox.SelectedValue) = True Then
                Throw New Exception("โปรดระบุสถานีปลายทาง (to location)")
            End If

            If String.IsNullOrEmpty(TruckMetroComboBox.SelectedValue) = True Then
                Throw New Exception("โปรดระบุรถบรรทุกที่ใช้ในการขนย้าย (truck no)")
            End If

            If String.IsNullOrEmpty(TruckDriverMetroComboBox.SelectedValue) = True Then
                Throw New Exception("โปรดระบุพนักงานขับรถ (truck driver)")
            End If

            If MessageBoxModule.Question("ท่านต้องการสร้าง movement no เพื่อเริ่มทำการขนย้ายสินค้าใช่หรือไม่?") = DialogResult.No Then
                Return
            End If

            Facade.ShippingMovementBL().Add(_crop, TruckMetroComboBox.SelectedValue, TruckDriverMetroComboBox.SelectedValue, _fromLocationNo, ToLocationMetroComboBox.SelectedValue, XUsername)
            MessageBoxModule.Info("สร้าง movement no สำเร็จ")
            Me.Close()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroButton_Click(sender As Object, e As EventArgs) Handles BackMetroButton.Click
        Me.Close()
    End Sub
End Class
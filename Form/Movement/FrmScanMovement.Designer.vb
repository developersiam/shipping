﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmScanMovement
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim MetroTile2 As MetroFramework.Controls.MetroTile
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.BarcodeTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroPanel2 = New MetroFramework.Controls.MetroPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MovementNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.FromTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel17 = New MetroFramework.Controls.MetroLabel()
        Me.ToTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.TruckNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel18 = New MetroFramework.Controls.MetroLabel()
        Me.DriverTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.TotalCaseTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel19 = New MetroFramework.Controls.MetroLabel()
        Me.SendStatusTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.whTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.GradeTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.netrealTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.netdefTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.casenoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.DeleteButton = New System.Windows.Forms.Button()
        Me.FinishedButton = New System.Windows.Forms.Button()
        Me.SettingButton = New System.Windows.Forms.Button()
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.BackButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel12 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.bc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.caseno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.customer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.customerrs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.graders = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.netdef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bay = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.frompddate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.frompdno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.issued = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.issuedto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        MetroTile2 = New MetroFramework.Controls.MetroTile()
        Me.MetroPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel12.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroTile2
        '
        MetroTile2.ActiveControl = Nothing
        MetroTile2.Location = New System.Drawing.Point(3, 3)
        MetroTile2.Name = "MetroTile2"
        MetroTile2.Size = New System.Drawing.Size(175, 40)
        MetroTile2.Style = MetroFramework.MetroColorStyle.Lime
        MetroTile2.TabIndex = 192
        MetroTile2.Text = "Movement Information"
        MetroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        MetroTile2.Theme = MetroFramework.MetroThemeStyle.Light
        MetroTile2.TileImageAlign = System.Drawing.ContentAlignment.TopCenter
        MetroTile2.UseSelectable = True
        '
        'BarcodeTextBox
        '
        Me.BarcodeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.BarcodeTextBox.CustomButton.Image = Nothing
        Me.BarcodeTextBox.CustomButton.Location = New System.Drawing.Point(346, 1)
        Me.BarcodeTextBox.CustomButton.Name = ""
        Me.BarcodeTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.BarcodeTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BarcodeTextBox.CustomButton.TabIndex = 1
        Me.BarcodeTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BarcodeTextBox.CustomButton.UseSelectable = True
        Me.BarcodeTextBox.CustomButton.Visible = False
        Me.BarcodeTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BarcodeTextBox.Lines = New String(-1) {}
        Me.BarcodeTextBox.Location = New System.Drawing.Point(3, 22)
        Me.BarcodeTextBox.MaxLength = 100
        Me.BarcodeTextBox.Name = "BarcodeTextBox"
        Me.BarcodeTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BarcodeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BarcodeTextBox.SelectedText = ""
        Me.BarcodeTextBox.SelectionLength = 0
        Me.BarcodeTextBox.SelectionStart = 0
        Me.BarcodeTextBox.ShortcutsEnabled = True
        Me.BarcodeTextBox.Size = New System.Drawing.Size(380, 35)
        Me.BarcodeTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.BarcodeTextBox.TabIndex = 1
        Me.BarcodeTextBox.UseSelectable = True
        Me.BarcodeTextBox.UseStyleColors = True
        Me.BarcodeTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BarcodeTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroPanel2
        '
        Me.MetroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroPanel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.MetroPanel2.HorizontalScrollbarBarColor = True
        Me.MetroPanel2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.HorizontalScrollbarSize = 10
        Me.MetroPanel2.Location = New System.Drawing.Point(1, 1)
        Me.MetroPanel2.Margin = New System.Windows.Forms.Padding(1)
        Me.MetroPanel2.Name = "MetroPanel2"
        Me.MetroPanel2.Size = New System.Drawing.Size(188, 582)
        Me.MetroPanel2.TabIndex = 179
        Me.MetroPanel2.VerticalScrollbarBarColor = True
        Me.MetroPanel2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.VerticalScrollbarSize = 10
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(MetroTile2)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.MovementNoTextbox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.FromTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel17)
        Me.FlowLayoutPanel1.Controls.Add(Me.ToTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel3)
        Me.FlowLayoutPanel1.Controls.Add(Me.TruckNoTextbox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel18)
        Me.FlowLayoutPanel1.Controls.Add(Me.DriverTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel9)
        Me.FlowLayoutPanel1.Controls.Add(Me.TotalCaseTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel19)
        Me.FlowLayoutPanel1.Controls.Add(Me.SendStatusTextBox)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(1)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(181, 580)
        Me.FlowLayoutPanel1.TabIndex = 174
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 56)
        Me.MetroLabel1.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(88, 17)
        Me.MetroLabel1.TabIndex = 167
        Me.MetroLabel1.Text = "Movement No"
        '
        'MovementNoTextbox
        '
        Me.MovementNoTextbox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.MovementNoTextbox.CustomButton.Image = Nothing
        Me.MovementNoTextbox.CustomButton.Location = New System.Drawing.Point(141, 1)
        Me.MovementNoTextbox.CustomButton.Name = ""
        Me.MovementNoTextbox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.MovementNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MovementNoTextbox.CustomButton.TabIndex = 1
        Me.MovementNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MovementNoTextbox.CustomButton.UseSelectable = True
        Me.MovementNoTextbox.CustomButton.Visible = False
        Me.MovementNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.MovementNoTextbox.Lines = New String(-1) {}
        Me.MovementNoTextbox.Location = New System.Drawing.Point(3, 76)
        Me.MovementNoTextbox.MaxLength = 2
        Me.MovementNoTextbox.Name = "MovementNoTextbox"
        Me.MovementNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MovementNoTextbox.PromptText = "Movement No"
        Me.MovementNoTextbox.ReadOnly = True
        Me.MovementNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MovementNoTextbox.SelectedText = ""
        Me.MovementNoTextbox.SelectionLength = 0
        Me.MovementNoTextbox.SelectionStart = 0
        Me.MovementNoTextbox.ShortcutsEnabled = True
        Me.MovementNoTextbox.Size = New System.Drawing.Size(175, 35)
        Me.MovementNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.MovementNoTextbox.TabIndex = 168
        Me.MovementNoTextbox.UseCustomBackColor = True
        Me.MovementNoTextbox.UseSelectable = True
        Me.MovementNoTextbox.UseStyleColors = True
        Me.MovementNoTextbox.WaterMark = "Movement No"
        Me.MovementNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MovementNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel2.Location = New System.Drawing.Point(3, 124)
        Me.MetroLabel2.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(36, 17)
        Me.MetroLabel2.TabIndex = 169
        Me.MetroLabel2.Text = "From"
        '
        'FromTextBox
        '
        Me.FromTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.FromTextBox.CustomButton.Image = Nothing
        Me.FromTextBox.CustomButton.Location = New System.Drawing.Point(141, 1)
        Me.FromTextBox.CustomButton.Name = ""
        Me.FromTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.FromTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.FromTextBox.CustomButton.TabIndex = 1
        Me.FromTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromTextBox.CustomButton.UseSelectable = True
        Me.FromTextBox.CustomButton.Visible = False
        Me.FromTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.FromTextBox.Lines = New String(-1) {}
        Me.FromTextBox.Location = New System.Drawing.Point(3, 144)
        Me.FromTextBox.MaxLength = 2
        Me.FromTextBox.Name = "FromTextBox"
        Me.FromTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.FromTextBox.PromptText = "From Location"
        Me.FromTextBox.ReadOnly = True
        Me.FromTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.FromTextBox.SelectedText = ""
        Me.FromTextBox.SelectionLength = 0
        Me.FromTextBox.SelectionStart = 0
        Me.FromTextBox.ShortcutsEnabled = True
        Me.FromTextBox.Size = New System.Drawing.Size(175, 35)
        Me.FromTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.FromTextBox.TabIndex = 170
        Me.FromTextBox.UseCustomBackColor = True
        Me.FromTextBox.UseSelectable = True
        Me.FromTextBox.UseStyleColors = True
        Me.FromTextBox.WaterMark = "From Location"
        Me.FromTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FromTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel17
        '
        Me.MetroLabel17.AutoSize = True
        Me.MetroLabel17.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel17.Location = New System.Drawing.Point(3, 192)
        Me.MetroLabel17.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.MetroLabel17.Name = "MetroLabel17"
        Me.MetroLabel17.Size = New System.Drawing.Size(20, 17)
        Me.MetroLabel17.TabIndex = 173
        Me.MetroLabel17.Text = "To"
        '
        'ToTextBox
        '
        Me.ToTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.ToTextBox.CustomButton.Image = Nothing
        Me.ToTextBox.CustomButton.Location = New System.Drawing.Point(141, 1)
        Me.ToTextBox.CustomButton.Name = ""
        Me.ToTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.ToTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.ToTextBox.CustomButton.TabIndex = 1
        Me.ToTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.ToTextBox.CustomButton.UseSelectable = True
        Me.ToTextBox.CustomButton.Visible = False
        Me.ToTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.ToTextBox.Lines = New String(-1) {}
        Me.ToTextBox.Location = New System.Drawing.Point(3, 212)
        Me.ToTextBox.MaxLength = 2
        Me.ToTextBox.Name = "ToTextBox"
        Me.ToTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.ToTextBox.PromptText = "To Location"
        Me.ToTextBox.ReadOnly = True
        Me.ToTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.ToTextBox.SelectedText = ""
        Me.ToTextBox.SelectionLength = 0
        Me.ToTextBox.SelectionStart = 0
        Me.ToTextBox.ShortcutsEnabled = True
        Me.ToTextBox.Size = New System.Drawing.Size(175, 35)
        Me.ToTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.ToTextBox.TabIndex = 174
        Me.ToTextBox.UseCustomBackColor = True
        Me.ToTextBox.UseSelectable = True
        Me.ToTextBox.UseStyleColors = True
        Me.ToTextBox.WaterMark = "To Location"
        Me.ToTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.ToTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel3.Location = New System.Drawing.Point(3, 260)
        Me.MetroLabel3.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(56, 17)
        Me.MetroLabel3.TabIndex = 171
        Me.MetroLabel3.Text = "Truck No"
        '
        'TruckNoTextbox
        '
        Me.TruckNoTextbox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TruckNoTextbox.CustomButton.Image = Nothing
        Me.TruckNoTextbox.CustomButton.Location = New System.Drawing.Point(141, 1)
        Me.TruckNoTextbox.CustomButton.Name = ""
        Me.TruckNoTextbox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.TruckNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoTextbox.CustomButton.TabIndex = 1
        Me.TruckNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoTextbox.CustomButton.UseSelectable = True
        Me.TruckNoTextbox.CustomButton.Visible = False
        Me.TruckNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TruckNoTextbox.Lines = New String(-1) {}
        Me.TruckNoTextbox.Location = New System.Drawing.Point(3, 280)
        Me.TruckNoTextbox.MaxLength = 2
        Me.TruckNoTextbox.Name = "TruckNoTextbox"
        Me.TruckNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckNoTextbox.PromptText = "Truck No"
        Me.TruckNoTextbox.ReadOnly = True
        Me.TruckNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoTextbox.SelectedText = ""
        Me.TruckNoTextbox.SelectionLength = 0
        Me.TruckNoTextbox.SelectionStart = 0
        Me.TruckNoTextbox.ShortcutsEnabled = True
        Me.TruckNoTextbox.Size = New System.Drawing.Size(175, 35)
        Me.TruckNoTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TruckNoTextbox.TabIndex = 172
        Me.TruckNoTextbox.UseCustomBackColor = True
        Me.TruckNoTextbox.UseSelectable = True
        Me.TruckNoTextbox.UseStyleColors = True
        Me.TruckNoTextbox.WaterMark = "Truck No"
        Me.TruckNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel18
        '
        Me.MetroLabel18.AutoSize = True
        Me.MetroLabel18.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel18.Location = New System.Drawing.Point(3, 328)
        Me.MetroLabel18.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.MetroLabel18.Name = "MetroLabel18"
        Me.MetroLabel18.Size = New System.Drawing.Size(41, 17)
        Me.MetroLabel18.TabIndex = 175
        Me.MetroLabel18.Text = "Driver"
        '
        'DriverTextBox
        '
        Me.DriverTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.DriverTextBox.CustomButton.Image = Nothing
        Me.DriverTextBox.CustomButton.Location = New System.Drawing.Point(141, 1)
        Me.DriverTextBox.CustomButton.Name = ""
        Me.DriverTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.DriverTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.DriverTextBox.CustomButton.TabIndex = 1
        Me.DriverTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.DriverTextBox.CustomButton.UseSelectable = True
        Me.DriverTextBox.CustomButton.Visible = False
        Me.DriverTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.DriverTextBox.Lines = New String(-1) {}
        Me.DriverTextBox.Location = New System.Drawing.Point(3, 348)
        Me.DriverTextBox.MaxLength = 2
        Me.DriverTextBox.Name = "DriverTextBox"
        Me.DriverTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.DriverTextBox.PromptText = "Driver"
        Me.DriverTextBox.ReadOnly = True
        Me.DriverTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.DriverTextBox.SelectedText = ""
        Me.DriverTextBox.SelectionLength = 0
        Me.DriverTextBox.SelectionStart = 0
        Me.DriverTextBox.ShortcutsEnabled = True
        Me.DriverTextBox.Size = New System.Drawing.Size(175, 35)
        Me.DriverTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.DriverTextBox.TabIndex = 176
        Me.DriverTextBox.UseCustomBackColor = True
        Me.DriverTextBox.UseSelectable = True
        Me.DriverTextBox.UseStyleColors = True
        Me.DriverTextBox.WaterMark = "Driver"
        Me.DriverTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.DriverTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel9.Location = New System.Drawing.Point(3, 396)
        Me.MetroLabel9.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(63, 17)
        Me.MetroLabel9.TabIndex = 167
        Me.MetroLabel9.Text = "Total Case"
        '
        'TotalCaseTextBox
        '
        Me.TotalCaseTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TotalCaseTextBox.CustomButton.Image = Nothing
        Me.TotalCaseTextBox.CustomButton.Location = New System.Drawing.Point(141, 1)
        Me.TotalCaseTextBox.CustomButton.Name = ""
        Me.TotalCaseTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.TotalCaseTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalCaseTextBox.CustomButton.TabIndex = 1
        Me.TotalCaseTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalCaseTextBox.CustomButton.UseSelectable = True
        Me.TotalCaseTextBox.CustomButton.Visible = False
        Me.TotalCaseTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TotalCaseTextBox.Lines = New String(-1) {}
        Me.TotalCaseTextBox.Location = New System.Drawing.Point(3, 416)
        Me.TotalCaseTextBox.MaxLength = 2
        Me.TotalCaseTextBox.Name = "TotalCaseTextBox"
        Me.TotalCaseTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalCaseTextBox.PromptText = "Total Case"
        Me.TotalCaseTextBox.ReadOnly = True
        Me.TotalCaseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalCaseTextBox.SelectedText = ""
        Me.TotalCaseTextBox.SelectionLength = 0
        Me.TotalCaseTextBox.SelectionStart = 0
        Me.TotalCaseTextBox.ShortcutsEnabled = True
        Me.TotalCaseTextBox.Size = New System.Drawing.Size(175, 35)
        Me.TotalCaseTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalCaseTextBox.TabIndex = 168
        Me.TotalCaseTextBox.UseCustomBackColor = True
        Me.TotalCaseTextBox.UseCustomForeColor = True
        Me.TotalCaseTextBox.UseSelectable = True
        Me.TotalCaseTextBox.UseStyleColors = True
        Me.TotalCaseTextBox.WaterMark = "Total Case"
        Me.TotalCaseTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalCaseTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel19
        '
        Me.MetroLabel19.AutoSize = True
        Me.MetroLabel19.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel19.Location = New System.Drawing.Point(3, 464)
        Me.MetroLabel19.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
        Me.MetroLabel19.Name = "MetroLabel19"
        Me.MetroLabel19.Size = New System.Drawing.Size(71, 17)
        Me.MetroLabel19.TabIndex = 184
        Me.MetroLabel19.Text = "Send Status"
        '
        'SendStatusTextBox
        '
        Me.SendStatusTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.SendStatusTextBox.CustomButton.Image = Nothing
        Me.SendStatusTextBox.CustomButton.Location = New System.Drawing.Point(141, 1)
        Me.SendStatusTextBox.CustomButton.Name = ""
        Me.SendStatusTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.SendStatusTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.SendStatusTextBox.CustomButton.TabIndex = 1
        Me.SendStatusTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SendStatusTextBox.CustomButton.UseSelectable = True
        Me.SendStatusTextBox.CustomButton.Visible = False
        Me.SendStatusTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.SendStatusTextBox.Lines = New String(-1) {}
        Me.SendStatusTextBox.Location = New System.Drawing.Point(3, 484)
        Me.SendStatusTextBox.MaxLength = 2
        Me.SendStatusTextBox.Name = "SendStatusTextBox"
        Me.SendStatusTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.SendStatusTextBox.PromptText = "Send Status"
        Me.SendStatusTextBox.ReadOnly = True
        Me.SendStatusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.SendStatusTextBox.SelectedText = ""
        Me.SendStatusTextBox.SelectionLength = 0
        Me.SendStatusTextBox.SelectionStart = 0
        Me.SendStatusTextBox.ShortcutsEnabled = True
        Me.SendStatusTextBox.Size = New System.Drawing.Size(175, 35)
        Me.SendStatusTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.SendStatusTextBox.TabIndex = 185
        Me.SendStatusTextBox.UseCustomBackColor = True
        Me.SendStatusTextBox.UseCustomForeColor = True
        Me.SendStatusTextBox.UseSelectable = True
        Me.SendStatusTextBox.UseStyleColors = True
        Me.SendStatusTextBox.WaterMark = "Send Status"
        Me.SendStatusTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.SendStatusTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel10.Location = New System.Drawing.Point(786, 3)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(69, 17)
        Me.MetroLabel10.TabIndex = 191
        Me.MetroLabel10.Text = "Warehouse"
        '
        'whTextBox
        '
        Me.whTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.whTextBox.CustomButton.Image = Nothing
        Me.whTextBox.CustomButton.Location = New System.Drawing.Point(70, 1)
        Me.whTextBox.CustomButton.Name = ""
        Me.whTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.whTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.whTextBox.CustomButton.TabIndex = 1
        Me.whTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.whTextBox.CustomButton.UseSelectable = True
        Me.whTextBox.CustomButton.Visible = False
        Me.whTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.whTextBox.Lines = New String(-1) {}
        Me.whTextBox.Location = New System.Drawing.Point(786, 21)
        Me.whTextBox.MaxLength = 11
        Me.whTextBox.Name = "whTextBox"
        Me.whTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.whTextBox.ReadOnly = True
        Me.whTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.whTextBox.SelectedText = ""
        Me.whTextBox.SelectionLength = 0
        Me.whTextBox.SelectionStart = 0
        Me.whTextBox.ShortcutsEnabled = True
        Me.whTextBox.Size = New System.Drawing.Size(104, 35)
        Me.whTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.whTextBox.TabIndex = 190
        Me.whTextBox.UseCustomBackColor = True
        Me.whTextBox.UseSelectable = True
        Me.whTextBox.UseStyleColors = True
        Me.whTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.whTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel5.Location = New System.Drawing.Point(467, 3)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(41, 17)
        Me.MetroLabel5.TabIndex = 189
        Me.MetroLabel5.Text = "Grade"
        '
        'GradeTextBox
        '
        Me.GradeTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.GradeTextBox.CustomButton.Image = Nothing
        Me.GradeTextBox.CustomButton.Location = New System.Drawing.Point(136, 1)
        Me.GradeTextBox.CustomButton.Name = ""
        Me.GradeTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.GradeTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.GradeTextBox.CustomButton.TabIndex = 1
        Me.GradeTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.GradeTextBox.CustomButton.UseSelectable = True
        Me.GradeTextBox.CustomButton.Visible = False
        Me.GradeTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.GradeTextBox.Lines = New String(-1) {}
        Me.GradeTextBox.Location = New System.Drawing.Point(467, 21)
        Me.GradeTextBox.MaxLength = 11
        Me.GradeTextBox.Name = "GradeTextBox"
        Me.GradeTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.GradeTextBox.ReadOnly = True
        Me.GradeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.GradeTextBox.SelectedText = ""
        Me.GradeTextBox.SelectionLength = 0
        Me.GradeTextBox.SelectionStart = 0
        Me.GradeTextBox.ShortcutsEnabled = True
        Me.GradeTextBox.Size = New System.Drawing.Size(170, 35)
        Me.GradeTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.GradeTextBox.TabIndex = 188
        Me.GradeTextBox.UseCustomBackColor = True
        Me.GradeTextBox.UseSelectable = True
        Me.GradeTextBox.UseStyleColors = True
        Me.GradeTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.GradeTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel13.Location = New System.Drawing.Point(716, 3)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(51, 17)
        Me.MetroLabel13.TabIndex = 187
        Me.MetroLabel13.Text = "NetReal"
        '
        'netrealTextbox
        '
        Me.netrealTextbox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.netrealTextbox.CustomButton.Image = Nothing
        Me.netrealTextbox.CustomButton.Location = New System.Drawing.Point(30, 1)
        Me.netrealTextbox.CustomButton.Name = ""
        Me.netrealTextbox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.netrealTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.netrealTextbox.CustomButton.TabIndex = 1
        Me.netrealTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.netrealTextbox.CustomButton.UseSelectable = True
        Me.netrealTextbox.CustomButton.Visible = False
        Me.netrealTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.netrealTextbox.Lines = New String(-1) {}
        Me.netrealTextbox.Location = New System.Drawing.Point(716, 21)
        Me.netrealTextbox.MaxLength = 11
        Me.netrealTextbox.Name = "netrealTextbox"
        Me.netrealTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.netrealTextbox.ReadOnly = True
        Me.netrealTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.netrealTextbox.SelectedText = ""
        Me.netrealTextbox.SelectionLength = 0
        Me.netrealTextbox.SelectionStart = 0
        Me.netrealTextbox.ShortcutsEnabled = True
        Me.netrealTextbox.Size = New System.Drawing.Size(64, 35)
        Me.netrealTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.netrealTextbox.TabIndex = 186
        Me.netrealTextbox.UseCustomBackColor = True
        Me.netrealTextbox.UseSelectable = True
        Me.netrealTextbox.UseStyleColors = True
        Me.netrealTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.netrealTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel12.Location = New System.Drawing.Point(643, 3)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(28, 17)
        Me.MetroLabel12.TabIndex = 185
        Me.MetroLabel12.Text = "Net"
        '
        'netdefTextBox
        '
        Me.netdefTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.netdefTextBox.CustomButton.Image = Nothing
        Me.netdefTextBox.CustomButton.Location = New System.Drawing.Point(33, 1)
        Me.netdefTextBox.CustomButton.Name = ""
        Me.netdefTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.netdefTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.netdefTextBox.CustomButton.TabIndex = 1
        Me.netdefTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.netdefTextBox.CustomButton.UseSelectable = True
        Me.netdefTextBox.CustomButton.Visible = False
        Me.netdefTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.netdefTextBox.Lines = New String(-1) {}
        Me.netdefTextBox.Location = New System.Drawing.Point(643, 21)
        Me.netdefTextBox.MaxLength = 11
        Me.netdefTextBox.Name = "netdefTextBox"
        Me.netdefTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.netdefTextBox.ReadOnly = True
        Me.netdefTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.netdefTextBox.SelectedText = ""
        Me.netdefTextBox.SelectionLength = 0
        Me.netdefTextBox.SelectionStart = 0
        Me.netdefTextBox.ShortcutsEnabled = True
        Me.netdefTextBox.Size = New System.Drawing.Size(67, 35)
        Me.netdefTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.netdefTextBox.TabIndex = 184
        Me.netdefTextBox.UseCustomBackColor = True
        Me.netdefTextBox.UseSelectable = True
        Me.netdefTextBox.UseStyleColors = True
        Me.netdefTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.netdefTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel11.Location = New System.Drawing.Point(389, 3)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(34, 17)
        Me.MetroLabel11.TabIndex = 183
        Me.MetroLabel11.Text = "Case"
        '
        'casenoTextBox
        '
        Me.casenoTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.casenoTextBox.CustomButton.Image = Nothing
        Me.casenoTextBox.CustomButton.Location = New System.Drawing.Point(38, 1)
        Me.casenoTextBox.CustomButton.Name = ""
        Me.casenoTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.casenoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.casenoTextBox.CustomButton.TabIndex = 1
        Me.casenoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.casenoTextBox.CustomButton.UseSelectable = True
        Me.casenoTextBox.CustomButton.Visible = False
        Me.casenoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.casenoTextBox.Lines = New String(-1) {}
        Me.casenoTextBox.Location = New System.Drawing.Point(389, 21)
        Me.casenoTextBox.MaxLength = 11
        Me.casenoTextBox.Name = "casenoTextBox"
        Me.casenoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.casenoTextBox.ReadOnly = True
        Me.casenoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.casenoTextBox.SelectedText = ""
        Me.casenoTextBox.SelectionLength = 0
        Me.casenoTextBox.SelectionStart = 0
        Me.casenoTextBox.ShortcutsEnabled = True
        Me.casenoTextBox.Size = New System.Drawing.Size(72, 35)
        Me.casenoTextBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.casenoTextBox.TabIndex = 182
        Me.casenoTextBox.UseCustomBackColor = True
        Me.casenoTextBox.UseSelectable = True
        Me.casenoTextBox.UseStyleColors = True
        Me.casenoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.casenoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bc, Me.caseno, Me.customer, Me.grade, Me.customerrs, Me.graders, Me.netdef, Me.wh, Me.bay, Me.frompddate, Me.frompdno, Me.issued, Me.issuedto})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(3, 61)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.MetroGrid.RowHeadersWidth = 51
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 38
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(935, 516)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 182
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel4.Location = New System.Drawing.Point(3, 3)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(52, 17)
        Me.MetroLabel4.TabIndex = 181
        Me.MetroLabel4.Text = "Barcode"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.MetroLabel4)
        Me.Panel1.Controls.Add(Me.MetroGrid)
        Me.Panel1.Controls.Add(Me.casenoTextBox)
        Me.Panel1.Controls.Add(Me.whTextBox)
        Me.Panel1.Controls.Add(Me.MetroLabel11)
        Me.Panel1.Controls.Add(Me.MetroLabel10)
        Me.Panel1.Controls.Add(Me.BarcodeTextBox)
        Me.Panel1.Controls.Add(Me.MetroLabel12)
        Me.Panel1.Controls.Add(Me.netdefTextBox)
        Me.Panel1.Controls.Add(Me.netrealTextbox)
        Me.Panel1.Controls.Add(Me.MetroLabel5)
        Me.Panel1.Controls.Add(Me.MetroLabel13)
        Me.Panel1.Controls.Add(Me.GradeTextBox)
        Me.Panel1.Location = New System.Drawing.Point(193, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(955, 582)
        Me.Panel1.TabIndex = 192
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.DeleteButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.FinishedButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.SettingButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.RefreshButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.BackButton)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(192, 3)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(370, 54)
        Me.FlowLayoutPanel6.TabIndex = 193
        '
        'DeleteButton
        '
        Me.DeleteButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.DeleteButton.BackColor = System.Drawing.Color.Tomato
        Me.DeleteButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteButton.Enabled = False
        Me.DeleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.DeleteButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_delete_24
        Me.DeleteButton.Location = New System.Drawing.Point(2, 2)
        Me.DeleteButton.Margin = New System.Windows.Forms.Padding(2)
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Size = New System.Drawing.Size(70, 50)
        Me.DeleteButton.TabIndex = 180
        Me.DeleteButton.Text = "Delete"
        Me.DeleteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.DeleteButton.UseVisualStyleBackColor = False
        '
        'FinishedButton
        '
        Me.FinishedButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FinishedButton.BackColor = System.Drawing.Color.LimeGreen
        Me.FinishedButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FinishedButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.FinishedButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_lock_24
        Me.FinishedButton.Location = New System.Drawing.Point(76, 2)
        Me.FinishedButton.Margin = New System.Windows.Forms.Padding(2)
        Me.FinishedButton.Name = "FinishedButton"
        Me.FinishedButton.Size = New System.Drawing.Size(70, 50)
        Me.FinishedButton.TabIndex = 179
        Me.FinishedButton.Text = "Finished"
        Me.FinishedButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.FinishedButton.UseVisualStyleBackColor = False
        '
        'SettingButton
        '
        Me.SettingButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SettingButton.BackColor = System.Drawing.Color.Gainsboro
        Me.SettingButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SettingButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SettingButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_clear_24
        Me.SettingButton.Location = New System.Drawing.Point(150, 2)
        Me.SettingButton.Margin = New System.Windows.Forms.Padding(2)
        Me.SettingButton.Name = "SettingButton"
        Me.SettingButton.Size = New System.Drawing.Size(70, 50)
        Me.SettingButton.TabIndex = 183
        Me.SettingButton.Text = "Clear"
        Me.SettingButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.SettingButton.UseVisualStyleBackColor = False
        '
        'RefreshButton
        '
        Me.RefreshButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.RefreshButton.BackColor = System.Drawing.Color.MediumTurquoise
        Me.RefreshButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RefreshButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_refresh_24
        Me.RefreshButton.Location = New System.Drawing.Point(224, 2)
        Me.RefreshButton.Margin = New System.Windows.Forms.Padding(2)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(70, 50)
        Me.RefreshButton.TabIndex = 181
        Me.RefreshButton.Text = "Refresh"
        Me.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.RefreshButton.UseVisualStyleBackColor = False
        '
        'BackButton
        '
        Me.BackButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackButton.BackColor = System.Drawing.Color.White
        Me.BackButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BackButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_back_24
        Me.BackButton.Location = New System.Drawing.Point(298, 2)
        Me.BackButton.Margin = New System.Windows.Forms.Padding(2)
        Me.BackButton.Name = "BackButton"
        Me.BackButton.Size = New System.Drawing.Size(70, 50)
        Me.BackButton.TabIndex = 184
        Me.BackButton.Text = "Back"
        Me.BackButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BackButton.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.MetroPanel2)
        Me.FlowLayoutPanel2.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 69)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1151, 588)
        Me.FlowLayoutPanel2.TabIndex = 194
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel5)
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1160, 670)
        Me.FlowLayoutPanel3.TabIndex = 195
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.AutoSize = True
        Me.FlowLayoutPanel5.Controls.Add(Me.FlowLayoutPanel12)
        Me.FlowLayoutPanel5.Controls.Add(Me.FlowLayoutPanel6)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(565, 60)
        Me.FlowLayoutPanel5.TabIndex = 196
        '
        'FlowLayoutPanel12
        '
        Me.FlowLayoutPanel12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel12.Controls.Add(Me.FlowLayoutPanel4)
        Me.FlowLayoutPanel12.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel12.Name = "FlowLayoutPanel12"
        Me.FlowLayoutPanel12.Size = New System.Drawing.Size(183, 36)
        Me.FlowLayoutPanel12.TabIndex = 196
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.MetroTile1)
        Me.FlowLayoutPanel4.Controls.Add(Me.UsernameMetroLabel)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(109, 30)
        Me.FlowLayoutPanel4.TabIndex = 175
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Default
        Me.MetroTile1.Location = New System.Drawing.Point(3, 3)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(24, 24)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.icons8_user_24
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(33, 0)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(73, 30)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        Me.UsernameMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bc
        '
        Me.bc.DataPropertyName = "bc"
        Me.bc.HeaderText = "bc"
        Me.bc.MinimumWidth = 6
        Me.bc.Name = "bc"
        Me.bc.ReadOnly = True
        Me.bc.Width = 63
        '
        'caseno
        '
        Me.caseno.DataPropertyName = "caseno"
        Me.caseno.HeaderText = "caseno"
        Me.caseno.MinimumWidth = 6
        Me.caseno.Name = "caseno"
        Me.caseno.ReadOnly = True
        Me.caseno.Width = 109
        '
        'customer
        '
        Me.customer.DataPropertyName = "customer"
        Me.customer.HeaderText = "customer"
        Me.customer.MinimumWidth = 6
        Me.customer.Name = "customer"
        Me.customer.ReadOnly = True
        Me.customer.Width = 132
        '
        'grade
        '
        Me.grade.DataPropertyName = "grade"
        Me.grade.HeaderText = "grade"
        Me.grade.MinimumWidth = 6
        Me.grade.Name = "grade"
        Me.grade.ReadOnly = True
        Me.grade.Width = 96
        '
        'customerrs
        '
        Me.customerrs.DataPropertyName = "customerrs"
        Me.customerrs.HeaderText = "customerrs"
        Me.customerrs.MinimumWidth = 6
        Me.customerrs.Name = "customerrs"
        Me.customerrs.ReadOnly = True
        Me.customerrs.Width = 149
        '
        'graders
        '
        Me.graders.DataPropertyName = "graders"
        Me.graders.HeaderText = "graders"
        Me.graders.MinimumWidth = 6
        Me.graders.Name = "graders"
        Me.graders.ReadOnly = True
        Me.graders.Width = 113
        '
        'netdef
        '
        Me.netdef.DataPropertyName = "netdef"
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.netdef.DefaultCellStyle = DataGridViewCellStyle3
        Me.netdef.HeaderText = "netdef"
        Me.netdef.MinimumWidth = 6
        Me.netdef.Name = "netdef"
        Me.netdef.ReadOnly = True
        Me.netdef.Width = 105
        '
        'wh
        '
        Me.wh.DataPropertyName = "wh"
        Me.wh.HeaderText = "wh"
        Me.wh.MinimumWidth = 6
        Me.wh.Name = "wh"
        Me.wh.ReadOnly = True
        Me.wh.Width = 70
        '
        'bay
        '
        Me.bay.DataPropertyName = "bay"
        Me.bay.HeaderText = "bay"
        Me.bay.MinimumWidth = 6
        Me.bay.Name = "bay"
        Me.bay.ReadOnly = True
        Me.bay.Width = 76
        '
        'frompddate
        '
        Me.frompddate.DataPropertyName = "frompddate"
        DataGridViewCellStyle4.Format = "dd/MM/yyyy"
        Me.frompddate.DefaultCellStyle = DataGridViewCellStyle4
        Me.frompddate.HeaderText = "frompddate"
        Me.frompddate.MinimumWidth = 6
        Me.frompddate.Name = "frompddate"
        Me.frompddate.ReadOnly = True
        Me.frompddate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.frompddate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.frompddate.Width = 135
        '
        'frompdno
        '
        Me.frompdno.DataPropertyName = "frompdno"
        Me.frompdno.HeaderText = "frompdno"
        Me.frompdno.MinimumWidth = 6
        Me.frompdno.Name = "frompdno"
        Me.frompdno.ReadOnly = True
        Me.frompdno.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.frompdno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.frompdno.Width = 116
        '
        'issued
        '
        Me.issued.DataPropertyName = "issued"
        Me.issued.HeaderText = "issued"
        Me.issued.MinimumWidth = 6
        Me.issued.Name = "issued"
        Me.issued.ReadOnly = True
        Me.issued.Width = 79
        '
        'issuedto
        '
        Me.issuedto.DataPropertyName = "issuedto"
        Me.issuedto.HeaderText = "issuedto"
        Me.issuedto.MinimumWidth = 6
        Me.issuedto.Name = "issuedto"
        Me.issuedto.ReadOnly = True
        Me.issuedto.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.issuedto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'FrmScanMovement
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1200, 750)
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Name = "FrmScanMovement"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Scan Barcode To Movement"
        Me.MetroPanel2.ResumeLayout(False)
        Me.MetroPanel2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.FlowLayoutPanel12.ResumeLayout(False)
        Me.FlowLayoutPanel12.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BarcodeTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroPanel2 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents netrealTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents netdefTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents casenoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents GradeTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents whTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents TypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MovementNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FromTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel17 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ToTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TruckNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel18 As MetroFramework.Controls.MetroLabel
    Friend WithEvents DriverTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalCaseTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel19 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SendStatusTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents FinishedButton As Button
    Friend WithEvents DeleteButton As Button
    Friend WithEvents RefreshButton As Button
    Friend WithEvents SettingButton As Button
    Friend WithEvents BackButton As Button
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel12 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents bc As DataGridViewTextBoxColumn
    Friend WithEvents caseno As DataGridViewTextBoxColumn
    Friend WithEvents customer As DataGridViewTextBoxColumn
    Friend WithEvents grade As DataGridViewTextBoxColumn
    Friend WithEvents customerrs As DataGridViewTextBoxColumn
    Friend WithEvents graders As DataGridViewTextBoxColumn
    Friend WithEvents netdef As DataGridViewTextBoxColumn
    Friend WithEvents wh As DataGridViewTextBoxColumn
    Friend WithEvents bay As DataGridViewTextBoxColumn
    Friend WithEvents frompddate As DataGridViewTextBoxColumn
    Friend WithEvents frompdno As DataGridViewTextBoxColumn
    Friend WithEvents issued As DataGridViewCheckBoxColumn
    Friend WithEvents issuedto As DataGridViewTextBoxColumn
End Class

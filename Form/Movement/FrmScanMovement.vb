﻿Imports FactoryBL
Imports FactoryEntities
Imports FastMember

Public Class FrmScanMovement
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Public _movementNo As String
    Public _truckDriver As String
    Public _truckNo As String
    Public _fromLocation As String
    Public _fromLocationName As String
    Public _toLocationName As String
    Dim _barcodeInfo As sp_Shipping_SEL_BarcodeDetailFromPC_Result
    Dim _movementNoInfo As sp_Shipping_SEL_ShippingMovementByMovementNo_Result
    Dim _movementDetailsList As List(Of pd)

    Private Sub FrmScanMovement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            UsernameMetroLabel.Text = XUsername
            MovementNoInfoBinding()
            MovementDetailBinding()
            ClearForm()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackButton_Click(sender As Object, e As EventArgs) Handles BackButton.Click
        Me.Close()
    End Sub

    Private Sub BarcodeTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles BarcodeTextBox.KeyDown
        If e.KeyCode <> Keys.Enter Then
            Return
        End If
        Save()
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles SettingButton.Click
        ClearForm()
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        MovementDetailBinding()
        MovementNoInfoBinding()
    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As EventArgs) Handles DeleteButton.Click
        Try
            If _barcodeInfo Is Nothing Then
                Throw New Exception("โปรดคลิกเลือกแถวข้อมูลที่ต้องการลบจากด้านล่าง")
            End If

            If MessageBoxModule.Question("ต้องการลบ Barcode " & BarcodeTextBox.Text & " ใช่หรือไม่ ?") = DialogResult.No Then
                Return
            End If

            MovementNoInfoBinding()
            If _movementNoInfo Is Nothing Then
                Throw New Exception("ไม่พบเลข movement นี้ในระบบ")
            End If

            If _movementNoInfo.MovementFromFinished = True Then
                Throw New Exception("movement no นี้ถูก finished แล้ว ไม่สามารถลบข้อมูล barcode นี้ออกจาก movement ได้")
            End If

            db.sp_Shipping_MovementCancleSend(MovementNoTextbox.Text, BarcodeTextBox.Text, XUsername)
            MessageBoxModule.Info("ลบ Barcode นี้เรียบร้อยแล้ว")

            MovementDetailBinding()
            ClearForm()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid.CellMouseClick
        Try
            If e.RowIndex < 0 Then
                Return
            End If

            BarcodeTextBox.Text = MetroGrid.Rows(e.RowIndex).Cells(0).Value
            BarcodeInfoBinding()
            If _barcodeInfo Is Nothing Then
                MessageBoxModule.Warning("ไม่พบเลข Barcode กรุณาตรวจสอบอีกครั้ง")
                Return
            End If

            casenoTextBox.Text = _barcodeInfo.caseno
            GradeTextBox.Text = _barcodeInfo.graders
            netdefTextBox.Text = Convert.ToDecimal(_barcodeInfo.netdef).ToString("N2")
            netrealTextbox.Text = Convert.ToDecimal(_barcodeInfo.netreal).ToString("N2")
            whTextBox.Text = _barcodeInfo.wh
            DeleteButton.Enabled = True
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FinishedButton_Click(sender As Object, e As EventArgs) Handles FinishedButton.Click
        Try
            If String.IsNullOrEmpty(MovementNoTextbox.Text) = True Then
                Throw New Exception("ไม่มีข้อมูล Movement no.")
            End If

            If _movementDetailsList.Count <= 0 Then
                Throw New Exception("จะต้องมีรายการ  barcode สินค้าที่จะทำการเคลื่อนย้ายใน movement นี้อย่างน้อย 1 รายการขึ้นไป")
            End If

            MovementNoInfoBinding()
            If _movementNoInfo Is Nothing Then
                Throw New Exception("ไม่พบ movement no นี้ในระบบ")
            End If

            If Not _movementNoInfo Is Nothing And _movementNoInfo.MovementFromFinished = True Then
                Throw New Exception("Movement no นี้ได้ Finished ไปแล้ว")
            End If

            If MessageBoxModule.Question("ท่านต้องการ Finished movement no นี้ ใช่หรือไม่ ?") = DialogResult.No Then
                Return
            End If

            db.sp_Shipping_MovementSendFinished(MovementNoTextbox.Text)
            MessageBoxModule.Info("Finished Movement No นี้เรียบร้อยแล้ว")
            MovementNoInfoBinding()
            MovementDetailBinding()
            ClearForm()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MovementDetailBinding()
        Try
            _movementDetailsList = Facade.pdBL().GetByMovementNo(_movementNo)
            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_movementDetailsList)
            dt.Load(reader)
            MetroGrid.AutoGenerateColumns = False
            MetroGrid.DataSource = Nothing
            MetroGrid.DataSource = dt
            MetroGrid.ClearSelection()
            DeleteButton.Enabled = False
            TotalCaseTextBox.Text = _movementDetailsList.Count().ToString("N0")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub ClearForm()
        BarcodeTextBox.Text = ""
        casenoTextBox.Text = ""
        GradeTextBox.Text = ""
        netdefTextBox.Text = ""
        netrealTextbox.Text = ""
        whTextBox.Text = ""
        MetroGrid.ClearSelection()
        DeleteButton.Enabled = False
        BarcodeTextBox.Focus()
        _barcodeInfo = Nothing
    End Sub

    Private Sub BarcodeInfoBinding()
        Try
            _barcodeInfo = Facade.ShippingMovementBL().GetBarcodeInformation(BarcodeTextBox.Text)
            If _barcodeInfo IsNot Nothing Then
                casenoTextBox.Text = _barcodeInfo.caseno
                GradeTextBox.Text = _barcodeInfo.graders
                netdefTextBox.Text = Convert.ToDecimal(_barcodeInfo.netdef).ToString("N2")
                netrealTextbox.Text = Convert.ToDecimal(_barcodeInfo.netreal).ToString("N2")
                whTextBox.Text = _barcodeInfo.wh
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MovementNoInfoBinding()
        Try
            FromTextBox.Text = _fromLocationName
            TruckNoTextbox.Text = _truckNo
            ToTextBox.Text = _toLocationName
            DriverTextBox.Text = _truckDriver
            MovementNoTextbox.Text = _movementNo

            _movementNoInfo = Facade.ShippingMovementBL().GetShippingMovementByMovementNo(_movementNo)
            If _movementNoInfo Is Nothing Then
                Throw New Exception("ไม่ Movement No นี้ กรุณาตรวจสอบอีกครั้ง")
            End If

            SendStatusTextBox.Text = _movementNoInfo.MovementStatusName

            If _movementNoInfo.MovementFromFinished = True Then
                If _movementNoInfo.DateReceived Is Nothing Then
                    SendStatusTextBox.BackColor = Color.Yellow
                Else
                    SendStatusTextBox.BackColor = Color.LimeGreen
                End If
            Else
                SendStatusTextBox.BackColor = Color.Salmon
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub Save()
        Try
            MovementNoInfoBinding()
            If _movementNoInfo Is Nothing Then
                Throw New Exception("ไม่พบเลข movement นี้ในระบบ")
            End If

            If _movementNoInfo.MovementFromFinished = True Then
                Throw New Exception("movement no นี้ถูก finished ไปแล้ว ไม่สามารถบันทึกข้อมูลได้อีก")
            End If

            BarcodeInfoBinding()
            If _barcodeInfo Is Nothing Then
                Throw New Exception("ไม่พบเลข Barcode กรุณาตรวจสอบอีกครั้ง")
            End If

            If _barcodeInfo.caseno Is Nothing Then
                Throw New Exception("ยากล่องดังกล่าว ไม่ได้ระบุ case no")
            End If

            'If _barcodeInfo.caseno <= 0 Then
            '    Throw New Exception("ระบบไม่อนุญาตให้ทำการเคลื่อนย้ายยา remnant (case no 0)")
            'End If

            If _barcodeInfo.graders Is Nothing Then
                Throw New Exception("ยากล่องนี้ไม่ได้ระบุเกรด โปรดตรวจสอบ")
            End If

            'ถ้า Barcode นี้ warehouse ไม่ตรงกับ Station ที่เลือก ให้ show alert message
            If Microsoft.VisualBasic.Left(_barcodeInfo.wh, 4).ToUpper() <> Microsoft.VisualBasic.Left(_fromLocationName, 4).ToUpper() Then
                Throw New Exception("สถานะของยากล่องนี้ไม่ได้อยู่ที่ " + _fromLocationName + " ข้อมูลล่าสุดอยู่ที่ " + _barcodeInfo.wh)
            End If

            If _barcodeInfo.issued = True Then
                Throw New Exception("Barcode นี้ถูกนำไปใช้แล้วโดยการ " & _barcodeInfo.issuedto)
            End If

            If Not String.IsNullOrEmpty(_barcodeInfo.spno) Then
                Throw New Exception("Barcode นี้ถูกจองเพื่อทำการ shipping แล้ว (รหัส spno " & _barcodeInfo.spno)
            End If

            If Not IsDBNull(_barcodeInfo.MovementStatusID) Then
                If _barcodeInfo.MovementStatusID = False Then
                    Throw New Exception("Barcode นี้อยู่ในระหว่างการเคลื่อนย้ายไปปลายทางเพื่อรอผู้รับทำการรับเข้าระบบ (movement no " & _barcodeInfo.MovementNo)
                End If
            End If

            db.sp_Shipping_MovementSend(MovementNoTextbox.Text, BarcodeTextBox.Text, XUsername)
            MovementDetailBinding()
            ClearForm()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
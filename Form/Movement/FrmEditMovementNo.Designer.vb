﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmEditMovementNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SaveMetroButton = New MetroFramework.Controls.MetroButton()
        Me.BackMetroButton = New MetroFramework.Controls.MetroButton()
        Me.SendDateMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.TruckDriverMetroComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TruckMetroComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.CropMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.FromMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.ToMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MovementNoMetroTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.AutoSize = True
        Me.FlowLayoutPanel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel7.Controls.Add(Me.SaveMetroButton)
        Me.FlowLayoutPanel7.Controls.Add(Me.BackMetroButton)
        Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(3, 290)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(343, 36)
        Me.FlowLayoutPanel7.TabIndex = 188
        '
        'SaveMetroButton
        '
        Me.SaveMetroButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SaveMetroButton.FontSize = MetroFramework.MetroButtonSize.Tall
        Me.SaveMetroButton.Location = New System.Drawing.Point(3, 3)
        Me.SaveMetroButton.Name = "SaveMetroButton"
        Me.SaveMetroButton.Size = New System.Drawing.Size(100, 30)
        Me.SaveMetroButton.Style = MetroFramework.MetroColorStyle.Green
        Me.SaveMetroButton.TabIndex = 186
        Me.SaveMetroButton.Text = "Save"
        Me.SaveMetroButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SaveMetroButton.UseSelectable = True
        Me.SaveMetroButton.UseStyleColors = True
        '
        'BackMetroButton
        '
        Me.BackMetroButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackMetroButton.FontSize = MetroFramework.MetroButtonSize.Tall
        Me.BackMetroButton.Location = New System.Drawing.Point(109, 3)
        Me.BackMetroButton.Name = "BackMetroButton"
        Me.BackMetroButton.Size = New System.Drawing.Size(100, 30)
        Me.BackMetroButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BackMetroButton.TabIndex = 187
        Me.BackMetroButton.Text = "Back"
        Me.BackMetroButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BackMetroButton.UseSelectable = True
        Me.BackMetroButton.UseStyleColors = True
        '
        'SendDateMetroTextBox
        '
        '
        '
        '
        Me.SendDateMetroTextBox.CustomButton.Image = Nothing
        Me.SendDateMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.SendDateMetroTextBox.CustomButton.Name = ""
        Me.SendDateMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.SendDateMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.SendDateMetroTextBox.CustomButton.TabIndex = 1
        Me.SendDateMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SendDateMetroTextBox.CustomButton.UseSelectable = True
        Me.SendDateMetroTextBox.CustomButton.Visible = False
        Me.SendDateMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.SendDateMetroTextBox.Enabled = False
        Me.SendDateMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.SendDateMetroTextBox.Lines = New String(-1) {}
        Me.SendDateMetroTextBox.Location = New System.Drawing.Point(3, 85)
        Me.SendDateMetroTextBox.MaxLength = 32767
        Me.SendDateMetroTextBox.Name = "SendDateMetroTextBox"
        Me.SendDateMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.SendDateMetroTextBox.PromptText = "Send Date"
        Me.SendDateMetroTextBox.ReadOnly = True
        Me.SendDateMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.SendDateMetroTextBox.SelectedText = ""
        Me.SendDateMetroTextBox.SelectionLength = 0
        Me.SendDateMetroTextBox.SelectionStart = 0
        Me.SendDateMetroTextBox.ShortcutsEnabled = True
        Me.SendDateMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.SendDateMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.SendDateMetroTextBox.TabIndex = 181
        Me.SendDateMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.SendDateMetroTextBox.UseSelectable = True
        Me.SendDateMetroTextBox.UseStyleColors = True
        Me.SendDateMetroTextBox.WaterMark = "Send Date"
        Me.SendDateMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.SendDateMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TruckDriverMetroComboBox
        '
        Me.TruckDriverMetroComboBox.DisplayMember = "FirstName"
        Me.TruckDriverMetroComboBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.TruckDriverMetroComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.TruckDriverMetroComboBox.FormattingEnabled = True
        Me.TruckDriverMetroComboBox.ItemHeight = 29
        Me.TruckDriverMetroComboBox.Location = New System.Drawing.Point(3, 208)
        Me.TruckDriverMetroComboBox.Name = "TruckDriverMetroComboBox"
        Me.TruckDriverMetroComboBox.PromptText = "Driver"
        Me.TruckDriverMetroComboBox.Size = New System.Drawing.Size(343, 35)
        Me.TruckDriverMetroComboBox.Style = MetroFramework.MetroColorStyle.Green
        Me.TruckDriverMetroComboBox.TabIndex = 183
        Me.TruckDriverMetroComboBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckDriverMetroComboBox.UseSelectable = True
        Me.TruckDriverMetroComboBox.UseStyleColors = True
        Me.TruckDriverMetroComboBox.ValueMember = "PersonID"
        '
        'TruckMetroComboBox
        '
        Me.TruckMetroComboBox.DisplayMember = "TruckNo"
        Me.TruckMetroComboBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.TruckMetroComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.TruckMetroComboBox.FormattingEnabled = True
        Me.TruckMetroComboBox.ItemHeight = 29
        Me.TruckMetroComboBox.Location = New System.Drawing.Point(3, 249)
        Me.TruckMetroComboBox.Name = "TruckMetroComboBox"
        Me.TruckMetroComboBox.PromptText = "Truck No."
        Me.TruckMetroComboBox.Size = New System.Drawing.Size(343, 35)
        Me.TruckMetroComboBox.Style = MetroFramework.MetroColorStyle.Green
        Me.TruckMetroComboBox.TabIndex = 185
        Me.TruckMetroComboBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckMetroComboBox.UseSelectable = True
        Me.TruckMetroComboBox.UseStyleColors = True
        Me.TruckMetroComboBox.ValueMember = "TruckID"
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.MovementNoMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.CropMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.SendDateMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.FromMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.ToMetroTextBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.TruckDriverMetroComboBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.TruckMetroComboBox)
        Me.FlowLayoutPanel6.Controls.Add(Me.FlowLayoutPanel7)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(378, 370)
        Me.FlowLayoutPanel6.TabIndex = 182
        '
        'CropMetroTextBox
        '
        '
        '
        '
        Me.CropMetroTextBox.CustomButton.Image = Nothing
        Me.CropMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.CropMetroTextBox.CustomButton.Name = ""
        Me.CropMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.CropMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CropMetroTextBox.CustomButton.TabIndex = 1
        Me.CropMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CropMetroTextBox.CustomButton.UseSelectable = True
        Me.CropMetroTextBox.CustomButton.Visible = False
        Me.CropMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.CropMetroTextBox.Enabled = False
        Me.CropMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CropMetroTextBox.Lines = New String(-1) {}
        Me.CropMetroTextBox.Location = New System.Drawing.Point(3, 44)
        Me.CropMetroTextBox.MaxLength = 32767
        Me.CropMetroTextBox.Name = "CropMetroTextBox"
        Me.CropMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CropMetroTextBox.PromptText = "Crop"
        Me.CropMetroTextBox.ReadOnly = True
        Me.CropMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CropMetroTextBox.SelectedText = ""
        Me.CropMetroTextBox.SelectionLength = 0
        Me.CropMetroTextBox.SelectionStart = 0
        Me.CropMetroTextBox.ShortcutsEnabled = True
        Me.CropMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.CropMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.CropMetroTextBox.TabIndex = 180
        Me.CropMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CropMetroTextBox.UseSelectable = True
        Me.CropMetroTextBox.UseStyleColors = True
        Me.CropMetroTextBox.WaterMark = "Crop"
        Me.CropMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CropMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FromMetroTextBox
        '
        '
        '
        '
        Me.FromMetroTextBox.CustomButton.Image = Nothing
        Me.FromMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.FromMetroTextBox.CustomButton.Name = ""
        Me.FromMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.FromMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.FromMetroTextBox.CustomButton.TabIndex = 1
        Me.FromMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromMetroTextBox.CustomButton.UseSelectable = True
        Me.FromMetroTextBox.CustomButton.Visible = False
        Me.FromMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.FromMetroTextBox.Enabled = False
        Me.FromMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.FromMetroTextBox.Lines = New String(-1) {}
        Me.FromMetroTextBox.Location = New System.Drawing.Point(3, 126)
        Me.FromMetroTextBox.MaxLength = 32767
        Me.FromMetroTextBox.Name = "FromMetroTextBox"
        Me.FromMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.FromMetroTextBox.PromptText = "From"
        Me.FromMetroTextBox.ReadOnly = True
        Me.FromMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.FromMetroTextBox.SelectedText = ""
        Me.FromMetroTextBox.SelectionLength = 0
        Me.FromMetroTextBox.SelectionStart = 0
        Me.FromMetroTextBox.ShortcutsEnabled = True
        Me.FromMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.FromMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.FromMetroTextBox.TabIndex = 192
        Me.FromMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromMetroTextBox.UseSelectable = True
        Me.FromMetroTextBox.UseStyleColors = True
        Me.FromMetroTextBox.WaterMark = "From"
        Me.FromMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FromMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ToMetroTextBox
        '
        '
        '
        '
        Me.ToMetroTextBox.CustomButton.Image = Nothing
        Me.ToMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.ToMetroTextBox.CustomButton.Name = ""
        Me.ToMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.ToMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.ToMetroTextBox.CustomButton.TabIndex = 1
        Me.ToMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.ToMetroTextBox.CustomButton.UseSelectable = True
        Me.ToMetroTextBox.CustomButton.Visible = False
        Me.ToMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.ToMetroTextBox.Enabled = False
        Me.ToMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.ToMetroTextBox.Lines = New String(-1) {}
        Me.ToMetroTextBox.Location = New System.Drawing.Point(3, 167)
        Me.ToMetroTextBox.MaxLength = 32767
        Me.ToMetroTextBox.Name = "ToMetroTextBox"
        Me.ToMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.ToMetroTextBox.PromptText = "To"
        Me.ToMetroTextBox.ReadOnly = True
        Me.ToMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.ToMetroTextBox.SelectedText = ""
        Me.ToMetroTextBox.SelectionLength = 0
        Me.ToMetroTextBox.SelectionStart = 0
        Me.ToMetroTextBox.ShortcutsEnabled = True
        Me.ToMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.ToMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.ToMetroTextBox.TabIndex = 193
        Me.ToMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.ToMetroTextBox.UseSelectable = True
        Me.ToMetroTextBox.UseStyleColors = True
        Me.ToMetroTextBox.WaterMark = "To"
        Me.ToMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.ToMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MovementNoMetroTextBox
        '
        '
        '
        '
        Me.MovementNoMetroTextBox.CustomButton.Image = Nothing
        Me.MovementNoMetroTextBox.CustomButton.Location = New System.Drawing.Point(309, 1)
        Me.MovementNoMetroTextBox.CustomButton.Name = ""
        Me.MovementNoMetroTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.MovementNoMetroTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.MovementNoMetroTextBox.CustomButton.TabIndex = 1
        Me.MovementNoMetroTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MovementNoMetroTextBox.CustomButton.UseSelectable = True
        Me.MovementNoMetroTextBox.CustomButton.Visible = False
        Me.MovementNoMetroTextBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.MovementNoMetroTextBox.Enabled = False
        Me.MovementNoMetroTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.MovementNoMetroTextBox.Lines = New String(-1) {}
        Me.MovementNoMetroTextBox.Location = New System.Drawing.Point(3, 3)
        Me.MovementNoMetroTextBox.MaxLength = 32767
        Me.MovementNoMetroTextBox.Name = "MovementNoMetroTextBox"
        Me.MovementNoMetroTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.MovementNoMetroTextBox.PromptText = "Movement No"
        Me.MovementNoMetroTextBox.ReadOnly = True
        Me.MovementNoMetroTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MovementNoMetroTextBox.SelectedText = ""
        Me.MovementNoMetroTextBox.SelectionLength = 0
        Me.MovementNoMetroTextBox.SelectionStart = 0
        Me.MovementNoMetroTextBox.ShortcutsEnabled = True
        Me.MovementNoMetroTextBox.Size = New System.Drawing.Size(343, 35)
        Me.MovementNoMetroTextBox.Style = MetroFramework.MetroColorStyle.Green
        Me.MovementNoMetroTextBox.TabIndex = 194
        Me.MovementNoMetroTextBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MovementNoMetroTextBox.UseSelectable = True
        Me.MovementNoMetroTextBox.UseStyleColors = True
        Me.MovementNoMetroTextBox.WaterMark = "Movement No"
        Me.MovementNoMetroTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.MovementNoMetroTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FrmEditMovementNo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 450)
        Me.Controls.Add(Me.FlowLayoutPanel6)
        Me.Name = "FrmEditMovementNo"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "Edit Movement No"
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
    Friend WithEvents SaveMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents BackMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents SendDateMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents TruckDriverMetroComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TruckMetroComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents CropMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents FromMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents ToMetroTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MovementNoMetroTextBox As MetroFramework.Controls.MetroTextBox
End Class

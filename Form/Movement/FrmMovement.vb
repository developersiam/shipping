﻿Imports FactoryBL
Imports FactoryEntities
Imports FastMember

Public Class FrmMovement
    Inherits MetroFramework.Forms.MetroForm
    Dim currentCrop As Integer
    Dim _selectedRow As DataGridViewRow
    Dim _movement As New sp_Shipping_SEL_ShippingMovementV2023_Result
    Dim selectedCases As Int16
    Dim db As New ShippingDataClassesDataContext

    Private _movementNoList As List(Of FactoryEntities.sp_Shipping_SEL_ShippingMovementV2023_Result)
    Public Property NewProperty() As List(Of FactoryEntities.sp_Shipping_SEL_ShippingMovementV2023_Result)
        Get
            Return _movementNoList
        End Get
        Set(ByVal value As List(Of FactoryEntities.sp_Shipping_SEL_ShippingMovementV2023_Result))
            _movementNoList = value
        End Set
    End Property

    Private _totalRecord As Int16
    Public Property TotalRecord() As Int16
        Get
            Return _totalRecord
        End Get
        Set(ByVal value As Int16)
            _totalRecord = value
        End Set
    End Property

    Private Sub CropListBinding()
        Try
            currentCrop = Year(Now)
            Dim cropList As New List(Of Integer)
            Dim dataTable As New ShippingDataSet.ShippingMovementDataTable
            Dim tableAdapter As New ShippingDataSetTableAdapters.ShippingMovementTableAdapter
            tableAdapter.Fill(dataTable)

            For Each i As ShippingDataSet.ShippingMovementRow In dataTable
                cropList.Add(i.Crop)
            Next

            If cropList.Find(Function(x) x = currentCrop) = Nothing Then
                cropList.Add(currentCrop)
            End If

            CropComboBox.DataSource = Nothing
            CropComboBox.DataSource = cropList.OrderByDescending(Function(x) x).ToList()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MovementDataGridBinding()
        Try
            If CropComboBox.SelectedIndex <= -1 Then
                Return
            End If

            If FromLocationComboBox.Text = Nothing Then
                Return
            End If

            _movementNoList = Facade.ShippingMovementBL().GetShippingMovementV2023(CropComboBox.SelectedValue, FromLocationComboBox.SelectedValue).OrderByDescending(Function(x) x.MovementNo).ToList()
            _totalRecord = _movementNoList.Count()

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_movementNoList)
            dt.Load(reader)

            MetroGrid.AutoGenerateColumns = False
            MetroGrid.DataSource = Nothing
            MetroGrid.DataSource = dt
            TotalRecordMetroLabel.Text = _totalRecord.ToString("N0")

            selectedCases = Nothing
            _selectedRow = Nothing

            EditButton.Enabled = False
            RemoveButton.Enabled = False
            StartToScanButton.Enabled = False
            MetroGrid.ClearSelection()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FrmMovement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            UsernameMetroLabel.Text = XUsername
            EditButton.Enabled = False
            RemoveButton.Enabled = False
            StartToScanButton.Enabled = False

            CropListBinding()
            CropComboBox.Text = currentCrop

            Dim locationList = Facade.ShippingLocationBL().GetAll().Where(Function(x) x.Status = True).OrderByDescending(Function(x) x.LocName).ToList()
            FromLocationComboBox.DataSource = Nothing
            FromLocationComboBox.DataSource = locationList

            MovementDataGridBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FromLocationComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FromLocationComboBox.SelectedIndexChanged
        MovementDataGridBinding()
    End Sub

    Private Sub CreateButton_Click(sender As Object, e As EventArgs) Handles CreateButton.Click
        Try
            Dim frm As New FrmAddMovementNo
            frm.Crop = CropComboBox.Text
            frm.FromLocationNo = FromLocationComboBox.SelectedValue
            frm.FromLocationName = FromLocationComboBox.Text

            frm.ShowDialog()
            MovementDataGridBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        MovementDataGridBinding()
    End Sub

    Private Sub BackButton_Click(sender As Object, e As EventArgs) Handles BackButton.Click
        Me.Close()
    End Sub

    Private Sub StartToScanButton_Click(sender As Object, e As EventArgs) Handles StartToScanButton.Click
        Try
            If _selectedRow Is Nothing Then
                Throw New Exception("กรุณาเลือก Movement no. ที่ต้องการจากตาราง")
            End If

            Dim frm As New FrmScanMovement
            frm._movementNo = _movement.MovementNo
            frm._truckNo = _movement.TruckNo
            frm._truckDriver = _movement.DriverName
            frm._toLocationName = _movement.ToLocation
            frm._fromLocation = FromLocationComboBox.SelectedValue
            frm._fromLocationName = FromLocationComboBox.Text
            frm.ShowDialog()
            MovementDataGridBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid.CellMouseClick
        Try
            Dim rowIndex = MetroGrid.CurrentRow.Index
            _selectedRow = MetroGrid.Rows(rowIndex)
            If _selectedRow Is Nothing Then
                Return
            End If

            _movement.Crop = _selectedRow.Cells(0).Value
            _movement.MovementNo = _selectedRow.Cells(1).Value
            _movement.ToLocation = _selectedRow.Cells(2).Value
            _movement.TruckNo = _selectedRow.Cells(3).Value
            _movement.DriverName = If(String.IsNullOrEmpty(_selectedRow.Cells(4).Value.ToString()), "", _selectedRow.Cells(4).Value)
            _movement.MovementStatusName = _selectedRow.Cells(5).Value
            _movement.TotalCase = _selectedRow.Cells(8).Value

            EditButton.Enabled = True
            RemoveButton.Enabled = True
            StartToScanButton.Enabled = True
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub RemoveButton_Click(sender As Object, e As EventArgs) Handles RemoveButton.Click
        Try
            If _selectedRow Is Nothing Then
                Throw New Exception("กรุณาเลือก Movement no. ที่ต้องการจากตาราง")
            End If

            If _movement.TotalCase > 0 Then
                Throw New Exception("ไม่สามารถลบข้อมูล Movement no.ได้เนื่องจากมี Barcode อยู่ จะต้องลบ Barcode ที่อยู่ในเลขนี้ให้หมดก่อน")
            End If

            If MessageBoxModule.Question("ต้องการลบ Movement no. " & _selectedRow.Cells(1).Value & " ใช่หรือไม่ ?") = DialogResult.No Then
                Return
            End If

            db.sp_Shipping_DEL_ShippingMovement(_selectedRow.Cells(1).Value, XUsername)
            MessageBox.Show("ลบ Movement No. นี้เรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            MovementDataGridBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub CropComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CropComboBox.SelectedIndexChanged
        MovementDataGridBinding()
    End Sub

    Private Sub SettingButton_Click(sender As Object, e As EventArgs) Handles SettingButton.Click
        Dim window = New MovementSetting
        window.ShowDialog()
    End Sub

    Private Sub EditButton_Click(sender As Object, e As EventArgs) Handles EditButton.Click
        Try
            If _selectedRow Is Nothing Then
                Throw New Exception("กรุณาเลือก Movement no. ที่ต้องการจากตาราง")
            End If

            Dim frm As New FrmEditMovementNo
            frm.MovementNo = _movement.MovementNo
            frm.ShowDialog()
            MovementDataGridBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
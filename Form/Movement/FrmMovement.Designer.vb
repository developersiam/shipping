﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMovement
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.Crop = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToLocation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TruckNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Driver = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SendDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceivedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalCase = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementFromUser = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FromLocationComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.CropComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel12 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel15 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.TotalRecordMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.CropDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementnoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TruckidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementstatusidDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.MovementfromDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DatestartDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementtoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DatereceivedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceiveduserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementfromuserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementfromfinishedDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.MovementfromfinisheddateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DriverDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TrucknoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NBarcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.CreateButton = New System.Windows.Forms.Button()
        Me.EditButton = New System.Windows.Forms.Button()
        Me.RemoveButton = New System.Windows.Forms.Button()
        Me.StartToScanButton = New System.Windows.Forms.Button()
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.SettingButton = New System.Windows.Forms.Button()
        Me.BackButton = New System.Windows.Forms.Button()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel12.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel15.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Crop, Me.MovementNo, Me.ToLocation, Me.TruckNo, Me.Driver, Me.Status, Me.SendDate, Me.ReceivedDate, Me.TotalCase, Me.MovementFromUser})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle11
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(3, 111)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Calibri", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 38
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(1154, 576)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 130
        '
        'Crop
        '
        Me.Crop.DataPropertyName = "Crop"
        Me.Crop.HeaderText = "Crop"
        Me.Crop.Name = "Crop"
        Me.Crop.ReadOnly = True
        Me.Crop.Width = 70
        '
        'MovementNo
        '
        Me.MovementNo.DataPropertyName = "MovementNo"
        Me.MovementNo.HeaderText = "MovementNo"
        Me.MovementNo.Name = "MovementNo"
        Me.MovementNo.ReadOnly = True
        Me.MovementNo.Width = 139
        '
        'ToLocation
        '
        Me.ToLocation.DataPropertyName = "ToLocation"
        Me.ToLocation.HeaderText = "ToLocation"
        Me.ToLocation.Name = "ToLocation"
        Me.ToLocation.ReadOnly = True
        Me.ToLocation.Width = 116
        '
        'TruckNo
        '
        Me.TruckNo.DataPropertyName = "TruckNo"
        Me.TruckNo.HeaderText = "TruckNo"
        Me.TruckNo.Name = "TruckNo"
        Me.TruckNo.ReadOnly = True
        Me.TruckNo.Width = 97
        '
        'Driver
        '
        Me.Driver.DataPropertyName = "DriverName"
        Me.Driver.HeaderText = "Driver"
        Me.Driver.Name = "Driver"
        Me.Driver.ReadOnly = True
        Me.Driver.Width = 81
        '
        'Status
        '
        Me.Status.DataPropertyName = "MovementStatusName"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        Me.Status.Width = 81
        '
        'SendDate
        '
        Me.SendDate.DataPropertyName = "StartDate"
        DataGridViewCellStyle9.Format = "dd/MM/yyyy HH:MM"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.SendDate.DefaultCellStyle = DataGridViewCellStyle9
        Me.SendDate.HeaderText = "SendDate"
        Me.SendDate.Name = "SendDate"
        Me.SendDate.ReadOnly = True
        Me.SendDate.Width = 107
        '
        'ReceivedDate
        '
        Me.ReceivedDate.DataPropertyName = "ReceiveDate"
        DataGridViewCellStyle10.Format = "dd/MM/yyyy HH:MM"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.ReceivedDate.DefaultCellStyle = DataGridViewCellStyle10
        Me.ReceivedDate.HeaderText = "ReceivedDate"
        Me.ReceivedDate.Name = "ReceivedDate"
        Me.ReceivedDate.ReadOnly = True
        Me.ReceivedDate.Width = 137
        '
        'TotalCase
        '
        Me.TotalCase.DataPropertyName = "TotalCase"
        Me.TotalCase.HeaderText = "Total Case"
        Me.TotalCase.Name = "TotalCase"
        Me.TotalCase.ReadOnly = True
        Me.TotalCase.Width = 109
        '
        'MovementFromUser
        '
        Me.MovementFromUser.DataPropertyName = "MovementFromUser"
        Me.MovementFromUser.HeaderText = "Movement User"
        Me.MovementFromUser.Name = "MovementFromUser"
        Me.MovementFromUser.ReadOnly = True
        Me.MovementFromUser.Width = 157
        '
        'FromLocationComboBox
        '
        Me.FromLocationComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FromLocationComboBox.DisplayMember = "LocName"
        Me.FromLocationComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.FromLocationComboBox.FormattingEnabled = True
        Me.FromLocationComboBox.ItemHeight = 29
        Me.FromLocationComboBox.Location = New System.Drawing.Point(214, 20)
        Me.FromLocationComboBox.Margin = New System.Windows.Forms.Padding(3, 3, 3, 5)
        Me.FromLocationComboBox.Name = "FromLocationComboBox"
        Me.FromLocationComboBox.PromptText = "From Location"
        Me.FromLocationComboBox.Size = New System.Drawing.Size(343, 35)
        Me.FromLocationComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.FromLocationComboBox.TabIndex = 163
        Me.FromLocationComboBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FromLocationComboBox.UseSelectable = True
        Me.FromLocationComboBox.ValueMember = "LocNo"
        '
        'CropComboBox
        '
        Me.CropComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CropComboBox.DisplayMember = "crop"
        Me.CropComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.CropComboBox.FormattingEnabled = True
        Me.CropComboBox.ItemHeight = 29
        Me.CropComboBox.Location = New System.Drawing.Point(0, 20)
        Me.CropComboBox.Margin = New System.Windows.Forms.Padding(0, 3, 3, 5)
        Me.CropComboBox.Name = "CropComboBox"
        Me.CropComboBox.PromptText = "Crop"
        Me.CropComboBox.Size = New System.Drawing.Size(208, 35)
        Me.CropComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropComboBox.TabIndex = 165
        Me.CropComboBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CropComboBox.UseSelectable = True
        Me.CropComboBox.ValueMember = "crop"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.FlowLayoutPanel12)
        Me.FlowLayoutPanel2.Controls.Add(Me.FlowLayoutPanel15)
        Me.FlowLayoutPanel2.Controls.Add(Me.MetroGrid)
        Me.FlowLayoutPanel2.Controls.Add(Me.FlowLayoutPanel5)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1160, 722)
        Me.FlowLayoutPanel2.TabIndex = 166
        '
        'FlowLayoutPanel12
        '
        Me.FlowLayoutPanel12.AutoSize = True
        Me.FlowLayoutPanel12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel12.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel12.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel12.Name = "FlowLayoutPanel12"
        Me.FlowLayoutPanel12.Size = New System.Drawing.Size(110, 36)
        Me.FlowLayoutPanel12.TabIndex = 175
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroTile1)
        Me.FlowLayoutPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(104, 30)
        Me.FlowLayoutPanel1.TabIndex = 175
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(33, 0)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(68, 30)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        Me.UsernameMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel15
        '
        Me.FlowLayoutPanel15.AutoSize = True
        Me.FlowLayoutPanel15.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel15.Controls.Add(Me.CropComboBox)
        Me.FlowLayoutPanel15.Controls.Add(Me.FromLocationComboBox)
        Me.FlowLayoutPanel15.Controls.Add(Me.FlowLayoutPanel6)
        Me.FlowLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel15.Location = New System.Drawing.Point(3, 45)
        Me.FlowLayoutPanel15.Name = "FlowLayoutPanel15"
        Me.FlowLayoutPanel15.Size = New System.Drawing.Size(1154, 60)
        Me.FlowLayoutPanel15.TabIndex = 176
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.CreateButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.EditButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.RemoveButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.StartToScanButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.RefreshButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.SettingButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.BackButton)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(563, 3)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(518, 54)
        Me.FlowLayoutPanel6.TabIndex = 169
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.AutoSize = True
        Me.FlowLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel5.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel5.Controls.Add(Me.TotalRecordMetroLabel)
        Me.FlowLayoutPanel5.Controls.Add(Me.MetroLabel3)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(3, 693)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(145, 19)
        Me.FlowLayoutPanel5.TabIndex = 178
        '
        'MetroLabel1
        '
        Me.MetroLabel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(40, 19)
        Me.MetroLabel1.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel1.TabIndex = 177
        Me.MetroLabel1.Text = "Total "
        Me.MetroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TotalRecordMetroLabel
        '
        Me.TotalRecordMetroLabel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TotalRecordMetroLabel.AutoSize = True
        Me.TotalRecordMetroLabel.Location = New System.Drawing.Point(49, 0)
        Me.TotalRecordMetroLabel.Name = "TotalRecordMetroLabel"
        Me.TotalRecordMetroLabel.Size = New System.Drawing.Size(36, 19)
        Me.TotalRecordMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalRecordMetroLabel.TabIndex = 178
        Me.TotalRecordMetroLabel.Text = "x,xxx"
        Me.TotalRecordMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel3
        '
        Me.MetroLabel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(91, 0)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(51, 19)
        Me.MetroLabel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel3.TabIndex = 179
        Me.MetroLabel3.Text = "Record"
        Me.MetroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(74, 25)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(0, 0)
        Me.FlowLayoutPanel3.TabIndex = 167
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(407, 404)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(0, 0)
        Me.FlowLayoutPanel4.TabIndex = 168
        '
        'CropDataGridViewTextBoxColumn
        '
        Me.CropDataGridViewTextBoxColumn.DataPropertyName = "crop"
        Me.CropDataGridViewTextBoxColumn.HeaderText = "crop"
        Me.CropDataGridViewTextBoxColumn.Name = "CropDataGridViewTextBoxColumn"
        Me.CropDataGridViewTextBoxColumn.Width = 68
        '
        'MovementnoDataGridViewTextBoxColumn
        '
        Me.MovementnoDataGridViewTextBoxColumn.DataPropertyName = "movementno"
        Me.MovementnoDataGridViewTextBoxColumn.HeaderText = "movementno"
        Me.MovementnoDataGridViewTextBoxColumn.Name = "MovementnoDataGridViewTextBoxColumn"
        Me.MovementnoDataGridViewTextBoxColumn.Width = 136
        '
        'TruckidDataGridViewTextBoxColumn
        '
        Me.TruckidDataGridViewTextBoxColumn.DataPropertyName = "truckid"
        Me.TruckidDataGridViewTextBoxColumn.HeaderText = "truckid"
        Me.TruckidDataGridViewTextBoxColumn.Name = "TruckidDataGridViewTextBoxColumn"
        Me.TruckidDataGridViewTextBoxColumn.Width = 87
        '
        'MovementstatusidDataGridViewCheckBoxColumn
        '
        Me.MovementstatusidDataGridViewCheckBoxColumn.DataPropertyName = "movementstatusid"
        Me.MovementstatusidDataGridViewCheckBoxColumn.HeaderText = "movementstatusid"
        Me.MovementstatusidDataGridViewCheckBoxColumn.Name = "MovementstatusidDataGridViewCheckBoxColumn"
        Me.MovementstatusidDataGridViewCheckBoxColumn.Width = 158
        '
        'MovementfromDataGridViewTextBoxColumn
        '
        Me.MovementfromDataGridViewTextBoxColumn.DataPropertyName = "movementfrom"
        Me.MovementfromDataGridViewTextBoxColumn.HeaderText = "movementfrom"
        Me.MovementfromDataGridViewTextBoxColumn.Name = "MovementfromDataGridViewTextBoxColumn"
        Me.MovementfromDataGridViewTextBoxColumn.Width = 154
        '
        'DatestartDataGridViewTextBoxColumn
        '
        Me.DatestartDataGridViewTextBoxColumn.DataPropertyName = "datestart"
        Me.DatestartDataGridViewTextBoxColumn.HeaderText = "datestart"
        Me.DatestartDataGridViewTextBoxColumn.Name = "DatestartDataGridViewTextBoxColumn"
        Me.DatestartDataGridViewTextBoxColumn.Width = 103
        '
        'MovementtoDataGridViewTextBoxColumn
        '
        Me.MovementtoDataGridViewTextBoxColumn.DataPropertyName = "movementto"
        Me.MovementtoDataGridViewTextBoxColumn.HeaderText = "movementto"
        Me.MovementtoDataGridViewTextBoxColumn.Name = "MovementtoDataGridViewTextBoxColumn"
        Me.MovementtoDataGridViewTextBoxColumn.Width = 132
        '
        'DatereceivedDataGridViewTextBoxColumn
        '
        Me.DatereceivedDataGridViewTextBoxColumn.DataPropertyName = "datereceived"
        Me.DatereceivedDataGridViewTextBoxColumn.HeaderText = "datereceived"
        Me.DatereceivedDataGridViewTextBoxColumn.Name = "DatereceivedDataGridViewTextBoxColumn"
        Me.DatereceivedDataGridViewTextBoxColumn.Width = 132
        '
        'ReceiveduserDataGridViewTextBoxColumn
        '
        Me.ReceiveduserDataGridViewTextBoxColumn.DataPropertyName = "receiveduser"
        Me.ReceiveduserDataGridViewTextBoxColumn.HeaderText = "receiveduser"
        Me.ReceiveduserDataGridViewTextBoxColumn.Name = "ReceiveduserDataGridViewTextBoxColumn"
        Me.ReceiveduserDataGridViewTextBoxColumn.Width = 132
        '
        'MovementfromuserDataGridViewTextBoxColumn
        '
        Me.MovementfromuserDataGridViewTextBoxColumn.DataPropertyName = "movementfromuser"
        Me.MovementfromuserDataGridViewTextBoxColumn.HeaderText = "movementfromuser"
        Me.MovementfromuserDataGridViewTextBoxColumn.Name = "MovementfromuserDataGridViewTextBoxColumn"
        Me.MovementfromuserDataGridViewTextBoxColumn.Width = 188
        '
        'MovementfromfinishedDataGridViewCheckBoxColumn
        '
        Me.MovementfromfinishedDataGridViewCheckBoxColumn.DataPropertyName = "movementfromfinished"
        Me.MovementfromfinishedDataGridViewCheckBoxColumn.HeaderText = "movementfromfinished"
        Me.MovementfromfinishedDataGridViewCheckBoxColumn.Name = "MovementfromfinishedDataGridViewCheckBoxColumn"
        Me.MovementfromfinishedDataGridViewCheckBoxColumn.Width = 196
        '
        'MovementfromfinisheddateDataGridViewTextBoxColumn
        '
        Me.MovementfromfinisheddateDataGridViewTextBoxColumn.DataPropertyName = "movementfromfinisheddate"
        Me.MovementfromfinisheddateDataGridViewTextBoxColumn.HeaderText = "movementfromfinisheddate"
        Me.MovementfromfinisheddateDataGridViewTextBoxColumn.Name = "MovementfromfinisheddateDataGridViewTextBoxColumn"
        Me.MovementfromfinisheddateDataGridViewTextBoxColumn.Width = 249
        '
        'DriverDataGridViewTextBoxColumn
        '
        Me.DriverDataGridViewTextBoxColumn.DataPropertyName = "driver"
        Me.DriverDataGridViewTextBoxColumn.HeaderText = "driver"
        Me.DriverDataGridViewTextBoxColumn.Name = "DriverDataGridViewTextBoxColumn"
        Me.DriverDataGridViewTextBoxColumn.Width = 79
        '
        'TrucknoDataGridViewTextBoxColumn
        '
        Me.TrucknoDataGridViewTextBoxColumn.DataPropertyName = "truckno"
        Me.TrucknoDataGridViewTextBoxColumn.HeaderText = "truckno"
        Me.TrucknoDataGridViewTextBoxColumn.Name = "TrucknoDataGridViewTextBoxColumn"
        Me.TrucknoDataGridViewTextBoxColumn.Width = 93
        '
        'NBarcodeDataGridViewTextBoxColumn
        '
        Me.NBarcodeDataGridViewTextBoxColumn.DataPropertyName = "NBarcode"
        Me.NBarcodeDataGridViewTextBoxColumn.HeaderText = "NBarcode"
        Me.NBarcodeDataGridViewTextBoxColumn.Name = "NBarcodeDataGridViewTextBoxColumn"
        Me.NBarcodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.NBarcodeDataGridViewTextBoxColumn.Width = 108
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Default
        Me.MetroTile1.Location = New System.Drawing.Point(3, 3)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(24, 24)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.icons8_user_24
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'CreateButton
        '
        Me.CreateButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.CreateButton.BackColor = System.Drawing.Color.LimeGreen
        Me.CreateButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CreateButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CreateButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_add_24
        Me.CreateButton.Location = New System.Drawing.Point(2, 2)
        Me.CreateButton.Margin = New System.Windows.Forms.Padding(2)
        Me.CreateButton.Name = "CreateButton"
        Me.CreateButton.Size = New System.Drawing.Size(70, 50)
        Me.CreateButton.TabIndex = 179
        Me.CreateButton.Text = "Create"
        Me.CreateButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.CreateButton.UseVisualStyleBackColor = False
        '
        'EditButton
        '
        Me.EditButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.EditButton.BackColor = System.Drawing.Color.Yellow
        Me.EditButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EditButton.Enabled = False
        Me.EditButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.EditButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_edit_24
        Me.EditButton.Location = New System.Drawing.Point(76, 2)
        Me.EditButton.Margin = New System.Windows.Forms.Padding(2)
        Me.EditButton.Name = "EditButton"
        Me.EditButton.Size = New System.Drawing.Size(70, 50)
        Me.EditButton.TabIndex = 185
        Me.EditButton.Text = "Edit"
        Me.EditButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.EditButton.UseVisualStyleBackColor = False
        '
        'RemoveButton
        '
        Me.RemoveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.RemoveButton.BackColor = System.Drawing.Color.Tomato
        Me.RemoveButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RemoveButton.Enabled = False
        Me.RemoveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RemoveButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_delete_24
        Me.RemoveButton.Location = New System.Drawing.Point(150, 2)
        Me.RemoveButton.Margin = New System.Windows.Forms.Padding(2)
        Me.RemoveButton.Name = "RemoveButton"
        Me.RemoveButton.Size = New System.Drawing.Size(70, 50)
        Me.RemoveButton.TabIndex = 180
        Me.RemoveButton.Text = "Remove"
        Me.RemoveButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.RemoveButton.UseVisualStyleBackColor = False
        '
        'StartToScanButton
        '
        Me.StartToScanButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.StartToScanButton.BackColor = System.Drawing.Color.Orange
        Me.StartToScanButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.StartToScanButton.Enabled = False
        Me.StartToScanButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.StartToScanButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_barcode_24__1_
        Me.StartToScanButton.Location = New System.Drawing.Point(224, 2)
        Me.StartToScanButton.Margin = New System.Windows.Forms.Padding(2)
        Me.StartToScanButton.Name = "StartToScanButton"
        Me.StartToScanButton.Size = New System.Drawing.Size(70, 50)
        Me.StartToScanButton.TabIndex = 182
        Me.StartToScanButton.Text = "Scan"
        Me.StartToScanButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.StartToScanButton.UseVisualStyleBackColor = False
        '
        'RefreshButton
        '
        Me.RefreshButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.RefreshButton.BackColor = System.Drawing.Color.MediumTurquoise
        Me.RefreshButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RefreshButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_refresh_24
        Me.RefreshButton.Location = New System.Drawing.Point(298, 2)
        Me.RefreshButton.Margin = New System.Windows.Forms.Padding(2)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(70, 50)
        Me.RefreshButton.TabIndex = 181
        Me.RefreshButton.Text = "Refresh"
        Me.RefreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.RefreshButton.UseVisualStyleBackColor = False
        '
        'SettingButton
        '
        Me.SettingButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SettingButton.BackColor = System.Drawing.Color.Gainsboro
        Me.SettingButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SettingButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SettingButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_setting_24
        Me.SettingButton.Location = New System.Drawing.Point(372, 2)
        Me.SettingButton.Margin = New System.Windows.Forms.Padding(2)
        Me.SettingButton.Name = "SettingButton"
        Me.SettingButton.Size = New System.Drawing.Size(70, 50)
        Me.SettingButton.TabIndex = 183
        Me.SettingButton.Text = "Setting"
        Me.SettingButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.SettingButton.UseVisualStyleBackColor = False
        '
        'BackButton
        '
        Me.BackButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackButton.BackColor = System.Drawing.Color.White
        Me.BackButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BackButton.Image = Global.ShippingSystem.My.Resources.Resources.icons8_back_24
        Me.BackButton.Location = New System.Drawing.Point(446, 2)
        Me.BackButton.Margin = New System.Windows.Forms.Padding(2)
        Me.BackButton.Name = "BackButton"
        Me.BackButton.Size = New System.Drawing.Size(70, 50)
        Me.BackButton.TabIndex = 184
        Me.BackButton.Text = "Back"
        Me.BackButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.BackButton.UseVisualStyleBackColor = False
        '
        'FrmMovement
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1200, 805)
        Me.Controls.Add(Me.FlowLayoutPanel4)
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Name = "FrmMovement"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Shipping Movement"
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel12.ResumeLayout(False)
        Me.FlowLayoutPanel12.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel15.ResumeLayout(False)
        Me.FlowLayoutPanel15.PerformLayout()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents FromLocationComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents CropComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel12 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel15 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents TotalRecordMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MovementnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TruckidDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MovementstatusidDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents MovementfromDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DatestartDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MovementtoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DatereceivedDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceiveduserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MovementfromuserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MovementfromfinishedDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents MovementfromfinisheddateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DriverDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TrucknoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NBarcodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Crop As DataGridViewTextBoxColumn
    Friend WithEvents MovementNo As DataGridViewTextBoxColumn
    Friend WithEvents ToLocation As DataGridViewTextBoxColumn
    Friend WithEvents TruckNo As DataGridViewTextBoxColumn
    Friend WithEvents Driver As DataGridViewTextBoxColumn
    Friend WithEvents Status As DataGridViewTextBoxColumn
    Friend WithEvents SendDate As DataGridViewTextBoxColumn
    Friend WithEvents ReceivedDate As DataGridViewTextBoxColumn
    Friend WithEvents TotalCase As DataGridViewTextBoxColumn
    Friend WithEvents MovementFromUser As DataGridViewTextBoxColumn
    Friend WithEvents CreateButton As Button
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents RemoveButton As Button
    Friend WithEvents RefreshButton As Button
    Friend WithEvents StartToScanButton As Button
    Friend WithEvents SettingButton As Button
    Friend WithEvents BackButton As Button
    Friend WithEvents EditButton As Button
End Class

﻿Public Class MovementSetting
    Inherits MetroFramework.Forms.MetroForm

    Private Sub TruckMetroTile_Click(sender As Object, e As EventArgs) Handles TruckMetroTile.Click
        Dim window = New TruckList
        window.ShowDialog()
    End Sub

    Private Sub TruckDriverMetroTile_Click(sender As Object, e As EventArgs) Handles TruckDriverMetroTile.Click
        Dim window = New TruckDriverList
        window.ShowDialog()
    End Sub
End Class
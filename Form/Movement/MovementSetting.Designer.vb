﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MovementSetting
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TruckMetroTile = New MetroFramework.Controls.MetroTile()
        Me.TruckDriverMetroTile = New MetroFramework.Controls.MetroTile()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.TruckMetroTile)
        Me.FlowLayoutPanel1.Controls.Add(Me.TruckDriverMetroTile)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(23, 76)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(224, 103)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'TruckMetroTile
        '
        Me.TruckMetroTile.ActiveControl = Nothing
        Me.TruckMetroTile.Location = New System.Drawing.Point(3, 3)
        Me.TruckMetroTile.Name = "TruckMetroTile"
        Me.TruckMetroTile.Size = New System.Drawing.Size(106, 97)
        Me.TruckMetroTile.Style = MetroFramework.MetroColorStyle.Green
        Me.TruckMetroTile.TabIndex = 0
        Me.TruckMetroTile.Text = "Truck"
        Me.TruckMetroTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.TruckMetroTile.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.icons8_truck_24
        Me.TruckMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.TruckMetroTile.UseSelectable = True
        Me.TruckMetroTile.UseTileImage = True
        '
        'TruckDriverMetroTile
        '
        Me.TruckDriverMetroTile.ActiveControl = Nothing
        Me.TruckDriverMetroTile.Location = New System.Drawing.Point(115, 3)
        Me.TruckDriverMetroTile.Name = "TruckDriverMetroTile"
        Me.TruckDriverMetroTile.Size = New System.Drawing.Size(106, 97)
        Me.TruckDriverMetroTile.Style = MetroFramework.MetroColorStyle.Brown
        Me.TruckDriverMetroTile.TabIndex = 1
        Me.TruckDriverMetroTile.Text = "Truck Driver"
        Me.TruckDriverMetroTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.TruckDriverMetroTile.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckDriverMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.icons8_driver_24
        Me.TruckDriverMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.TruckDriverMetroTile.UseSelectable = True
        Me.TruckDriverMetroTile.UseTileImage = True
        '
        'MovementSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(464, 282)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "MovementSetting"
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "Movement Setting"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents TruckMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents TruckDriverMetroTile As MetroFramework.Controls.MetroTile
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TruckDriverList
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.AddMetroButton = New MetroFramework.Controls.MetroButton()
        Me.IsActiveMetroCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.TruckDriverMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.PersonID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Prefix = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FirstName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LastName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActiveStatus = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.TotalRecordMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.TruckDriverMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.TruckDriverMetroGrid)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(760, 434)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.Controls.Add(Me.AddMetroButton)
        Me.FlowLayoutPanel2.Controls.Add(Me.IsActiveMetroCheckBox)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(211, 36)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'AddMetroButton
        '
        Me.AddMetroButton.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.AddMetroButton.FontWeight = MetroFramework.MetroButtonWeight.Light
        Me.AddMetroButton.Location = New System.Drawing.Point(3, 3)
        Me.AddMetroButton.Name = "AddMetroButton"
        Me.AddMetroButton.Size = New System.Drawing.Size(109, 30)
        Me.AddMetroButton.Style = MetroFramework.MetroColorStyle.Green
        Me.AddMetroButton.TabIndex = 4
        Me.AddMetroButton.Text = "เพิ่มรายชื่อพนักงาน"
        Me.AddMetroButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.AddMetroButton.UseSelectable = True
        '
        'IsActiveMetroCheckBox
        '
        Me.IsActiveMetroCheckBox.AutoSize = True
        Me.IsActiveMetroCheckBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.IsActiveMetroCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.IsActiveMetroCheckBox.Location = New System.Drawing.Point(118, 3)
        Me.IsActiveMetroCheckBox.Name = "IsActiveMetroCheckBox"
        Me.IsActiveMetroCheckBox.Size = New System.Drawing.Size(90, 30)
        Me.IsActiveMetroCheckBox.Style = MetroFramework.MetroColorStyle.Green
        Me.IsActiveMetroCheckBox.TabIndex = 5
        Me.IsActiveMetroCheckBox.Text = "แสดงทั้งหมด"
        Me.IsActiveMetroCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.IsActiveMetroCheckBox.Theme = MetroFramework.MetroThemeStyle.Light
        Me.IsActiveMetroCheckBox.UseSelectable = True
        Me.IsActiveMetroCheckBox.UseStyleColors = True
        '
        'TruckDriverMetroGrid
        '
        Me.TruckDriverMetroGrid.AllowUserToAddRows = False
        Me.TruckDriverMetroGrid.AllowUserToDeleteRows = False
        Me.TruckDriverMetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TruckDriverMetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.TruckDriverMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TruckDriverMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TruckDriverMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.TruckDriverMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(89, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TruckDriverMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.TruckDriverMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TruckDriverMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PersonID, Me.Prefix, Me.FirstName, Me.LastName, Me.ActiveStatus})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TruckDriverMetroGrid.DefaultCellStyle = DataGridViewCellStyle3
        Me.TruckDriverMetroGrid.EnableHeadersVisualStyles = False
        Me.TruckDriverMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.TruckDriverMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TruckDriverMetroGrid.Location = New System.Drawing.Point(3, 45)
        Me.TruckDriverMetroGrid.Name = "TruckDriverMetroGrid"
        Me.TruckDriverMetroGrid.ReadOnly = True
        Me.TruckDriverMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(89, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TruckDriverMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.TruckDriverMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.TruckDriverMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.TruckDriverMetroGrid.Size = New System.Drawing.Size(754, 355)
        Me.TruckDriverMetroGrid.Style = MetroFramework.MetroColorStyle.Green
        Me.TruckDriverMetroGrid.TabIndex = 3
        Me.TruckDriverMetroGrid.Theme = MetroFramework.MetroThemeStyle.Light
        '
        'PersonID
        '
        Me.PersonID.DataPropertyName = "PersonID"
        Me.PersonID.HeaderText = "PersonID"
        Me.PersonID.Name = "PersonID"
        Me.PersonID.ReadOnly = True
        '
        'Prefix
        '
        Me.Prefix.DataPropertyName = "Prefix"
        Me.Prefix.HeaderText = "Prefix"
        Me.Prefix.Name = "Prefix"
        Me.Prefix.ReadOnly = True
        '
        'FirstName
        '
        Me.FirstName.DataPropertyName = "FirstName"
        Me.FirstName.HeaderText = "FirstName"
        Me.FirstName.Name = "FirstName"
        Me.FirstName.ReadOnly = True
        '
        'LastName
        '
        Me.LastName.DataPropertyName = "LastName"
        Me.LastName.HeaderText = "LastName"
        Me.LastName.Name = "LastName"
        Me.LastName.ReadOnly = True
        '
        'ActiveStatus
        '
        Me.ActiveStatus.DataPropertyName = "ActiveStatus"
        Me.ActiveStatus.HeaderText = "ActiveStatus"
        Me.ActiveStatus.Name = "ActiveStatus"
        Me.ActiveStatus.ReadOnly = True
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel4.Controls.Add(Me.TotalRecordMetroLabel)
        Me.FlowLayoutPanel4.Controls.Add(Me.MetroLabel3)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 406)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(131, 19)
        Me.FlowLayoutPanel4.TabIndex = 4
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(29, 19)
        Me.MetroLabel1.TabIndex = 1
        Me.MetroLabel1.Text = "รวม"
        '
        'TotalRecordMetroLabel
        '
        Me.TotalRecordMetroLabel.AutoSize = True
        Me.TotalRecordMetroLabel.Location = New System.Drawing.Point(38, 0)
        Me.TotalRecordMetroLabel.Name = "TotalRecordMetroLabel"
        Me.TotalRecordMetroLabel.Size = New System.Drawing.Size(36, 19)
        Me.TotalRecordMetroLabel.TabIndex = 2
        Me.TotalRecordMetroLabel.Text = "x,xxx"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(80, 0)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(48, 19)
        Me.MetroLabel3.TabIndex = 3
        Me.MetroLabel3.Text = "รายการ"
        '
        'TruckDriverList
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(800, 514)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "TruckDriverList"
        Me.Style = MetroFramework.MetroColorStyle.Green
        Me.Text = "จัดการข้อมูลพนักงานขับรถบรรทุก"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        CType(Me.TruckDriverMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents TruckDriverMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalRecordMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents AddMetroButton As MetroFramework.Controls.MetroButton
    Friend WithEvents IsActiveMetroCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents PersonID As DataGridViewTextBoxColumn
    Friend WithEvents Prefix As DataGridViewTextBoxColumn
    Friend WithEvents FirstName As DataGridViewTextBoxColumn
    Friend WithEvents LastName As DataGridViewTextBoxColumn
    Friend WithEvents ActiveStatus As DataGridViewCheckBoxColumn
End Class

﻿Imports DomainModelHris
Imports FactoryBL
Imports FactoryBL.Helper
Imports FactoryBL.Model
Imports FastMember
Imports HRISSystemBL.BL.HRISBL

Public Class AddTruckDriver
    Inherits MetroFramework.Forms.MetroForm


    Private _truckDriverFromHRList As List(Of m_TruckDriver)
    Public Property TruckDriverFromHRList() As List(Of m_TruckDriver)
        Get
            Return _truckDriverFromHRList
        End Get
        Set(ByVal value As List(Of m_TruckDriver))
            _truckDriverFromHRList = value
        End Set
    End Property

    Private _totalRecord As Int16
    Public Property TotalRecord() As Int16
        Get
            Return _totalRecord
        End Get
        Set(ByVal value As Int16)
            _totalRecord = value
        End Set
    End Property

#Region "Functions"
    Private Sub TruckDriverListBinding()
        Try
            _truckDriverFromHRList = FactoryBL.Helper.h_TruckDriver.GetByDistinct()
            _totalRecord = _truckDriverFromHRList.Count()

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_truckDriverFromHRList)
            dt.Load(reader)

            HRTruckDriverMetroGrid.AutoGenerateColumns = False
            HRTruckDriverMetroGrid.DataSource = dt
            TotalRecordMetroLabel.Text = _totalRecord.ToString("N0")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub AddTruckDriver_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TruckDriverListBinding()
    End Sub

    Private Sub HRTruckDriverMetroGrid_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles HRTruckDriverMetroGrid.CellClick
        Try
            If e.RowIndex = -1 Then
                Return
            End If

            If MessageBoxModule.Question("ท่านต้องการเพิ่มพนักงานรายนี้ในรายการตัวเลือกพนักงานขับรถบรรทุกใช่หรือไม่?") = MsgBoxResult.No Then
                Return
            End If

            Dim row = HRTruckDriverMetroGrid.Rows(e.RowIndex)
            Dim employeeID = row.Cells(0).Value

            Facade.ShippingTruckDriverBL().Add(employeeID, XUsername)
            TruckDriverListBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
#End Region
End Class
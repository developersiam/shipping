﻿Imports System.Runtime.Remoting.Channels
Imports FactoryBL
Imports FactoryBL.Model
Imports FastMember
Imports HRISSystemBL
Imports MetroFramework.Controls

Public Class TruckDriverList
    Inherits MetroFramework.Forms.MetroForm

#Region "Properties"
    Private _totalRecord As Int16
    Public Property TotalRecord() As Int16
        Get
            Return _totalRecord
        End Get
        Set(ByVal value As Int16)
            _totalRecord = value
        End Set
    End Property

    Private _truckDrivers As List(Of m_TruckDriver)
    Public Property TruckDrivers() As List(Of m_TruckDriver)
        Get
            Return _truckDrivers
        End Get
        Set(ByVal value As List(Of m_TruckDriver))
            _truckDrivers = value
        End Set
    End Property

    Private _isActive As Boolean
    Public Property IsActive() As Boolean
        Get
            Return _isActive
        End Get
        Set(ByVal value As Boolean)
            _isActive = value
        End Set
    End Property
#End Region


#Region "Function"
    Sub TruckDriversBinding()
        Try
            _truckDrivers = Helper.h_TruckDriver.GetAll()
            If _isActive = False Then
                _truckDrivers = _truckDrivers.Where(Function(x) x.ActiveStatus = True).OrderBy(Function(x) x.FirstName).ToList()
            End If
            _totalRecord = _truckDrivers.Count()

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_truckDrivers)
            dt.Load(reader)

            TruckDriverMetroGrid.AutoGenerateColumns = False
            TruckDriverMetroGrid.DataSource = dt
            TotalRecordMetroLabel.Text = _totalRecord.ToString("N0")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub IsActiveCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles IsActiveMetroCheckBox.CheckedChanged
        _isActive = IsActiveMetroCheckBox.Checked
        TruckDriversBinding()
    End Sub

    Private Sub TruckDriverList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _isActive = False
        IsActiveMetroCheckBox.Checked = _isActive
        TruckDriversBinding()
    End Sub

    Private Sub TruckDriverMetroGrid_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles TruckDriverMetroGrid.CellClick
        Try
            If e.RowIndex = -1 Then
                Return
            End If

            If e.ColumnIndex <> 4 Then
                Return
            End If

            If MessageBoxModule.Question("ท่านต้องการเปลี่ยนสถานะพนักงานขับรถบรรทุกรายนี้ใช่หรือไม่?") = MsgBoxResult.No Then
                Return
            End If

            Dim row = TruckDriverMetroGrid.Rows(e.RowIndex)
            Dim personID = row.Cells(0).Value
            Dim status = row.Cells(4).Value

            Facade.ShippingTruckDriverBL().SetActiveStatus(personID, Not status, XUsername)

            TruckDriversBinding()
            MessageBoxModule.Info("บันทึกข้อมูลสำเร็จ")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub AddMetroButton_Click(sender As Object, e As EventArgs) Handles AddMetroButton.Click
        Try
            Dim window = New AddTruckDriver
            window.ShowDialog()
            TruckDriversBinding()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

#End Region
End Class
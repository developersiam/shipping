﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmShowPackedGrade
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.TransferGradeTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cases = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductionType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToCustomer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FormDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetdefDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Net = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELPackedgradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.CropComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.CropFromPackedGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.CropFromPackedGradeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.CropFromPackedGradeTableAdapter()
        Me.TypeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.typeTableAdapter()
        Me.Sp_Shipping_SEL_PackedgradeTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeTableAdapter()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELPackedgradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CropFromPackedGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.TransferGradeTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(1, 46)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1269, 62)
        Me.MetroPanel1.TabIndex = 187
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'TransferGradeTile
        '
        Me.TransferGradeTile.ActiveControl = Nothing
        Me.TransferGradeTile.AutoSize = True
        Me.TransferGradeTile.BackColor = System.Drawing.Color.White
        Me.TransferGradeTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TransferGradeTile.Location = New System.Drawing.Point(840, 17)
        Me.TransferGradeTile.Name = "TransferGradeTile"
        Me.TransferGradeTile.Size = New System.Drawing.Size(36, 35)
        Me.TransferGradeTile.Style = MetroFramework.MetroColorStyle.White
        Me.TransferGradeTile.TabIndex = 139
        Me.TransferGradeTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.TransferGradeTile.TileImage = Global.ShippingSystem.My.Resources.Resources.TopicMoved32
        Me.TransferGradeTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.TransferGradeTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.TransferGradeTile.UseSelectable = True
        Me.TransferGradeTile.UseTileImage = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(882, 23)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(96, 20)
        Me.MetroLabel4.TabIndex = 138
        Me.MetroLabel4.Text = "transfer grade"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(1002, 13)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 126
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(1044, 23)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel1.TabIndex = 118
        Me.MetroLabel1.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1189, 7)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 23)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Cases, Me.ProductionType, Me.ToCustomer, Me.FormDataGridViewTextBoxColumn, Me.NetdefDataGridViewTextBoxColumn, Me.Net})
        Me.MetroGrid.DataSource = Me.SpShippingSELPackedgradeBindingSource
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle7
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(1, 157)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 28
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(1269, 558)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 197
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "graders"
        Me.Column1.HeaderText = "Current Grade"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 157
        '
        'Cases
        '
        Me.Cases.DataPropertyName = "Cases"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Cases.DefaultCellStyle = DataGridViewCellStyle3
        Me.Cases.HeaderText = "Cases"
        Me.Cases.Name = "Cases"
        Me.Cases.ReadOnly = True
        Me.Cases.Width = 84
        '
        'ProductionType
        '
        Me.ProductionType.DataPropertyName = "ProductionType"
        Me.ProductionType.HeaderText = "ProductionType"
        Me.ProductionType.Name = "ProductionType"
        Me.ProductionType.ReadOnly = True
        Me.ProductionType.Width = 167
        '
        'ToCustomer
        '
        Me.ToCustomer.DataPropertyName = "ToCustomer"
        Me.ToCustomer.HeaderText = "ToCustomer"
        Me.ToCustomer.Name = "ToCustomer"
        Me.ToCustomer.ReadOnly = True
        Me.ToCustomer.Width = 137
        '
        'FormDataGridViewTextBoxColumn
        '
        Me.FormDataGridViewTextBoxColumn.DataPropertyName = "form"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.FormDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.FormDataGridViewTextBoxColumn.HeaderText = "Form"
        Me.FormDataGridViewTextBoxColumn.Name = "FormDataGridViewTextBoxColumn"
        Me.FormDataGridViewTextBoxColumn.ReadOnly = True
        Me.FormDataGridViewTextBoxColumn.Width = 80
        '
        'NetdefDataGridViewTextBoxColumn
        '
        Me.NetdefDataGridViewTextBoxColumn.DataPropertyName = "netdef"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.NetdefDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.NetdefDataGridViewTextBoxColumn.HeaderText = "Net def."
        Me.NetdefDataGridViewTextBoxColumn.Name = "NetdefDataGridViewTextBoxColumn"
        Me.NetdefDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetdefDataGridViewTextBoxColumn.Width = 103
        '
        'Net
        '
        Me.Net.DataPropertyName = "Net"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.Net.DefaultCellStyle = DataGridViewCellStyle6
        Me.Net.HeaderText = "Net"
        Me.Net.Name = "Net"
        Me.Net.ReadOnly = True
        Me.Net.Width = 67
        '
        'SpShippingSELPackedgradeBindingSource
        '
        Me.SpShippingSELPackedgradeBindingSource.DataMember = "sp_Shipping_SEL_Packedgrade"
        Me.SpShippingSELPackedgradeBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CropComboBox
        '
        Me.CropComboBox.DataSource = Me.CropFromPackedGradeBindingSource
        Me.CropComboBox.DisplayMember = "crop"
        Me.CropComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.CropComboBox.FormattingEnabled = True
        Me.CropComboBox.ItemHeight = 29
        Me.CropComboBox.Location = New System.Drawing.Point(47, 116)
        Me.CropComboBox.Name = "CropComboBox"
        Me.CropComboBox.Size = New System.Drawing.Size(120, 35)
        Me.CropComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropComboBox.TabIndex = 221
        Me.CropComboBox.UseSelectable = True
        Me.CropComboBox.ValueMember = "crop"
        '
        'CropFromPackedGradeBindingSource
        '
        Me.CropFromPackedGradeBindingSource.DataMember = "CropFromPackedGrade"
        Me.CropFromPackedGradeBindingSource.DataSource = Me.ShippingDataSet
        '
        'TypeComboBox
        '
        Me.TypeComboBox.DataSource = Me.TypeBindingSource
        Me.TypeComboBox.DisplayMember = "type"
        Me.TypeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 29
        Me.TypeComboBox.Location = New System.Drawing.Point(242, 114)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(101, 35)
        Me.TypeComboBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.TypeComboBox.TabIndex = 222
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type"
        '
        'TypeBindingSource
        '
        Me.TypeBindingSource.DataMember = "type"
        Me.TypeBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(197, 129)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel5.TabIndex = 224
        Me.MetroLabel5.Text = "Type"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(2, 129)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel2.TabIndex = 223
        Me.MetroLabel2.Text = "Crop"
        '
        'CropFromPackedGradeTableAdapter
        '
        Me.CropFromPackedGradeTableAdapter.ClearBeforeFill = True
        '
        'TypeTableAdapter
        '
        Me.TypeTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_PackedgradeTableAdapter
        '
        Me.Sp_Shipping_SEL_PackedgradeTableAdapter.ClearBeforeFill = True
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ShippingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Me.TypeTableAdapter
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'FrmShowPackedGrade
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1418, 775)
        Me.Controls.Add(Me.CropComboBox)
        Me.Controls.Add(Me.TypeComboBox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmShowPackedGrade"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELPackedgradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CropFromPackedGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents CropComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents CropFromPackedGradeBindingSource As BindingSource
    Friend WithEvents CropFromPackedGradeTableAdapter As ShippingDataSetTableAdapters.CropFromPackedGradeTableAdapter
    Friend WithEvents TypeBindingSource As BindingSource
    Friend WithEvents TypeTableAdapter As ShippingDataSetTableAdapters.typeTableAdapter
    Friend WithEvents SpShippingSELPackedgradeBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PackedgradeTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PackedgradeTableAdapter
    Friend WithEvents TransferGradeTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Cases As DataGridViewTextBoxColumn
    Friend WithEvents ProductionType As DataGridViewTextBoxColumn
    Friend WithEvents ToCustomer As DataGridViewTextBoxColumn
    Friend WithEvents FormDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetdefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Net As DataGridViewTextBoxColumn
    Friend WithEvents SecurityBindingSource As BindingSource
    Friend WithEvents SecurityTableAdapter As ShippingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
End Class

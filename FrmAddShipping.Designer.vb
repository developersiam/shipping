﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAddShipping
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.DeleteTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.UnFinishedTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.FinishedTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.SaveTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.OrderNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.CBalesFromMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.FromBayComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpShippingSELShippingBayFromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroGridFrom = New MetroFramework.Controls.MetroGrid()
        Me.BCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CASENODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetdefDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetrealDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.customerrs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.customercaseno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELFromBayFromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingBayFromTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingBayFromTableAdapter()
        Me.Sp_Shipping_SEL_FromBayFromTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayFromTableAdapter()
        Me.CropLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.StatusLabel = New MetroFramework.Controls.MetroLabel()
        Me.CustomerLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel17 = New MetroFramework.Controls.MetroLabel()
        Me.ContainerLabel = New MetroFramework.Controls.MetroLabel()
        Me.CaseLabel = New MetroFramework.Controls.MetroLabel()
        Me.WeightLabel = New MetroFramework.Controls.MetroLabel()
        Me.CustomerGradeLabel = New MetroFramework.Controls.MetroLabel()
        Me.PackedGradeLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel15 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.ShippedDate = New MetroFramework.Controls.MetroDateTime()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.SpNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_GETMAX_PdSPBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_GETMAX_PdSPTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_GETMAX_PdSPTableAdapter()
        Me.MetroGrid1 = New MetroFramework.Controls.MetroGrid()
        Me.SPNODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CASESDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ISSUED = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.SpShippingSELShippedByOrderNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MetroLabel16 = New MetroFramework.Controls.MetroLabel()
        Me.TotalShippedCasesMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.InfoTile = New MetroFramework.Controls.MetroTile()
        Me.Sp_Shipping_SEL_ShippedByOrderNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippedByOrderNoTableAdapter()
        Me.MetroGrid2 = New MetroFramework.Controls.MetroGrid()
        Me.BCDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GradeDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CASENODataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetdefDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetrealDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerrsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomercasenoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cusrunno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.caserunno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullMigration = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.FullMigrationDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpShippingSELFromBayAllIssuedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.CustCaseTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.Sp_Shipping_SEL_PdSpBySpNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_SEL_PdSpBySpNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_PdSpBySpNoTableAdapter()
        Me.Sp_Shipping_SEL_FromBayAllIssuedTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayAllIssuedTableAdapter()
        Me.SelectAllCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.Sp_Shipping_GETMAX_TransferNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Shipping_GETMAX_TransferNoTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_GETMAX_TransferNoTableAdapter()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.SpShippingSELShippingBayFromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MetroGridFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELFromBayFromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_GETMAX_PdSPBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MetroGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELShippedByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.MetroGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELFromBayAllIssuedBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_PdSpBySpNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_GETMAX_TransferNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.DeleteTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel8)
        Me.MetroPanel1.Controls.Add(Me.UnFinishedTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel12)
        Me.MetroPanel1.Controls.Add(Me.FinishedTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.SaveTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 54)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1269, 51)
        Me.MetroPanel1.TabIndex = 188
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'DeleteTile
        '
        Me.DeleteTile.ActiveControl = Nothing
        Me.DeleteTile.AutoSize = True
        Me.DeleteTile.BackColor = System.Drawing.Color.White
        Me.DeleteTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteTile.Location = New System.Drawing.Point(577, 10)
        Me.DeleteTile.Name = "DeleteTile"
        Me.DeleteTile.Size = New System.Drawing.Size(36, 35)
        Me.DeleteTile.Style = MetroFramework.MetroColorStyle.White
        Me.DeleteTile.TabIndex = 143
        Me.DeleteTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DeleteTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Cancel32
        Me.DeleteTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DeleteTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.DeleteTile.UseSelectable = True
        Me.DeleteTile.UseTileImage = True
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(619, 18)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(48, 20)
        Me.MetroLabel8.TabIndex = 142
        Me.MetroLabel8.Text = "delete"
        '
        'UnFinishedTile
        '
        Me.UnFinishedTile.ActiveControl = Nothing
        Me.UnFinishedTile.AutoSize = True
        Me.UnFinishedTile.BackColor = System.Drawing.Color.White
        Me.UnFinishedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.UnFinishedTile.Location = New System.Drawing.Point(852, 12)
        Me.UnFinishedTile.Name = "UnFinishedTile"
        Me.UnFinishedTile.Size = New System.Drawing.Size(36, 35)
        Me.UnFinishedTile.Style = MetroFramework.MetroColorStyle.White
        Me.UnFinishedTile.TabIndex = 141
        Me.UnFinishedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnFinishedTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Unlock32
        Me.UnFinishedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnFinishedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.UnFinishedTile.UseSelectable = True
        Me.UnFinishedTile.UseTileImage = True
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(894, 18)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(73, 20)
        Me.MetroLabel12.TabIndex = 140
        Me.MetroLabel12.Text = "unfinished"
        '
        'FinishedTile
        '
        Me.FinishedTile.ActiveControl = Nothing
        Me.FinishedTile.AutoSize = True
        Me.FinishedTile.BackColor = System.Drawing.Color.White
        Me.FinishedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FinishedTile.Location = New System.Drawing.Point(707, 12)
        Me.FinishedTile.Name = "FinishedTile"
        Me.FinishedTile.Size = New System.Drawing.Size(36, 35)
        Me.FinishedTile.Style = MetroFramework.MetroColorStyle.White
        Me.FinishedTile.TabIndex = 139
        Me.FinishedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FinishedTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Lock32
        Me.FinishedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FinishedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FinishedTile.UseSelectable = True
        Me.FinishedTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(749, 18)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(57, 20)
        Me.MetroLabel6.TabIndex = 138
        Me.MetroLabel6.Text = "finished"
        '
        'SaveTile
        '
        Me.SaveTile.ActiveControl = Nothing
        Me.SaveTile.AutoSize = True
        Me.SaveTile.BackColor = System.Drawing.Color.White
        Me.SaveTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveTile.Location = New System.Drawing.Point(457, 12)
        Me.SaveTile.Name = "SaveTile"
        Me.SaveTile.Size = New System.Drawing.Size(36, 35)
        Me.SaveTile.Style = MetroFramework.MetroColorStyle.White
        Me.SaveTile.TabIndex = 135
        Me.SaveTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Save32
        Me.SaveTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SaveTile.UseSelectable = True
        Me.SaveTile.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(499, 18)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(37, 20)
        Me.MetroLabel2.TabIndex = 134
        Me.MetroLabel2.Text = "save"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(1002, 8)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 126
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(1044, 18)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel1.TabIndex = 118
        Me.MetroLabel1.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1189, 2)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 115
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(49, 44)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(58, 17)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel3.Location = New System.Drawing.Point(38, 35)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(63, 17)
        Me.MetroLabel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel3.TabIndex = 133
        Me.MetroLabel3.Text = "Order No."
        Me.MetroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel3.UseCustomForeColor = True
        '
        'OrderNoLabel
        '
        Me.OrderNoLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.OrderNoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.OrderNoLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.OrderNoLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.OrderNoLabel.Location = New System.Drawing.Point(111, 19)
        Me.OrderNoLabel.Name = "OrderNoLabel"
        Me.OrderNoLabel.Size = New System.Drawing.Size(85, 35)
        Me.OrderNoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.OrderNoLabel.TabIndex = 132
        Me.OrderNoLabel.Text = "Username"
        Me.OrderNoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.OrderNoLabel.UseCustomBackColor = True
        Me.OrderNoLabel.UseCustomForeColor = True
        '
        'CBalesFromMetroLabel
        '
        Me.CBalesFromMetroLabel.AutoSize = True
        Me.CBalesFromMetroLabel.BackColor = System.Drawing.Color.White
        Me.CBalesFromMetroLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CBalesFromMetroLabel.Location = New System.Drawing.Point(600, 124)
        Me.CBalesFromMetroLabel.Name = "CBalesFromMetroLabel"
        Me.CBalesFromMetroLabel.Size = New System.Drawing.Size(50, 20)
        Me.CBalesFromMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CBalesFromMetroLabel.TabIndex = 207
        Me.CBalesFromMetroLabel.Text = "CBales"
        Me.CBalesFromMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBalesFromMetroLabel.UseCustomBackColor = True
        Me.CBalesFromMetroLabel.UseCustomForeColor = True
        '
        'FromBayComboBox
        '
        Me.FromBayComboBox.DataSource = Me.SpShippingSELShippingBayFromBindingSource
        Me.FromBayComboBox.DisplayMember = "BAY"
        Me.FromBayComboBox.FormattingEnabled = True
        Me.FromBayComboBox.ItemHeight = 24
        Me.FromBayComboBox.Location = New System.Drawing.Point(428, 114)
        Me.FromBayComboBox.Name = "FromBayComboBox"
        Me.FromBayComboBox.Size = New System.Drawing.Size(149, 30)
        Me.FromBayComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.FromBayComboBox.TabIndex = 206
        Me.FromBayComboBox.UseSelectable = True
        Me.FromBayComboBox.ValueMember = "BAY"
        '
        'SpShippingSELShippingBayFromBindingSource
        '
        Me.SpShippingSELShippingBayFromBindingSource.DataMember = "sp_Shipping_SEL_ShippingBayFrom"
        Me.SpShippingSELShippingBayFromBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(335, 124)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(67, 20)
        Me.MetroLabel4.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel4.TabIndex = 205
        Me.MetroLabel4.Text = "From Bay"
        '
        'MetroGridFrom
        '
        Me.MetroGridFrom.AllowUserToAddRows = False
        Me.MetroGridFrom.AllowUserToDeleteRows = False
        Me.MetroGridFrom.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGridFrom.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGridFrom.AutoGenerateColumns = False
        Me.MetroGridFrom.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGridFrom.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGridFrom.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGridFrom.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGridFrom.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGridFrom.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGridFrom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGridFrom.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BCDataGridViewTextBoxColumn, Me.GradeDataGridViewTextBoxColumn, Me.CASENODataGridViewTextBoxColumn, Me.NetdefDataGridViewTextBoxColumn, Me.NetrealDataGridViewTextBoxColumn, Me.MovementNo, Me.customerrs, Me.customercaseno, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn1})
        Me.MetroGridFrom.DataSource = Me.SpShippingSELFromBayFromBindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGridFrom.DefaultCellStyle = DataGridViewCellStyle6
        Me.MetroGridFrom.EnableHeadersVisualStyles = False
        Me.MetroGridFrom.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGridFrom.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGridFrom.Location = New System.Drawing.Point(326, 152)
        Me.MetroGridFrom.Name = "MetroGridFrom"
        Me.MetroGridFrom.ReadOnly = True
        Me.MetroGridFrom.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGridFrom.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.MetroGridFrom.RowHeadersWidth = 51
        Me.MetroGridFrom.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGridFrom.RowTemplate.Height = 28
        Me.MetroGridFrom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGridFrom.Size = New System.Drawing.Size(946, 601)
        Me.MetroGridFrom.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGridFrom.TabIndex = 204
        Me.MetroGridFrom.UseCustomBackColor = True
        Me.MetroGridFrom.UseCustomForeColor = True
        Me.MetroGridFrom.UseStyleColors = True
        '
        'BCDataGridViewTextBoxColumn
        '
        Me.BCDataGridViewTextBoxColumn.DataPropertyName = "BC"
        Me.BCDataGridViewTextBoxColumn.HeaderText = "Barcode"
        Me.BCDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.BCDataGridViewTextBoxColumn.Name = "BCDataGridViewTextBoxColumn"
        Me.BCDataGridViewTextBoxColumn.ReadOnly = True
        Me.BCDataGridViewTextBoxColumn.Width = 105
        '
        'GradeDataGridViewTextBoxColumn
        '
        Me.GradeDataGridViewTextBoxColumn.DataPropertyName = "grade"
        Me.GradeDataGridViewTextBoxColumn.HeaderText = "grade"
        Me.GradeDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.GradeDataGridViewTextBoxColumn.Name = "GradeDataGridViewTextBoxColumn"
        Me.GradeDataGridViewTextBoxColumn.ReadOnly = True
        Me.GradeDataGridViewTextBoxColumn.Width = 84
        '
        'CASENODataGridViewTextBoxColumn
        '
        Me.CASENODataGridViewTextBoxColumn.DataPropertyName = "CASENO"
        Me.CASENODataGridViewTextBoxColumn.HeaderText = "CASENO"
        Me.CASENODataGridViewTextBoxColumn.MinimumWidth = 6
        Me.CASENODataGridViewTextBoxColumn.Name = "CASENODataGridViewTextBoxColumn"
        Me.CASENODataGridViewTextBoxColumn.ReadOnly = True
        Me.CASENODataGridViewTextBoxColumn.Width = 105
        '
        'NetdefDataGridViewTextBoxColumn
        '
        Me.NetdefDataGridViewTextBoxColumn.DataPropertyName = "netdef"
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.NetdefDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.NetdefDataGridViewTextBoxColumn.HeaderText = "Net"
        Me.NetdefDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.NetdefDataGridViewTextBoxColumn.Name = "NetdefDataGridViewTextBoxColumn"
        Me.NetdefDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetdefDataGridViewTextBoxColumn.Width = 67
        '
        'NetrealDataGridViewTextBoxColumn
        '
        Me.NetrealDataGridViewTextBoxColumn.DataPropertyName = "netreal"
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.NetrealDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.NetrealDataGridViewTextBoxColumn.HeaderText = "netreal"
        Me.NetrealDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.NetrealDataGridViewTextBoxColumn.Name = "NetrealDataGridViewTextBoxColumn"
        Me.NetrealDataGridViewTextBoxColumn.ReadOnly = True
        Me.NetrealDataGridViewTextBoxColumn.Visible = False
        Me.NetrealDataGridViewTextBoxColumn.Width = 97
        '
        'MovementNo
        '
        Me.MovementNo.DataPropertyName = "MovementNo"
        Me.MovementNo.HeaderText = "Move No."
        Me.MovementNo.MinimumWidth = 6
        Me.MovementNo.Name = "MovementNo"
        Me.MovementNo.ReadOnly = True
        Me.MovementNo.Width = 118
        '
        'customerrs
        '
        Me.customerrs.DataPropertyName = "customerrs"
        Me.customerrs.HeaderText = "customerrs"
        Me.customerrs.MinimumWidth = 6
        Me.customerrs.Name = "customerrs"
        Me.customerrs.ReadOnly = True
        Me.customerrs.Width = 130
        '
        'customercaseno
        '
        Me.customercaseno.DataPropertyName = "customercaseno"
        Me.customercaseno.HeaderText = "Cust. Case No."
        Me.customercaseno.MinimumWidth = 6
        Me.customercaseno.Name = "customercaseno"
        Me.customercaseno.ReadOnly = True
        Me.customercaseno.Width = 157
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "FullMigration"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "FullMigration"
        Me.DataGridViewCheckBoxColumn1.MinimumWidth = 6
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.ReadOnly = True
        Me.DataGridViewCheckBoxColumn1.Width = 125
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "FullMigrationDate"
        DataGridViewCellStyle5.Format = "d"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn1.HeaderText = "FullMigrationDate"
        Me.DataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 187
        '
        'SpShippingSELFromBayFromBindingSource
        '
        Me.SpShippingSELFromBayFromBindingSource.DataMember = "sp_Shipping_SEL_FromBayFrom"
        Me.SpShippingSELFromBayFromBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingBayFromTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingBayFromTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_FromBayFromTableAdapter
        '
        Me.Sp_Shipping_SEL_FromBayFromTableAdapter.ClearBeforeFill = True
        '
        'CropLabel
        '
        Me.CropLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CropLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CropLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.CropLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CropLabel.Location = New System.Drawing.Point(242, 19)
        Me.CropLabel.Name = "CropLabel"
        Me.CropLabel.Size = New System.Drawing.Size(56, 35)
        Me.CropLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropLabel.TabIndex = 154
        Me.CropLabel.Text = "Username"
        Me.CropLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CropLabel.UseCustomBackColor = True
        Me.CropLabel.UseCustomForeColor = True
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel14.Location = New System.Drawing.Point(202, 35)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(34, 17)
        Me.MetroLabel14.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel14.TabIndex = 153
        Me.MetroLabel14.Text = "Crop"
        Me.MetroLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel14.UseCustomForeColor = True
        '
        'StatusLabel
        '
        Me.StatusLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.StatusLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.StatusLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.StatusLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.StatusLabel.Location = New System.Drawing.Point(213, 288)
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(85, 35)
        Me.StatusLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.StatusLabel.TabIndex = 152
        Me.StatusLabel.Text = "Username"
        Me.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.StatusLabel.UseCustomBackColor = True
        Me.StatusLabel.UseCustomForeColor = True
        '
        'CustomerLabel
        '
        Me.CustomerLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CustomerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CustomerLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.CustomerLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CustomerLabel.Location = New System.Drawing.Point(113, 63)
        Me.CustomerLabel.Name = "CustomerLabel"
        Me.CustomerLabel.Size = New System.Drawing.Size(185, 35)
        Me.CustomerLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerLabel.TabIndex = 150
        Me.CustomerLabel.Text = "Username"
        Me.CustomerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CustomerLabel.UseCustomBackColor = True
        Me.CustomerLabel.UseCustomForeColor = True
        '
        'MetroLabel17
        '
        Me.MetroLabel17.AutoSize = True
        Me.MetroLabel17.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel17.Location = New System.Drawing.Point(40, 81)
        Me.MetroLabel17.Name = "MetroLabel17"
        Me.MetroLabel17.Size = New System.Drawing.Size(61, 17)
        Me.MetroLabel17.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel17.TabIndex = 149
        Me.MetroLabel17.Text = "Customer"
        Me.MetroLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel17.UseCustomForeColor = True
        '
        'ContainerLabel
        '
        Me.ContainerLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ContainerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContainerLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.ContainerLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ContainerLabel.Location = New System.Drawing.Point(113, 288)
        Me.ContainerLabel.Name = "ContainerLabel"
        Me.ContainerLabel.Size = New System.Drawing.Size(91, 35)
        Me.ContainerLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.ContainerLabel.TabIndex = 148
        Me.ContainerLabel.Text = "Username"
        Me.ContainerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ContainerLabel.UseCustomBackColor = True
        Me.ContainerLabel.UseCustomForeColor = True
        '
        'CaseLabel
        '
        Me.CaseLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CaseLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CaseLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.CaseLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CaseLabel.Location = New System.Drawing.Point(113, 197)
        Me.CaseLabel.Name = "CaseLabel"
        Me.CaseLabel.Size = New System.Drawing.Size(185, 35)
        Me.CaseLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CaseLabel.TabIndex = 147
        Me.CaseLabel.Text = "Username"
        Me.CaseLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CaseLabel.UseCustomBackColor = True
        Me.CaseLabel.UseCustomForeColor = True
        '
        'WeightLabel
        '
        Me.WeightLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.WeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.WeightLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.WeightLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.WeightLabel.Location = New System.Drawing.Point(113, 243)
        Me.WeightLabel.Name = "WeightLabel"
        Me.WeightLabel.Size = New System.Drawing.Size(185, 35)
        Me.WeightLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.WeightLabel.TabIndex = 146
        Me.WeightLabel.Text = "Username"
        Me.WeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.WeightLabel.UseCustomBackColor = True
        Me.WeightLabel.UseCustomForeColor = True
        '
        'CustomerGradeLabel
        '
        Me.CustomerGradeLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CustomerGradeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CustomerGradeLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.CustomerGradeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CustomerGradeLabel.Location = New System.Drawing.Point(113, 152)
        Me.CustomerGradeLabel.Name = "CustomerGradeLabel"
        Me.CustomerGradeLabel.Size = New System.Drawing.Size(185, 35)
        Me.CustomerGradeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustomerGradeLabel.TabIndex = 145
        Me.CustomerGradeLabel.Text = "Username"
        Me.CustomerGradeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CustomerGradeLabel.UseCustomBackColor = True
        Me.CustomerGradeLabel.UseCustomForeColor = True
        '
        'PackedGradeLabel
        '
        Me.PackedGradeLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.PackedGradeLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PackedGradeLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.PackedGradeLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.PackedGradeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PackedGradeLabel.Location = New System.Drawing.Point(113, 109)
        Me.PackedGradeLabel.Name = "PackedGradeLabel"
        Me.PackedGradeLabel.Size = New System.Drawing.Size(162, 35)
        Me.PackedGradeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.PackedGradeLabel.TabIndex = 144
        Me.PackedGradeLabel.Text = "Username"
        Me.PackedGradeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.PackedGradeLabel.UseCustomBackColor = True
        Me.PackedGradeLabel.UseCustomForeColor = True
        '
        'MetroLabel15
        '
        Me.MetroLabel15.AutoSize = True
        Me.MetroLabel15.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel15.Location = New System.Drawing.Point(38, 306)
        Me.MetroLabel15.Name = "MetroLabel15"
        Me.MetroLabel15.Size = New System.Drawing.Size(61, 17)
        Me.MetroLabel15.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel15.TabIndex = 143
        Me.MetroLabel15.Text = "Container"
        Me.MetroLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel15.UseCustomForeColor = True
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel13.Location = New System.Drawing.Point(20, 127)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(81, 17)
        Me.MetroLabel13.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel13.TabIndex = 141
        Me.MetroLabel13.Text = "Packed grade"
        Me.MetroLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel13.UseCustomForeColor = True
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel11.Location = New System.Drawing.Point(62, 215)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(39, 17)
        Me.MetroLabel11.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel11.TabIndex = 139
        Me.MetroLabel11.Text = "Cases"
        Me.MetroLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel11.UseCustomForeColor = True
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel9.Location = New System.Drawing.Point(53, 252)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(48, 17)
        Me.MetroLabel9.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel9.TabIndex = 137
        Me.MetroLabel9.Text = "Weight"
        Me.MetroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel9.UseCustomForeColor = True
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel7.Location = New System.Drawing.Point(5, 170)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(96, 17)
        Me.MetroLabel7.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel7.TabIndex = 135
        Me.MetroLabel7.Text = "Customer grade"
        Me.MetroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel7.UseCustomForeColor = True
        '
        'Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource.DataMember = "sp_Shipping_SEL_ShippingOrderByOrderNo"
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ShippedDate
        '
        Me.ShippedDate.FontSize = MetroFramework.MetroDateTimeSize.Small
        Me.ShippedDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ShippedDate.Location = New System.Drawing.Point(60, 22)
        Me.ShippedDate.MinimumSize = New System.Drawing.Size(0, 27)
        Me.ShippedDate.Name = "ShippedDate"
        Me.ShippedDate.Size = New System.Drawing.Size(185, 27)
        Me.ShippedDate.Style = MetroFramework.MetroColorStyle.Lime
        Me.ShippedDate.TabIndex = 203
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel10.Location = New System.Drawing.Point(20, 32)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(34, 17)
        Me.MetroLabel10.TabIndex = 204
        Me.MetroLabel10.Text = "Date"
        Me.MetroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'SpNoLabel
        '
        Me.SpNoLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.SpNoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SpNoLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.SpNoLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.SpNoLabel.ForeColor = System.Drawing.Color.Olive
        Me.SpNoLabel.Location = New System.Drawing.Point(60, 52)
        Me.SpNoLabel.Name = "SpNoLabel"
        Me.SpNoLabel.Size = New System.Drawing.Size(185, 35)
        Me.SpNoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.SpNoLabel.TabIndex = 153
        Me.SpNoLabel.Text = "Username"
        Me.SpNoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SpNoLabel.UseCustomBackColor = True
        Me.SpNoLabel.UseCustomForeColor = True
        '
        'MetroLabel20
        '
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel20.Location = New System.Drawing.Point(10, 70)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(44, 17)
        Me.MetroLabel20.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel20.TabIndex = 154
        Me.MetroLabel20.Text = "SP No."
        Me.MetroLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel20.UseCustomForeColor = True
        '
        'Sp_Shipping_GETMAX_PdSPBindingSource
        '
        Me.Sp_Shipping_GETMAX_PdSPBindingSource.DataMember = "sp_Shipping_GETMAX_PdSP"
        Me.Sp_Shipping_GETMAX_PdSPBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_GETMAX_PdSPTableAdapter
        '
        Me.Sp_Shipping_GETMAX_PdSPTableAdapter.ClearBeforeFill = True
        '
        'MetroGrid1
        '
        Me.MetroGrid1.AllowUserToAddRows = False
        Me.MetroGrid1.AllowUserToDeleteRows = False
        Me.MetroGrid1.AllowUserToResizeRows = False
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.MetroGrid1.AutoGenerateColumns = False
        Me.MetroGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.MetroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SPNODataGridViewTextBoxColumn, Me.BAYDataGridViewTextBoxColumn, Me.CASESDataGridViewTextBoxColumn, Me.ISSUED})
        Me.MetroGrid1.DataSource = Me.SpShippingSELShippedByOrderNoBindingSource
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid1.DefaultCellStyle = DataGridViewCellStyle10
        Me.MetroGrid1.EnableHeadersVisualStyles = False
        Me.MetroGrid1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid1.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid1.Location = New System.Drawing.Point(6, 90)
        Me.MetroGrid1.Name = "MetroGrid1"
        Me.MetroGrid1.ReadOnly = True
        Me.MetroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid1.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.MetroGrid1.RowHeadersWidth = 51
        Me.MetroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid1.RowTemplate.Height = 28
        Me.MetroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid1.Size = New System.Drawing.Size(305, 161)
        Me.MetroGrid1.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid1.TabIndex = 205
        Me.MetroGrid1.UseCustomBackColor = True
        Me.MetroGrid1.UseCustomForeColor = True
        Me.MetroGrid1.UseStyleColors = True
        '
        'SPNODataGridViewTextBoxColumn
        '
        Me.SPNODataGridViewTextBoxColumn.DataPropertyName = "SPNO"
        Me.SPNODataGridViewTextBoxColumn.HeaderText = "SPNO"
        Me.SPNODataGridViewTextBoxColumn.MinimumWidth = 6
        Me.SPNODataGridViewTextBoxColumn.Name = "SPNODataGridViewTextBoxColumn"
        Me.SPNODataGridViewTextBoxColumn.ReadOnly = True
        Me.SPNODataGridViewTextBoxColumn.Width = 70
        '
        'BAYDataGridViewTextBoxColumn
        '
        Me.BAYDataGridViewTextBoxColumn.DataPropertyName = "BAY"
        Me.BAYDataGridViewTextBoxColumn.HeaderText = "BAY"
        Me.BAYDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.BAYDataGridViewTextBoxColumn.Name = "BAYDataGridViewTextBoxColumn"
        Me.BAYDataGridViewTextBoxColumn.ReadOnly = True
        Me.BAYDataGridViewTextBoxColumn.Width = 58
        '
        'CASESDataGridViewTextBoxColumn
        '
        Me.CASESDataGridViewTextBoxColumn.DataPropertyName = "CASES"
        Me.CASESDataGridViewTextBoxColumn.HeaderText = "CASES"
        Me.CASESDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.CASESDataGridViewTextBoxColumn.Name = "CASESDataGridViewTextBoxColumn"
        Me.CASESDataGridViewTextBoxColumn.ReadOnly = True
        Me.CASESDataGridViewTextBoxColumn.Width = 73
        '
        'ISSUED
        '
        Me.ISSUED.DataPropertyName = "ISSUED"
        Me.ISSUED.HeaderText = "ISSUED"
        Me.ISSUED.MinimumWidth = 6
        Me.ISSUED.Name = "ISSUED"
        Me.ISSUED.ReadOnly = True
        Me.ISSUED.Width = 55
        '
        'SpShippingSELShippedByOrderNoBindingSource
        '
        Me.SpShippingSELShippedByOrderNoBindingSource.DataMember = "sp_Shipping_SEL_ShippedByOrderNo"
        Me.SpShippingSELShippedByOrderNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.MetroLabel16)
        Me.GroupBox1.Controls.Add(Me.TotalShippedCasesMetroLabel)
        Me.GroupBox1.Controls.Add(Me.MetroGrid1)
        Me.GroupBox1.Controls.Add(Me.ShippedDate)
        Me.GroupBox1.Controls.Add(Me.MetroLabel20)
        Me.GroupBox1.Controls.Add(Me.MetroLabel10)
        Me.GroupBox1.Controls.Add(Me.SpNoLabel)
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(3, 449)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(317, 304)
        Me.GroupBox1.TabIndex = 211
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Shipped detail"
        '
        'MetroLabel16
        '
        Me.MetroLabel16.AutoSize = True
        Me.MetroLabel16.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel16.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MetroLabel16.Location = New System.Drawing.Point(10, 264)
        Me.MetroLabel16.Name = "MetroLabel16"
        Me.MetroLabel16.Size = New System.Drawing.Size(151, 25)
        Me.MetroLabel16.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel16.TabIndex = 206
        Me.MetroLabel16.Text = "Total of Shipped"
        Me.MetroLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MetroLabel16.UseCustomForeColor = True
        '
        'TotalShippedCasesMetroLabel
        '
        Me.TotalShippedCasesMetroLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TotalShippedCasesMetroLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TotalShippedCasesMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.TotalShippedCasesMetroLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.TotalShippedCasesMetroLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TotalShippedCasesMetroLabel.Location = New System.Drawing.Point(167, 254)
        Me.TotalShippedCasesMetroLabel.Name = "TotalShippedCasesMetroLabel"
        Me.TotalShippedCasesMetroLabel.Size = New System.Drawing.Size(131, 35)
        Me.TotalShippedCasesMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalShippedCasesMetroLabel.TabIndex = 207
        Me.TotalShippedCasesMetroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.TotalShippedCasesMetroLabel.UseCustomBackColor = True
        Me.TotalShippedCasesMetroLabel.UseCustomForeColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.InfoTile)
        Me.GroupBox2.Controls.Add(Me.OrderNoLabel)
        Me.GroupBox2.Controls.Add(Me.MetroLabel3)
        Me.GroupBox2.Controls.Add(Me.CropLabel)
        Me.GroupBox2.Controls.Add(Me.MetroLabel7)
        Me.GroupBox2.Controls.Add(Me.MetroLabel14)
        Me.GroupBox2.Controls.Add(Me.MetroLabel9)
        Me.GroupBox2.Controls.Add(Me.StatusLabel)
        Me.GroupBox2.Controls.Add(Me.MetroLabel11)
        Me.GroupBox2.Controls.Add(Me.MetroLabel13)
        Me.GroupBox2.Controls.Add(Me.CustomerLabel)
        Me.GroupBox2.Controls.Add(Me.MetroLabel15)
        Me.GroupBox2.Controls.Add(Me.MetroLabel17)
        Me.GroupBox2.Controls.Add(Me.PackedGradeLabel)
        Me.GroupBox2.Controls.Add(Me.ContainerLabel)
        Me.GroupBox2.Controls.Add(Me.CustomerGradeLabel)
        Me.GroupBox2.Controls.Add(Me.CaseLabel)
        Me.GroupBox2.Controls.Add(Me.WeightLabel)
        Me.GroupBox2.Font = New System.Drawing.Font("Calibri", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(3, 114)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(317, 329)
        Me.GroupBox2.TabIndex = 212
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Order detail"
        '
        'InfoTile
        '
        Me.InfoTile.ActiveControl = Nothing
        Me.InfoTile.AutoSize = True
        Me.InfoTile.BackColor = System.Drawing.Color.White
        Me.InfoTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.InfoTile.Location = New System.Drawing.Point(275, 109)
        Me.InfoTile.Name = "InfoTile"
        Me.InfoTile.Size = New System.Drawing.Size(36, 35)
        Me.InfoTile.Style = MetroFramework.MetroColorStyle.White
        Me.InfoTile.TabIndex = 313
        Me.InfoTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.InfoTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Info32
        Me.InfoTile.TileImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.InfoTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.InfoTile.UseSelectable = True
        Me.InfoTile.UseTileImage = True
        '
        'Sp_Shipping_SEL_ShippedByOrderNoTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippedByOrderNoTableAdapter.ClearBeforeFill = True
        '
        'MetroGrid2
        '
        Me.MetroGrid2.AllowUserToAddRows = False
        Me.MetroGrid2.AllowUserToDeleteRows = False
        Me.MetroGrid2.AllowUserToResizeRows = False
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid2.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle12
        Me.MetroGrid2.AutoGenerateColumns = False
        Me.MetroGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid2.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.MetroGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BCDataGridViewTextBoxColumn1, Me.GradeDataGridViewTextBoxColumn1, Me.CASENODataGridViewTextBoxColumn1, Me.NetdefDataGridViewTextBoxColumn1, Me.NetrealDataGridViewTextBoxColumn1, Me.MovementNoDataGridViewTextBoxColumn, Me.CustomerrsDataGridViewTextBoxColumn, Me.CustomercasenoDataGridViewTextBoxColumn, Me.cusrunno, Me.caserunno, Me.FullMigration, Me.FullMigrationDate})
        Me.MetroGrid2.DataSource = Me.SpShippingSELFromBayAllIssuedBindingSource
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid2.DefaultCellStyle = DataGridViewCellStyle17
        Me.MetroGrid2.EnableHeadersVisualStyles = False
        Me.MetroGrid2.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid2.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid2.Location = New System.Drawing.Point(462, 191)
        Me.MetroGrid2.Name = "MetroGrid2"
        Me.MetroGrid2.ReadOnly = True
        Me.MetroGrid2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(81, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid2.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.MetroGrid2.RowHeadersWidth = 51
        Me.MetroGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid2.RowTemplate.Height = 28
        Me.MetroGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid2.Size = New System.Drawing.Size(946, 544)
        Me.MetroGrid2.Style = MetroFramework.MetroColorStyle.Brown
        Me.MetroGrid2.TabIndex = 213
        Me.MetroGrid2.UseCustomBackColor = True
        Me.MetroGrid2.UseCustomForeColor = True
        Me.MetroGrid2.UseStyleColors = True
        Me.MetroGrid2.Visible = False
        '
        'BCDataGridViewTextBoxColumn1
        '
        Me.BCDataGridViewTextBoxColumn1.DataPropertyName = "BC"
        Me.BCDataGridViewTextBoxColumn1.HeaderText = "BC"
        Me.BCDataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.BCDataGridViewTextBoxColumn1.Name = "BCDataGridViewTextBoxColumn1"
        Me.BCDataGridViewTextBoxColumn1.ReadOnly = True
        Me.BCDataGridViewTextBoxColumn1.Width = 59
        '
        'GradeDataGridViewTextBoxColumn1
        '
        Me.GradeDataGridViewTextBoxColumn1.DataPropertyName = "grade"
        Me.GradeDataGridViewTextBoxColumn1.HeaderText = "grade"
        Me.GradeDataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.GradeDataGridViewTextBoxColumn1.Name = "GradeDataGridViewTextBoxColumn1"
        Me.GradeDataGridViewTextBoxColumn1.ReadOnly = True
        Me.GradeDataGridViewTextBoxColumn1.Width = 84
        '
        'CASENODataGridViewTextBoxColumn1
        '
        Me.CASENODataGridViewTextBoxColumn1.DataPropertyName = "CASENO"
        Me.CASENODataGridViewTextBoxColumn1.HeaderText = "Cases"
        Me.CASENODataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.CASENODataGridViewTextBoxColumn1.Name = "CASENODataGridViewTextBoxColumn1"
        Me.CASENODataGridViewTextBoxColumn1.ReadOnly = True
        Me.CASENODataGridViewTextBoxColumn1.Width = 84
        '
        'NetdefDataGridViewTextBoxColumn1
        '
        Me.NetdefDataGridViewTextBoxColumn1.DataPropertyName = "netdef"
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.NetdefDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle14
        Me.NetdefDataGridViewTextBoxColumn1.HeaderText = "Net"
        Me.NetdefDataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.NetdefDataGridViewTextBoxColumn1.Name = "NetdefDataGridViewTextBoxColumn1"
        Me.NetdefDataGridViewTextBoxColumn1.ReadOnly = True
        Me.NetdefDataGridViewTextBoxColumn1.Width = 67
        '
        'NetrealDataGridViewTextBoxColumn1
        '
        Me.NetrealDataGridViewTextBoxColumn1.DataPropertyName = "netreal"
        DataGridViewCellStyle15.Format = "N2"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.NetrealDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle15
        Me.NetrealDataGridViewTextBoxColumn1.HeaderText = "netreal"
        Me.NetrealDataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.NetrealDataGridViewTextBoxColumn1.Name = "NetrealDataGridViewTextBoxColumn1"
        Me.NetrealDataGridViewTextBoxColumn1.ReadOnly = True
        Me.NetrealDataGridViewTextBoxColumn1.Visible = False
        Me.NetrealDataGridViewTextBoxColumn1.Width = 78
        '
        'MovementNoDataGridViewTextBoxColumn
        '
        Me.MovementNoDataGridViewTextBoxColumn.DataPropertyName = "MovementNo"
        Me.MovementNoDataGridViewTextBoxColumn.HeaderText = "Move No."
        Me.MovementNoDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.MovementNoDataGridViewTextBoxColumn.Name = "MovementNoDataGridViewTextBoxColumn"
        Me.MovementNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.MovementNoDataGridViewTextBoxColumn.Width = 118
        '
        'CustomerrsDataGridViewTextBoxColumn
        '
        Me.CustomerrsDataGridViewTextBoxColumn.DataPropertyName = "customerrs"
        Me.CustomerrsDataGridViewTextBoxColumn.HeaderText = "customerrs"
        Me.CustomerrsDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.CustomerrsDataGridViewTextBoxColumn.Name = "CustomerrsDataGridViewTextBoxColumn"
        Me.CustomerrsDataGridViewTextBoxColumn.ReadOnly = True
        Me.CustomerrsDataGridViewTextBoxColumn.Width = 130
        '
        'CustomercasenoDataGridViewTextBoxColumn
        '
        Me.CustomercasenoDataGridViewTextBoxColumn.DataPropertyName = "fromcuslotno"
        Me.CustomercasenoDataGridViewTextBoxColumn.HeaderText = "Cust.lot no."
        Me.CustomercasenoDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.CustomercasenoDataGridViewTextBoxColumn.Name = "CustomercasenoDataGridViewTextBoxColumn"
        Me.CustomercasenoDataGridViewTextBoxColumn.ReadOnly = True
        Me.CustomercasenoDataGridViewTextBoxColumn.Width = 134
        '
        'cusrunno
        '
        Me.cusrunno.DataPropertyName = "cusrunno"
        Me.cusrunno.HeaderText = "cusrunno"
        Me.cusrunno.MinimumWidth = 6
        Me.cusrunno.Name = "cusrunno"
        Me.cusrunno.ReadOnly = True
        Me.cusrunno.Width = 115
        '
        'caserunno
        '
        Me.caserunno.DataPropertyName = "caserunno"
        Me.caserunno.HeaderText = "caserunno"
        Me.caserunno.MinimumWidth = 6
        Me.caserunno.Name = "caserunno"
        Me.caserunno.ReadOnly = True
        Me.caserunno.Width = 124
        '
        'FullMigration
        '
        Me.FullMigration.DataPropertyName = "FullMigration"
        Me.FullMigration.HeaderText = "FullMigration"
        Me.FullMigration.MinimumWidth = 6
        Me.FullMigration.Name = "FullMigration"
        Me.FullMigration.ReadOnly = True
        Me.FullMigration.Width = 125
        '
        'FullMigrationDate
        '
        Me.FullMigrationDate.DataPropertyName = "FullMigrationDate"
        DataGridViewCellStyle16.Format = "d"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.FullMigrationDate.DefaultCellStyle = DataGridViewCellStyle16
        Me.FullMigrationDate.HeaderText = "FullMigrationDate"
        Me.FullMigrationDate.MinimumWidth = 6
        Me.FullMigrationDate.Name = "FullMigrationDate"
        Me.FullMigrationDate.ReadOnly = True
        Me.FullMigrationDate.Width = 187
        '
        'SpShippingSELFromBayAllIssuedBindingSource
        '
        Me.SpShippingSELFromBayAllIssuedBindingSource.DataMember = "sp_Shipping_SEL_FromBayAllIssued"
        Me.SpShippingSELFromBayAllIssuedBindingSource.DataSource = Me.ShippingDataSet
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(1052, 124)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(102, 20)
        Me.MetroLabel5.TabIndex = 237
        Me.MetroLabel5.Text = "Cust. case start"
        Me.MetroLabel5.Visible = False
        '
        'CustCaseTextbox
        '
        '
        '
        '
        Me.CustCaseTextbox.CustomButton.Image = Nothing
        Me.CustCaseTextbox.CustomButton.Location = New System.Drawing.Point(74, 2)
        Me.CustCaseTextbox.CustomButton.Name = ""
        Me.CustCaseTextbox.CustomButton.Size = New System.Drawing.Size(35, 35)
        Me.CustCaseTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CustCaseTextbox.CustomButton.TabIndex = 1
        Me.CustCaseTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CustCaseTextbox.CustomButton.UseSelectable = True
        Me.CustCaseTextbox.CustomButton.Visible = False
        Me.CustCaseTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CustCaseTextbox.Lines = New String(-1) {}
        Me.CustCaseTextbox.Location = New System.Drawing.Point(1160, 106)
        Me.CustCaseTextbox.MaxLength = 32767
        Me.CustCaseTextbox.Name = "CustCaseTextbox"
        Me.CustCaseTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CustCaseTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CustCaseTextbox.SelectedText = ""
        Me.CustCaseTextbox.SelectionLength = 0
        Me.CustCaseTextbox.SelectionStart = 0
        Me.CustCaseTextbox.ShortcutsEnabled = True
        Me.CustCaseTextbox.Size = New System.Drawing.Size(112, 40)
        Me.CustCaseTextbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.CustCaseTextbox.TabIndex = 236
        Me.CustCaseTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CustCaseTextbox.UseSelectable = True
        Me.CustCaseTextbox.UseStyleColors = True
        Me.CustCaseTextbox.Visible = False
        Me.CustCaseTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CustCaseTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Sp_Shipping_SEL_PdSpBySpNoBindingSource
        '
        Me.Sp_Shipping_SEL_PdSpBySpNoBindingSource.DataMember = "sp_Shipping_SEL_PdSpBySpNo"
        Me.Sp_Shipping_SEL_PdSpBySpNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_SEL_PdSpBySpNoTableAdapter
        '
        Me.Sp_Shipping_SEL_PdSpBySpNoTableAdapter.ClearBeforeFill = True
        '
        'Sp_Shipping_SEL_FromBayAllIssuedTableAdapter
        '
        Me.Sp_Shipping_SEL_FromBayAllIssuedTableAdapter.ClearBeforeFill = True
        '
        'SelectAllCheckBox
        '
        Me.SelectAllCheckBox.AutoSize = True
        Me.SelectAllCheckBox.Checked = True
        Me.SelectAllCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SelectAllCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.SelectAllCheckBox.Location = New System.Drawing.Point(695, 124)
        Me.SelectAllCheckBox.Name = "SelectAllCheckBox"
        Me.SelectAllCheckBox.Size = New System.Drawing.Size(85, 20)
        Me.SelectAllCheckBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.SelectAllCheckBox.TabIndex = 238
        Me.SelectAllCheckBox.Text = "Select all"
        Me.SelectAllCheckBox.UseSelectable = True
        '
        'Sp_Shipping_GETMAX_TransferNoBindingSource
        '
        Me.Sp_Shipping_GETMAX_TransferNoBindingSource.DataMember = "sp_Shipping_GETMAX_TransferNo"
        Me.Sp_Shipping_GETMAX_TransferNoBindingSource.DataSource = Me.ShippingDataSet
        '
        'Sp_Shipping_GETMAX_TransferNoTableAdapter
        '
        Me.Sp_Shipping_GETMAX_TransferNoTableAdapter.ClearBeforeFill = True
        '
        'FrmAddShipping
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1280, 773)
        Me.Controls.Add(Me.SelectAllCheckBox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.CustCaseTextbox)
        Me.Controls.Add(Me.MetroGrid2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CBalesFromMetroLabel)
        Me.Controls.Add(Me.FromBayComboBox)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.MetroGridFrom)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmAddShipping"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        Me.Text = "Add Shipping"
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.SpShippingSELShippingBayFromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MetroGridFrom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELFromBayFromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_GETMAX_PdSPBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MetroGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELShippedByOrderNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.MetroGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELFromBayAllIssuedBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_PdSpBySpNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_GETMAX_TransferNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents OrderNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SaveTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CBalesFromMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents FromBayComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroGridFrom As MetroFramework.Controls.MetroGrid
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents SpShippingSELShippingBayFromBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingBayFromTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingBayFromTableAdapter
    Friend WithEvents SpShippingSELFromBayFromBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_FromBayFromTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayFromTableAdapter
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel15 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ContainerLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CaseLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents WeightLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CustomerGradeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents PackedGradeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CustomerLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel17 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByOrderNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents StatusLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents FinishedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SpNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_GETMAX_PdSPBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_GETMAX_PdSPTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_GETMAX_PdSPTableAdapter
    Friend WithEvents ShippedDate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroGrid1 As MetroFramework.Controls.MetroGrid
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents SPNODataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BAYDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CASESDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SpShippingSELShippedByOrderNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippedByOrderNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippedByOrderNoTableAdapter
    Friend WithEvents UnFinishedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ISSUED As DataGridViewCheckBoxColumn
    Friend WithEvents MetroGrid2 As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CustCaseTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Sp_Shipping_SEL_PdSpBySpNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_PdSpBySpNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_PdSpBySpNoTableAdapter
    Friend WithEvents SpShippingSELFromBayAllIssuedBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_FromBayAllIssuedTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_FromBayAllIssuedTableAdapter
    Friend WithEvents BCDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GradeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CASENODataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetdefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetrealDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MovementNo As DataGridViewTextBoxColumn
    Friend WithEvents customerrs As DataGridViewTextBoxColumn
    Friend WithEvents customercaseno As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents SelectAllCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents DeleteTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents InfoTile As MetroFramework.Controls.MetroTile
    Friend WithEvents Sp_Shipping_GETMAX_TransferNoBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_GETMAX_TransferNoTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_GETMAX_TransferNoTableAdapter
    Friend WithEvents BCDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GradeDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CASENODataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NetdefDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NetrealDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MovementNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerrsDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomercasenoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cusrunno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents caserunno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FullMigration As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents FullMigrationDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MetroLabel16 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalShippedCasesMetroLabel As MetroFramework.Controls.MetroLabel
End Class

﻿Public Class FrmAddShipping
    Inherits MetroFramework.Forms.MetroForm
    Public XOrderNo As String
    Dim db As New ShippingDataClassesDataContext
    Dim XchkSpNo As Boolean = False

    Private Sub SumTotalShippedCases()
        Try
            Dim a = Me.ShippingDataSet.sp_Shipping_SEL_ShippedByOrderNo
            Dim totalShippedCases = 0
            If a.Count > 0 Then
                For i As Integer = 0 To a.Rows.Count - 1
                    totalShippedCases = totalShippedCases + a.Rows(i)(2)
                Next
            End If
            TotalShippedCasesMetroLabel.Text = totalShippedCases.ToString("N0")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FrmAddShipping_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.sp_Shipping_GETMAX_TransferNo' table. You can move, or remove it, as needed.
        Me.Sp_Shipping_GETMAX_TransferNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_GETMAX_TransferNo)
        Try
            UsernameMetroLabel.Text = XUsername
            OrderNoLabel.Text = XOrderNo
            CBalesFromMetroLabel.Text = ""

            MetroGridFrom.Visible = True
            MetroGridFrom.Location = New Point(326, 152)
            MetroGrid2.Visible = False

            CropLabel.Text = ""
            CustomerLabel.Text = ""
            PackedGradeLabel.Text = ""
            CustomerGradeLabel.Text = ""
            WeightLabel.Text = ""
            CaseLabel.Text = ""
            ContainerLabel.Text = ""
            StatusLabel.Text = ""
            ShippedDate.Value = Now
            SpNoLabel.Text = ""

            Me.Sp_Shipping_SEL_ShippingBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingBayFrom)

            Dim xRow1 As ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNoRow
            Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, XOrderNo)
            xRow1 = Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo.FindByOrderNo(XOrderNo)
            If Not xRow1 Is Nothing Then
                If Not xRow1.IscropNull Then
                    CropLabel.Text = xRow1.crop
                End If

                CustomerLabel.Text = xRow1.Customer
                PackedGradeLabel.Text = xRow1.PackedGrade
                CustomerGradeLabel.Text = xRow1.CustomerGrade
                WeightLabel.Text = xRow1.TotalWeightRequest
                CaseLabel.Text = xRow1.TotalCaseRequest
                If Not xRow1.IsContainerNull Then
                    ContainerLabel.Text = xRow1.Container
                End If
                StatusLabel.Text = xRow1.StatusName
                'If Not xRow1.IsShippedDateNull Then
                '    ShippedDate.Value = xRow1.ShippedDate
                'End If
                'SpNoLabel.Text = xRow1.
            End If

            'Show ShippedByOrderNo into Grid
            Me.Sp_Shipping_SEL_ShippedByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippedByOrderNo, XOrderNo)
            SumTotalShippedCases()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FromBayComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FromBayComboBox.SelectedIndexChanged
        Try
            If FromBayComboBox.Text <> "" Then
                XchkSpNo = False

                MetroGridFrom.Visible = True
                MetroGrid2.Location = New Point(326, 152)
                MetroGrid2.Visible = False
                Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                CBalesFromMetroLabel.Text = SpShippingSELFromBayFromBindingSource.Count & " Cases"

            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub SaveTile_Click(sender As Object, e As EventArgs) Handles SaveTile.Click
        Try
            Dim XChkSave As Boolean = True
            Dim XCustCaseNo As Int32 = 0

            'เช็คว่าถ้าสถานะเป็น Complete แล้วจะทำการบันทึกข้อมูลไม่ได้ เนื่องจาก Complete คือOrderนี้เสร็จสมบูรณ์แล้ว
            If StatusLabel.Text = "Complete" Then
                Throw New Exception("Order นี้อยู่ในสถานะ Completed ไม่สามารถทำการใดๆ กับข้อมูลได้อีก")
            End If

            'เช็คว่าถ้า Finished Shipped ไปแล้วจะทำการบันทึกข้อมูลไม่ได้ ให้ทำการ UnFinished ข้อมูลก่อน
            Dim XFRow As ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNoRow
            Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, OrderNoLabel.Text)
            XFRow = ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo.FindByOrderNo(OrderNoLabel.Text)
            If Not XFRow Is Nothing Then
                If Not XFRow.IsShippedDateNull Then
                    Throw New Exception("Order นี้ถูกเปลี่ยนสถานะเป็น Finished แล้ว" &
                        "หากต้องการปลดล๊อกกรุณาตรวจสอบการนำเข้าข้อมูล Shipping No. นี้กับแผนก Green Leaf Account" &
                        " และแจ้งเจ้าหน้าที่ IT ให้ทำการเปลี่ยนสถานะเป็น Unfinish ก่อน")
                End If
            End If

            If FromBayComboBox.Text = "" Then
                Throw New Exception("กรุณาระบฺุ Bay ที่ต้องการ Shipped!")
            End If

            If Convert.ToInt16(Val(CBalesFromMetroLabel.Text)) <= 0 Then
                Throw New Exception("Bay นี้ไม่มีข้อมูลยาที่ต้องการ Shipped กรุณาตรวจสอบข้อมูล")
            End If

            'SelectedRowCount เก็บค่าที่ทำไฮไลท์สีเขียว
            Dim SelectedRowCount As Integer
            'ถ้าเลือก SelectAll จะทำการShippedให้ทั้งหมดใน Bay นี้
            If SelectAllCheckBox.Checked = True Then
                SelectedRowCount = Convert.ToInt16(Val(CBalesFromMetroLabel.Text))
            Else
                SelectedRowCount = MetroGridFrom.Rows.GetRowCount(DataGridViewElementStates.Selected)
            End If

            If SelectedRowCount > 99 Then
                Throw New Exception("ระบบไม่อนุญาติให้ Shipped เกิน 99 กล่อง")
            End If

            'เช็คว่าหากไม่ทำการคีย์ Cust. case no ไม่อนุญาตให้Save
            'If CustCaseTextbox.Text = "" Then
            '    MessageBoxModule.Info("กรุณาคีย์เลข Cust. Case No. เริ่มต้น", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    Return
            'End If
            'XCustCaseNo = Val(CustCaseTextbox.Text)

            'เช็คค่าว่าใน Barcode ที่เลือกมีแถวไหนที่Customer ไม่ตรงกันระหว่าง customer ในตารางกับ customer ที่กำหนดใน Order แต่สำหรับกรณี Customerในตารางเป็น SPEC ไม่นับเพราะเป็น Stock ว่างๆ ยังไม่มีเจ้าของ
            Dim XchkCustomer As Boolean = True
            If (SelectedRowCount > 0) Then
                For i As Integer = 0 To SelectedRowCount - 1
                    'MessageBoxModule.Info(MetroGridFrom.Rows(i).Cells(6).Value.ToString())
                    If SelectAllCheckBox.Checked = True Then
                        If (CustomerLabel.Text <> MetroGridFrom.Rows(i).Cells(6).Value.ToString()) AndAlso (MetroGridFrom.Rows(i).Cells(6).Value.ToString() <> "SPEC") Then
                            XchkCustomer = False
                        End If
                    Else
                        If (CustomerLabel.Text <> MetroGridFrom.SelectedRows(i).Cells(6).Value.ToString()) AndAlso (MetroGridFrom.SelectedRows(i).Cells(6).Value.ToString() <> "SPEC") Then
                            XchkCustomer = False
                        End If
                    End If
                Next
            End If
            If XchkCustomer = False Then
                Throw New Exception("มีบาร์โค้ดที่ customer ไม่ตรงกับที่ระบุไว้ใน order")
            End If

            'เช็คค่าว่าใน Barcode ที่เลือกมีแถวไหนที่ CustomerGrader ไม่ตรงกับ เกรด Graders ในตาราง ให้แจ้งถามว่าต้องการshipped หรือไม่
            Dim XchkPackedGrade As Boolean = True
            If (SelectedRowCount > 0) Then
                For i As Integer = 0 To SelectedRowCount - 1
                    If SelectAllCheckBox.Checked = True Then
                        If (PackedGradeLabel.Text <> MetroGridFrom.Rows(i).Cells(1).Value.ToString()) Then
                            XchkPackedGrade = False
                        End If
                    Else
                        If (PackedGradeLabel.Text <> MetroGridFrom.SelectedRows(i).Cells(1).Value.ToString()) Then
                            XchkPackedGrade = False
                        End If
                    End If

                Next
            End If
            If XchkPackedGrade = False Then
                Throw New Exception("มีบาร์โค้ดที่ grade ไม่ตรงกับที่ระบุไว้ใน order")
            End If

            'เช็คว่าถ้าBayนี้เคยบันทึกไปแล้วให้ Chkspno = true เพื่อไม่ต้อง gen SPNO ใหม่   ให้ใช้ SPNo เดิม
            For Xi1 = 0 To MetroGrid1.RowCount - 1
                If FromBayComboBox.Text = MetroGrid1.Item(1, Xi1).Value Then
                    XchkSpNo = True
                    SpNoLabel.Text = MetroGrid1.Item(0, Xi1).Value
                End If
            Next

            'ถ้า XchkSPno = false แสดงว่าไม่ได้ใช้ spNo อันเดิม ระบบจะ Gen SPNo ใหม่ให้อัตโนมัติ
            If XchkSpNo = False Then
                'Get spno from database 
                Me.Sp_Shipping_GETMAX_PdSPTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_GETMAX_PdSP, CropLabel.Text)
                SpNoLabel.Text = ShippingDataSet.sp_Shipping_GETMAX_PdSP.Item(0).SpNo

                'Insert into PdSP
                db.sp_Shipping_INS_PdSP(SpNoLabel.Text, ShippedDate.Value, XUsername, OrderNoLabel.Text)
            End If

            ''หาค่า Transfer number --> ไม่ต้องหาเพราะไม่มีการ transfer ไปเกรดอื่นแน่นอน
            'Dim XTransferNo As Decimal

            'XBC เก็บค่า Barcodeในแถวที่ทำไฮไลท์สีเขียว
            Dim result = MessageBoxModule.Question("ต้องการ shipped order นี้ใช่หรือไม่?")
            If result = DialogResult.Yes Then
                Dim XBC As String
                If (SelectedRowCount > 0) Then
                    For i As Integer = 0 To SelectedRowCount - 1
                        If SelectAllCheckBox.Checked = True Then
                            XBC = MetroGridFrom.Rows(i).Cells(0).Value.ToString()
                            db.sp_Shipping_UPD_PdBySpNo(SpNoLabel.Text, FromBayComboBox.Text, XBC)

                            'หากลูกค้าเป็น SPEC จะต้องเข้าไป Update Transfer grader ด้วย
                            If (MetroGridFrom.Rows(i).Cells(6).Value = "SPEC") Then
                                db.sp_Shipping_UPD_TransferGrade(CropLabel.Text, MetroGridFrom.Rows(i).Cells(1).Value, CustomerLabel.Text, MetroGridFrom.Rows(i).Cells(1).Value, XUsername, XBC, XCustCaseNo, 1, "", 0)
                            End If
                            'XCustCaseNo = XCustCaseNo + 1
                        Else
                            XBC = MetroGridFrom.SelectedRows(i).Cells(0).Value.ToString()
                            db.sp_Shipping_UPD_PdBySpNo(SpNoLabel.Text, FromBayComboBox.Text, XBC)

                            'หากลูกค้าเป็น SPEC จะต้องเข้าไป Update Transfer grader ด้วย
                            If (MetroGridFrom.Rows(i).Cells(6).Value = "SPEC") Then
                                db.sp_Shipping_UPD_TransferGrade(CropLabel.Text, MetroGridFrom.SelectedRows(i).Cells(1).Value, CustomerLabel.Text, MetroGridFrom.SelectedRows(i).Cells(1).Value, XUsername, XBC, XCustCaseNo, 1, "", 0)
                            End If
                            'XCustCaseNo = XCustCaseNo + 1
                        End If
                    Next
                End If
                MessageBoxModule.Info("บันทึกข้อมูลเรียบร้อยแล้ว")
                If FromBayComboBox.Text <> "" Then
                    Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                    CBalesFromMetroLabel.Text = SpShippingSELFromBayFromBindingSource.Count & " Cases"
                End If

                'Show ShippedByOrderNo into Grid
                Me.Sp_Shipping_SEL_ShippedByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippedByOrderNo, XOrderNo)
                XchkSpNo = False
                SpNoLabel.Text = ""
            End If

            SumTotalShippedCases()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub FinishedTile_Click(sender As Object, e As EventArgs) Handles FinishedTile.Click
        Try
            If XDepartment <> "Leaf Accounting" Then
                Throw New Exception("อนุญาตเฉพาะ แผนกบัญชี ทำการ Finish Order ได้")
                Return
            End If

            'เช็คว่าถ้าสถานะเป็น Complete แล้วจะทำการบันทึกข้อมูลไม่ได้ เนื่องจาก Complete คือOrderนี้เสร็จสมบูรณ์แล้ว
            If StatusLabel.Text = "Complete" Then
                Throw New Exception("Orderนี้Completedแล้ว จึงไม่สามารถทำการใดๆกับข้อมูลได้อีก กรุณาตรวจสอบกับเจ้าหน้าที่บันทึกOrder")
            End If

            'เช็คว่าถ้า Finished Shipped ไปแล้วจะทำการบันทึกข้อมูลไม่ได้ ให้ทำการ UnFinished ข้อมูลก่อน
            Dim XFRow As ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNoRow
            Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, OrderNoLabel.Text)
            XFRow = ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo.FindByOrderNo(OrderNoLabel.Text)
            If Not XFRow Is Nothing Then
                If Not XFRow.IsShippedDateNull Then
                    Throw New Exception("คุณ Finished Order นี้แล้ว จึงไม่สามารถบันทึกข้อมูลได้อีก หากต้องการปลดล๊อกกรุณาตรวจสอบการนำเข้าข้อมูล Shipping No.นี้กับแผนก Green Leaf Account และ Email แจ้งเจ้าหน้าที่ IT ให้ทำการ UnFinished")
                End If
            End If

            'เช็คว่ามีการบันทึก spno แล้วหรือยัง ถ้าไม่มีมาก่อนไม่อนุญาตให้ finish
            Me.Sp_Shipping_SEL_ShippedByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippedByOrderNo, OrderNoLabel.Text)
            If ShippingDataSet.sp_Shipping_SEL_ShippedByOrderNo.Count <= 0 Then
                Throw New Exception("Order นี้ยังไม่มี shipping no. กรุณาเลือก Bay ที่ต้องการและบันทึกข้อมูลหรือตรวจสอบข้อมูลอีกครั้ง")
            End If

            Dim result = MessageBoxModule.Question("ต้องการ Finished การ Shipped ของ Order นี้ใช่หรือไม่?")
            If result = DialogResult.Yes Then
                db.sp_Shipping_UPD_Shipped(OrderNoLabel.Text, XUsername)
                MessageBoxModule.Info("บันทึกข้อมูลเรียบร้อยแล้ว")
            End If
            'Show ShippedByOrderNo into Grid
            Me.Sp_Shipping_SEL_ShippedByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippedByOrderNo, XOrderNo)
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            XchkSpNo = False
            ShippedDate.Value = Now
            SpNoLabel.Text = ""
            MetroGridFrom.Visible = True
            MetroGridFrom.Location = New Point(326, 152)
            MetroGrid2.Visible = False

            If FromBayComboBox.Text <> "" Then
                Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                CBalesFromMetroLabel.Text = SpShippingSELFromBayFromBindingSource.Count & " Cases"

            End If
            'Show ShippedByOrderNo into Grid
            Me.Sp_Shipping_SEL_ShippedByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippedByOrderNo, XOrderNo)
            SumTotalShippedCases()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub MetroGrid1_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MetroGrid1.CellMouseClick
        Try
            'กำหนดให้ Bay คือรายการที่เลือกในแถว
            FromBayComboBox.Text = MetroGrid1.Item(1, MetroGrid1.CurrentCell.RowIndex).Value
            SpNoLabel.Text = MetroGrid1.Item(0, MetroGrid1.CurrentCell.RowIndex).Value

            'แสดงข้อมูลให้ดู โดยเช็คว่า ถ้า Order นี้ Shipped แล้วจะแสดงโดยไม่สนใจ issued  แต่ถ้ายังไม่ได้ Shipped ให้แสดงเฉพาะที่ issued = 0 เท่านั้น
            Dim XFRow As ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNoRow
            Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, OrderNoLabel.Text)
            XFRow = ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo.FindByOrderNo(OrderNoLabel.Text)
            If Not XFRow Is Nothing Then
                If Not XFRow.IsShippedDateNull Then
                    MetroGrid2.Visible = True
                    MetroGrid2.Location = New Point(326, 152)
                    MetroGridFrom.Visible = False
                    If FromBayComboBox.Text <> "" Then
                        Me.Sp_Shipping_SEL_FromBayAllIssuedTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayAllIssued, FromBayComboBox.Text, CropLabel.Text, SpNoLabel.Text)
                        CBalesFromMetroLabel.Text = SpShippingSELFromBayAllIssuedBindingSource.Count & " Cases"
                        ShippedDate.Value = XFRow.ShippedDate
                    End If
                Else
                    MetroGridFrom.Visible = True
                    MetroGridFrom.Location = New Point(326, 152)
                    MetroGrid2.Visible = False
                    If FromBayComboBox.Text <> "" Then
                        Sp_Shipping_SEL_FromBayFromTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_FromBayFrom, FromBayComboBox.Text)
                        CBalesFromMetroLabel.Text = SpShippingSELFromBayFromBindingSource.Count & " Cases"

                    End If
                End If
            End If

            XchkSpNo = True

            'Dim XFRow2 As ShippingDataSet.sp_Shipping_SEL_PdSpBySpNoRow
            'Me.Sp_Shipping_SEL_PdSpBySpNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_PdSpBySpNo, SpNoLabel.Text)
            'XFRow2 = ShippingDataSet.sp_Shipping_SEL_PdSpBySpNo.FindByspno(SpNoLabel.Text)
            'If Not XFRow2 Is Nothing Then
            '    If Not XFRow2.Is_dateNull Then
            '        ShippedDate.Value = XFRow2._date
            '    End If
            'End If

        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub UnFinishedTile_Click(sender As Object, e As EventArgs) Handles UnFinishedTile.Click
        Try
            If StatusLabel.Text = "Complete" Then
                Throw New Exception("Order นี้ Completed แล้ว จึงไม่สามารถทำการใดๆกับข้อมูลได้อีก กรุณาตรวจสอบกับเจ้าหน้าที่บันทึก Order")
            End If

            MessageBoxModule.Info("กรุณาตรวจสอบการนำเข้าข้อมูล Shipping No.นี้กับแผนก Green Leaf Account และ Email แจ้งเจ้าหน้าที่ IT ให้ทำการ UnFinished")
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub CropForCustomerTextbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CustCaseTextbox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub DeleteTile_Click(sender As Object, e As EventArgs) Handles DeleteTile.Click
        Try
            If OrderNoLabel.Text = "" Then
                Throw New Exception("ไม่มีเลข Order No กรุณาตรวจสอบข้อมูล")
            End If

            'เช็คว่าถ้าสถานะเป็น Complete แล้วจะทำการบันทึกข้อมูลไม่ได้ เนื่องจาก Complete คือOrderนี้เสร็จสมบูรณ์แล้ว
            If StatusLabel.Text = "Complete" Then
                Throw New Exception("Order นี้ Completed แล้ว จึงไม่สามารถทำการใดๆกับข้อมูลได้อีก กรุณาตรวจสอบกับเจ้าหน้าที่บันทึก Order")
            End If

            'เช็คว่าถ้า Finished Shipped ไปแล้วจะทำการบันทึกข้อมูลไม่ได้ ให้ทำการ UnFinished ข้อมูลก่อน
            Dim XFRow As ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNoRow
            Me.Sp_Shipping_SEL_ShippingOrderByOrderNoTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo, OrderNoLabel.Text)
            XFRow = ShippingDataSet.sp_Shipping_SEL_ShippingOrderByOrderNo.FindByOrderNo(OrderNoLabel.Text)
            If Not XFRow Is Nothing Then
                If Not XFRow.IsShippedDateNull Then
                    Throw New Exception("คุณ Finished Order นี้แล้ว จึงไม่สามารถบันทึกข้อมูลได้อีก หากต้องการปลดล๊อกกรุณาตรวจสอบการนำเข้าข้อมูล Shipping No.นี้กับแผนก Green Leaf Account และ Email แจ้งเจ้าหน้าที่ IT ให้ทำการ UnFinished")
                End If
            End If

            If Convert.ToInt16(Val(CBalesFromMetroLabel.Text)) <= 0 Then
                Throw New Exception("Bay นี้ไม่มีข้อมูลยาที่ต้องการ Shipped กรุณาตรวจสอบข้อมูล")
            End If

            If SpNoLabel.Text = "" Then
                Throw New Exception("กรุณาเลือก SP No.ที่ต้องการลบจากตารางด้านซ้ายมือ")
            End If

            Dim result = MessageBoxModule.Question("ต้องการลบ SP No. " & SpNoLabel.Text & " ใช่หรือไม่ ?")
            If result = DialogResult.Yes Then
                If SelectAllCheckBox.Checked = True Then
                    db.sp_Shipping_UPD_CancleSpNo(OrderNoLabel.Text, SpNoLabel.Text, FromBayComboBox.Text, "", 0)
                Else
                    'SelectedRowCount เก็บค่าที่ทำไฮไลท์สีเขียว
                    Dim SelectedRowCount As Integer
                    SelectedRowCount = MetroGridFrom.Rows.GetRowCount(DataGridViewElementStates.Selected)

                    If SelectedRowCount <= 0 Then
                        Throw New Exception("กรุณา Click เลือกบาร์โค้ดที่ต้องการลบการshippingจากตาราง")
                    End If

                    Dim XBC As String
                    For i As Integer = 0 To SelectedRowCount - 1
                        XBC = MetroGridFrom.SelectedRows(i).Cells(0).Value.ToString()
                        db.sp_Shipping_UPD_CancleSpNo(OrderNoLabel.Text, SpNoLabel.Text, FromBayComboBox.Text, XBC, 1)
                    Next

                End If
                MessageBoxModule.Info("ลบ SP No.นี้เรียบร้อยแล้ว")
                RefreshTile.PerformClick()
            End If
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub

    Private Sub InfoTile_Click(sender As Object, e As EventArgs) Handles InfoTile.Click
        Try
            Dim XGrader As String
            Dim XCustomer2 As String

            XGrader = PackedGradeLabel.Text
            XCustomer2 = CustomerLabel.Text
            Dim xfrm As New FrmShowPackedGradeDetail
            xfrm.XGraders = XGrader
            xfrm.XCustomer = XCustomer2
            xfrm.ShowDialog()
            xfrm.Dispose()
        Catch ex As Exception
            MessageBoxModule.Exception(ex)
        End Try
    End Sub
End Class
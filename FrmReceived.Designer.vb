﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmReceived
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.ShippingDataSet = New ShippingSystem.ShippingDataSet()
        Me.Sp_Shipping_SEL_ShippingLocationTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingLocationTableAdapter()
        Me.SpShippingSELShippingLocationBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ShippingLocationComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.TableAdapterManager = New ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager()
        Me.SecurityTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.securityTableAdapter()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.DetailTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.MetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MovementStatusCombobox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter = New ShippingSystem.ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter()
        Me.Crop = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MovementNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TruckNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Driver = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateStart = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateReceived = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NBales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpShippingSELShippingLocationBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(694, 197)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(0, 0)
        Me.MetroLabel1.TabIndex = 0
        '
        'ShippingDataSet
        '
        Me.ShippingDataSet.DataSetName = "ShippingDataSet"
        Me.ShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Shipping_SEL_ShippingLocationTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.ClearBeforeFill = True
        '
        'SpShippingSELShippingLocationBindingSource
        '
        Me.SpShippingSELShippingLocationBindingSource.DataMember = "sp_Shipping_SEL_ShippingLocation"
        Me.SpShippingSELShippingLocationBindingSource.DataSource = Me.ShippingDataSet
        '
        'ShippingLocationComboBox
        '
        Me.ShippingLocationComboBox.DataSource = Me.SpShippingSELShippingLocationBindingSource
        Me.ShippingLocationComboBox.DisplayMember = "LocName"
        Me.ShippingLocationComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.ShippingLocationComboBox.FormattingEnabled = True
        Me.ShippingLocationComboBox.ItemHeight = 29
        Me.ShippingLocationComboBox.Location = New System.Drawing.Point(102, 101)
        Me.ShippingLocationComboBox.Name = "ShippingLocationComboBox"
        Me.ShippingLocationComboBox.Size = New System.Drawing.Size(343, 35)
        Me.ShippingLocationComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.ShippingLocationComboBox.TabIndex = 169
        Me.ShippingLocationComboBox.UseSelectable = True
        Me.ShippingLocationComboBox.ValueMember = "LocNo"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(16, 101)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(80, 25)
        Me.MetroLabel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel3.TabIndex = 168
        Me.MetroLabel3.Text = "Location"
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ShippingSystem.ShippingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ShippingDataSet
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(716, 2)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(66, 58)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Refresh64
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'DetailTile
        '
        Me.DetailTile.ActiveControl = Nothing
        Me.DetailTile.AutoSize = True
        Me.DetailTile.BackColor = System.Drawing.Color.White
        Me.DetailTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DetailTile.Location = New System.Drawing.Point(553, 2)
        Me.DetailTile.Name = "DetailTile"
        Me.DetailTile.Size = New System.Drawing.Size(70, 57)
        Me.DetailTile.Style = MetroFramework.MetroColorStyle.White
        Me.DetailTile.TabIndex = 120
        Me.DetailTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DetailTile.TileImage = Global.ShippingSystem.My.Resources.Resources.Search64
        Me.DetailTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DetailTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.DetailTile.UseSelectable = True
        Me.DetailTile.UseTileImage = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(629, 21)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(44, 20)
        Me.MetroLabel5.TabIndex = 117
        Me.MetroLabel5.Text = "Detail"
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(788, 22)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1103, 2)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(63, 60)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ShippingSystem.My.Resources.Resources.CircledLeft264
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(5, 3)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ShippingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'MetroGrid
        '
        Me.MetroGrid.AllowUserToAddRows = False
        Me.MetroGrid.AllowUserToDeleteRows = False
        Me.MetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MetroGrid.AutoGenerateColumns = False
        Me.MetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Crop, Me.MovementNo, Me.TruckNo, Me.Driver, Me.DateStart, Me.DateReceived, Me.NBales})
        Me.MetroGrid.DataSource = Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.MetroGrid.EnableHeadersVisualStyles = False
        Me.MetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid.Location = New System.Drawing.Point(18, 152)
        Me.MetroGrid.Name = "MetroGrid"
        Me.MetroGrid.ReadOnly = True
        Me.MetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Calibri", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(165, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.MetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid.RowTemplate.Height = 38
        Me.MetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid.Size = New System.Drawing.Size(1176, 517)
        Me.MetroGrid.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroGrid.TabIndex = 167
        '
        'Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource
        '
        Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource.DataMember = "sp_Shipping_SEL_ShippingMovementByMovementStatusID"
        Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource.DataSource = Me.ShippingDataSet
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(58, 22)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.DetailTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(13, 16)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1179, 62)
        Me.MetroPanel1.TabIndex = 166
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MovementStatusCombobox
        '
        Me.MovementStatusCombobox.DisplayMember = "Crop"
        Me.MovementStatusCombobox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.MovementStatusCombobox.FormattingEnabled = True
        Me.MovementStatusCombobox.ItemHeight = 29
        Me.MovementStatusCombobox.Items.AddRange(New Object() {"Not Received", "Received"})
        Me.MovementStatusCombobox.Location = New System.Drawing.Point(537, 101)
        Me.MovementStatusCombobox.Name = "MovementStatusCombobox"
        Me.MovementStatusCombobox.Size = New System.Drawing.Size(208, 35)
        Me.MovementStatusCombobox.Style = MetroFramework.MetroColorStyle.Orange
        Me.MovementStatusCombobox.TabIndex = 171
        Me.MovementStatusCombobox.UseSelectable = True
        Me.MovementStatusCombobox.ValueMember = "Crop"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(465, 101)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(59, 25)
        Me.MetroLabel2.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroLabel2.TabIndex = 170
        Me.MetroLabel2.Text = "Status"
        '
        'Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter
        '
        Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter.ClearBeforeFill = True
        '
        'Crop
        '
        Me.Crop.DataPropertyName = "Crop"
        Me.Crop.HeaderText = "Crop"
        Me.Crop.Name = "Crop"
        Me.Crop.ReadOnly = True
        Me.Crop.Width = 97
        '
        'MovementNo
        '
        Me.MovementNo.DataPropertyName = "MovementNo"
        Me.MovementNo.HeaderText = "Move No."
        Me.MovementNo.Name = "MovementNo"
        Me.MovementNo.ReadOnly = True
        Me.MovementNo.Width = 154
        '
        'TruckNo
        '
        Me.TruckNo.DataPropertyName = "TruckNo"
        Me.TruckNo.HeaderText = "TruckNo"
        Me.TruckNo.Name = "TruckNo"
        Me.TruckNo.ReadOnly = True
        Me.TruckNo.Width = 137
        '
        'Driver
        '
        Me.Driver.DataPropertyName = "Driver"
        Me.Driver.HeaderText = "Driver"
        Me.Driver.Name = "Driver"
        Me.Driver.ReadOnly = True
        Me.Driver.Width = 112
        '
        'DateStart
        '
        Me.DateStart.DataPropertyName = "MovementFromFinishedDate"
        DataGridViewCellStyle3.Format = "g"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DateStart.DefaultCellStyle = DataGridViewCellStyle3
        Me.DateStart.HeaderText = "Send"
        Me.DateStart.Name = "DateStart"
        Me.DateStart.ReadOnly = True
        Me.DateStart.Width = 99
        '
        'DateReceived
        '
        Me.DateReceived.DataPropertyName = "DateReceived"
        DataGridViewCellStyle4.Format = "g"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DateReceived.DefaultCellStyle = DataGridViewCellStyle4
        Me.DateReceived.HeaderText = "Received"
        Me.DateReceived.Name = "DateReceived"
        Me.DateReceived.ReadOnly = True
        Me.DateReceived.Width = 145
        '
        'NBales
        '
        Me.NBales.DataPropertyName = "NBales"
        Me.NBales.HeaderText = "Cases"
        Me.NBales.Name = "NBales"
        Me.NBales.ReadOnly = True
        Me.NBales.Width = 106
        '
        'FrmReceived
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1588, 692)
        Me.Controls.Add(Me.MovementStatusCombobox)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.ShippingLocationComboBox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "FrmReceived"
        Me.Style = MetroFramework.MetroColorStyle.Lime
        CType(Me.ShippingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpShippingSELShippingLocationBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ShippingDataSet As ShippingDataSet
    Friend WithEvents Sp_Shipping_SEL_ShippingLocationTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingLocationTableAdapter
    Friend WithEvents SpShippingSELShippingLocationBindingSource As BindingSource
    Friend WithEvents ShippingLocationComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TableAdapterManager As ShippingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents SecurityTableAdapter As ShippingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents SecurityBindingSource As BindingSource
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents DetailTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MovementStatusCombobox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementByMovementStatusIDBindingSource As BindingSource
    Friend WithEvents Sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter As ShippingDataSetTableAdapters.sp_Shipping_SEL_ShippingMovementByMovementStatusIDTableAdapter
    Friend WithEvents Crop As DataGridViewTextBoxColumn
    Friend WithEvents MovementNo As DataGridViewTextBoxColumn
    Friend WithEvents TruckNo As DataGridViewTextBoxColumn
    Friend WithEvents Driver As DataGridViewTextBoxColumn
    Friend WithEvents DateStart As DataGridViewTextBoxColumn
    Friend WithEvents DateReceived As DataGridViewTextBoxColumn
    Friend WithEvents NBales As DataGridViewTextBoxColumn
End Class

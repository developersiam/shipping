﻿Public Class FrmAddShippingLocation
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ShippingDataClassesDataContext
    Private Sub FrmAddShippingLocation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ShippingDataSet.WareHouse' table. You can move, or remove it, as needed.

        Try
            Me.Size = New Size(1280, 768)
            LocIDTextbox.Text = ""
            LocNameTextbox.Text = ""
            Me.WareHouseTableAdapter.Fill(Me.ShippingDataSet.WareHouse)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If LocIDTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Location ID", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If LocNameTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Location name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            db.sp_Shipping_INS_ShippingLocation(LocIDTextbox.Text, LocNameTextbox.Text, StationComboBox.Text)

            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
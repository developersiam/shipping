﻿Public Class FrmShippingLocation
    Inherits MetroFramework.Forms.MetroForm
    Private Sub FrmShippingLocation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = XUsername
            Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingLocation)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingLocation)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CreateTile_Click(sender As Object, e As EventArgs) Handles CreateTile.Click
        Try
            Dim frmAddShippingLocation As New FrmAddShippingLocation
            frmAddShippingLocation.ShowDialog()
            frmAddShippingLocation.Dispose()
            Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingLocation)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MatRcMetroGrid_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles MatRcMetroGrid.CellDoubleClick
        Try
            Dim xUp As New FrmUpdateShippingLocation
            xUp.LocIDTextbox.Text = MatRcMetroGrid.Item(0, MatRcMetroGrid.CurrentCell.RowIndex).Value
            xUp.ShowDialog()
            xUp.Dispose()
            Me.Sp_Shipping_SEL_ShippingLocationTableAdapter.Fill(Me.ShippingDataSet.sp_Shipping_SEL_ShippingLocation)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class